<?php
  //ini_set('display_errors', 'On');
  header('Access-Control-Allow-Origin: *');
  require('../model/consultas.php');

  if(count($_POST) > 0){
    // $document = '/var/www/html/Git/inmonet';
    //$document = 'D:/MAMP/htdocs/inmonet';
    $document = '/home/rriveros/public_html/inmobiliaria';
    // $document = '/home/livingne/inmonet.cl/test';


    //Documentos Escritura
    if(!is_dir("../repositorio/" . $_POST['codigoProyecto'])){
      mkdir("../repositorio/" . $_POST['codigoProyecto'], 0777);
      mkdir("../repositorio/" . $_POST['codigoProyecto'] . "/escritura", 0777);
    }
    if(!is_dir("../repositorio/" . $_POST['codigoProyecto'] . "/escritura")){
      mkdir("../repositorio/" . $_POST['codigoProyecto'] . "/escritura", 0777);
    }

    $row = consultaEscrituraEspecificaCargaDocs($_POST['codigoProyecto'], $_POST['numeroOperacion']);

    $rutaES = $document . '/repositorio/' . $row[0]['ES_PDF'];
    $rutaEN = $document . '/repositorio/' . $row[0]['EN_PDF'];
    $rutaDE = $document . '/repositorio/' . $row[0]['DE_PDF'];
    //Carga Ficha Escritura
    $esDoc = $_FILES['esEscrituraDoc']['name'];
    $esExt = pathinfo($esDoc, PATHINFO_EXTENSION);

    if($esExt != ''){
      if($rutaES != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['esEscrituraDoc']['tmp_name'],$rutaES);
      }
      else{
        $rutaES = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/escritura/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_ES.pdf';
        move_uploaded_file($_FILES['esEscrituraDoc']['tmp_name'],$rutaES);
      }
    }

    //Carga Escritura Notarial
    $enDoc = $_FILES['enEscrituraDoc']['name'];
    $enExt = pathinfo($enDoc, PATHINFO_EXTENSION);

    if($enExt != ''){
      if($rutaEN != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['enEscrituraDoc']['tmp_name'],$rutaEN);
      }
      else{
        $rutaEN = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/escritura/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_EN.pdf';
        move_uploaded_file($_FILES['enEscrituraDoc']['tmp_name'],$rutaEN);
      }
    }

    //Carga Documento Entrega Unidad
    $deDoc = $_FILES['deEscrituraDoc']['name'];
    $deExt = pathinfo($deDoc, PATHINFO_EXTENSION);

    if($deExt != ''){
      if($rutaDE != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['deEscrituraDoc']['tmp_name'],$rutaDE);
      }
      else{
        $rutaDE = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/escritura/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_DE.pdf';
        move_uploaded_file($_FILES['deEscrituraDoc']['tmp_name'],$rutaDE);
      }
    }


    //Local e inmonet

    //Inmonet Test
    //actualizaRutasDocumentosEscritura(substr($rutaES,38,strlen($rutaES)),substr($rutaEN,38,strlen($rutaEN)),substr($rutaDE,38,strlen($rutaDE)),$row[0]['IDESCRITURA']);
    // NICO
    actualizaRutasDocumentosEscritura(substr($rutaES,38,strlen($rutaES)),substr($rutaEN,38,strlen($rutaEN)),substr($rutaDE,38,strlen($rutaDE)),$row[0]['IDESCRITURA']);



    //Documentos Promesa

    if(!is_dir("../repositorio/" . $_POST['codigoProyecto'])){
      mkdir("../repositorio/" . $_POST['codigoProyecto'], 0777);
      mkdir("../repositorio/" . $_POST['codigoProyecto'] . "/promesa", 0777);
    }
    if(!is_dir("../repositorio/" . $_POST['codigoProyecto'] . "/promesa")){
      mkdir("../repositorio/" . $_POST['codigoProyecto'] . "/promesa", 0777);
    }

    $row = consultaPromesaEspecificaCargaDocs($_POST['codigoProyecto'], $_POST['numeroOperacion']);

    $rutaPR = $document . '/repositorio/' . $row[0]['PR_PDF'];
    $rutaCP = $document . '/repositorio/' . $row[0]['CP_PDF'];
    $rutaPF = $document . '/repositorio/' . $row[0]['PF_PDF'];
    $rutaPS = $document . '/repositorio/' . $row[0]['PS_PDF'];
        //Carga Ficha Promesa
    $prDoc = $_FILES['prPromesaDoc']['name'];
    $prExt = pathinfo($prDoc, PATHINFO_EXTENSION);

    if($prExt != ''){
      if($rutaPR != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['prPromesaDoc']['tmp_name'],$rutaPR);
      }
      else{
        $rutaPR = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/promesa/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_PR.pdf';
        move_uploaded_file($_FILES['prPromesaDoc']['tmp_name'],$rutaPR);
      }
    }

    //Carga Comprobantes Pago Promesa
    $cpDoc = $_FILES['cpPromesaDoc']['name'];
    $cpExt = pathinfo($cpDoc, PATHINFO_EXTENSION);

    if($cpExt != ''){
      if($rutaCP != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['cpPromesaDoc']['tmp_name'],$rutaCP);
      }
      else{
        $rutaCP = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/promesa/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_CP.pdf';
        move_uploaded_file($_FILES['cpPromesaDoc']['tmp_name'],$rutaCP);
      }
    }

    //Carga Promesa Firmada
    $pfDoc = $_FILES['pfPromesaDoc']['name'];
    $pfExt = pathinfo($pfDoc, PATHINFO_EXTENSION);

    if($pfExt != ''){
      if($rutaPF != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['pfPromesaDoc']['tmp_name'],$rutaPF);
      }
      else{
        $rutaPF = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/promesa/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_PF.pdf';
        move_uploaded_file($_FILES['pfPromesaDoc']['tmp_name'],$rutaPF);
      }
    }

    //Carga Poliza Seguros
    $psDoc = $_FILES['psPromesaDoc']['name'];
    $psExt = pathinfo($psDoc, PATHINFO_EXTENSION);

    if($psExt != ''){
      if($rutaPS != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['psPromesaDoc']['tmp_name'],$rutaPS);
      }
      else{
        $rutaPS = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/promesa/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_PS.pdf';
        move_uploaded_file($_FILES['psPromesaDoc']['tmp_name'],$rutaPS);
      }
    }
    //Local e inmonet

    //Inmonet Test
    // actualizaRutasDocumentosPromesa(substr($rutaPR,38,strlen($rutaPR)),substr($rutaCP,38,strlen($rutaCP)),substr($rutaPF,38,strlen($rutaPF)),$row[0]['IDPROMESA']);
    actualizaRutasDocumentosPromesa(substr($rutaPR,38,strlen($rutaPR)),substr($rutaCP,38,strlen($rutaCP)),substr($rutaPF,38,strlen($rutaPF)),substr($rutaPS,38,strlen($rutaPS)),$row[0]['IDPROMESA']);

    //Documentos Reserva

    $row = consultaReservaEspecificaCargaDocs($_POST['codigoProyecto'], $_POST['numeroOperacion']);

    $rutaFicha = $document . '/repositorio/' . $row[0]['FICHA_PDF'];
    $rutaCO = $document . '/repositorio/' . $row[0]['COTIZA_PDF'];
    $rutaRCompra = $document . '/repositorio/' . $row[0]['RCOMPRA_PDF'];
    $rutaCI = $document . '/repositorio/' . $row[0]['CI_PDF'];
    $rutaPB = $document . '/repositorio/' . $row[0]['PB_PDF'];
    $rutaCR = $document . '/repositorio/' . $row[0]['CR_PDF'];
    $rutaCC = $document . '/repositorio/' . $row[0]['CC_PDF'];

    //Crga Ficha
    $fichaDoc = $_FILES['fichaReservaDoc']['name'];
    $fichaExt = pathinfo($fichaDoc, PATHINFO_EXTENSION);

    if($fichaExt != ''){
      if($rutaFicha != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['fichaReservaDoc']['tmp_name'],$rutaFicha);
      }
      else{
        $rutaFicha = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/reserva/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_FC.pdf';
        move_uploaded_file($_FILES['fichaReservaDoc']['tmp_name'],$rutaFicha);
      }
    }

    //Crga Cotizacion
    $coDoc = $_FILES['cotizacionDoc']['name'];
    $coExt = pathinfo($coDoc, PATHINFO_EXTENSION);

    if($coExt != ''){
      if($rutaCO != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['cotizacionDoc']['tmp_name'],$rutaCO);
      }
      else{
        $rutaCO = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/cotizacion/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '.pdf';
        move_uploaded_file($_FILES['cotizacionDoc']['tmp_name'],$rutaCO);
      }
    }

    //Carga RCompra
    $rcompraDoc = $_FILES['rcompraReservaDoc']['name'];
    $rcompraExt = pathinfo($rcompraDoc, PATHINFO_EXTENSION);

    if($rcompraExt != ''){
      if($rutaRCompra != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['rcompraReservaDoc']['tmp_name'],$rutaRCompra);
      }
      else{
        $rutaRCompra = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/reserva/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_RE.pdf';
        move_uploaded_file($_FILES['rcompraReservaDoc']['tmp_name'],$rutaRCompra);
      }
    }

    //Carga CI
    $ciDoc = $_FILES['ciReservaDoc']['name'];
    $ciExt = pathinfo($ciDoc, PATHINFO_EXTENSION);

    if($ciExt != ''){
      if($rutaCI != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['ciReservaDoc']['tmp_name'],$rutaCI);
      }
      else{
        $rutaCI = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/reserva/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_CI.pdf';
        move_uploaded_file($_FILES['ciReservaDoc']['tmp_name'],$rutaCI);
      }
    }

    //Carga PB
    $pbDoc = $_FILES['pbReservaDoc']['name'];
    $pbExt = pathinfo($pbDoc, PATHINFO_EXTENSION);

    if($pbExt != ''){
      if($rutaPB != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['pbReservaDoc']['tmp_name'],$rutaPB);
      }
      else{
        $rutaPB = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/reserva/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_PB.pdf';
        move_uploaded_file($_FILES['pbReservaDoc']['tmp_name'],$rutaPB);
      }
    }

    //Carga PB
    $pbDoc = $_FILES['pbReservaDoc']['name'];
    $pbExt = pathinfo($pbDoc, PATHINFO_EXTENSION);

    if($pbExt != ''){
      if($rutaPB != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['pbReservaDoc']['tmp_name'],$rutaPB);
      }
      else{
        $rutaPB = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/reserva/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_PB.pdf';
        move_uploaded_file($_FILES['pbReservaDoc']['tmp_name'],$rutaPB);
      }
    }

    //Carga CC
    $ccDoc = $_FILES['ccReservaDoc']['name'];
    $ccExt = pathinfo($ccDoc, PATHINFO_EXTENSION);

    if($ccExt != ''){
      if($rutaCC != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['ccReservaDoc']['tmp_name'],$rutaCC);
      }
      else{
        $rutaCC = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/reserva/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_CC.pdf';
        move_uploaded_file($_FILES['ccReservaDoc']['tmp_name'],$rutaCC);
      }
    }

    //Carga CR
    $crDoc = $_FILES['crReservaDoc']['name'];
    $crExt = pathinfo($crDoc, PATHINFO_EXTENSION);

    if($crExt != ''){
      if($rutaCR != ($document . '/repositorio/')){
        move_uploaded_file($_FILES['crReservaDoc']['tmp_name'],$rutaCR);
      }
      else{
        $rutaCR = $document  . '/repositorio/' . $_POST['codigoProyecto'] . '/reserva/' . $_POST['numeroOperacion'] . '_' . $_POST['codigoProyecto'] . '_' . $row[0]['CODIGO'] . '_' . str_replace(' ', '_',$row[0]['NOMBRES']) . '_' . str_replace(' ', '_',$row[0]['APELLIDOS']) . '_CR.pdf';
        move_uploaded_file($_FILES['crReservaDoc']['tmp_name'],$rutaCR);
      }
    }

    //Local e inmonet
    //actualizaRutasDocumentosReserva(substr($rutaFicha,38,strlen($rutaFicha)),substr($rutaRCompra,38,strlen($rutaRCompra)),substr($rutaCI,38,strlen($rutaCI)),substr($rutaPB,38,strlen($rutaPB)),substr($rutaCC,38,strlen($rutaCC)),substr($rutaCR,38,strlen($rutaCR)),substr($rutaCO,38,strlen($rutaCO)),$row[0]['IDRESERVA']);

    actualizaRutasDocumentosReserva(substr($rutaFicha,38,strlen($rutaFicha)),substr($rutaRCompra,38,strlen($rutaRCompra)),substr($rutaCI,38,strlen($rutaCI)),substr($rutaPB,38,strlen($rutaPB)),substr($rutaCC,38,strlen($rutaCC)),substr($rutaCR,38,strlen($rutaCR)),substr($rutaCO,38,strlen($rutaCO)),$row[0]['IDRESERVA']);

    $documentos = "";
    if($fichaDoc != NULL ){
      $documentos .= "Ficha cliente, ";
    }
    if($coDoc != NULL ){
      $documentos .= "Cotizacion, ";
    }
    if($rcompraDoc != NULL ){
      $documentos .= "Ficha reserva, ";
    }
    if($ciDoc != NULL ){
      $documentos .= "Cedula de identidad, ";
    }
    if($pbDoc != NULL ){
      $documentos .= "Preaprobacion bancaria, ";
    }
    if($ccDoc != NULL ){
      $documentos .= "Carpeta cliente, ";
    }
    if($crDoc != NULL ){
      $documentos .= "Comprobante reserva, ";
    }
    if($prDoc != NULL ){
      $documentos .= "Ficha promesa, ";
    }
    if($cpDoc != NULL ){
      $documentos .= "Comprobante promesa, ";
    }
    if($pfDoc != NULL ){
      $documentos .= "Promesa firmada, ";
    }
    if($psDoc != NULL ){
      $documentos .= "Poliza seguros, ";
    }
    if($esDoc  != NULL ){
      $documentos .= "Ficha escritura, ";
    }
    if($enDoc  != NULL ){
      $documentos .= "Escritura notarial, ";
    }
    if($deDoc  != NULL ){
      $documentos .= "Entrega unidad, ";
    }
    $documentos = trim($documentos, ', ');


    echo "Ok¬" . $_POST['codigoProyecto'] . "¬" . $_POST['numeroOperacion'] . "¬" . $documentos;
  }
?>

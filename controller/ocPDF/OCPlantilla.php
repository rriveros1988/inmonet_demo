<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
  <head>
    <style type="text/css">
    #tablaOC{
      width: 100%;
      margin-left: 40px;
      margin-top: 40px;
    }
    #cabecera1{
      width: 45%;
      vertical-align: top;
      height: 10%;
    }
    #cabecera2{
      width: 45%;
      text-align: right;
      vertical-align: top;
      height: 10%;
    }
    #cabecera3{
      width: 45%;
      text-align: left;
      vertical-align: top;
      height: 13%;
    }
    #cabecera4{
      width: 45%;
      text-align: left;
      vertical-align: top;
      height: 13%;
    }
    #cuerpo1{
      border: 2;
      vertical-align: top;
      height: 6%;
    }
    #cuerpo2{
      border: 2;
      vertical-align: top;
      height: 25%;
    }
    #espacio1{
      height: 1%;
    }
    #cuerpo3{
      border: 0;
      vertical-align: top;
      height: 15%;
    }
    #cuerpo4{
      border: 1;
      vertical-align: top;
      height: 15%;
    }
    #cuerpo5{
      border: 0;
      vertical-align: top;
      height: 15%;
    }
    #cuerpo6{
      border: 1;
      vertical-align: top;
      height: 15%;
    }
    </style>
    <title>Orden de Compra</title>
  </head>
  <body>
    <table id="tablaOC">
      <tr>
        <td id="cabecera1">
          <img src="../../view/img/logos/dc_logo_transparente2.png" style="width: 150px;">
          <p>
            Dirección: Lira 245
            <br/>
            Ciudad: Santiago - Código postal: 1203900
            <br/>
            Teléfono: (2) 235317 - Fax: (2) 2353172
            <br/>
            <br/>
            <font style="font-weight: bold;">Número de OC: 0000001</font>
          </p>
        </td>
        <td id="cabecera2">
          <font style="font-size: 30px;">Orden de compra</font>
        </td>
      </tr>
      <tr>
        <td id="cabecera3">
          <br/>
          <br/>
          <font style="font-weight: bold;">Para:</font>
          <br/>
          Nombre:
          <br/>
          Compañía:
          <br/>
          Dirección:
          <br/>
          Ciudad: - Código postal:
          <br/>
          Teléfono:
        </td>
        <td id="cabecera4">
          <br/>
          <br/>
          <font style="font-weight: bold;">Enviar a:</font>
          <br/>
          Nombre:
          <br/>
          Compañía:
          <br/>
          Dirección:
          <br/>
          Ciudad: - Código postal:
          <br/>
          Teléfono:
        </td>
      </tr>
      <tr>
        <td id="cuerpo1" colspan="2"></td>
      </tr>
      <tr>
        <td id="espacio1" colspan="2"></td>
      </tr>
      <tr>
        <td id="cuerpo2" colspan="2"></td>
      </tr>
      <tr>
        <td id="cuerpo3"></td>
        <td id="cuerpo4"></td>
      </tr>
      <tr>
        <td id="cuerpo5"></td>
        <td id="cuerpo6"></td>
      </tr>
    </table>
  </body>
</html>

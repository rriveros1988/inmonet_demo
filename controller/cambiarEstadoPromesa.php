<?php
// ini_set('display_errors', 'On');
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');
	session_start();
	if(count($_POST) > 0){
			$codigoProyecto = $_POST['codigoProyecto'];
			$numeroOperacion = $_POST['numeroOperacion'];
			$estadoPromesa = $_POST['estadoPromesa'];
			$unidadRevisar = $_POST['unidadRevisar'];
			$proy = consultaProyectoEspecificoCodigo($codigoProyecto);
			// var_dump($unidadRevisar);
			if ($codigoProyecto == "COR"){
				$row = cambiarEstadoPromesaCorretaje($codigoProyecto, $numeroOperacion, $estadoPromesa);

				$comCorretaje = consultaComisionPromesaCorretaje($codigoProyecto, $numeroOperacion, $unidadRevisar)[0];
				$totalUnidad = $comCorretaje['VALORUF'];
				$accion = $comCorretaje['IDACCION'];
				$promesa = datosPromesaParaComision($codigoProyecto, $numeroOperacion);

				$idUsuario = $promesa[0]["IDUSUARIO"];
				$fechaComisionPromesa = new DateTime($promesa[0]['FECHAPROMESA']);
				$fechaComisionPromesa = $fechaComisionPromesa->format('Y-m-d');
				$idProyecto = $proy[0]["IDPROYECTO"];
				$idPromesa = $promesa[0]["IDPROMESA"];
				$_SESSION['estadoPromesa'] = $estadoPromesa;
				if ($row->error == "") {
					if ($estadoPromesa != '3') {
						$consultaPromesa = consultaPromesaEspecifica($codigoProyecto, $numeroOperacion);
					    $idProyecto = $consultaPromesa[0]['IDPROYECTO'];
					    $idPromesa = $consultaPromesa[0]['IDPROMESA'];
					    $unidad = consultaUnidadEspecifica($consultaPromesa[0]['IDUNIDAD']);
					    $cliente1 = consultaClienteEspecifico($consultaPromesa[0]['IDCLIENTE1']);
						$proyecto = consultaProyectoEspecifico($idProyecto);
				        $_SESSION['promesaCodigoProyecto'] = $codigoProyecto;
				        $_SESSION['promesaLogoProyecto'] = '../' . $proyecto[0]['LOGO'];
				        $_SESSION['promesaAccion'] = $unidad[0]['ACCIONDET'];
				        $fecha = new DateTime($consultaPromesa[0]['FECHAPROMESA']);
						$fechaPromesa = $fecha->format("d-m-Y");
						$_SESSION['promesaFecha'] = $fechaPromesa;
						$_SESSION['numeroOperacion'] = $numeroOperacion;
						$_SESSION['promesaNombreCliente'] = $cliente1[0]['NOMBRES'];
				        $_SESSION['promesaApellidoCliente'] = $cliente1[0]['APELLIDOS'];
				        $_SESSION['promesaRutCliente'] = $cliente1[0]['RUT'];
				        $_SESSION['promesaNacionalidadCliente1'] = $cliente1[0]['NACIONALIDAD'];
				        $_SESSION['promesaEstadoCivilIDCliente1'] = $cliente1[0]['ESTADOCIVIL'];
				        $_SESSION['promesaFechaNacCliente1'] = $cliente1[0]['FECHANAC'];
				        $_SESSION['promesaProfesionCliente1'] = $cliente1[0]['PROFESION'];
				        if ($cliente1[0]['TIPODOMICILIO'] != '') {
					        $_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'] . ', ' . $cliente1[0]['TIPODOMICILIO'];
				        }else{
							$_SESSION['promesaDomicilioCliente1'] = $cliente1[0]['DOMICILIO'] . ' ' . $cliente1[0]['NUMERODOMICILIO'];
				        }
				        $_SESSION['promesaComunaCliente1'] = $cliente1[0]['COMUNA'];
				        $_SESSION['promesaCiudadCliente1'] = $cliente1[0]['CIUDAD'];
				        $_SESSION['promesaRegionCliente1'] = $cliente1[0]['REGION'];
				        $_SESSION['promesaTelefonoCliente1'] = $cliente1[0]['CELULAR'];
				        $_SESSION['promesaMailCliente1'] = $cliente1[0]['EMAIL'];
				        $_SESSION['bodegasClientePromesa'] = consultaBodegasPromesadas($idPromesa);
				        $_SESSION['estacionamientosClientePromesa'] = consultaEstacionamientosPromesados($idPromesa);
				        $_SESSION['tipoUnidad'] = $unidad[0]['TIPOUNIDAD'];
				        $_SESSION['promesaNumeroDepto'] = $unidad[0]['CODIGO'];
				        $_SESSION['promesaTotalUF'] = $consultaPromesa[0]['VALORTOTALUF'];
				        $_SESSION['promesaReservaUF'] = $consultaPromesa[0]['VALORRESERVAUF'];
				        $_SESSION['promesaPiePromesaUF'] = $consultaPromesa[0]['VALORPIEPROMESAUF'];
				        $_SESSION['valorPiePromesa'] = $consultaPromesa[0]['VALORPIEPROMESA'];
				        $_SESSION['promesaFormaPagoPromesa'] = consultaFormaPagoReservaEspecifica($consultaPromesa[0]['FORMAPAGOPROMESA']);
				        $_SESSION['promesaBancoPromesa'] = $consultaPromesa[0]['BANCOPROMESA'];
				        $_SESSION['promesaSerieChequePromesa'] = $consultaPromesa[0]['SERIECHEQUEPROMESA'];
				        $_SESSION['promesaNroChequePromesa'] = $consultaPromesa[0]['NROCHEQUEPROMESA'];
				        $_SESSION['promesaPieCuotas'] = $consultaPromesa[0]['VALORPIESALDOUF'];
				        $_SESSION['valorPieSaldo'] = $consultaPromesa[0]['VALORPIESALDO'];
				        $cuotasPromesa = consultaCuotasPromesadas($codigoProyecto,$numeroOperacion);
				        if(count($cuotas_promesa) > 0){
					        $_SESSION['promesaFormaPagoCuota'] = array();

					        $_SESSION['cuotasPromesa'] = $cuotasPromesa;

					        foreach ($cuotasPromesa as $cuota) {

					            array_push($_SESSION['promesaFormaPagoCuota'], consultaFormaPagoReservaEspecifica($forma_cuota[0]));

					        }
					      }
					      else{
					        unset($_SESSION['cuotasPromesa']);
					      }
				        $_SESSION['promesaCuotasPie'] = $consultaPromesa[0]['CANTCUOTASPIE'];
				        $_SESSION['promesaSaldoTotalUF'] = $consultaPromesa[0]['VALORSALDOTOTALUF'];
				        $vendedor = chequeaUsuarioID($consultaPromesa[0]['IDUSUARIO']);
					    $_SESSION['promesaVendedor'] = $vendedor['NOMBRES'] . ' ' . $vendedor['APELLIDOS'];
					    $_SESSION['promesaVendedorRut'] = $vendedor['RUT'];
					    $_SESSION['MONTOUFCOMISION'] = '&nbsp;&nbsp;';
						$_SESSION['MONTOPESOCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
						$_SESSION['FORMAPAGOCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
						$_SESSION['BANCOCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
						$_SESSION['SERIECOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
						$_SESSION['NROCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
						$_SESSION['FECHAPAGOCOMISION'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
						eliminaFormaPagoComision($codigoProyecto, $numeroOperacion, $row);
						eliminaComisionPromesaCorretaje($codigoProyecto, $numeroOperacion, $row);
						$row->query("COMMIT");
					}
					else if($estadoPromesa == 3){
			    		if ($accion == 1){ //Venta
							if ($comCorretaje['COMISIONLVDUENO'] != '0' && $comCorretaje['COMISIONLVCLIENTE'] != '0') {
								// $comVentaDueno = $comCorretaje['COMISIONLVDUENO'];
								// $comVentaCliente = $comCorretaje['COMISIONLVCLIENTE'];

								// $montoVentaDueno = ($comVentaDueno / 100) * $totalUnidad;
								// $montoVentaCliente = ($comVentaCliente / 100) * $totalUnidad;

								// $insercionDueno = insertarComisionPromesaCorretaje($idUsuario, $fechaComisionPromesa, $idProyecto, $idPromesa, $montoVentaDueno, $comVentaDueno, 'Dueño',$row);
								// $insercionCliente = insertarComisionPromesaCorretaje($idUsuario, $fechaComisionPromesa, $idProyecto, $idPromesa, $montoVentaCliente, $comVentaCliente, 'Cliente',$row);

								// if ($insercionDueno->error == "" && $insercionCliente->error == "") {
								// 	$datos = array('idDueno' => $insercionDueno->insert_id, 'idCliente' => $insercionCliente->insert_id);
								// 	$_SESSION['cambioEstadoPromesa'] = $row;
								// 	$_SESSION['insercionDueno'] = $insercionDueno;
								// 	$_SESSION['insercionCliente'] = $insercionCliente;
								// 	echo json_encode($datos);
								// }else{
								// 	echo "Sin datos dentro";
								// }
							}else{
								echo "sincomisiones";
							}
						}
						else{ // Arriendo
							if($comCorretaje['COMISIONVENDUENO'] != '0' && $comCorretaje['COMISIONVENCLIENTE'] != '0') {
								// $comArriendoDueno = $comCorretaje['COMISIONVENDUENO'];
								// $comArriendoCliente = $comCorretaje['COMISIONVENCLIENTE'];

								// $montoArriendoDueno = ($comArriendoDueno / 100) * $totalUnidad;
								// $montoArriendoCliente = ($comArriendoCliente / 100) * $totalUnidad;

								// $insercionDueno = insertarComisionPromesaCorretaje($idUsuario, $fechaComisionPromesa, $idProyecto, $idPromesa, $montoArriendoDueno, $comArriendoDueno, 'Dueño', $row);
								// $insercionCliente = insertarComisionPromesaCorretaje($idUsuario, $fechaComisionPromesa, $idProyecto, $idPromesa, $montoArriendoCliente, $comArriendoCliente, 'Cliente', $row);
								// $datos = array('idDueno' => $insercionDueno->insert_id, 'idCliente' => $insercionCliente->insert_id);
								// $insercionDueno->query("COMMIT");
								// $insercionCliente->query("COMMIT");
								// echo json_encode($datos);
							}else{
								echo "sincomisiones";
							}
						}
			    	}else{
			    		echo "Sin datos";
			    	}
					echo "Ok";
				}else{
					echo "Sin datos";
				}
			}
			else{
				if($proy[0]["COMISIONLVPROMESA"] != '0' && $proy[0]["COMISIONVENPROMESA"] != '0' && $proy[0]["COMISIONLVPROMESA"] != null && $proy[0]["COMISIONVENPROMESA"] != null && $proy[0]["COMISIONLVPROMESA2"] != '0' && $proy[0]["COMISIONVENPROMESA2"] != '0' && $proy[0]["COMISIONLVPROMESA2"] != null && $proy[0]["COMISIONVENPROMESA2"] != null && $proy[0]['SALTO'] != '0' && $proy[0]['SALTO'] != null){
		    		$row = cambiarEstadoPromesa($codigoProyecto, $numeroOperacion, $estadoPromesa);

			    	if($row == "Ok"){
							if($estadoPromesa == '7'){
								liberaUnidadesPromesaEstacionamiento($codigoProyecto,$numeroOperacion);
						    	liberaUnidadesPromesaBodega($codigoProyecto,$numeroOperacion);
							    liberaUnidadesPromesadas($codigoProyecto,$numeroOperacion);
								cambiarEstadoReserva($codigoProyecto, $numeroOperacion, 7);
								liberaUnidadesReservaBodega($codigoProyecto,$numeroOperacion);
						    	liberaUnidadesReservaEstacionamiento($codigoProyecto,$numeroOperacion);
						    	liberaUnidadesReserva($codigoProyecto,$numeroOperacion);
							}
							else if($estadoPromesa == '1'){
								liberaUnidadesPromesaEstacionamiento($codigoProyecto,$numeroOperacion);
							    liberaUnidadesPromesaBodega($codigoProyecto,$numeroOperacion);
							    liberaUnidadesPromesadas($codigoProyecto,$numeroOperacion);
								cambiarEstadoReserva($codigoProyecto, $numeroOperacion, 9);
								liberaUnidadesReservaBodega($codigoProyecto,$numeroOperacion);
							    liberaUnidadesReservaEstacionamiento($codigoProyecto,$numeroOperacion);
							    liberaUnidadesReserva($codigoProyecto,$numeroOperacion);
							}
							if ($estadoPromesa == '2') {
								eliminaComisionPromesa($codigoProyecto, $numeroOperacion);
							}
							if($estadoPromesa == '1' || $estadoPromesa == '3' || $estadoPromesa == '6'){
								$promesa = datosPromesaParaComision($codigoProyecto, $numeroOperacion);

								$idUsuario = $promesa[0]["IDUSUARIO"];

								if(is_null(checkComisionPromesa($codigoProyecto, $numeroOperacion, $idUsuario))){
									$proy = consultaProyectoEspecificoCodigo($codigoProyecto);
									$totalUF = $promesa[0]["VALORTOTALUF"];
				      		$fechaComisionPromesa = new DateTime($promesa[0]['FECHAPROMESA']);
				      		$fechaComisionPromesa = $fechaComisionPromesa->format('Y-m-d');
									$idProyecto = $proy[0]["IDPROYECTO"];
									$idPromesa = $promesa[0]["IDPROMESA"];

									$IVA = $proy[0]["IVA"]/100;
									$valorAgregado = 1 + $IVA;
									$valorNeto = $totalUF / $valorAgregado;
									$valorNeto = number_format($valorNeto, 2, '.', '');

									if (checkComisionCantidadVentasPromesa($idUsuario, $fechaComisionPromesa, $idProyecto)[0]['VENTAS'] > $proy[0]['SALTO']) {
										$porcentajeComisionEmpresa = $proy[0]["COMISIONLVPROMESA2"];
										$comisionEmpresaCalculado = $porcentajeComisionEmpresa/100;
										$porcentajeComisionVendedor = $proy[0]["COMISIONVENPROMESA2"];
										$comisionVendedorCalculado = $porcentajeComisionVendedor/100;

										$comisionesSalto = checkComisionPromesaParaSaltoDeUnidad($idUsuario, $fechaComisionPromesa);
										for ($i=0; $i < count($comisionesSalto); $i++) {
											$idComisionPromesa = $comisionesSalto[$i]['IDCOMISIONPROMESA'];
											$montoUFAntiguo = $comisionesSalto[$i]['MONTOUFVENDEDOR'];
											$porcentajeAntiguo = $comisionesSalto[$i]['PORCENTAJECOMISIONVENDEDOR'];
											$montoUFNuevo = ($montoUFAntiguo / $porcentajeAntiguo) * $porcentajeComisionVendedor;
											$montoUFNuevo = number_format($montoUFNuevo,2,'.','');

											actualizarPagosComisionPromesaSalto($idComisionPromesa, $idUsuario, $montoUFNuevo, $porcentajeComisionVendedor);

										}
									}
									else{
											$porcentajeComisionEmpresa = $proy[0]["COMISIONLVPROMESA"];
											$comisionEmpresaCalculado = $porcentajeComisionEmpresa/100;
											$porcentajeComisionVendedor = $proy[0]["COMISIONVENPROMESA"];
											$comisionVendedorCalculado = $porcentajeComisionVendedor/100;
									}

									if (is_null(checkUnidadComisionPromesa($unidadRevisar, $codigoProyecto))) {
										$montoUFEmpresa = $valorNeto * $comisionEmpresaCalculado;
									}
									else{
										$montoUFEmpresa = 0;
									}
									$montoUFEmpresa = number_format($montoUFEmpresa, 2,'.','');

									$montoUFVendedor = $valorNeto * $comisionVendedorCalculado;
									$montoUFVendedor = number_format($montoUFVendedor,2,'.','');

									insertarComisionPromesa($idUsuario, $fechaComisionPromesa, $idProyecto, $idPromesa, $montoUFEmpresa, $montoUFVendedor, $porcentajeComisionEmpresa, $porcentajeComisionVendedor)->query("COMMIT");
								}
								else{
									echo "Existente";
								}
							}
							echo "Ok";
						}
						else{
							echo "Sin datos";
						}
				}
				else{
					echo "sincomisiones";
				}
			}
		}
		else{
    		echo "Sin datos";
  	}
?>

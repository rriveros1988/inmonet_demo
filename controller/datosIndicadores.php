<?php
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');
	date_default_timezone_set("America/Santiago");
	session_start();

	if(count($_POST) >= 0){
		$ch = curl_init();

		$fecha = new Datetime();
		$dia = $fecha->format('d');
		$mes = $fecha->format('m');
		$ano = $fecha->format('Y');
		$valor = $_POST['valorUFJqueryHoy'];

		$fecha = $fecha->format('d-m-Y');

		echo $fecha . "," . moneda_chilena($valor) . "," . $_SESSION['nombreUser'];

	 // close curl resource to free up system resources
	 curl_close($ch);
	}
	else{
		echo "Sin datos";
	}

function moneda_chilena($numero){
	$n = explode(".", (string)$numero);
	$numero = $n[0];
	$puntos = floor((strlen($numero)-1)/3);
	$tmp = "";
	$pos = 1;
	for($i=strlen($numero)-1; $i>=0; $i--){
		$tmp = $tmp.substr($numero, $i, 1);
		if($pos%3==0 && $pos!=strlen($numero))
		$tmp = $tmp . ".";
		$pos = $pos + 1;
	}
	$formateado = "$ " . strrev($tmp) .  "&#44;" . $n[1];
	return $formateado;
}
?>

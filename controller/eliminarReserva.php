<?php
  //ini_set('display_errors', 'On');
  header('Access-Control-Allow-Origin: *');
  require('../model/consultas.php');
  session_start();

	if(count($_POST) > 0){
    $codigoProyecto = $_POST['codigoProyecto'];
    $numeroOperacion = $_POST['numeroOperacion'];

    liberaUnidadesReservaBodega($codigoProyecto,$numeroOperacion);
    liberaUnidadesReservaEstacionamiento($codigoProyecto,$numeroOperacion);
    liberaUnidadesReserva($codigoProyecto,$numeroOperacion);
    eliminaReservaBodega($codigoProyecto, $numeroOperacion);
    eliminaReservaEstacionamiento($codigoProyecto, $numeroOperacion);
    retornaEstadoCotizacion($codigoProyecto, $numeroOperacion);
    eliminaComentarioReserva($codigoProyecto, $numeroOperacion);
    $row = eliminaReserva($codigoProyecto, $numeroOperacion);

    if($row == "Ok")
    {
      $url_F = $_POST['url_F'];
      $url_F = '../' . $url_F;
      $url_RE = $_POST['url_RE'];
      $url_RE = '../' . $url_RE;
      $url_CI = $_POST['url_CI'];
      $url_CI = '../' . $url_CI;
      $url_PB = $_POST['url_PB'];
      $url_PB = '../' . $url_PB;
      $url_CC = $_POST['url_CC'];
      $url_CC = '../' . $url_CC;
      $url_CR = $_POST['url_CR'];
      $url_CR = '../' . $url_CR;

      unlink($url_F);
      unlink($url_RE);
      unlink($url_CI);
      unlink($url_PB);
      unlink($url_CC);
      unlink($url_CR);
      echo "Ok";
    }
    else{
      echo "Sin datos";
    }
	}
	else{
		echo "Sin datos1";
	}
?>

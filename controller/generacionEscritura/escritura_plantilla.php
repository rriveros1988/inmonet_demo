<script>
	function randomTextoLoadJs(){
	  var str = '';
	  var ref = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRST0123456789';
	  for (var i = 0; i < 8 ; i++)
	  {
	    str += ref.charAt(Math.floor(Math.random()*ref.length));
	  }
	  return str;
	}
  var js = document.createElement('script');
  js.src = 'view/js/funciones.js?idLoad=' + randomTextoLoadJs();
  document.getElementsByTagName('head')[0].appendChild(js);
</script>
<style type="text/css">
.headTabla{
  background-color: #e6f2ff;
  border: 1px solid black;
  padding: 2px;
}
.bodyTabla{
  border: 1px solid black;
  padding: 1px;
}
label{
  font-size: 12px;
}
input{
  font-size: 12px;
}
</style>
<?php
  session_start();
?>
<div id="divDerechaCrearEscritura" style="padding-left: 0; padding-right: 0;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Fecha promesa:</label>
    <input disabled id="promesaFecha" value=<?php echo '"' . $_SESSION['escrituraFechaPromesa'] . '"' ?> class="form-control"></input>
  </div>
  <!-- <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>N° cotizacion:</label>
    <input disabled  value=<?php echo '"' . $_SESSION['escrituraNumeroCotizacion']  . '"'; ?> class="form-control"></input>
  </div> -->
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>N° operación:</label>
    <input disabled  value=<?php echo '"' . $_SESSION['escrituraNumeroOperacion']  . '"'; ?> class="form-control"></input><!-- Pendiente -->
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Nombre cliente:</label>
    <input id="reservaNombreCliente" disabled value=<?php echo '"' . $_SESSION['escrituraNombreCliente'] . '"'; ?> class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Apellido cliente:</label>
    <input id="reservaApellidoCliente" disabled value=<?php echo '"' . $_SESSION['escrituraApellidoCliente'] . '"'; ?> class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Rut:</label>
    <input id="reservaRutCliente" disabled value=<?php echo '"' . $_SESSION['escrituraRutCliente'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Celular:</label>
    <input id="reservaCelularCliente" disabled value=<?php echo '"' . $_SESSION['escrituraCelularCliente'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
    <label>Email:</label>
    <input id="reservaEmailCliente" disabled value=<?php echo '"' . $_SESSION['escrituraEmailCliente'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="margin-top: 20pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
    <button style="width: 100%; margin-bottom: 5pt;" class="btn btn-primary" id="anexoDatosCliente"><i clasS="fa fa-plus-square"></i>&nbsp;&nbsp;Agregar datos de cliente 1</button>
  </div>
  <div style="margin-top: 20pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
    <button disabled style="width: 100%; margin-bottom: 5pt;" class="btn btn-primary" id="anexoDatosCliente2"><i clasS="fa fa-plus-square"></i>&nbsp;&nbsp;Agregar datos de cliente 2</button>
  </div>
</div>
<div id="divIzquierdaCrearEscritura" style="padding-left: 0; padding-right: 0;   border-left-style: solid; border-left-width: 1px; border-left-color: #c1c1c1;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Fecha:</label>
    <input id="escrituraFecha" class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Vendedor:</label>
    <select id="escrituraVendedor" class="form-control custom">
      <?php
        // ini_set('display_errors', 'On');
        require('../../model/consultas.php');
        $ven = consultaUsuariosVendedores();
        $us = consultaPromesaEspecifica($_SESSION["escrituraCodigoProyecto"], $_SESSION["escrituraNumeroOperacion"]);
        for($i = 0; $i < count($ven); $i++){
          if($ven[$i]['IDUSUARIO'] === $us[0]['IDUSUARIO']){
              echo '<option selected value="' . $ven[$i]['IDUSUARIO'] . '">' . $ven[$i]['NOMBRE'] . '</option>';
          }
          else{
              echo '<option value="' . $ven[$i]['IDUSUARIO'] . '">' . $ven[$i]['NOMBRE'] . '</option>';
          }
        }
      ?>
    </select>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Departamento:</label>
    <input disabled value=<?php echo '"' . $_SESSION['escrituraNumeroDepto'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Tipología:</label>
    <input disabled value=<?php echo '"' . $_SESSION['escrituraTipologiaDepto'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Modelo:</label>
    <input disabled value=<?php echo '"' . $_SESSION['escrituraModeloDepto'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Orientación:</label>
    <input disabled value=<?php echo '"' . $_SESSION['escrituraOrientacionDepto'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Bodegas:</label>
    <input disabled value=<?php echo '"' . $_SESSION['escrituraBod'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Estacionamientos:</label>
    <input disabled value=<?php echo '"' . $_SESSION['escrituraEst']  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Utiles:</label>
    <input disabled value=<?php echo '"' . number_format($_SESSION['escrituraMT2UtilesDepto'],2,',','.')  . ' Mt2"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Terraza:</label>
    <input disabled value=<?php echo '"' . number_format($_SESSION['escrituraMT2TerrazaDepto'],2,',','.')  . ' Mt2"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Total:</label>
    <input disabled value=<?php echo '"' . number_format($_SESSION['escrituraMT2TotalDepto'],2,',','.')  . ' Mt2"'; ?>  class="form-control"></input>
  </div>
</div>

<div style="text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <hr style="background-color: #c1c1c1; height: 1px;" />
</div>

<div id="divIzquierdaCrearEscritura" style="text-align: left; padding-left: 0; padding-right: 0;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Departamento:</label>
    <input id="escritura_departamentoUF" disabled value=<?php echo '"UF ' . number_format($_SESSION['escrituraValorDepto'], 2, ',', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input disabled value=<?php echo '"$ ' . number_format(($_SESSION['escrituraValorDepto']*$_SESSION['escrituraUFActual']), 0, '.', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Bodega:</label>
    <input id="escritura_bodegaUF" disabled value=<?php echo '"UF '.number_format($_SESSION['escrituraValorBod'], 2, ',', '.').'"';?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="bodegaMonto" disabled value=

    <?php
      echo '"$ ' . number_format(($_SESSION['escrituraValorBod']*$_SESSION['escrituraUFActual']), 0, '.', '.')  . '"';
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Estacionamiento:</label>
    <input id="escritura_estacionamientoUF" disabled value=
    <?php
      echo '"UF ' . number_format($_SESSION['escrituraValorEst'], 2, ',', '.')  . '"';
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="estacionamientoMonto" disabled value=
    <?php
      echo '"$ ' . number_format(($_SESSION['escrituraValorEst']*$_SESSION['escrituraUFActual']), 0, '.', '.')  . '"';
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Bono:</label>
    <input id="escritura_bonoUF" disabled value=
    <?php
      echo '"UF ' . number_format($_SESSION['escrituraBono'], 2, ',', '.')  . '"';
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="bonoMonto" disabled value=
    <?php
      echo '"$ ' . number_format(($_SESSION['escrituraBono']*$_SESSION['escrituraUFActual']), 0, '.', '.')  . '"';
    ?>  class="form-control"></input>
  </div>

  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Desc. sala:</label>
    <input  id="escritura_desc1UF" disabled value=<?php
      echo '"UF ' . number_format(($_SESSION['escrituraDescuentoSala']*$_SESSION['escrituraValorDepto']/100), 2, ',', '.')  . '"';
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input disabled value=<?php
      echo '"$ ' . number_format(number_format(($_SESSION['escrituraDescuentoSala']*$_SESSION['escrituraValorDepto']/100), 2, '.', '.')*$_SESSION['escrituraUFActual'], 0, '.', '.')  . '"';
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input disabled value=<?php
      echo '"' . number_format($_SESSION['escrituraDescuentoSala'], 2, '.', '')  . ' %"';
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Desc. especial:</label>
    <input id="escritura_desc2UF" disabled value=<?php
       echo '"UF ' .  number_format(($_SESSION['escrituraDescuentoEspecial']*$_SESSION['escrituraTotal2']/100), 0, '.', '.') . '"';
     ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input disabled value=<?php
       echo '"$ ' . number_format(number_format(($_SESSION['escrituraDescuentoEspecial']*$_SESSION['escrituraTotal2']/100), 2, '.', '.')*$_SESSION['escrituraUFActual'], 0, '.', '.')  . '"';
     ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input disabled value=<?php
      echo '"' . number_format($_SESSION['escrituraDescuentoEspecial'], 2, '.', '')  . ' %"';
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Total:</label>
    <input id="totalUnoUF" disabled value=<?php
          echo '"UF ' . number_format($_SESSION['escrituraTotalUF'], 2, ',', '.')  . '"';
     ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="totalUnoMonto" disabled value=<?php
      echo '"$ ' . number_format(($_SESSION['escrituraTotalUF']*$_SESSION['escrituraUFActual']), 0, '.', '.')  . '"';
    ?>  class="form-control"></input>
  </div>
</div>
<div id="divDerechaCrearEscritura" style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <div id="divBodegasCrearEscritura" class="col-lg-12 col-md-12	col-sm-12 col-xs-12" style="margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; margin-bottom: 10pt;">
    <input disabled style="display: none;" value="<?php echo $_SESSION['idPromesa'];?>" id="escritura_idpromesa"/>
    <label style="font-weight: bold;">Bodegas</label>
    <select multiple="multiple"  id="bodegasCrearEscritura" name="bodegasCrearEscritura[]">

    </select>
  </div>
  <div id="divEstacionamientosCrearEscritura" class="col-lg-12 col-md-12	col-sm-12 col-xs-12" style="margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; margin-bottom: 10pt;">
    <label style="font-weight: bold;">Estacionamientos</label>
    <select multiple="multiple" id="estacionamientosCrearEscritura" name="estacionamientosCrearEscritura[]">

    </select>
  </div>
</div>

<div style="text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <hr style="background-color: #c1c1c1; height: 1px;" />
</div>

<div style="text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <font style="font-weight: bold;">Formas de pago</font>
</div>
<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0;  padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Reserva:</label>
    <input id="escritura_reservaUF" disabled value=<?php echo '"UF ' . number_format($_SESSION['escrituraReserva'], 2, ',', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0;  padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Monto $:</label>
    <input id="escritura_reservaMonto" disabled value=<?php echo '"$ ' . number_format($_SESSION['escrituraValorPagoReserva'], 0, '.', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0;  padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Porcentaje:</label>
    <input id="escritura_reservaPorcentaje" disabled value=<?php echo '"' . number_format((($_SESSION['escrituraReserva']/$_SESSION['escrituraTotalUF'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
  </div>
</div>

<!--Campos nuevos -->
<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Forma pago reserva:</label>
    <select class="form-control custom" type="text" id="escritura_FormaPagoReserva" disabled>
      <option value="<?php echo $_SESSION['reservaFormaPago'] ?>" selected><?php echo $_SESSION['escrituraFormaPagoNombreReserva']; ?></option>
    </select>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Banco:</label>
    <input id="escritura_BancoReserva" type="text" class="form-control" disabled value="<?php echo $_SESSION['escrituraBancoReserva'] ?>"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Serie/Nro:</label>
    <input id="escritura_SerieReserva" type="text" class="form-control" disabled value="<?php echo $_SESSION['escrituraSerieNroReserva'] ?>"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Nro Trans/Cheque:</label>
    <input id="escritura_NroReserva" type="text" class="form-control" disabled value="<?php echo $_SESSION['escrituraNroTransChequeReserva'] ?>"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Fecha pago:</label>
    <input id="escritura_FechaChequeReserva" type="text" class="form-control hasDatepicker" value="<?php echo $_SESSION['escrituraFechaPagoReserva'] ?>" disabled></input>
  </div>
</div>
<!-- Fin campos nuevos -->

<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Pie promesa contado:</label>
    <input id="escritura_piePromesaUFEditable" disabled value=<?php echo '"UF ' .  number_format($_SESSION['escrituraPieContado'], 2, ',', '.') . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="escritura_piePromesaMontoEditable" disabled value=<?php echo '"$ ' . number_format($_SESSION['escrituraPieContadoMonto'], 0, ',', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="escritura_piePromesaPorEditable" disabled value=<?php echo '"' . number_format((($_SESSION['escrituraPieContado']/$_SESSION['escrituraTotalUF'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
  </div>
</div>
<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Forma pago promesa:</label>
    <select class="form-control custom" type="text" id="escritura_FormaPagoPromesa" disabled>
      <option value="<?php echo $_SESSION['promesaFormaPago'] ?>" selected><?php echo $_SESSION['escrituraFormaPagoNombrePromesa']; ?></option>
    </select>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Banco:</label>
    <input id="escritura_BancoPromesa" type="text" class="form-control" value="<?php echo $_SESSION['escrituraBancoPromesa']; ?>" disabled></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Serie/Nro:</label>
    <input id="escritura_SeriePromesa" type="text" class="form-control" value="<?php echo $_SESSION['escrituraSerieNroPromesa']; ?>" disabled></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Nro Trans/Cheque:</label>
    <input id="escritura_NroPromesa" type="text" class="form-control" value="<?php echo $_SESSION['escrituraNroTransChequePromesa']; ?>" disabled></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Fecha pago:</label>
    <input id="escritura_FechaChequePromesa" type="text" class="form-control" value="<?php echo $_SESSION['escrituraFechaPagoPromesa']; ?>" disabled></input>
  </div>
</div>

<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Pie promesa cuotas:</label>
    <input id="escritura_pieCuotasUFEditable" disabled value=<?php echo '"UF ' . number_format($_SESSION['escrituraPieCuotasUF'], 2, ',', '.') . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="escritura_pieCuotasMontoEditable" disabled value=<?php echo '"$ ' . number_format($_SESSION['escrituraPieCuotas'], 0, '.', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="escritura_pieCuotasPorEditable" disabled value=<?php echo '"' . number_format((($_SESSION['escrituraPieCuotasUF']/$_SESSION['escrituraTotalUF'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
  </div>
</div>
<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Cuotas:</label>
    <input disabled style="display: none;" id="totalMontoCuotas"/>
    <input disabled id="escritura_cuotasEditable" type="number" step="1" min="1" max="100" value=<?php echo '"' . $_SESSION['escrituraPieCantCuotas']  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0; display: none;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="escritura_cuotasUFEditable" disabled value=<?php
    if($_SESSION['escrituraPieCantCuotas'] == '0'){
      echo '"UF ' . number_format(0, 2, ',', '.')   . '"';
    }
    else{
      echo '"UF ' . number_format($_SESSION['escrituraPieCuotasUF']/$_SESSION['escrituraPieCantCuotas'], 2, ',', '.')   . '"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0; display: none;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="escritura_cuotasMontoEditable" disabled value=<?php
    if($_SESSION['escrituraPieCantCuotas'] == '0'){
      echo '"$ ' . number_format(0, 0, ',', '.')   . '"';
    }
    else{
      echo '"$ ' . number_format($_SESSION['escrituraPieCuotas']/$_SESSION['escrituraPieCantCuotas'], 0, ',', '.')   . '"';
    }
      ?>  class="form-control"></input>
    </div>
</div>

<div id="escritura_cantidadCuotas">

</div>
<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <label>Diferencial UF:</label>
    <input id="cuotaResidual" class="form-control"></input>
  </div>
</div>
<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Forma pago diferencial UF:</label>
    <select class="form-control custom" type="text" id="cuotaResidualFormaPago">

    </select>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Banco:</label>
    <input id="cuotaResidualBanco" type="text" class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Serie/Nro:</label>
    <input id="cuotaResidualSerie" type="text" class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Nro Trans/Cheque:</label>
    <input id="cuotaResidualNro" type="text" class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Fecha pago:</label>
    <input id="cuotaResidualFechaCheque" type="text" class="form-control"></input>
  </div>
</div>
<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Saldo:</label>
    <input id="escritura_saldoUFEditable" disabled value=<?php echo '"UF ' . number_format($_SESSION['escrituraPieSaldo'], 2, ',', '.') . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <?php
      if($_SESSION['escrituraCodigoProyecto'] == 'COR' && $_SESSION['escrituraAccion'] == 'Arriendo'){
        echo '<input id="escritura_saldoMontoEditable" value="$ ' . number_format(($_SESSION['escrituraPieSaldo']*$_SESSION['escrituraUFActual']), 0, '.', '.')  . '" class="form-control"></input>';
      }
      else{
        echo '<input id="escritura_saldoMontoEditable" disabled value="$ ' . number_format(($_SESSION['escrituraPieSaldo']*$_SESSION['escrituraUFActual']), 0, '.', '.')  . '" class="form-control"></input>';
      }
    ?>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="escritura_saldoPorEditable" disabled value=<?php echo '"' . number_format((($_SESSION['escrituraPieSaldo']/$_SESSION['escrituraTotalUF'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
  </div>
</div>
<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Forma pago escritura:</label>
    <select class="form-control custom" type="text" id="escrituraFormaPago">

    </select>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Banco:</label>
    <input id="escrituraBanco" type="text" class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Serie/Nro:</label>
    <input id="escrituraSerie" type="text" class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Nro Trans/Cheque:</label>
    <input id="escrituraNro" type="text" class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Fecha pago:</label>
    <input id="escrituraFechaCheque" type="text" class="form-control"></input>
  </div>
</div>


<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Total:</label>
    <input id="escritura_totalUFEditable" disabled value=<?php echo '"UF ' . number_format($_SESSION['escrituraTotalUF'], 2, ',', '.') . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="escritura_totalMonto" disabled value=<?php echo '"$ ' . number_format(($_SESSION['escrituraTotalUF']*$_SESSION['escrituraUFActual']), 0, '.', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="escritura_totalPor" disabled value=<?php echo '"' . number_format((($_SESSION['escrituraTotalUF']/$_SESSION['escrituraTotalUF'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
  </div>
</div>
<div style="text-align: left; margin-top: 20pt;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <?php
   echo 'Precios Montos en pesos $CH a UF <font id="valorUFDia">' . number_format($_SESSION['escrituraUFActual'], 2, ',', '.') . '</font> corresponden como referencia al valor UF al <font id="fechaDia">' . $_SESSION['escrituraFecha'] . '</font>';

  ?>
</div>

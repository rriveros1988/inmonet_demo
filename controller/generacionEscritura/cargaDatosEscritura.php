<?php
//ini_set('display_errors', 'On');
date_default_timezone_set("America/Santiago");
require('../../model/consultas.php');
session_start();
$ch = curl_init();

$fecha = new Datetime();
$dia = $fecha->format('d');
$mes = $fecha->format('m');
$ano = $fecha->format('Y');

$fecha = $fecha->format('d-m-Y');

$valor = $_POST['valorUFJqueryHoy'];

$_SESSION['escrituraUFActual'] = $valor;

$promesa = consultaPromesaEspecifica($_POST['codigoProyecto'], $_POST['numeroOperacion']);
$reserva = consultaReservaEspecifica($_POST['codigoProyecto'], $_POST['numeroOperacion']);
$cotizacion = consultaCodigoCotizacionPromesa($promesa[0]['IDRESERVA']);
$proyecto = consultaProyectoEspecifico($promesa[0]['IDPROYECTO']);
$cliente = consultaClienteEspecifico($promesa[0]['IDCLIENTE1']);
$unidad = consultaUnidadEspecifica($promesa[0]['IDUNIDAD']);
$promesaBodega = consultaPromesaBodega($promesa[0]['IDPROMESA']);
$promesaEstacionamiento = consultaPromesaEstacionamiento($promesa[0]['IDPROMESA']);
$promesaBodegaCantidad = consultaPromesaBodegaCantidad($promesa[0]['IDPROMESA']);
$promesaEstacionamientoCantidad = consultaPromesaEstacionamientoCantidad($promesa[0]['IDPROMESA']);
$usuario = chequeaUsuario($_SESSION['rutUser']);


$_SESSION['escrituraCodigoProyecto'] = $_POST['codigoProyecto'];
//datos extra
$_SESSION['idReserva'] = $promesa[0]['IDRESERVA'];
$_SESSION['idPromesa'] = $promesa[0]['IDPROMESA'];
$_SESSION['idCliente1'] = $cliente[0]['IDCLIENTE'];
$_SESSION['idUnidad'] = $unidad[0]['IDUNIDAD'];
$_SESSION['idProyecto'] = $proyecto[0]['IDPROYECTO'];
$_SESSION['tipoUnidad'] = $unidad[0]['TIPOUNIDAD'];

$_SESSION['escrituraNombreProyecto'] = $proyecto[0]['NOMBRE'];
$_SESSION['escrituraLogoProyecto'] = '../' . $proyecto[0]['LOGO'];
if($_SESSION['escrituraLogoProyecto'] == '../'){
  $_SESSION['escrituraLogoProyecto'] = '../../view/img/imagenes_cliente/proyectos/NO_LOGO/no_logo.png';
}

$_SESSION['idUsuario'] = $usuario['IDUSUARIO'];


//Campos de cliente deben ser chequeados para ingreso de promesa
$_SESSION['escrituraNombreCliente'] = $cliente[0]['NOMBRES'];
$_SESSION['escrituraApellidoCliente'] = $cliente[0]['APELLIDOS'];
$_SESSION['escrituraRutCliente'] = $cliente[0]['RUT'];
$_SESSION['escrituraCelularCliente'] = $cliente[0]['CELULAR'];
$_SESSION['escrituraEmailCliente'] = $cliente[0]['EMAIL'];
$_SESSION['escrituraDomicilioCliente'] = $cliente[0]['DOMICILIO'];
$_SESSION['escrituraNumeroDomicilioCliente'] = $cliente[0]['NUMERODOMICILIO'];
$_SESSION['escrituraComunaCliente'] = $cliente[0]['COMUNA'];
$_SESSION['escrituraCiudadCliente'] = $cliente[0]['CIUDAD'];
$_SESSION['escrituraRegionCliente'] = $cliente[0]['REGION'];
$_SESSION['escrituraPaisCliente'] = $cliente[0]['PAIS'];
$_SESSION['escrituraProfesionCliente'] = $cliente[0]['PROFESION'];
$_SESSION['escrituraInstitucionCliente'] = $cliente[0]['INSTITUCION'];
$_SESSION['escrituraNacionalidadCliente'] = $cliente[0]['NACIONALIDAD'];
$_SESSION['escrituraSexoCliente'] = $cliente[0]['SEXO'];

//Generamos datos en session para la escritura
$fechaReserva = new Datetime($reserva[0]['FECHARESERVA']);
$fechaPagoReserva = new Datetime($reserva[0]['FECHAPAGORESERVA']);
$_SESSION['escrituraFechaReserva'] = $fechaReserva->format('d-m-Y');
$_SESSION['escrituraFechaPagoReserva'] = $fechaPagoReserva->format('d-m-Y');

$fechaPromesa = new Datetime($promesa[0]['FECHAPROMESA']);
$fechaPagoPromesa = new Datetime($promesa[0]['FECHAPAGOPROMESA']);
$_SESSION['escrituraFechaPromesa'] = $fechaPromesa->format('d-m-Y');
$_SESSION['escrituraFechaPagoPromesa'] = $fechaPagoPromesa->format('d-m-Y');

$_SESSION['escrituraNumeroOperacion'] = $promesa[0]['NUMERO'];
$_SESSION['escrituraNumeroCotizacion'] = $cotizacion[0]['CODIGO'];


//Demas datos
$_SESSION['escrituraNumeroDepto'] = $unidad[0]['CODIGO'];
$_SESSION['escrituraTipologiaDepto'] = $unidad[0]['TIPOLOGIADET'];
$_SESSION['escrituraModeloDepto'] = $unidad[0]['CODIGOTIPO'];
$_SESSION['escrituraOrientacionDepto'] = $unidad[0]['ORIENTACION'];
$_SESSION['escrituraMT2UtilesDepto'] = $unidad[0]['M2UTIL'];
$_SESSION['escrituraMT2TerrazaDepto'] = $unidad[0]['M2TERRAZA'];
$_SESSION['escrituraMT2TotalDepto'] = $unidad[0]['M2TOTAL'];
$_SESSION['escrituraEst'] = $promesaEstacionamientoCantidad[0]['EST'];
$_SESSION['escrituraBod'] = $promesaBodegaCantidad[0]['BOD'];
$_SESSION['escrituraValorDepto'] = $unidad[0]['VALORUF'];

//UF de bodega y estacionamiento
$_SESSION['escrituraValorEst'] = 0;
$_SESSION['escrituraValorBod'] = 0;

for($i = 0; $i < count($promesaEstacionamiento); $i++){
  $_SESSION['escrituraValorEst'] = $_SESSION['escrituraValorEst'] + $promesaEstacionamiento[0]['VALORUF'];
}

for($i = 0; $i < count($promesaBodega); $i++){
  $_SESSION['escrituraValorBod'] = $_SESSION['escrituraValorBod'] + $promesaBodega[0]['VALORUF'];
}


$_SESSION['escrituraValorBrutoUF'] = round(($_SESSION['escrituraValorDepto']+$_SESSION['escrituraValorEst']+$_SESSION['escrituraValorBod']),2);

//Seguimos con datos necesarios
$_SESSION['escrituraBono'] = $promesa[0]['BONOVENTAUF'];

$_SESSION['escrituraDescuentoSala'] = number_format(($promesa[0]['DESCUENTO1']),2, '.', '');
$_SESSION['escrituraDescuentoEspecial'] = number_format(($promesa[0]['DESCUENTO2']), 2, '.', '');

$_SESSION['escrituraTotal2'] = number_format(($_SESSION['escrituraValorDepto']-$_SESSION['escrituraValorDepto']*$_SESSION['escrituraDescuentoSala']/100),2,'.','');

$_SESSION['escrituraTotalUF'] = $promesa[0]['VALORTOTALUF'];
$_SESSION['escrituraReserva'] = $promesa[0]['VALORRESERVAUF'];
$_SESSION['escrituraPieContado'] = $promesa[0]['VALORPIEPROMESAUF'];
$_SESSION['escrituraPieContadoMonto'] = $promesa[0]['VALORPIEPROMESA'];
$_SESSION['escrituraPieCuotas'] = $promesa[0]['VALORPIESALDO'];
$_SESSION['escrituraPieCuotasUF'] = $promesa[0]['VALORPIESALDOUF'];
//$_SESSION['escrituraPieSaldo'] = $promesa[0]['VALORSALDOTOTALUF'];
$_SESSION['escrituraPieSaldo'] = $promesa[0]['VALORTOTALUF'] - $promesa[0]['VALORRESERVAUF'] - $promesa[0]['VALORPIEPROMESAUF'] - $promesa[0]['VALORPIESALDOUF'];

if($_SESSION['escrituraPieSaldo'] < 0){
  $_SESSION['escrituraPieSaldo'] = 0;
}

$_SESSION['escrituraPieCantCuotas'] = $promesa[0]['CANTCUOTASPIE'];
$_SESSION['escrituraAccion'] = $unidad[0]['ACCIONDET'];

//VALOR FORMA PAGO PROMESA
$_SESSION['escrituraValorPagoPromesa'] = $promesa[0]['VALORPAGOPROMESA'];
$_SESSION['promesaFormaPago'] = $promesa[0]['FORMAPAGOPROMESA'];
$formaPagoPromesa = consultaFormaPagoReservaEspecifica($_SESSION['promesaFormaPago']);
$_SESSION['escrituraFormaPagoNombrePromesa'] = $formaPagoPromesa[0]['NOMBRE'];
$_SESSION['escrituraBancoPromesa'] = $promesa[0]['BANCOPROMESA'];
$_SESSION['escrituraSerieNroPromesa'] = $promesa[0]['SERIECHEQUEPROMESA'];
$_SESSION['escrituraNroTransChequePromesa'] = $promesa[0]['NROCHEQUEPROMESA'];

//VALOR FORMA PAGO RESERVA
$_SESSION['escrituraValorPagoReserva'] = $promesa[0]['VALORPAGORESERVA'];
$_SESSION['reservaFormaPago'] = $reserva[0]['FORMAPAGORESERVA'];
$formaPagoReserva = consultaFormaPagoReservaEspecifica($_SESSION['reservaFormaPago']);
$_SESSION['escrituraFormaPagoNombreReserva'] = $formaPagoReserva[0]['NOMBRE'];
$_SESSION['escrituraBancoReserva'] = $reserva[0]['BANCORESERVA'];
$_SESSION['escrituraSerieNroReserva'] = $reserva[0]['SERIECHEQUERESERVA'];
$_SESSION['escrituraNroTransChequeReserva'] = $reserva[0]['NROCHEQUERESERVA'];



$obj->nombreproyecto = $proyecto[0]['NOMBRE'];
$obj->idpromesa = $promesa[0]['IDPROMESA'];
echo json_encode($obj);
?>

<?php
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');
	session_start();

	if(count($_POST) >= 0){
			$codigoProyecto = $_POST['codigoProyecto'];
			$accionUnidad =  $_POST['accionUnidad'];

			if($codigoProyecto != ''){
				$row = consultaUnidadesProyectosCotizacion($codigoProyecto,$accionUnidad);
				$_SESSION['codigoProyectoBack'] = $codigoProyecto;
				$_SESSION['accionUnidadBack'] = $accionUnidad;
			}
			else{
	    	$row = consultaUnidadesProyectosCotizacion($_SESSION['codigoProyectoBack'],$_SESSION['accionUnidadBack']);
			}


        if(is_array($row))
        {
            $return = "";

            for($i = 0; $i < count($row); $i++){
                if($row[$i] != null){
                    if($i == 0){
                      $return = $return . utf8_encode($row[$i]['CODIGO']) . '¬' . utf8_encode($row[$i]['PISO'])  . '¬' . utf8_encode($row[$i]['TIPO'])  . '¬' . utf8_encode($row[$i]['DIRECCION']). '¬' . utf8_encode($row[$i]['ACCION']);
                    }
                    else{
                      $return = $return . '¬' . utf8_encode($row[$i]['CODIGO']) . '¬' . utf8_encode($row[$i]['PISO'])  . '¬' . utf8_encode($row[$i]['TIPO'])  . '¬' . utf8_encode($row[$i]['DIRECCION']). '¬' . utf8_encode($row[$i]['ACCION']);
                    }
                }
            }

            echo $return;
        }
        else{
            echo "Sin datos";
        }
	}
	else{
		echo "Sin datos";
	}
?>

<?php
  header('Access-Control-Allow-Origin: *');
  //ini_set('display_errors', 'On');
  require('../model/consultas.php');
  session_start();

if(count($_SESSION) > 0 && count($_POST) > 0){
    $codigoProyecto = $_SESSION['codigoProyecto'];
    $numeroOperacion = $_SESSION['numeroOperacion'];
    $valorReservaUF = $_POST['valorReservaUF'];
    $valorPiePromesaUF = $_POST['valorPiePromesaUF'];
    $valorPieSaldoUF = $_POST['valorPieSaldoUF'];
    $cantidadCuotasPie = $_POST['cantidadCuotasPie'];
    $valorSaldoTotalUF = $_POST['valorSaldoTotalUF'];
    $fecha2 =  new DateTime($_POST['fechaPagoReserva']);
    $fechaPagoReserva = $fecha2->format("Y-m-d");
    $valorPagoReserva = $_POST['valorPagoReserva'];
    $formaPagoReserva = $_POST['formaPagoReserva'];
    $bancoReserva = $_POST['bancoReserva'];
    $serieChequeReserva = $_POST['serieChequeReserva'];
    $nroChequeReserva = $_POST['nroChequeReserva'];
    $valorPiePromesa = $_POST['valorPiePromesa'];
    $valorPieSaldo = $_POST['valorPieSaldo'];
    $_SESSION['observacionCrearReserva'] = $_POST['observacion'];
    $vendedorID = $_POST['reservaVendedor'];
    $_SESSION['reservaIdVendedor'] = $_POST['reservaVendedor'];
    $_SESSION['reservaCodigoProyecto'] = $_SESSION['codigoProyecto'];

    if(array_key_exists('reservaIdVendedor', $_SESSION)){
      $vendedor = chequeaUsuarioID($_SESSION['reservaIdVendedor']);
      $_SESSION['reservaVendedor'] = $vendedor['NOMBRES'] . ' ' . $vendedor['APELLIDOS'];
      $_SESSION['reservaVendedorRut'] = $vendedor['RUT'];
    }
    else{
      $_SESSION['reservaVendedor'] = $_SESSION['nombreUser'];
      $_SESSION['reservaVendedorRut'] = $_SESSION['rutUser'];
    }

    /*
    if($_SESSION['codProyectoClienteCotizacion'] == "COR"){
      $descuento1UF = 0;
      $descuento2UF = 0;
      $valorReservaUF = 0;
      $valorPiePromesaUF = 0;
      $valorPieSaldoUF = 0;
      $cantidadCuotasPie = 0;
      $valorSaldoTotalUF = 0;
    }else{
      $descuento1UF = $_SESSION['descuento1ClienteCotizacion'];
      $descuento2UF = $_SESSION['descuento2ClienteCotizacion'];
      $valorReservaUF = $_SESSION['reservaClienteCotizacion'];
      $valorPiePromesaUF = $_SESSION['piePromesaClienteCotizacion'];
      $valorPieSaldoUF = $_SESSION['pieCuotasClienteCotizacion'];
      $cantidadCuotasPie = $_SESSION['cuotasClienteCotizacion'];
      $valorSaldoTotalUF = $_SESSION['saldoClienteCotizacion'];
      $bonoVenta = $_SESSION['bonoVentaCrearCotizacion'];
    }
    */

    liberaUnidadesReservaEstacionamiento($codigoProyecto,$numeroOperacion);
    liberaUnidadesReservaBodega($codigoProyecto,$numeroOperacion);
    eliminaReservaBodega($codigoProyecto, $numeroOperacion);
    eliminaReservaEstacionamiento($codigoProyecto, $numeroOperacion);

    $row = actualizarReserva($codigoProyecto, $numeroOperacion, $valorReservaUF, $valorPiePromesaUF, $valorPieSaldoUF, $cantidadCuotasPie, $valorSaldoTotalUF, $fechaPagoReserva, $valorPagoReserva, $formaPagoReserva, $bancoReserva, $serieChequeReserva, $nroChequeReserva, $valorPiePromesa, $valorPieSaldo, $vendedorID);

    if($row != "Error")
    {
      $in = 'Ok';
      $in2 = 'Ok';

      if($_POST['bodegasClienteReserva'] != ''){
        for($i = 0; $i < count($_POST['bodegasClienteReserva']); $i++){
          $in = ingresaReservaBodega($_SESSION['idReserva'],$_POST['bodegasClienteReserva'][$i], $row);
          if($in == "Error"){
            break;
          }
          else{
            actualizaEstadoUnidad($_POST['bodegasClienteReserva'][$i], 3, $row);
          }
        }
      }

      if(is_array($_POST['estacionamientosClienteReserva'])){
        for($i = 0; $i < count($_POST['estacionamientosClienteReserva']); $i++){
          $in2 = ingresaReservaEstacionamiento($_SESSION['idReserva'],$_POST['estacionamientosClienteReserva'][$i], $row);
          if($in2 == "Error"){
            break;
          }
          else{
            actualizaEstadoUnidad($_POST['estacionamientosClienteReserva'][$i], 3, $row);
          }
        }
      }

      if($in != "Error" && $in2 != "Error"){
        $row->query("COMMIT");
        /* Datos para documentos PDF */
        //consultas
        $proyecto = consultaProyectoEspecificoCodigo($codigoProyecto);
        $reserva = consultaReservaEspecifica($codigoProyecto, $numeroOperacion);
        $cliente = consultaClienteEspecifico($reserva[0]['IDCLIENTE1']);
        $unidad = consultaUnidadEspecificaReserva($reserva[0]['IDUNIDAD']);
        $cotizacion = consultaCotizacionEspecificaID($reserva[0]['IDCOTIZACION']);
        $cotizacionBodega = consultaReservaBodega($reserva[0]['IDRESERVA']);
        $cotizacionEstacionamiento = consultaReservaEstacionamiento($reserva[0]['IDRESERVA']);

        //Datos a Session
        $_SESSION['reservaFormaPagoReserva'] = $reserva[0]['FORMAPAGORESERVA'];
        $_SESSION['reservaBancoReserva'] = $reserva[0]['BANCORESERVA'];
        $_SESSION['reservaSerieChequeReserva'] = $reserva[0]['SERIECHEQUERESERVA'];
        $_SESSION['reservaNroChequeReserva'] = $reserva[0]['NROCHEQUERESERVA'];
        $_SESSION['reservaValorPagoReserva'] = $reserva[0]['VALORPAGORESERVA'];


        $_SESSION['reservaLogoProyecto'] = '../' . $proyecto[0]['LOGO'];
        if($_SESSION['reservaLogoProyecto'] == '../'){
          $_SESSION['reservaLogoProyecto'] = '../../view/img/imagenes_cliente/proyectos/NO_LOGO/no_logo.png';
        }
        $_SESSION['reservaNombreProyecto'] = $proyecto[0]['NOMBRE'];
        $_SESSION['reservaValidesReserva'] = $proyecto[0]['DIASRES'];
        $fechaReserva = new DateTime($reserva[0]['FECHARESERVA']);
        $_SESSION['reservaFecha'] = $fechaReserva->format('d-m-Y');
        $_SESSION['reservaNombreCliente'] = $cliente[0]['NOMBRES'];
        $_SESSION['reservaApellidoCliente'] = $cliente[0]['APELLIDOS'];
        $_SESSION['reservaRutCliente'] = $cliente[0]['RUT'];
        $_SESSION['reservaCelularCliente1'] = $cliente[0]['CELULAR'];
        $_SESSION['reservaEmailCliente1'] = $cliente[0]['EMAIL'];
        $_SESSION['reservaDomicilioCliente1'] = $cliente[0]['DOMICILIO'];
        $_SESSION['reservaNumeroDomicilioCliente1'] = $cliente[0]['NUMERODOMICILIO'];
        $_SESSION['reservaComunaCliente1'] = $cliente[0]['COMUNA'];
        $_SESSION['reservaCiudadCliente1'] = $cliente[0]['CIUDAD'];
        $_SESSION['reservaRegionCliente1'] = $cliente[0]['REGION'];
        $_SESSION['reservaPaisCliente1'] = $cliente[0]['PAIS'];
        $_SESSION['reservaProfesionCliente1'] = $cliente[0]['PROFESION'];
        $_SESSION['reservaInstitucionCliente1'] = $cliente[0]['INSTITUCION'];
        $_SESSION['reservaNacionalidadCliente1'] = $cliente[0]['NACIONALIDAD'];
        $_SESSION['reservaResidenciaCliente1'] = $cliente[0]['RESIDENCIA'];
        $_SESSION['reservaEstadoCivilIDCliente1'] = $cliente[0]['ESTADOCIVIL'];
        $_SESSION['reservaNivelEducCliente1'] = $cliente[0]['NIVELEDUCACIONAL'];
        $_SESSION['reservaCasaHabitaCliente1'] = $cliente[0]['CASAHABITA'];
        $_SESSION['reservaMotivoCompraCliente1'] = $cliente[0]['MOTIVOCOMPRA'];
        $_SESSION['reservaSexoCliente1'] = $cliente[0]['SEXO'];
        $_SESSION['reservaRentaCliente1'] = $cliente[0]['EM_RENTALIQUIDA'];
        $_SESSION['reservaTelefonoCliente1'] = $cliente[0]['CELULAR'];
        $_SESSION['reservaMailCliente1'] = $cliente[0]['EMAIL'];
        $_SESSION['reservaActividadCliente1'] = $cliente[0]['ACTIVIDAD'];
        $_SESSION['reservaFechaNacCliente1'] = $cliente[0]['FECHANAC'];

        $_SESSION['reservaNumeroDepto'] = $unidad[0]['CODIGO'];
        $_SESSION['reservaTipologiaDepto'] = $unidad[0]['TIPOLOGIA'];
        $_SESSION['reservaModeloDepto'] = $unidad[0]['CODIGOTIPO'];
        $_SESSION['reservaOrientacionDepto'] = $unidad[0]['ORIENTACION'];
        $_SESSION['reservaMT2UtilesDepto'] = $unidad[0]['M2UTIL'];
        $_SESSION['reservaMT2TerrazaDepto'] = $unidad[0]['M2TERRAZA'];
        $_SESSION['reservaMT2TotalDepto'] = $unidad[0]['M2TOTAL'];
        $_SESSION['reservaNumeroCotiza'] = $cotizacion[0]['CODIGO'];
        $_SESSION['reservaEst'] = $cotizacion[0]['CANTEST'];
        $_SESSION['reservaBod'] = $cotizacion[0]['CANTBOD'];
        $_SESSION['reservaValorDepto'] = $cotizacion[0]['VALORUNIDADUF'];
        $_SESSION['reservaDescuentoSala'] = number_format(($cotizacion[0]['DESCUENTO1UF']),2, '.', '');
        $_SESSION['reservaDescuentoEspecial'] = number_format(($cotizacion[0]['DESCUENTO2UF']), 2, '.', '');
        $_SESSION['reservaBono'] = $cotizacion[0]['BONOVENTA'];
        $_SESSION['reservaTotal2'] = number_format(($_SESSION['reservaValorDepto']-$_SESSION['reservaValorDepto']*$_SESSION['reservaDescuentoSala']/100),2,'.','');
        $_SESSION['reservaTotal3'] = number_format(
        ($_SESSION['reservaValorDepto'] -
        ($_SESSION['reservaValorDepto']*$_SESSION['reservaDescuentoSala']/100)-
        ($_SESSION['reservaTotal2']*$_SESSION['reservaDescuentoEspecial']/100)
        ),2,'.','');
        $_SESSION['bodegasClienteReserva'] = $_POST['bodegasClienteReserva'];
        $_SESSION['estacionamientosClienteReserva'] =  $_POST['estacionamientosClienteReserva'];

        $_SESSION['reservaValorEst'] = 0;
        $_SESSION['reservaValorBod'] = 0;

        for($i = 0; $i < count($cotizacionEstacionamiento); $i++){
          $_SESSION['reservaValorEst'] = $_SESSION['reservaValorEst'] + $cotizacionEstacionamiento[0]['VALORUF'];
        }

        for($i = 0; $i < count($cotizacionBodega); $i++){
          $_SESSION['reservaValorBod'] = $_SESSION['reservaValorBod'] + $cotizacionBodega[0]['VALORUF'];
        }

        $_SESSION['reservaTotalF'] = $reserva[0]['VALORTOTALUF'];
        $_SESSION['reservaReservaUF'] = $reserva[0]['VALORRESERVAUF'];
        $_SESSION['reservaPiePromesaUF'] = $reserva[0]['VALORPIEPROMESAUF'];
        $_SESSION['reservaCuotasPie'] = $reserva[0]['CANTCUOTASPIE'];
        $_SESSION['reservaPieSaldoUF'] = $reserva[0]['VALORPIESALDOUF'];
        $_SESSION['reservaSaldoTotalUF'] = $reserva[0]['VALORSALDOTOTALUF'];
        $_SESSION['reservaInmobiliaria'] = $proyecto[0]['INMOBILIARIA'];



        echo "Ok¬" . $codigoProyecto . "¬" . $numeroOperacion;
      }
      else{
        $row->query("ROLLBACK");
        echo "Sin datos";
      }
    }
  	else{
  		echo "Sin datos";
  	}
	}
	else{
		echo "Sin datos";
	}
?>

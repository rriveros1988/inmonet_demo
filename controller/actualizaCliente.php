<?php
  header('Access-Control-Allow-Origin: *');
  require('../model/consultas.php');

	if(count($_POST) > 0){
    $rutDetalleCliente = str_replace(".",'',$_POST['rutDetalleCliente']);
    $apellidosDetalleCliente = $_POST['apellidosDetalleCliente'];
    $nombresDetalleCliente = $_POST['nombresDetalleCliente'];
    $emailDetalleCliente = $_POST['emailDetalleCliente'];
    $celularDetalleCliente = $_POST['celularDetalleCliente'];
    $domicilioDetalleCliente = $_POST['domicilioDetalleCliente'];
    $numeroDomicilioDetalleCliente = $_POST['numeroDomicilioDetalleCliente'];
    $tipoDomicilioDetalleCliente = $_POST['tipoDomicilioDetalleCliente'];
    $comunaDetalleCliente = $_POST['comunaDetalleCliente'];
    $ciudadDetalleCliente = $_POST['ciudadDetalleCliente'];
    $paisDetalleCliente = $_POST['paisDetalleCliente'];
    $fNacDetalleCliente = $_POST['fNacDetalleCliente'];
    $estadoCivilDetalleCliente = $_POST['estadoCivilDetalleCliente'];
    $nivelEducacionalDetalleCliente = $_POST['nivelEducacionalDetalleCliente'];
    $casaHabitacionalDetalleCliente = $_POST['casaHabitacionalDetalleCliente'];
    $motivoDetalleCliente = $_POST['motivoDetalleCliente'];
    $profesionDetalleCliente = $_POST['profesionDetalleCliente'];
    $institucionDetalleCliente = $_POST['institucionDetalleCliente'];
    $actividadDetalleCliente = $_POST['actividadDetalleCliente'];
    $nacionalidadDetalleCliente = $_POST['nacionalidadDetalleCliente'];
    $residenciaDetalleCliente = $_POST['residenciaDetalleCliente'];
    $sexoDetalleCliente = $_POST['sexoDetalleCliente'];
    $fAltaDetalleCliente = $_POST['fAltaDetalleCliente'];
    $estadoDetalleCliente = $_POST['estadoDetalleCliente'];
    $EMRutDetalleCliente = $_POST['EMRutDetalleCliente'];
    $EMNombreDetalleCliente = $_POST['EMNombreDetalleCliente'];
    $EMDomicilioDetalleCliente = $_POST['EMDomicilioDetalleCliente'];
    $EMComunaDetalleCliente = $_POST['EMComunaDetalleCliente'];
    $EMCiudadDetalleCliente = $_POST['EMCiudadDetalleCliente'];
    $EMPaisDetalleCliente = $_POST['EMPaisDetalleCliente'];
    $EMFonoDetalleCliente = $_POST['EMFonoDetalleCliente'];
    $EMGiroDetalleCliente = $_POST['EMGiroDetalleCliente'];
    $EMFIngresoDetalleCliente = $_POST['EMFIngresoDetalleCliente'];
    $EMAntiguedadDetalleCliente = $_POST['EMFAntiguedadDetalleCliente'];
    $EMCargoDetalleCliente = $_POST['EMCargoDetalleCliente'];
    $EMRentaLiquidaDetalleCliente = $_POST['EMRentaLiquidaDetalleCliente'];
    $observacionDetalleCliente = $_POST['observacionDetalleCliente'];
    $regionDetalleCliente = $_POST['regionDetalleCliente'];

    if($celularDetalleCliente == ''){
      $celularDetalleCliente = 0;
    }
    if($numeroDomicilioDetalleCliente == ''){
      $numeroDomicilioDetalleCliente = 0;
    }
    if($EMFonoDetalleCliente == ''){
      $EMFonoDetalleCliente = 0;
    }
    if($EMAntiguedadDetalleCliente == ''){
      $EMAntiguedadDetalleCliente = 0;
    }
    if($EMRentaLiquidaDetalleCliente == ''){
      $EMRentaLiquidaDetalleCliente = 0;
    }

    if($fNacDetalleCliente == ''){
      $fNacDetalleCliente = '1900-01-01';
    }
    else{
      $date = new DateTime($fNacDetalleCliente);
      $fNacDetalleCliente = $date->format('Y-m-d');
    }
    if($EMFIngresoDetalleCliente == ''){
      $EMFIngresoDetalleCliente = '1900-01-01';
    }
    else{
      $date = new DateTime($EMFIngresoDetalleCliente);
      $EMFIngresoDetalleCliente  = $date->format('Y-m-d');
    }
    if($fAltaDetalleCliente == ''){
      $fAltaDetalleCliente = '1900-01-01';
    }
    else{
      $date = new DateTime($fAltaDetalleCliente);
      $fAltaDetalleCliente  = $date->format('Y-m-d');
    }

    $row = actualizaCliente($rutDetalleCliente,$nombresDetalleCliente,$apellidosDetalleCliente,$emailDetalleCliente,$celularDetalleCliente,$domicilioDetalleCliente,$numeroDomicilioDetalleCliente,$tipoDomicilioDetalleCliente,$comunaDetalleCliente,$ciudadDetalleCliente,$paisDetalleCliente,$fNacDetalleCliente,$estadoCivilDetalleCliente,$nivelEducacionalDetalleCliente,$casaHabitacionalDetalleCliente,$motivoDetalleCliente,$profesionDetalleCliente,$institucionDetalleCliente,$actividadDetalleCliente,$nacionalidadDetalleCliente,
    $residenciaDetalleCliente,$sexoDetalleCliente,$EMRutDetalleCliente,$EMNombreDetalleCliente,$EMDomicilioDetalleCliente,$EMComunaDetalleCliente,$EMCiudadDetalleCliente,$EMPaisDetalleCliente,$EMFonoDetalleCliente,$EMGiroDetalleCliente,$EMFIngresoDetalleCliente,$EMAntiguedadDetalleCliente,$EMCargoDetalleCliente,$EMRentaLiquidaDetalleCliente,$observacionDetalleCliente,$estadoDetalleCliente,$fAltaDetalleCliente,$regionDetalleCliente);

    if($row == "Ok")
    {
      echo "Ok";
    }
    else{
      //echo $row;
      echo "Sin datos";
    }
	}
	else{
		echo "Sin datos";
	}
?>

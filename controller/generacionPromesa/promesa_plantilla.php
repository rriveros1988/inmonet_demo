<script>
	function randomTextoLoadJs(){
	  var str = '';
	  var ref = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRST0123456789';
	  for (var i = 0; i < 8 ; i++)
	  {
	    str += ref.charAt(Math.floor(Math.random()*ref.length));
	  }
	  return str;
	}
  var js = document.createElement('script');
  js.src = 'view/js/funciones.js?idLoad=' + randomTextoLoadJs();
  document.getElementsByTagName('head')[0].appendChild(js);
</script>
<style type="text/css">
.headTabla{
  background-color: #e6f2ff;
  border: 1px solid black;
  padding: 2px;
}
.bodyTabla{
  border: 1px solid black;
  padding: 1px;
}
label{
  font-size: 12px;
}
input{
  font-size: 12px;
}
</style>
<?php
  session_start();
?>
<div id="divDerechaCrearPromesa" style="padding-left: 0; padding-right: 0;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Fecha reserva:</label>
    <input disabled id="reservaFecha" value=<?php echo '"' . $_SESSION['promesaFechaReserva'] . '"' ?> class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>N° cotizacion:</label>
    <input disabled  value=<?php echo '"' . $_SESSION['promesaNumeroCotizacion']  . '"'; ?> class="form-control"></input><!-- Pendiente -->
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>N° operación:</label>
    <input disabled  value=<?php echo '"' . $_SESSION['promesaNumeroReserva']  . '"'; ?> class="form-control"></input><!-- Pendiente -->
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Nombre cliente:</label>
    <input id="reservaNombreCliente" disabled value=<?php echo '"' . $_SESSION['promesaNombreCliente'] . '"'; ?> class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Apellido cliente:</label>
    <input id="reservaApellidoCliente" disabled value=<?php echo '"' . $_SESSION['promesaApellidoCliente'] . '"'; ?> class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Rut:</label>
    <input id="reservaRutCliente" disabled value=<?php echo '"' . $_SESSION['promesaRutCliente'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Celular:</label>
    <input id="reservaCelularCliente" disabled value=<?php echo '"' . $_SESSION['promesaCelularCliente'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
    <label>Email:</label>
    <input id="reservaEmailCliente" disabled value=<?php echo '"' . $_SESSION['promesaEmailCliente'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="margin-top: 20pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
    <button style="width: 100%; margin-bottom: 5pt;" class="btn btn-primary" id="anexoDatosCliente"><i clasS="fa fa-plus-square"></i>&nbsp;&nbsp;Agregar datos de cliente 1</button>
  </div>
  <div style="margin-top: 20pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
    <button disabled style="width: 100%; margin-bottom: 5pt;" class="btn btn-primary" id="anexoDatosCliente2"><i clasS="fa fa-plus-square"></i>&nbsp;&nbsp;Agregar datos de cliente 2</button>
  </div>
</div>
<div id="divIzquierdaCrearPromesa" style="padding-left: 0; padding-right: 0;   border-left-style: solid; border-left-width: 1px; border-left-color: #c1c1c1;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Fecha:</label>
    <input id="promesaFecha" class="form-control"></input>
  </div>
	<div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<label>Vendedor:</label>
		<select id="promesaVendedor" class="form-control custom">
			<?php
				// ini_set('display_errors', 'On');
				require('../../model/consultas.php');
				$ven = consultaUsuariosVendedores();
				$us = consultaReservaEspecifica($_SESSION["promesaCodigoProyecto"], $_SESSION["promesaNumeroReserva"]);
				for($i = 0; $i < count($ven); $i++){
					if($ven[$i]['IDUSUARIO'] === $us[0]['IDUSUARIO']){
							echo '<option selected value="' . $ven[$i]['IDUSUARIO'] . '">' . $ven[$i]['NOMBRE'] . '</option>';
					}
					else{
							echo '<option value="' . $ven[$i]['IDUSUARIO'] . '">' . $ven[$i]['NOMBRE'] . '</option>';
					}
				}
			?>
		</select>
	</div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Departamento:</label>
    <input disabled value=<?php echo '"' . $_SESSION['promesaNumeroDepto'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Tipología:</label>
    <input disabled value=<?php echo '"' . $_SESSION['promesaTipologiaDepto'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Modelo:</label>
    <input disabled value=<?php echo '"' . $_SESSION['promesaModeloDepto'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Orientación:</label>
    <input disabled value=<?php echo '"' . $_SESSION['promesaOrientacionDepto'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Bodegas:</label>
    <input disabled value=<?php echo '"' . $_SESSION['promesaBod'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Estacionamientos:</label>
    <input disabled value=<?php echo '"' . $_SESSION['promesaEst']  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Utiles:</label>
    <input disabled value=<?php echo '"' . number_format($_SESSION['promesaMT2UtilesDepto'],2,',','.')  . ' Mt2"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Terraza:</label>
    <input disabled value=<?php echo '"' . number_format($_SESSION['promesaMT2TerrazaDepto'],2,',','.')  . ' Mt2"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Total:</label>
    <input disabled value=<?php echo '"' . number_format($_SESSION['promesaMT2TotalDepto'],2,',','.')  . ' Mt2"'; ?>  class="form-control"></input>
  </div>
</div>

<div style="text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <hr style="background-color: #c1c1c1; height: 1px;" />
</div>

<div id="divIzquierdaCrearPromesa" style="text-align: left; padding-left: 0; padding-right: 0;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Departamento:</label>
    <input id="promesa_departamentoUF" disabled value=<?php echo '"UF ' . number_format($_SESSION['promesaValorDepto'], 2, ',', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input disabled value=<?php echo '"$ ' . number_format(($_SESSION['promesaValorDepto']*$_SESSION['promesaUFActual']), 0, '.', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Bodega:</label>
    <input id="promesa_bodegaUF" disabled value=<?php echo '"UF '.number_format($_SESSION['promesaValorBod'], 2, ',', '.').'"';?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="bodegaMonto" disabled value=

    <?php
    if($_SESSION['promesaAccion'] != "Leasing"){
      echo '"$ ' . number_format(($_SESSION['promesaValorBod']*$_SESSION['promesaUFActual']), 0, '.', '.')  . '"';
    }
    else{
      echo '"$ ' . number_format(0, 0, '.', '.')  . '"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Estacionamiento:</label>
    <input id="promesa_estacionamientoUF" disabled value=
    <?php
    if($_SESSION['promesaAccion'] != "Leasing"){
      echo '"UF ' . number_format($_SESSION['promesaValorEst'], 2, ',', '.')  . '"';
    }
    else{
      echo '"UF ' . number_format(0, 2, ',', '.')  . '"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="estacionamientoMonto" disabled value=
    <?php
    if($_SESSION['promesaAccion'] != "Leasing"){
      echo '"$ ' . number_format(($_SESSION['promesaValorEst']*$_SESSION['promesaUFActual']), 0, '.', '.')  . '"';
    }
    else{
      echo '"$ ' . number_format(0, 0, '.', '.')  . '"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Bono:</label>
    <input id="promesa_bonoUF" disabled value=
    <?php
    if($_SESSION['promesaAccion'] != "Leasing"){
      echo '"UF ' . number_format($_SESSION['promesaBono'], 2, ',', '.')  . '"';
    }
    else{
      echo '"UF ' . number_format(0, 2, ',', '.')  . '"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="bonoMonto" disabled value=
    <?php
    if($_SESSION['promesaAccion'] != "Leasing"){
      echo '"$ ' . number_format(($_SESSION['promesaBono']*$_SESSION['promesaUFActual']), 0, '.', '.')  . '"';
    }
    else{
      echo '"$ ' . number_format(0, 0, '.', '.')  . '"';
    }
    ?>  class="form-control"></input>
  </div>

  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Desc. sala:</label>
    <input  id="promesa_desc1UF" disabled value=<?php
    if($_SESSION['promesaAccion'] != "Leasing"){
      echo '"UF ' . number_format(($_SESSION['promesaDescuentoSala']*$_SESSION['promesaValorDepto']/100), 2, ',', '.')  . '"';
    }
    else{
      echo '"UF ' . number_format(0, 2, ',', '.')  . '"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input disabled value=<?php
    if($_SESSION['promesaAccion'] != "Leasing"){
      echo '"$ ' . number_format(number_format(($_SESSION['promesaDescuentoSala']*$_SESSION['promesaValorDepto']/100), 2, '.', '.')*$_SESSION['promesaUFActual'], 0, '.', '.')  . '"';
    }
    else{
      echo '"$ ' . number_format(0, 0, ',', '.')  . '"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input disabled value=<?php
    if($_SESSION['promesaAccion'] != "Leasing"){
      echo '"' . number_format($_SESSION['promesaDescuentoSala'], 2, '.', '')  . ' %"';
    }
    else{
      echo '"' . number_format(0, 2, '.', '')  . ' %"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Desc. especial:</label>
    <input id="promesa_desc2UF" disabled value=<?php
     if($_SESSION['promesaAccion'] != "Leasing"){
       echo '"UF ' .  number_format(($_SESSION['promesaDescuentoEspecial']*$_SESSION['promesaTotal2']/100), 0, '.', '.') . '"';
     }
     else{
       echo '"UF ' .  number_format(0, 2, ',', '.') . '"';
     }
     ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input disabled value=<?php
     if($_SESSION['promesaAccion'] != "Leasing"){
       echo '"$ ' . number_format(number_format(($_SESSION['promesaDescuentoEspecial']*$_SESSION['promesaTotal2']/100), 2, '.', '.')*$_SESSION['promesaUFActual'], 0, '.', '.')  . '"';
     }
     else{
       echo '"$ ' . number_format(0, 0, '.', '.')  . '"';
     }
     ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input disabled value=<?php
    if($_SESSION['promesaAccion'] != "Leasing"){
      echo '"' . number_format($_SESSION['promesaDescuentoEspecial'], 2, '.', '')  . ' %"';
    }
    else{
      echo '"' . number_format(0, 2, '.', '')  . ' %"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Total:</label>
    <input id="totalUnoUF" disabled value=<?php
        if($_SESSION['promesaAccion'] != "Venta"){
          echo '"UF ' . number_format($_SESSION['promesaValorDepto'], 2, ',', '.')  . '"';
        }
        else{
          echo '"UF ' . number_format($_SESSION['promesaTotalUF'], 2, ',', '.')  . '"';
        }
     ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="totalUnoMonto" disabled value=<?php
    if($_SESSION['promesaAccion'] != "Venta"){
      echo '"$ ' . number_format(($_SESSION['promesaValorDepto']*$_SESSION['promesaUFActual']), 0, '.', '.')  . '"';
    }
    else{
      echo '"$ ' . number_format(($_SESSION['promesaTotalUF']*$_SESSION['promesaUFActual']), 0, '.', '.')  . '"';
    }
    ?>  class="form-control"></input>
  </div>
</div>
<div id="divDerechaCrearPromesa" style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <div id="divBodegasCrearPromesa" class="col-lg-12 col-md-12	col-sm-12 col-xs-12" style="margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; margin-bottom: 10pt;">
    <input disabled style="display: none;" value="<?php echo $_SESSION['idReserva'];?>" id="promesa_idreserva"/>
    <label style="font-weight: bold;">Bodegas</label>
    <select multiple="multiple" id="bodegasCrearPromesa" name="bodegasCrearPromesa[]">

    </select>
  </div>
  <div id="divEstacionamientosCrearPromesa" class="col-lg-12 col-md-12	col-sm-12 col-xs-12" style="margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; margin-bottom: 10pt;">
    <label style="font-weight: bold;">Estacionamientos</label>
    <select multiple="multiple" id="estacionamientosCrearPromesa" name="estacionamientosCrearPromesa[]">

    </select>
  </div>
</div>

<div style="text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <hr style="background-color: #c1c1c1; height: 1px;" />
</div>

<div style="text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <font style="font-weight: bold;">Formas de pago</font>
</div>
<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0;  padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Reserva:</label>
    <input id="promesa_reservaUF" disabled value=<?php echo '"UF ' . number_format($_SESSION['promesaReserva'], 2, ',', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0;  padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Monto $:</label>
    <input id="promesa_reservaMonto" disabled value=<?php echo '"$ ' . number_format($_SESSION['promesaValorPagoReserva'], 0, '.', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0;  padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Porcentaje:</label>
    <input id="promesa_reservaPorcentaje" disabled value=<?php echo '"' . number_format((($_SESSION['promesaReserva']/$_SESSION['promesaTotalUF'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
  </div>
</div>

<!--Campos nuevos -->
<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Forma pago reserva:</label>
    <select class="form-control custom" type="text" id="promesa_FormaPagoReserva" disabled>
      <option value="<?php echo $_SESSION['promesaFormaPagoValor'] ?>" selected><?php echo $_SESSION['promesaFormaPagoNombre']; ?></option>
    </select>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Banco:</label>
    <input id="promesa_BancoReserva" type="text" class="form-control" disabled value="<?php echo $_SESSION['promesaBanco'] ?>"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Serie/Nro:</label>
    <input id="promesa_SerieReserva" type="text" class="form-control" disabled value="<?php echo $_SESSION['promesaSerieNro'] ?>"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Nro Trans/Cheque:</label>
    <input id="promesa_NroReserva" type="text" class="form-control" disabled value="<?php echo $_SESSION['promesaNroTransCheque'] ?>"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Fecha pago:</label>
    <input id="promesa_FechaChequeReserva" type="text" class="form-control hasDatepicker" value="<?php echo $_SESSION['promesaFechaPagoReserva'] ?>" disabled></input>
  </div>
</div>
<!-- Fin campos nuevos -->

<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Pie promesa contado:</label>
    <input id="promesa_piePromesaUFEditable" disabled value=<?php echo '"UF ' .  number_format($_SESSION['promesaPieContado'], 2, ',', '.') . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="promesa_piePromesaMontoEditable" value=<?php echo '"$ ' . number_format($_SESSION['promesaPieContadoMonto'], 0, ',', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="promesa_piePromesaPorEditable" disabled value=<?php echo '"' . number_format((($_SESSION['promesaPieContado']/$_SESSION['promesaTotalUF'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
  </div>
</div>
<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Forma pago promesa:</label>
    <select class="form-control custom" type="text" id="promesa_FormaPagoPromesa" >

    </select>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Banco:</label>
    <input id="promesa_BancoPromesa" type="text" class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Serie/Nro:</label>
    <input id="promesa_SeriePromesa" type="text" class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Nro Trans/Cheque:</label>
    <input id="promesa_NroPromesa" type="text" class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Fecha pago:</label>
    <input id="promesa_FechaChequePromesa" type="text" class="form-control"></input>
  </div>
</div>

<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Pie promesa cuotas:</label>
    <input id="promesa_pieCuotasUFEditable" disabled value=<?php echo '"UF ' . number_format($_SESSION['promesaPieCuotasUF'], 2, ',', '.') . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="promesa_pieCuotasMontoEditable" value=<?php echo '"$ ' . number_format($_SESSION['promesaPieCuotas'], 0, '.', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="promesa_pieCuotasPorEditable" disabled value=<?php echo '"' . number_format((($_SESSION['promesaPieCuotasUF']/$_SESSION['promesaTotalUF'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
  </div>
</div>
<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Cuotas:</label>
    <input disabled style="display: none;" id="totalMontoCuotas"/>
    <input id="promesa_cuotasEditable" type="number" step="1" min="1" max="100" value=<?php echo '"' . $_SESSION['promesaPieCantCuotas']  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0; display: none;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="promesa_cuotasUFEditable" disabled value=<?php
    if($_SESSION['promesaPieCantCuotas'] == '0'){
      echo '"UF ' . number_format(0, 2, ',', '.')   . '"';
    }
    else{
      echo '"UF ' . number_format($_SESSION['promesaPieCuotasUF']/$_SESSION['promesaPieCantCuotas'], 2, ',', '.')   . '"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0; display: none;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="promesa_cuotasMontoEditable" disabled value=<?php
    if($_SESSION['promesaPieCantCuotas'] == '0'){
      echo '"$ ' . number_format(0, 0, ',', '.')   . '"';
    }
    else{
      echo '"$ ' . number_format($_SESSION['promesaPieCuotas']/$_SESSION['promesaPieCantCuotas'], 0, ',', '.')   . '"';
    }
      ?>  class="form-control"></input>
    </div>
</div>

<div id="promesa_cantidadCuotas">

</div>

<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Saldo:</label>
    <input id="promesa_saldoUFEditable" disabled value=<?php echo '"UF ' . number_format($_SESSION['promesaPieSaldo'], 2, ',', '.') . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="promesa_saldoMontoEditable" disabled value=<?php echo '"$ ' . number_format(($_SESSION['promesaPieSaldo']*$_SESSION['promesaUFActual']), 0, '.', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="promesa_saldoPorEditable" disabled value=<?php echo '"' . number_format((($_SESSION['promesaPieSaldo']/$_SESSION['promesaTotalUF'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
  </div>
</div>
<div class="col-lg-6 col-md-6 hidden-sm hidden-xs col-lg-offset-6 col-md-offset-6">
</div>


<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Total:</label>
    <input id="promesa_totalUFEditable" disabled value=<?php echo '"UF ' . number_format($_SESSION['promesaTotalUF'], 2, ',', '.') . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="promesa_totalMonto" disabled value=<?php echo '"$ ' . number_format(($_SESSION['promesaTotalUF']*$_SESSION['promesaUFActual']), 0, '.', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="promesa_totalPor" disabled value=<?php echo '"' . number_format((($_SESSION['promesaTotalUF']/$_SESSION['promesaTotalUF'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
  </div>
</div>

<?php
  if($_SESSION["promesaCodigoProyecto"] == 'COR' && $_SESSION["promesaAccion"] == "Arriendo"){
    echo '<div style="text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <hr style="background-color: #c1c1c1; height: 1px;" />
    </div>
    <div id="tituloGarantiaPromesa" style="text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <font style="font-weight: bold;">Garantía de Arriendo</font>
    </div>
    <div id="datosGarantiaPromesa" class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <label>Valor en UF:</label>
        <input id="valorUFGarantiaPromesa" class="form-control" value="UF 0,00"></input>
      </div>
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <label>Monto $:</label>
        <input id="valorPesoGarantiaPromesa" class="form-control" value="$ 0"></input>
      </div>
    </div>
    <div id="datosDctoGarantiaPromesa" class="col-lg-7 col-md-7 col-sm-12 col-xs-12" >
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <label>Forma pago garantía:</label>
        <select class="form-control custom" type="text" id="garantiaFormaPago">

        </select>
      </div>
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <label>Banco:</label>
        <input id="garantiaBanco" type="text" class="form-control"></input>
      </div>
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <label>Serie/Nro:</label>
        <input id="garantiaSerie" type="text" class="form-control"></input>
      </div>
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <label>Nro Trans/Cheque:</label>
        <input id="garantiaNro" type="text" class="form-control"></input>
      </div>
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <label>Fecha pago:</label>
        <input id="garantiaFechaCheque" type="text" class="form-control"></input>
      </div>
    </div>';
  }else if($_SESSION["promesaCodigoProyecto"] == 'COR' && $_SESSION["promesaAccion"] == "Venta"){
    echo '<div style="text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <hr style="background-color: #c1c1c1; height: 1px;" />
    </div>
    <div id="tituloGarantiaPromesa" style="text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <font style="font-weight: bold;">Garantía de Venta</font>
    </div>
    <div id="tituloGarantiaPromesaDueno" style="text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <font style="font-weight: bold;">Dueño</font>
    </div>
    <div id="datosGarantiaPromesaDueno" class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <label>Valor en UF:</label>
        <input id="valorUFGarantiaPromesaDueno" class="form-control" value="UF 0,00"></input>
      </div>
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <label>Monto $:</label>
        <input id="valorPesoGarantiaPromesaDueno" class="form-control" value="$ 0"></input>
      </div>
    </div>
    <div id="datosDctoGarantiaPromesaDueno" class="col-lg-7 col-md-7 col-sm-12 col-xs-12" >
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <label>Forma pago garantía:</label>
        <select class="form-control custom" type="text" id="garantiaFormaPagoDueno">

        </select>
      </div>
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <label>Banco:</label>
        <input id="garantiaBancoDueno" type="text" class="form-control"></input>
      </div>
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <label>Serie/Nro:</label>
        <input id="garantiaSerieDueno" type="text" class="form-control"></input>
      </div>
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <label>Nro Trans/Cheque:</label>
        <input id="garantiaNroDueno" type="text" class="form-control"></input>
      </div>
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <label>Fecha pago:</label>
        <input id="garantiaFechaChequeDueno" type="text" class="form-control"></input>
      </div>
    </div>
    <div id="tituloGarantiaPromesaCliente" style="text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <font style="font-weight: bold;">Cliente</font>
    </div>
    <div id="datosGarantiaPromesaCliente" class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <label>Valor en UF:</label>
        <input id="valorUFGarantiaPromesaCliente" class="form-control" value="UF 0,00"></input>
      </div>
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <label>Monto $:</label>
        <input id="valorPesoGarantiaPromesaCliente" class="form-control" value="$ 0"></input>
      </div>
    </div>
    <div id="datosDctoGarantiaPromesaCliente" class="col-lg-7 col-md-7 col-sm-12 col-xs-12" >
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <label>Forma pago garantía:</label>
        <select class="form-control custom" type="text" id="garantiaFormaPagoCliente">

        </select>
      </div>
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <label>Banco:</label>
        <input id="garantiaBancoCliente" type="text" class="form-control"></input>
      </div>
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <label>Serie/Nro:</label>
        <input id="garantiaSerieCliente" type="text" class="form-control"></input>
      </div>
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <label>Nro Trans/Cheque:</label>
        <input id="garantiaNroCliente" type="text" class="form-control"></input>
      </div>
      <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <label>Fecha pago:</label>
        <input id="garantiaFechaChequeCliente" type="text" class="form-control"></input>
      </div>
    </div>
    ';
  }
?>

<div style="text-align: left; margin-top: 20pt;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <?php
   echo 'Precios Montos en pesos $CH a UF <font id="valorUFDia">' . number_format($_SESSION['promesaUFActual'], 2, ',', '.') . '</font> corresponden como referencia al valor UF al <font id="fechaDia">' . $_SESSION['promesaFecha'] . "</font>";

  ?>
</div>
<input style="display: none;" id="proyectoCodigoHidden"></input>
<input style="display: none;" id="accionHidden"></input>

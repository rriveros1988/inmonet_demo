<?php
// ini_set('display_errors', 'On');
require '../html2pdf/vendor/autoload.php';
date_default_timezone_set("America/Santiago");
require('../../model/consultas.php');
session_start();

function pago($tasa, $monto, $meses){
  $I = $tasa / 12 / 100 ;
  $I2 = $I + 1 ;
  $I2 = pow($I2,-$meses) ;

  $CuotaMensual = ($I * $monto) / (1 - $I2);

  return $CuotaMensual;
}

if(!array_key_exists('cuotasPromesa', $_SESSION)){
  $cuotasPromesaPre = consultaCuotasPromesadas($_SESSION['promesaCodigoProyecto'],$_SESSION['numeroOperacion']);
  $cuotasPromesa = array();
  $_SESSION['promesaFormaPagoCuota'] = array();
  for($i = 0; $i < count($cuotasPromesaPre); $i++){
    $pos = array(
      "numero_cuota" => $cuotasPromesaPre[$i]['NUMEROCUOTA'],
      "uf_cuota" => $cuotasPromesaPre[$i]['VALORUFCUOTA'],
      "monto_cuota" => $cuotasPromesaPre[$i]['VALORMONTOCUOTA'],
      "forma_cuota" => $cuotasPromesaPre[$i]['FORMAPAGOCUOTA'],
      "banco_cuota" => $cuotasPromesaPre[$i]['BANCOCUOTA'],
      "serie_cuota" => $cuotasPromesaPre[$i]['SERIECHEQUECUOTA'],
      "nro_cuota" => $cuotasPromesaPre[$i]['NROCHEQUECUOTA'],
      "fecha_cuota" => $cuotasPromesaPre[$i]['FECHAPAGOCUOTA']
    );
    array_push($cuotasPromesa,$pos);
    array_push($_SESSION['promesaFormaPagoCuota'], consultaFormaPagoReservaEspecifica($cuotasPromesaPre[$i]['FORMAPAGOCUOTA']));
  }
  $_SESSION['cuotasPromesa'] = $cuotasPromesa;
  $_SESSION['promesaCuotasPie'] = count($cuotasPromesa);
  // echo "<pre>";
  // var_dump($cuotasPromesa);
  // var_dump($cuotasPromesaPre);
  // var_dump($_SESSION['promesaFormaPagoCuota']);
  // echo "</pre>";
}

if($_SESSION['promesaCodigoProyecto'] == 'COR'){
  $formPagoCliente = consultaPagoComisionCliente($_SESSION['promesaCodigoProyecto'],$_SESSION['numeroOperacion']);
  $_SESSION['formaPagoCliente'] = $formPagoCliente;
}


use Spipu\Html2Pdf\Html2Pdf;

ob_start();
/*
if($_SESSION['codProyectoClienteCotizacion'] == "COR"){
  require_once 'cotizacion_plantilla_pdf_corretaje.php';
}
else{
  require_once 'cotizacion_plantilla_pdf.php';
}
*/
require_once 'promesa_plantilla_pdf_ficha.php';

$html = ob_get_clean();

//$document = 'D:/MAMP/htdocs/inmonet';
// $document = '/var/www/html/Git/inmonet';
// $document = '/home/livingne/inmonet.cl/test';
$document = '/home/rriveros/public_html/inmobiliaria';

if(!is_dir("../../repositorio/" . $_SESSION['promesaCodigoProyecto'])){
  mkdir("../../repositorio/" . $_SESSION['promesaCodigoProyecto'], 0777);
  mkdir("../../repositorio/" . $_SESSION['promesaCodigoProyecto'] . "/promesa", 0777);
}
if(!is_dir("../../repositorio/" . $_SESSION['promesaCodigoProyecto'] . "/promesa")){
  mkdir("../../repositorio/" . $_SESSION['promesaCodigoProyecto'] . "/promesa", 0777);
}

$html2pdf = new Html2Pdf('P','LETTER','es','true','UTF-8');
$html2pdf->writeHTML($html);
$html2pdf->output($document . '/repositorio/' . $_SESSION['promesaCodigoProyecto'] . '/promesa/' . $_SESSION['numeroOperacion'] . '_' . $_SESSION['promesaCodigoProyecto'] . '_' . $_SESSION['promesaNumeroDepto'] . '_' . str_replace(' ', '_',$_SESSION['promesaNombreCliente']) . '_' . str_replace(' ', '_',$_SESSION['promesaApellidoCliente']) . '_PR.pdf', 'F');

$_SESSION['FICHA_PDF_ACTUAL'] = '';

if(file_exists($document . '/repositorio/' . $_SESSION['promesaCodigoProyecto'] . '/promesa/' . $_SESSION['numeroOperacion'] . '_' . $_SESSION['promesaCodigoProyecto'] . '_' . $_SESSION['promesaNumeroDepto'] . '_' . str_replace(' ', '_',$_SESSION['promesaNombreCliente']) . '_' . str_replace(' ', '_',$_SESSION['promesaApellidoCliente']) . '_PR.pdf')){
  echo "Ok";
  actualizaPromesaFICHA_PDF($_SESSION['promesaCodigoProyecto'] . '/promesa/' . $_SESSION['numeroOperacion'] . '_' . $_SESSION['promesaCodigoProyecto'] . '_' . $_SESSION['promesaNumeroDepto'] . '_' . str_replace(' ', '_',$_SESSION['promesaNombreCliente']) . '_' . str_replace(' ', '_',$_SESSION['promesaApellidoCliente']) . '_PR.pdf',$_SESSION['idPromesa']);
  $_SESSION['FICHA_PDF_ACTUAL'] = $_SESSION['promesaCodigoProyecto'] . '/promesa/' . $_SESSION['numeroOperacion'] . '_' . $_SESSION['promesaCodigoProyecto'] . '_' . $_SESSION['promesaNumeroDepto'] . '_' . str_replace(' ', '_',$_SESSION['promesaNombreCliente']) . '_' . str_replace(' ', '_',$_SESSION['promesaApellidoCliente']) . '_PR.pdf';
}
else{
  echo "Sin datos";
}
?>

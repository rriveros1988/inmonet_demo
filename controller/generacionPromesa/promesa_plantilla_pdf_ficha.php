<?php
  //ini_set('display_errors', 'On');
  // require('../../model/consultas.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
  <head>
    <style type="text/css">
    #tablaOC{
      width: 100%;
      margin-left: 35px;
      margin-right:35px;
      margin-top: 20px;
    }
    #cabecera1Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height:70px;
    }
    #cabecera1Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height:70px;
    }
    #cabecera2Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height: 45px;
    }
    #cabecera2Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height: 45px;
    }
    #cabecera3Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height: 400px;
    }
    #cabecera3Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height: 400px;
    }
    .headTabla{
      background-color: #e6f2ff;
      border: 1px solid black;
      padding: 2px;
    }
    .bodyTabla{
      border: 1px solid black;
      padding: 1px;
    }

    td {
      padding: 0;
      margin: 0;
    }

    tr {
      padding: 0;
      padding: 0;
    }

    </style>
    <title>Orden de Compra</title>
  </head>
  <body style="font-size: 9px; font-family: Arial">

    <table id="tablaOC">
      <tr>
        <td id="cabecera1Izquierda">
          <img src="../../view/img/logos/living_logo.png" style='height: 60px;'>
        </td>
        <td id="cabecera1Derecha">
          <?php
            echo "<img src='" . $_SESSION['promesaLogoProyecto'] . "' style='height: 60px;'>";
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-top: 20px; margin-left: 40px;">
      <tr>
        <td style="text-align: center; width:720px; font-size: 9px;">
          <b>PROMESA DE <?php if($_SESSION["promesaAccion"] == "Venta"){ //PENDIENTE
            echo "COMPRA";
          }
          else{
            echo "ARRIENDO";
          }
          ?></b>
        </td>
      </tr>
      <tr>
        <td style="text-align: center; width:720px; font-size: 9px;">

        </td>
      </tr>
      <tr>

      </tr>
    </table>
    <table style="margin-top: 20px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 100px;">

        </td>
        <td style="text-align: center; width: 100px;">

        </td>
        <td style="text-align: left; width: 280px;">

        </td>
        <td style="text-align: right; font-size: 9px; width: 100px; padding-bottom: 5px; padding-top: 5px;">

        </td>
        <td style="text-align: right; font-size: 9px; width: 100px; padding-bottom: 5px; padding-top: 5px;">
          <?php echo $_SESSION['promesaFecha']; ?>
          <br/>
          Numero Operación: <b><?php echo $_SESSION['numeroOperacion']; ?></b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 20px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 720px; font-size: 9px;">
          <b>1) Individualización del Oferente Comprador.</b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px; font-size: 9px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Nombre
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['promesaNombreCliente']) . ' ' . strtoupper($_SESSION['promesaApellidoCliente']);
          ?>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Cedula de Identidad
        </td>
        <td style="text-align: left; width: 122px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['promesaRutCliente']);
          ?>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Nacionalidad
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['promesaNacionalidadCliente1']);
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px; font-size: 9px;">
          Estado Civil
        </td>
        <td style="text-align: left; width: 100px; height: 10px; padding-left: 5px; font-size: 9px;">
          <?php
            echo strtoupper($_SESSION['promesaEstadoCivilIDCliente1']);
          ?>
        </td>
        <td style="text-align: left; width: 490px; height: 10px;font-size:8px;">
          1. Soltero | 2. Separado | 3.Viudo | 4. Cas. Soc. Conyugal | 5. Cas. Sep. Bienes | 6. Cas. Part. Ganancias | 7. Divorciado
        </td>
      </tr>
    </table>
    <table style="margin-left: 40px; font-size: 9px;">
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Fecha Nacimiento
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            $fechaNac = new Datetime($_SESSION['promesaFechaNacCliente1']);
            echo $fechaNac->format('d/m/Y');
          ?>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Profesión/Actividad
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['promesaProfesionCliente1']);
          ?>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Domicilio
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['promesaDomicilioCliente1']);
          ?>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Comuna
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['promesaComunaCliente1']);
          ?>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Ciudad
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['promesaCiudadCliente1']);
          ?>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Región
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['promesaRegionCliente1']);
          ?>
        </td>
      </tr>
      <tr>
        <td style="height: 10px;">
          &nbsp;
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;margin-top:20px;">
          Teléfonos
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['promesaTelefonoCliente1']);
          ?>
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 120px; height: 10px;">
          Email:
        </td>
        <td style="text-align: left; width: 600px; height: 10px; padding-left: 5px;">
          <?php
            echo strtoupper($_SESSION['promesaMailCliente1']);
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-top: 20px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 720px; font-size: 9px;">
          <b>2) Individualización de las Unidades</b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px;">
        <?php
                if($_SESSION['promesaCodigoProyecto'] != "COR"){
                  if($_SESSION['bodegasClientePromesa'] != '' && $_SESSION['estacionamientosClientePromesa'] != ''){
                    for($i = 0; $i < (count($_SESSION['bodegasClientePromesa']) + count($_SESSION['estacionamientosClientePromesa'])); $i++){
                      if ($i == 0 ) {
                        echo '<tr style="margin-left:10px;">
                      <td style="text-align: left; width: 130px; font-size: 9px;">
                        ' . strtoupper($_SESSION['tipoUnidad']) . ': <b>' .  $_SESSION['promesaNumeroDepto'] .
                      '</b></td>'.
                      '<td style="text-align: left; width: 100px; font-size: 9px;">BOD' . ($i+1) . ': <b>' .
                      consultaUnidadEspecificaReserva($_SESSION['bodegasClientePromesa'][$i])[0]['CODIGO'].
                      '</b></td>';
                      }else{
                        if ($i >= count($_SESSION['bodegasClientePromesa'])) {
                          if ($i % 6 == 0) {
                            echo '</tr><tr style="margin-left:10px;"><td style="text-align: left; width: 100px; font-size: 9px;">ESTAC'.($i+1-count($_SESSION['bodegasClientePromesa'])).': <b>'.consultaUnidadEspecificaReserva($_SESSION['estacionamientosClientePromesa'][$i-count($_SESSION['bodegasClientePromesa'])])[0]['CODIGO'].'</b></td>';
                           }else{
                            echo '<td style="text-align: left; width: 100px; font-size: 9px;">ESTAC'.($i+1-count($_SESSION['bodegasClientePromesa'])).': <b>'.consultaUnidadEspecificaReserva($_SESSION['estacionamientosClientePromesa'][$i-count($_SESSION['bodegasClientePromesa'])])[0]['CODIGO'].'</b></td>';
                            }
                          }else{
                            if ($i % 6 == 0) {
                            echo '</tr><tr style="margin-left:10px;"><td style="text-align: left; width: 100px; font-size: 9px;">BOD' . ($i+1) .': <b>' .consultaUnidadEspecificaReserva($_SESSION['bodegasClientePromesa'][$i])[0]['CODIGO'].'</b></td>';
                            }else{
                              echo '<td style="text-align: left; width: 100px; font-size: 9px;">BOD' . ($i+1) .': <b>' .consultaUnidadEspecificaReserva($_SESSION['bodegasClientePromesa'][$i])[0]['CODIGO'].'</b></td>';
                            }
                          }
                      }

                      if($i == (count($_SESSION['bodegasClientePromesa']) + count($_SESSION['estacionamientosClientePromesa'])-1)){
                        echo '</tr>';
                      }
                    }
                  }
                  else if($_SESSION['bodegasClientePromesa'] == ''){
                    for($i = 0; $i < count($_SESSION['estacionamientosClientePromesa']); $i++){
                      if ($i == 0 ) {
                        echo '<tr style="margin-left:10px;">
                      <td style="text-align: left; width: 130px; font-size: 9px;">
                        ' . strtoupper($_SESSION['tipoUnidad']) . ': <b>' .  $_SESSION['promesaNumeroDepto'] .
                      '</b></td>'.
                      '<td style="text-align: left; width: 100px; font-size: 9px;">ESTAC' . ($i+1) . ': <b>' .
                      consultaUnidadEspecificaReserva($_SESSION['estacionamientosClientePromesa'][$i])[0]['CODIGO'].
                      '</b></td>';
                      }else{
                          if ($i % 6 == 0) {
                            echo '</tr><tr style="margin-left:10px;"><td style="text-align: left; width: 100px; font-size: 9px;">ESTAC'.($i+1).': <b>'.consultaUnidadEspecificaReserva($_SESSION['estacionamientosClientePromesa'][$i])[0]['CODIGO'].'</b></td>';
                           }else{
                            echo '<td style="text-align: left; width: 100px; font-size: 9px;">ESTAC'.($i+1).': <b>'.consultaUnidadEspecificaReserva($_SESSION['estacionamientosClientePromesa'][$i])[0]['CODIGO'].'</b></td>';
                            }
                      }

                      if($i == (count($_SESSION['estacionamientosClientePromesa'])-1)){
                        echo '</tr>';
                      }
                    }
                  }
                  else if($_SESSION['estacionamientosClientePromesa'] == ''){
                    for($i = 0; $i < count($_SESSION['bodegasClientePromesa']); $i++){
                      if ($i == 0 ) {
                        echo '<tr style="margin-left:10px;">
                      <td style="text-align: left; width: 130px; font-size: 9px;">
                        ' . strtoupper($_SESSION['tipoUnidad']) . ': <b>' .  $_SESSION['promesaNumeroDepto'] .
                      '</b></td>'.
                      '<td style="text-align: left; width: 100px; font-size: 9px;">BOD' . ($i+1) . ': <b>' .
                      consultaUnidadEspecificaReserva($_SESSION['bodegasClientePromesa'][$i])[0]['CODIGO'].
                      '</b></td>';
                      }else{
                            if ($i % 6 == 0) {
                            echo '</tr><tr style="margin-left:10px;"><td style="text-align: left; width: 100px; font-size: 9px;">BOD' . ($i+1) .': <b>' .consultaUnidadEspecificaReserva($_SESSION['bodegasClientePromesa'][$i])[0]['CODIGO'].'</b></td>';
                            }else{
                              echo '<td style="text-align: left; width: 100px; font-size: 9px;">BOD' . ($i+1) .': <b>' .consultaUnidadEspecificaReserva($_SESSION['bodegasClientePromesa'][$i])[0]['CODIGO'].'</b></td>';
                            }
                      }
                      if($i == (count($_SESSION['bodegasClientePromesa'])-1)){
                        echo '</tr>';
                      }
                    }
                  }
                }
                if($_SESSION['promesaCodigoProyecto'] == "COR"){
                  $datosUnidadCor = datosUnidadCorretaje($_SESSION['promesaCodigoProyecto'], $_SESSION['promesaNumeroDepto'], $_SESSION['promesaAccion']);
                  echo '<tr style="margin-left:10px;">
                  <td style="text-align: left; width: 600px; font-size: 9px;">
                  ' . $_SESSION['tipoUnidad'] . ': <b>' .  $_SESSION['promesaNumeroDepto'] . '</b> - Dirección: ' . $datosUnidadCor['DIRECCION'] .
                  '</td></tr>';
                }
        ?>
    </table>
    <table style="margin-top: 10px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 720px; font-size: 9px;">
          <b>Precio Total de la Compraventa UF: <?php echo str_replace('.',',',$_SESSION['promesaTotalUF']); ?></b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 20px; margin-left: 40px;">
      <tr>
        <td style="text-align: left; width: 720px; font-size: 9px;">
          <b>3) Forma de pago</b>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px; font-size: 9px;">
      <tr>
        <td style="text-align: left; width: 100px;">
          <b>a)</b> Con la suma de
        </td>
        <td style="text-align: left; width: 60px;">
          <b>UF <?php echo str_replace('.',',',$_SESSION['promesaReservaUF']);?></b>
        </td>
        <td style="text-align: left; width: 540px;">
          que se pagaron como señal de seriedad de la respectiva oferta y reserva.
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px; font-size: 9px;">
      <tr>
        <td style="text-align: left; width: 100px;">
          <b>b)</b> Con la suma de
        </td>
        <td style="text-align: left; width: 60px;">
          <b>UF <?php echo str_replace('.',',',$_SESSION['promesaPiePromesaUF']);?></b>
        </td>
        <td style="text-align: left; width: 135px;">
          equivalentes a esta fecha
        </td>
        <td style="text-align: left; width: 80px;">
          <b>$ <?php echo number_format($_SESSION['valorPiePromesa'], 0, '.', '.'); ?></b>
        </td>
        <td style="text-align: left; width: 325px;">
          <?php
            if(number_format($_SESSION['valorPiePromesa'], 0, '.', '.') != '0'){
              echo "que el Prominente Comprador paga en este acto mediante";
            }
            else{
              echo "que el Prominente Comprador paga en este acto.";
            }
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-top: 0px; margin-left: 40px; font-size: 9px;">
      <tr>
        <td style="width: 720px;padding-left: 10px;">
          <?php
            if(number_format($_SESSION['valorPiePromesa'], 0, '.', '.') != '0'){
              echo "<b>" . $_SESSION['promesaFormaPagoPromesa'][0]['NOMBRE'] . "</b>, del Banco/Emisor <b>" .  $_SESSION['promesaBancoPromesa'] . "</b>, Nro de Serie <b>" . $_SESSION['promesaSerieChequePromesa'] . "</b>, Nro. Cheque <b>" . $_SESSION['promesaNroChequePromesa'] . "</b> que el Prominente Comprador declara recibir.";
            }
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-top: 10px; margin-left: 40px; font-size: 9px;">
      <tr>
        <td style="text-align: left; width: 100px;">
          <b>c)</b> Con la suma de
        </td>
        <td style="text-align: left; width: 60px;">
          <b>UF <?php echo str_replace('.',',',$_SESSION['promesaPieCuotas']);?></b>
        </td>
        <td style="text-align: left; width: 135px;">
          equivalentes a esta fecha
        </td>
        <td style="text-align: left; width: 80px;">
          <b>$ <?php echo number_format($_SESSION['valorPieSaldo'], 0, '.', '.'); ?></b>
        </td>
        <td style="text-align: left; width: 325px;">
          <?php
            if(array_key_exists('cuotasPromesa', $_SESSION) && count($_SESSION['cuotasPromesa']) > 0){
              echo "que el Prominente Comprador documenta en este acto";
            }
            else{
              echo "que el Prominente Comprador documenta en este acto.";
            }
          ?>
        </td>
      </tr>
    </table>
    <table style="margin-top: 0px; margin-left: 40px; font-size: 9px;">
      <tr>
        <?php
          if(array_key_exists('cuotasPromesa', $_SESSION) && count($_SESSION['cuotasPromesa']) > 0){
            echo '<td style="width: 720px; line-height: 15px; padding-left: 10px;">';
            echo 'en <b>' . $_SESSION['promesaCuotasPie'] . ' cuotas</b>, todas ellas mensuales venciendo la primera de ellas el día ';
            $fechaCuota0 = new Datetime($_SESSION['cuotasPromesa'][0]['fecha_cuota']);
            echo '<b>' . $fechaCuota0->format('d-m-Y') . '</b>.';
            echo '</td>';
          }
        ?>
      </tr>
    </table>

      <?php
        if(array_key_exists('cuotasPromesa', $_SESSION) && count($_SESSION['cuotasPromesa']) > 0){
          for($c = 0; $c < count($_SESSION['cuotasPromesa']); $c++){
            echo '<table style="margin-top: 10px; margin-left: 60px; font-size: 9px;"><tr>';
            $fechaCuotaX = new Datetime($_SESSION['cuotasPromesa'][$c]['fecha_cuota']);
            echo '<td style="width: 690px;"><b>'.$_SESSION['cuotasPromesa'][$c]['numero_cuota'].') '.$_SESSION['promesaFormaPagoCuota'][$c][0]['NOMBRE'].'</b>, del Banco/Emisor <b>'.$_SESSION['cuotasPromesa'][$c]['banco_cuota'].'</b>, Nro de Serie <b>'.$_SESSION['cuotasPromesa'][$c]['serie_cuota'].'</b>, Nro. Cheque <b>'.$_SESSION['cuotasPromesa'][$c]['nro_cuota'].'</b> por la suma de $<b> '.number_format($_SESSION['cuotasPromesa'][$c]['monto_cuota'], 0, '.', '.').'</b>, correspondiente a <b>UF '.str_replace('.',',',$_SESSION['cuotasPromesa'][$c]['uf_cuota']).'</b> con Fecha de Pago <b>'.$fechaCuotaX->format('d-m-Y').'</b></td>';
            echo "</tr></table>";
          }
        }
      ?>
    <table style="margin-top:10px; margin-left:40px; font-size: 9px;">
      <tr>
        <td>
        &nbsp;
        </td>
      </tr>
      <tr>
        <td style="text-align: left; width: 720px;">
          <b>d)</b> Con la suma de <b>UF <?php echo str_replace('.',',',$_SESSION['promesaSaldoTotalUF']);?></b> que se obliga a pagar con recursos propios, o con el dinero que obtenga en préstamo de una Institucion Financiera.
        </td>
      </tr>
    </table>
    <?php if ($_SESSION['promesaCodigoProyecto'] == "COR") {
      if($_SESSION['estadoPromesa'] == '3'){
        if($_SESSION['formaPagoCliente'][0]['MONTOPAGOCOM'] == '0'){
          echo
          '<table style="margin-top:10px; margin-left:40px; font-size: 9px;">'.
            '<tr>'.
              '<td>'.
              '&nbsp;'.
              '</td>'.
            '</tr>'.
            '<tr>'.
              '<td style="text-align: left; width: 720px;">'.
                // '<b>e)</b> Con la suma de UF ' . $_SESSION['MONTOUFCOMISION'] . ' equivalentes a esta fecha ' . $_SESSION['MONTOPESOCOMISION'] . ' que el Prominente Cliente declara pagar en este acto mediante ' . $_SESSION['FORMAPAGOCOMISION'] . ', del Banco/Emisor ' . $_SESSION['BANCOCOMISION'] . ', Nro de Serie ' . $_SESSION['SERIECOMISION'] . ', Nro. Cheque ' . $_SESSION['NROCOMISION'] . ' con Fecha de Pago ' . $_SESSION['FECHAPAGOCOMISION'] .'.'.
                '<b>e)</b> Con la suma de <b>UF ' . $_SESSION['MONTOUFCOMISION'] . '</b> correspondiente a comisión que se obliga a pagar con recursos propios, o con el dinero que obtenga en préstamo de una Institucion Financiera.' .
              '</td>'.
            '</tr>'.
          '</table>';
        }
        else{
          $fechaC = new Datetime($_SESSION['formaPagoCliente'][0]['FECHAPAGOCOMISION']);
          echo
          '<table style="margin-top:10px; margin-left:40px; font-size: 9px;">'.
            '<tr>'.
              '<td>'.
              '&nbsp;'.
              '</td>'.
            '</tr>'.
            '<tr>'.
              '<td style="text-align: left; width: 720px;">'.
                // '<b>e)</b> Con la suma de UF ' . $_SESSION['MONTOUFCOMISION'] . ' equivalentes a esta fecha ' . $_SESSION['MONTOPESOCOMISION'] . ' que el Prominente Cliente declara pagar en este acto mediante ' . $_SESSION['FORMAPAGOCOMISION'] . ', del Banco/Emisor ' . $_SESSION['BANCOCOMISION'] . ', Nro de Serie ' . $_SESSION['SERIECOMISION'] . ', Nro. Cheque ' . $_SESSION['NROCOMISION'] . ' con Fecha de Pago ' . $_SESSION['FECHAPAGOCOMISION'] .'.'.
                '<b>e)</b> Con la suma de <b>UF ' . $_SESSION['MONTOUFCOMISION'] . '</b> equivalentes a <b>$ ' . number_format($_SESSION['formaPagoCliente'][0]['MONTOPAGOCOM'], 0, '.', '.') . '</b> correspondientes a pago de comision que el Prominente Comprador documenta en esta acto mediante <b>' . $_SESSION['formaPagoCliente'][0]['FORMAPAGO'] . '</b> del Banco/Emisor <b>' . $_SESSION['formaPagoCliente'][0]['BANCOPROMESA'] .  '</b> Nro. serie <b>' . $_SESSION['formaPagoCliente'][0]['SERIECHEQUEPROMESA'] . '</b> Nro. cheque <b>' . $_SESSION['formaPagoCliente'][0]['NROCHEQUEPROMESA'] . '</b> con fecha <b>' . $fechaC->format('d-m-Y')  . '</b>.' .
              '</td>'.
            '</tr>'.
          '</table>';
        }
      }
    } ?>
    <table style="margin-top: 50px; margin-left: 120px;">
      <tr>
        <td style="width: 150px; text-align: center;">

        </td>
        <td style="width: 240px;">

        </td>
        <td style="width: 150px; text-align: center;">
          <hr style="height: 1px;"/>
            P/Vendedor<br>
            <?php
              $user = consultaUsuarioId($_SESSION['idUsuario']);
              echo $user[0]['NOMBRE'] . '<br/>' . $user[0]['RUT'];
            ?>
        </td>
      </tr>
    </table>
  </body>
</html>

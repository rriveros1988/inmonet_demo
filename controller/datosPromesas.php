<?php
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');
	session_start();

	if(count($_POST) >= 0){
    	$row = '';
			if(count($_POST) == 0){
				if($_SESSION['idperfil'] == 1 || $_SESSION['idperfil'] == 2){
					$row = consultaPromesas();
				}
				else if($_SESSION['idperfil'] == 4){
					$row = consultaPromesasCli($_SESSION['rutUser']);
				}
				else{
					$row = consultaPromesasUsu($_SESSION['rutUser']);
				}
			}
			else{
				if($_SESSION['idperfil'] == 1 || $_SESSION['idperfil'] == 2){
					if($_POST['codigoProyecto'] == "todos"){
							$row = consultaPromesas();
					}
					else{
							$row = consultaPromesasPro($_POST['codigoProyecto']);
					}
				}
				else if($_SESSION['idperfil'] == 4){
					if($_POST['codigoProyecto'] == "todos"){
							$row = consultaPromesasCli($_SESSION['rutUser']);
					}
					else{
							$row = consultaPromesasCliPro($_SESSION['rutUser'],$_POST['codigoProyecto']);
					}
				}
				else{
					if($_POST['codigoProyecto'] == "todos"){
							$row = consultaPromesasUsu($_SESSION['rutUser']);
					}
					else{
							$row = consultaPromesasUsuPro($_SESSION['rutUser'],$_POST['codigoProyecto']);
					}
				}
			}

      if(is_array($row))
      {
				$results = array(
						"sEcho" => 1,
						"iTotalRecords" => count($row),
						"iTotalDisplayRecords" => count($row),
						"aaData"=>$row
				);

				echo json_encode($results);
      }
      else{
				$results = array(
            "sEcho" => 1,
            "iTotalRecords" => 0,
            "iTotalDisplayRecords" => 0,
            "aaData"=>[]
        );
        echo json_encode($results);
      }
	}
	else{
		echo "Sin datos";
	}
?>

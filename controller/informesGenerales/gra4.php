<?php
// ini_set('display_errors', 'On');
require('../src/jpgraph.php');
require('../src/jpgraph_bar.php');
require('../../model/consultas.php');
date_default_timezone_set('America/Santiago');
session_start();

$ano = $_GET['ano'];
$mes = $_GET['mes'];
$codigoProyecto = $_GET['codigoProyecto'];
$ciclo = $_GET['ciclo'];
$accion = $_GET['accion'];
$datosInforme1NivelInteres = datosInforme1NivelInteres($accion, $ano, $mes, $codigoProyecto);
$datosInforme3 = datosInforme3($accion, $mes, $ano,$codigoProyecto);

$datos = array();
$labels = array();
$tam = 0;

$topeInferior = 29*($ciclo-1);
$topeSuperior = 29*($ciclo);
if($topeSuperior > count($datosInforme3)){
	$topeSuperior = count($datosInforme3);
}

for($i = $topeInferior; $i < $topeSuperior; $i++){
	$labels[] = $datosInforme3[$i]['CODIGO'];
	$datos[] = ($datosInforme3[$i][1]+$datosInforme3[$i][2]+$datosInforme3[$i][3]);

}

for($i = 0; $i < count($datosInforme3); $i++){
	if($tam < ($datosInforme3[$i][1]+$datosInforme3[$i][2]+$datosInforme3[$i][3])){
		$tam = ($datosInforme3[$i][1]+$datosInforme3[$i][2]+$datosInforme3[$i][3]);
	}
}

$alto = (816/38)*($topeSuperior-$topeInferior);

$grafico = new Graph(470, $alto);
$grafico->SetScale("textint",0,$tam+20);
// $grafico->title->SetFont(FF_ARIAL,FS_BOLD,12);
// $grafico->title->Set('Altas por agencia');
$grafico->xaxis->SetTickLabels($labels);
$grafico->xaxis->SetLabelAngle(0);
$grafico->xaxis->SetLabelMargin(10);
$grafico->yaxis->SetLabelMargin(10);
$grafico->Set90AndMargin(60,40,25,1);
$grafico->SetShadow();
// $grafico->xaxis->SetFont(FF_ARIAL,FS_BOLD,10);
// $grafico->yaxis->SetFont(FF_ARIAL,FS_BOLD,10);
$grafico->yaxis->SetWeight(2);
$grafico->xaxis->SetWeight(2);
$barplot1 = new barPlot($datos);

// Setup the values that are displayed on top of each bar
$barplot1->value->Show();

// Must use TTF fonts if we want text at an arbitrary angle
// $barplot1->value->SetFont(FF_ARIAL,FS_BOLD);
$barplot1->value->SetAngle(45);

// Black color for positive values and darkred for negative values
$barplot1->value->SetColor("#585858","#585858");

// Un gradiente Horizontal de morados
$barplot1->SetFillGradient("#585858", "#585858", GRAD_HOR);

// $barplot1->SetColor("black");

// 30 pixeles de ancho para cada barra
$barplot1->SetWidth(25);

// Join them in an accumulated (stacked) plot
$accbplot = new AccBarPlot(array($barplot1));
// $grafico->xaxis->SetFont(FF_ARIAL	,FS_NORMAL,9);

$grafico->Add($accbplot);

$barplot1->SetFillColor("#585858");

// //Show numero
// $barplot1->value->SetFormat('%d');
// $barplot1->value->Show();
// $barplot1->value->SetColor('white');
//
// $barplot2->value->SetFormat('%d');
// $barplot2->value->Show();
// $barplot2->value->SetColor('black');
//
// $barplot3->value->SetFormat('%d');
// $barplot3->value->Show();
// $barplot3->value->SetColor('white');

$grafico->Stroke();

?>

<?php
// ini_set('display_errors', 'On');
require('../src/jpgraph.php');
require('../src/jpgraph_bar.php');
require('../../model/consultas.php');
date_default_timezone_set('America/Santiago');
session_start();

$mes = $_GET['mes'];
$ano = $_GET['ano'];
$codigoProyecto = $_GET['codigoProyecto'];
$accion = $_GET['accion'];
$datosProyecto = consultaDatosProyecto($codigoProyecto);
$datosInforme2 = datosInforme2($accion, $mes, $ano,$codigoProyecto);


$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

$mesesCortos = array("ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic");

$fechasRango = array();
$fechasRangoIndex = array();

if($mes == 12){
	$fechaMax = new Datetime(($ano + 1) . '-' . '01-01');
}
else{
	$fechaMax = new Datetime($ano . '-' . ($mes + 1) . '-01');
}
for($i = 0; $i < 12; $i++){
	$f = strtotime(($i - 12) . 'month', strtotime($fechaMax->format('y-m-d')));
	$m =  date('m',$f);
	$y =  date('y',$f);
	$mText = $mesesCortos[$m-1];
	$fFinal = $mText . "-" . $y;
	$fechasRango[] = $fFinal;
	$fechasRangoIndex[] = $y . '_' . $m;
}

$datos = array(0,0,0,0,0,0,0,0,0,0,0,0);
$datos2 = array(0,0,0,0,0,0,0,0,0,0,0,0);
$datos3 = array(0,0,0,0,0,0,0,0,0,0,0,0);
$datos4 = array(0,0,0,0,0,0,0,0,0,0,0,0);
$datos5 = array(0,0,0,0,0,0,0,0,0,0,0,0);
$datos6 = array(0,0,0,0,0,0,0,0,0,0,0,0);
$datos7 = array(0,0,0,0,0,0,0,0,0,0,0,0);
$datos8 = array(0,0,0,0,0,0,0,0,0,0,0,0);
$datos9 = array(0,0,0,0,0,0,0,0,0,0,0,0);
$labels = $fechasRango;
$tam = 0;

for($i = 1; $i < 13; $i++){
	if(array_key_exists($fechasRangoIndex[$i-1],$datosInforme2[0])){
		$datos[$i-1] = $datosInforme2[0][$fechasRangoIndex[$i-1]];
	}
}

for($i = 1; $i < 13; $i++){
	if(array_key_exists($fechasRangoIndex[$i-1],$datosInforme2[1])){
		$datos2[$i-1] = $datosInforme2[1][$fechasRangoIndex[$i-1]];
	}
}

for($i = 1; $i < 13; $i++){
	if(array_key_exists($fechasRangoIndex[$i-1],$datosInforme2[2])){
		$datos3[$i-1] = $datosInforme2[2][$fechasRangoIndex[$i-1]];
	}
}

for($i = 1; $i < 13; $i++){
	if(array_key_exists($fechasRangoIndex[$i-1],$datosInforme2[3])){
		$datos4[$i-1] = $datosInforme2[3][$fechasRangoIndex[$i-1]];
	}
}

for($i = 1; $i < 13; $i++){
	if(array_key_exists($fechasRangoIndex[$i-1],$datosInforme2[4])){
		$datos5[$i-1] = $datosInforme2[4][$fechasRangoIndex[$i-1]];
	}
}

for($i = 1; $i < 13; $i++){
	if(array_key_exists($fechasRangoIndex[$i-1],$datosInforme2[5])){
		$datos6[$i-1] = $datosInforme2[5][$fechasRangoIndex[$i-1]];
	}
}

for($i = 1; $i < 13; $i++){
	if(array_key_exists($fechasRangoIndex[$i-1],$datosInforme2[6])){
		$datos7[$i-1] = $datosInforme2[6][$fechasRangoIndex[$i-1]];
	}
}

for($i = 1; $i < 13; $i++){
	if(array_key_exists($fechasRangoIndex[$i-1],$datosInforme2[7])){
		$datos8[$i-1] = $datosInforme2[7][$fechasRangoIndex[$i-1]];
	}
}

for($i = 1; $i < 13; $i++){
	if(array_key_exists($fechasRangoIndex[$i-1],$datosInforme2[8])){
		$datos9[$i-1] = $datosInforme2[8][$fechasRangoIndex[$i-1]];
	}
}

for($i = 0; $i < count($datosInforme2); $i++){
  for($j = 1; $j < count($datosInforme2[$i]); $j++){
		if($tam < $datosInforme2[$i][$j]){
			$tam = $datosInforme2[$i][$j];
		}
	}
}

$grafico = new Graph(800, 260, 'auto');
$grafico->img->SetMargin(40,10,10,200);
$grafico->SetScale("textint",0,$tam+30);
// $grafico->title->SetFont(FF_ARIAL,FS_BOLD,12);
// $grafico->title->Set('Altas por agencia');
$grafico->xaxis->SetTickLabels($labels);
$grafico->xaxis->SetLabelAngle(0);
// $grafico->xaxis->SetFont(FF_ARIAL,FS_BOLD,10);
// $grafico->yaxis->SetFont(FF_ARIAL,FS_BOLD,10);
$grafico->yaxis->SetWeight(2);
$grafico->xaxis->SetWeight(2);

$barplot1 = new barPlot($datos);

// Setup the values that are displayed on top of each bar
$barplot1->value->Show();

// Must use TTF fonts if we want text at an arbitrary angle
// $barplot1->value->SetFont(FF_ARIAL,FS_BOLD);
$barplot1->value->SetAngle(45);

// Black color for positive values and darkred for negative values
$barplot1->value->SetColor("#ff3383","#ff3383");

// Un gradiente Horizontal de morados
$barplot1->SetFillGradient("#ff3383", "#ff3383", GRAD_HOR);

// $barplot1->SetColor("black");

// 30 pixeles de ancho para cada barra
$barplot1->SetWidth(5);

//b2
$barplot2 = new barPlot($datos2);

// Setup the values that are displayed on top of each bar
$barplot2->value->Show();

// Must use TTF fonts if we want text at an arbitrary angle
// $barplot1->value->SetFont(FF_ARIAL,FS_BOLD);
$barplot2->value->SetAngle(45);

// Black color for positive values and darkred for negative values
$barplot2->value->SetColor("#33daff","#33daff");

// Un gradiente Horizontal de morados
$barplot2->SetFillGradient("#33daff", "#33daff", GRAD_HOR);

// $barplot2->SetColor("black");

// 30 pixeles de ancho para cada barra
$barplot2->SetWidth(5);

//b3
$barplot3 = new barPlot($datos3);

// Setup the values that are displayed on top of each bar
$barplot3->value->Show();

// Must use TTF fonts if we want text at an arbitrary angle
// $barpot1->value->SetFont(FF_ARIAL,FS_BOLD);
$barplot3->value->SetAngle(45);

// Black color for positive values and darkred for negative values
$barplot3->value->SetColor("#ffda33","#ffda33");

// $barplot3->SetColor("black");

// Un gradiente Horizontal de morados
$barplot3->SetFillGradient("#ffda33", "#ffda33", GRAD_HOR);
// 30 pixeles de ancho para cada barra
$barplot3->SetWidth(5);

//b4
$barplot4 = new barPlot($datos4);

// Setup the values that are displayed on top of each bar
$barplot4->value->Show();

// Must use TTF fonts if we want text at an arbitrary angle
// $barpot1->value->SetFont(FF_ARIAL,FS_BOLD);
$barplot4->value->SetAngle(45);

// Black color for positive values and darkred for negative values
$barplot4->value->SetColor("#8d33ff","#8d33ff");

// $barplot3->SetColor("black");

// Un gradiente Horizontal de morados
$barplot4->SetFillGradient("#8d33ff", "#8d33ff", GRAD_HOR);
// 30 pixeles de ancho para cada barra
$barplot4->SetWidth(5);

//b5
$barplot5 = new barPlot($datos5);

// Setup the values that are displayed on top of each bar
$barplot5->value->Show();

// Must use TTF fonts if we want text at an arbitrary angle
// $barpot1->value->SetFont(FF_ARIAL,FS_BOLD);
$barplot5->value->SetAngle(45);

// Black color for positive values and darkred for negative values
$barplot5->value->SetColor("#00700e","#00700e");

// $barplot5->SetColor("black");

// Un gradiente Horizontal de morados
$barplot5->SetFillGradient("#00700e", "#00700e", GRAD_HOR);
// 30 pixeles de ancho para cada barra
$barplot5->SetWidth(5);

//b6
$barplot6 = new barPlot($datos6);

// Setup the values that are displayed on top of each bar
$barplot6->value->Show();

// Must use TTF fonts if we want text at an arbitrary angle
// $barpot1->value->SetFont(FF_ARIAL,FS_BOLD);
$barplot6->value->SetAngle(45);

// Black color for positive values and darkred for negative values
$barplot6->value->SetColor("#e30000","#e30000");

// $barplot6->SetColor("black");

// Un gradiente Horizontal de morados
$barplot6->SetFillGradient("#e30000", "#e30000", GRAD_HOR);
// 30 pixeles de ancho para cada barra
$barplot6->SetWidth(5);

//b7
$barplot7 = new barPlot($datos7);

// Setup the values that are displayed on top of each bar
$barplot7->value->Show();

// Must use TTF fonts if we want text at an arbitrary angle
// $barpot1->value->SetFont(FF_ARIAL,FS_BOLD);
$barplot7->value->SetAngle(45);

// Black color for positive values and darkred for negative values
$barplot7->value->SetColor("#88a4ff","#88a4ff");

// $barplot7->SetColor("black");

// Un gradiente Horizontal de morados
$barplot7->SetFillGradient("#88a4ff", "#88a4ff", GRAD_HOR);
// 30 pixeles de ancho para cada barra
$barplot7->SetWidth(5);

//b8
$barplot8 = new barPlot($datos8);

// Setup the values that are displayed on top of each bar
$barplot8->value->Show();

// Must use TTF fonts if we want text at an arbitrary angle
// $barpot1->value->SetFont(FF_ARIAL,FS_BOLD);
$barplot8->value->SetAngle(45);

// Black color for positive values and darkred for negative values
$barplot8->value->SetColor("#2E2E2E","#2E2E2E");

// $barplot8->SetColor("black");

// Un gradiente Horizontal de morados
$barplot8->SetFillGradient("#2E2E2E", "#2E2E2E", GRAD_HOR);
// 30 pixeles de ancho para cada barra
$barplot8->SetWidth(5);

//b9
$barplot9 = new barPlot($datos9);

// Setup the values that are displayed on top of each bar
$barplot9->value->Show();

// Must use TTF fonts if we want text at an arbitrary angle
// $barpot1->value->SetFont(FF_ARIAL,FS_BOLD);
$barplot9->value->SetAngle(45);

// Black color for positive values and darkred for negative values
$barplot9->value->SetColor("#151515","#151515");

// $barplot9->SetColor("black");

// Un gradiente Horizontal de morados
$barplot9->SetFillGradient("#151515", "#151515", GRAD_HOR);
// 30 pixeles de ancho para cada barra
$barplot9->SetWidth(5);

// Join them in an accumulated (stacked) plot
$accbplot = new  GroupBarPlot(array($barplot1,$barplot2,$barplot3,$barplot4,$barplot5,$barplot6,$barplot7,$barplot8,$barplot9));
$grafico->legend->SetFrameWeight(1);
$grafico->legend->SetColumns(9);
$grafico->legend->SetPos(0.5,0.98,'center','bottom');
$grafico->legend->SetColor('#202020','#636363');

$grafico->Add($accbplot);

$barplot1->SetFillColor("#ff3383");
$barplot1->SetLegend($datosInforme2[0][0]);

$barplot2->SetFillColor("#33daff");
$barplot2->SetLegend($datosInforme2[1][0]);

$barplot3->SetFillColor("#ffda33");
$barplot3->SetLegend($datosInforme2[2][0]);

$barplot4->SetFillColor("#8d33ff");
$barplot4->SetLegend($datosInforme2[3][0]);

$barplot5->SetFillColor("#00700e");
$barplot5->SetLegend($datosInforme2[4][0]);

$barplot6->SetFillColor("#e30000");
$barplot6->SetLegend($datosInforme2[5][0]);

$barplot7->SetFillColor("#88a4ff");
$barplot7->SetLegend($datosInforme2[6][0]);

$barplot8->SetFillColor("#2E2E2E");
$barplot8->SetLegend($datosInforme2[7][0]);

$barplot9->SetFillColor("#151515");
$barplot9->SetLegend($datosInforme2[8][0]);
// //Show numero
// $barplot1->value->SetFormat('%d');
// $barplot1->value->Show();
// $barplot1->value->SetColor('white');
//
// $barplot2->value->SetFormat('%d');
// $barplot2->value->Show();
// $barplot2->value->SetColor('black');
//
// $barplot3->value->SetFormat('%d');
// $barplot3->value->Show();
// $barplot3->value->SetColor('white');

$grafico->Stroke();

?>

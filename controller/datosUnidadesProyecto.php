<?php
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');
	session_start();

	if(count($_POST) >= 0){
			$codigoProyecto = $_POST['codigoProyecto'];
			if($codigoProyecto != ''){
				$row = consultaUnidadesProyectos($codigoProyecto);
				$_SESSION['codigoProyectoBack'] = $codigoProyecto;
			}
			else{
	    	$row = consultaUnidadesProyectos($_SESSION['codigoProyectoBack']);
			}


        if(is_array($row))
        {
            $return = "";

            for($i = 0; $i < count($row); $i++){
                if($row[$i] != null){
                    if($i == 0){
                      $return = $return . utf8_encode($row[$i]['CODIGO']) . ',' . utf8_encode($row[$i]['PISO'])  . ',' . utf8_encode($row[$i]['TIPO'])  . ',' . utf8_encode($row[$i]['CODIGOTIPO']) . ',' . utf8_encode($row[$i]['ACCION']) . ',' . utf8_encode($row[$i]['TIPOLOGIA']) . ',' . utf8_encode($row[$i]['ORIENTACION']) . ',' . utf8_encode($row[$i]['M2UTIL']) . ',' . utf8_encode($row[$i]['M2TOTAL']) . ',' . utf8_encode($row[$i]['VALORUF']) . ',' . utf8_encode($row[$i]['ESTADO']) . ',' . utf8_encode($row[$i]['ESTADO2']);
                    }
                    else{
                      $return = $return . ',' . utf8_encode($row[$i]['CODIGO']) . ',' . utf8_encode($row[$i]['PISO'])  . ',' . utf8_encode($row[$i]['TIPO'])  . ',' . utf8_encode($row[$i]['CODIGOTIPO']) . ',' . utf8_encode($row[$i]['ACCION']) . ',' . utf8_encode($row[$i]['TIPOLOGIA']) . ',' . utf8_encode($row[$i]['ORIENTACION']) . ',' . utf8_encode($row[$i]['M2UTIL']) . ',' . utf8_encode($row[$i]['M2TOTAL']) . ',' . utf8_encode($row[$i]['VALORUF']) . ',' . utf8_encode($row[$i]['ESTADO']) . ',' . utf8_encode($row[$i]['ESTADO2']);
                    }
                }
            }

            echo $return;
        }
        else{
            echo "Sin datos";
        }
	}
	else{
		echo "Sin datos";
	}
?>

<?php
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');
	session_start();
	if(count($_POST) > 0){
			$rutVendedor = $_POST['rutVendedor'];
			$codigoProyecto = $_POST['codigoProyecto'];
			$numeroOperacion = $_POST['numeroOperacion'];
			if (array_key_exists('estadoPagoPromesa', $_POST) && count($_POST['estadoPagoPromesa']) > 0 && !array_key_exists('estadoPagoEscritura', $_POST)) {
				$estadoPagoPromesa = $_POST['estadoPagoPromesa'];
				if ($estadoPagoPromesa != NULL) {
					$cambioPromesa = actualizarComisionPromesa($rutVendedor, $codigoProyecto, $numeroOperacion, $estadoPagoPromesa);
					if ($cambioPromesa == 'Ok') {
						echo 'Ok';
						ingresoMonitoreoLog($_SESSION['nombreUser'], $_SESSION['rutUser'], "Comisiones", "Actualizacion pago comision", "Actualizacion de pago comision promesa a " . $estadoPagoPromesa, $codigoProyecto, $numeroOperacion); 
					}else{
						echo 'Sin datos';
					}
				}
			}
			else if (array_key_exists('estadoPagoEscritura', $_POST) && count($_POST['estadoPagoEscritura']) > 0 && !array_key_exists('estadoPagoPromesa', $_POST)) {
				$estadoPagoEscritura = $_POST['estadoPagoEscritura'];
				if ($estadoPagoEscritura != NULL) {
					$cambioEscritura = actualizarComisionEscritura($rutVendedor, $codigoProyecto, $numeroOperacion, $estadoPagoEscritura);
					if ($cambioEscritura == 'Ok') {
						echo 'Ok';
						ingresoMonitoreoLog($_SESSION['nombreUser'], $_SESSION['rutUser'], "Comisiones", "Actualizacion pago comision", "Actualizacion de pago comision escritura a " . $estadoPagoEscritura, $codigoProyecto, $numeroOperacion); 
					}else{
						echo 'Sin datos';
					}	
				}
			}
			else if (array_key_exists('estadoPagoEscritura', $_POST) && count($_POST['estadoPagoEscritura']) > 0 && array_key_exists('estadoPagoPromesa', $_POST) && count($_POST['estadoPagoPromesa']) > 0) {
				$estadoPagoPromesa = $_POST['estadoPagoPromesa'];
				if ($estadoPagoPromesa != NULL) {
					$cambioPromesa = actualizarComisionPromesa($rutVendedor, $codigoProyecto, $numeroOperacion, $estadoPagoPromesa);
					if ($cambioPromesa == 'Ok') {
						echo 'Ok';
						ingresoMonitoreoLog($_SESSION['nombreUser'], $_SESSION['rutUser'], "Comisiones", "Actualizacion pago comision", "Actualizacion de pago comision promesa a " . $estadoPagoPromesa, $codigoProyecto, $numeroOperacion); 
					}else{
						echo 'Sin datos';
					}
				}
				$estadoPagoEscritura = $_POST['estadoPagoEscritura'];
				if ($estadoPagoEscritura != NULL) {
					$cambioEscritura = actualizarComisionEscritura($rutVendedor, $codigoProyecto, $numeroOperacion, $estadoPagoEscritura);
					if ($cambioEscritura == 'Ok') {
						echo 'Ok';
						ingresoMonitoreoLog($_SESSION['nombreUser'], $_SESSION['rutUser'], "Comisiones", "Actualizacion pago comision", "Actualizacion de pago comision escritura a " . $estadoPagoEscritura, $codigoProyecto, $numeroOperacion); 
					}else{
						echo 'Sin datos';
					}	
				}
			}
				
				
		
		}else{
    		echo 'Sin datos';
  	}
?>

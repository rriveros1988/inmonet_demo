<?php
  require("PHPExcel.php");

  if(count($_POST) > 0){
    $datos = $_POST['array'];

    $detalle = array();
    $detalle[] = array('Código','Nombre','Inmobiliaria','Fecha inicio venta','Fecha fin obra','Fecha entrega','Estado');

    for($i = 0; $i < count($datos); $i++){
      $tmp = $datos[$i];
      $detalle[] = array($tmp[0],$tmp[1],$tmp[2],$tmp[3],$tmp[4],$tmp[5],substr($tmp[6],70,(strlen($tmp[6]) - 70)));
    }

    //Generamos archivo excel
    $excel = new PHPExcel();

    //Pasamos los datos del array
    $excel->getActiveSheet()->fromArray($detalle, null, 'A1');

    //Nombre de la hoja
    $excel->getActiveSheet()->setTitle('Listado Proyectos');

    //Tamaño de columnas automático
    $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

    // Save Excel 2007 file
    $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $writer->save('../tmp/listadoProyectos.xlsx');
    echo 'tmp/listadoProyectos.xlsx';
  }
?>

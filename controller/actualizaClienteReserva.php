<?php
  header('Access-Control-Allow-Origin: *');
  require('../model/consultas.php');

	if(count($_POST) > 0){
    $rutDetalleCliente = str_replace(".",'',$_POST['rutDetalleCliente']);
    $apellidosDetalleCliente = $_POST['apellidosDetalleCliente'];
    $nombresDetalleCliente = $_POST['nombresDetalleCliente'];
    $emailDetalleCliente = $_POST['emailDetalleCliente'];
    $celularDetalleCliente = $_POST['celularDetalleCliente'];
    $domicilioDetalleCliente = $_POST['domicilioDetalleCliente'];
    $numeroDomicilioDetalleCliente = $_POST['numeroDomicilioDetalleCliente'];
    $comunaDetalleCliente = $_POST['comunaDetalleCliente'];
    $ciudadDetalleCliente = $_POST['ciudadDetalleCliente'];
    $paisDetalleCliente = $_POST['paisDetalleCliente'];
    $estadoCivilDetalleCliente = $_POST['estadoCivilDetalleCliente'];
    $nivelEducacionalDetalleCliente = $_POST['nivelEducacionalDetalleCliente'];
    $casaHabitacionalDetalleCliente = $_POST['casaHabitacionalDetalleCliente'];
    $motivoDetalleCliente = $_POST['motivoDetalleCliente'];
    $profesionDetalleCliente = $_POST['profesionDetalleCliente'];
    $institucionDetalleCliente = $_POST['institucionDetalleCliente'];
    $nacionalidadDetalleCliente = $_POST['nacionalidadDetalleCliente'];
    $sexoDetalleCliente = $_POST['sexoDetalleCliente'];
    $regionDetalleCliente = $_POST['regionDetalleCliente'];
    $fNacDetalleCliente = $_POST['fNacDetalleCliente'];
    $tipoDomicilioDetalleCliente = $_POST['tipoDomicilioDetalleCliente'];
    $residenciaDetalleCliente = $_POST['residenciaDetalleCliente'];
    $actividadDetalleCliente = $_POST['actividadDetalleCliente'];

    if($fNacDetalleCliente == ''){
      $fNacDetalleCliente = '1900-01-01';
    }
    else{
      $date = new DateTime($fNacDetalleCliente);
      $fNacDetalleCliente = $date->format('Y-m-d');
    }

    if($celularDetalleCliente == ''){
      $celularDetalleCliente = 0;
    }
    if($numeroDomicilioDetalleCliente == ''){
      $numeroDomicilioDetalleCliente = 0;
    }

    $row = actualizaClienteReserva($rutDetalleCliente,$nombresDetalleCliente,$apellidosDetalleCliente,$emailDetalleCliente,$celularDetalleCliente,$domicilioDetalleCliente,$numeroDomicilioDetalleCliente,$comunaDetalleCliente,$ciudadDetalleCliente,$paisDetalleCliente,$estadoCivilDetalleCliente,$nivelEducacionalDetalleCliente,$casaHabitacionalDetalleCliente,$motivoDetalleCliente,$profesionDetalleCliente,$institucionDetalleCliente,$nacionalidadDetalleCliente,
  	$sexoDetalleCliente,$regionDetalleCliente,$fNacDetalleCliente,$tipoDomicilioDetalleCliente, $residenciaDetalleCliente, $actividadDetalleCliente);

    if($row == "Ok")
    {
      echo "Ok";
    }
    else{
      //echo $row;
      echo "Sin datos";
    }
	}
	else{
		echo "Sin datos";
	}
?>

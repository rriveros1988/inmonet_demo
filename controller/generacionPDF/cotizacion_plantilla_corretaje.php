<style type="text/css">
.headTabla{
  background-color: #e6f2ff;
  border: 1px solid black;
  padding: 2px;
}
.bodyTabla{
  border: 1px solid black;
  padding: 1px;
}
label{
  font-size: 12px;
}
input{
  font-size: 12px;
}
</style>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
  <label>Fecha:</label>
  <input disabled value=<?php session_start(); echo '"' . $_SESSION['fechaCotizacion'] . '"' ?> class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
  <label>A favor de:</label>
  <input disabled value=<?php echo '"' . $_SESSION['nombreClienteCotizacion'] . ' ' . $_SESSION['apellidoClienteCotizacion'] . '"' ?> class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
  <label>Domicilio:</label>
  <input disabled value=<?php echo '"' . $_SESSION['direccionCliClienteCotizacion'] . '"' ?> class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label>Fono:</label>
  <input disabled value=<?php echo '"' . $_SESSION['celularClienteCotizacion'] . '"' ?> class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label>RUT:</label>
  <input disabled value=<?php echo '"' . $_SESSION['rutClienteCotizacion'] . '"' ?> class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <label>E-Mail:</label>
  <input disabled value=<?php echo '"' . $_SESSION['mailClienteCotizacion'] . '"' ?> class="form-control"></input>
</div>
<div style="text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <hr style="background-color: #c1c1c1; height: 1px;" />
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
  <label>Propiedad Nro. Cod:</label>
  <input disabled value=<?php echo '"' . $_SESSION['departamentoClienteCotizacion'] . '"' ?> class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
  <label>Dirección propiedad:</label>
  <input disabled value=<?php echo '"' . $_SESSION['direccionCorClienteCotizacion'] . '"' ?> class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label>Precio:</label>
  <input disabled value=<?php echo '"UF ' . $_SESSION['precioCorClienteCotizacion'] . '"' ?> class="form-control"></input>
</div>
<div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <label>Comisión:</label>
  <input disabled value=<?php echo '"' . $_SESSION['comisionCorClienteCotizacion'] . ' %"' ?> class="form-control"></input>
</div>

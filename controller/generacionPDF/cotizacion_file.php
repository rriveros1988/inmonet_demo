<?php
//ini_set('display_errors', 'On');
require '../html2pdf/vendor/autoload.php';
date_default_timezone_set("America/Santiago");
require('../../model/consultas.php');
session_start();

function pago($tasa, $monto, $meses){
  $I = $tasa / 12 / 100 ;
  $I2 = $I + 1 ;
  $I2 = pow($I2,-$meses) ;

  $CuotaMensual = ($I * $monto) / (1 - $I2);

  return $CuotaMensual;
}

use Spipu\Html2Pdf\Html2Pdf;

ob_start();
if($_SESSION['codProyectoClienteCotizacion'] == "COR"){
  require_once 'cotizacion_plantilla_pdf_corretaje.php';
}
else{
  require_once 'cotizacion_plantilla_pdf.php';
}
$html = ob_get_clean();

// $document = '/var/www/html/Git/inmonet';
// $document = '/home/livingne/inmonet.cl/test';
$document = '/home/rriveros/public_html/inmobiliaria';

$html2pdf = new Html2Pdf('P','LETTER','es','true','UTF-8');
$html2pdf->writeHTML($html);
$html2pdf->output($document . '/repositorio/' . $_SESSION['codProyectoClienteCotizacion'] . '/cotizacion/' . $_SESSION['numeroCotizacion'] . '_' . $_SESSION['codProyectoClienteCotizacion'] . '_' . $_SESSION['departamentoClienteCotizacion'] . '_' . str_replace(' ', '_',$_SESSION['nombreClienteCotizacion']) . '_' . str_replace(' ', '_',$_SESSION['apellidoClienteCotizacion']) . '.pdf', 'F');

if(file_exists($document . '/repositorio/' . $_SESSION['codProyectoClienteCotizacion'] . '/cotizacion/' . $_SESSION['numeroCotizacion'] . '_' . $_SESSION['codProyectoClienteCotizacion'] . '_' . $_SESSION['departamentoClienteCotizacion'] . '_' . str_replace(' ', '_',$_SESSION['nombreClienteCotizacion']) . '_' . str_replace(' ', '_',$_SESSION['apellidoClienteCotizacion']) . '.pdf')){
  actualizaRUTA_PDF_COTIZA($_SESSION['codProyectoClienteCotizacion'] . '/cotizacion/' . $_SESSION['numeroCotizacion'] . '_' . $_SESSION['codProyectoClienteCotizacion'] . '_' . $_SESSION['departamentoClienteCotizacion'] . '_' . str_replace(' ', '_',$_SESSION['nombreClienteCotizacion']) . '_' . str_replace(' ', '_',$_SESSION['apellidoClienteCotizacion']) . '.pdf',$_SESSION['idCotizacion']);
  echo "Ok";
}
else{
  echo "Sin datos";
}
?>

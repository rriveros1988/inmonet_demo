<?php
  //ini_set('display_errors', 'On');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
  <head>
    <style type="text/css">
    #tablaOC{
      width: 100%;
      margin-left: 35px;
      margin-right:35px;
      margin-top: 20px;
    }
    #cabecera1Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height:70px;
    }
    #cabecera1Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height:70px;
    }
    #cabecera2Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height: 45px;
    }
    #cabecera2Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height: 45px;
    }
    #cabecera3Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height: 400px;
    }
    #cabecera3Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height: 400px;
    }
    .headTabla{
      background-color: #e6f2ff;
      border: 1px solid black;
      padding: 2px;
    }
    .bodyTabla{
      border: 1px solid black;
      padding: 1px;
    }

    td {
      padding: 0;
      margin: 0;
    }

    tr {
      padding: 0;
      padding: 0;
    }

    </style>
    <title>Orden de Compra</title>
  </head>
  <body style="font-size: 9px; font-family: Arial">
    <?php
      session_start();
    ?>
    <table id="tablaOC">
      <tr>
        <td id="cabecera1Izquierda">
          <?php
            echo "<img src='" . $_SESSION['logoProyectoCotizacion'] . "' style='height: 60px;'>";
          ?>
        </td>
        <td id="cabecera1Derecha">
          <img src="../../view/img/logos/living_logo.png" style='height: 60px;'>
        </td>
      </tr>
      <tr>
        <td id="cabecera2Izquierda">
          <?php
            echo "<font style='font-size: 9px;'>" . $_SESSION['direccionProyectoCotizacion'] . "</font>";
          ?>
          <table>
            <tr>
              <td>
                <?php
                  echo "Fono: " . $_SESSION['fono1ClienteCotizacion'] . "&nbsp;&nbsp;-&nbsp;&nbsp;Mail: " . $_SESSION['emailInmoClienteCotizacion'];
                ?>
              </td>
            </tr>
            <tr>
              <td>
                <?php
                  echo $_SESSION['webInmoClienteCotizacion'];
                ?>
              </td>
            </tr>
          </table>
        </td>
        <td id="cabecera2Derecha">
            <?php
              echo "Fecha: " . $_SESSION['fechaCotizacion'];
              echo "<br>Cotización: Válida por " . $_SESSION['diasCotProyectoCotizacion']  .  " días";
              echo "<br>Nro. Cotización: <font style='font-weight: bold'>" . $_SESSION['numeroCotizacion'] . "</font>";
            ?>
        </td>
      </tr>
      <tr>
        <td id="cabecera3Izquierda">
          &nbsp;Sr/Sra.
          <table>
            <tr>
              <td style="width: 150px;">
                <?php
                  echo "<font style='font-weight: bold;'>" .  $_SESSION['nombreClienteCotizacion'] . "</font>";
                ?>
              </td>
              <td>
                <?php
                  echo "<font style='font-weight: bold;'>" . $_SESSION['apellidoClienteCotizacion'] . "</font>";
                ?>
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                Rut
              </td>
              <td>
                Celular
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                <?php
                  echo "<font style='font-weight: bold;'>" . $_SESSION['rutClienteCotizacion'] . "</font>";
                ?>
              </td>
              <td>
                <?php
                  echo "<font style='font-weight: bold;'>" . $_SESSION['celularClienteCotizacion'] . "</font>";
                ?>
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                E-Mail
              </td>
              <td>

              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                <?php
                  echo "<font style='font-weight: bold;'>" . $_SESSION['mailClienteCotizacion'] . "</font>";
                ?>
              </td>
              <td>

              </td>
            </tr>
          </table>
          <hr style="color: #c1c1c1; height: 1px;" />
          <table style="border-collapse: collapse; border: none;">
            <tr>
              <td style="width: 150px;">
                <font style="font-size: 13px;">Departamento</font>
                <br/>
                <br/>
              </td>
              <td>
                <?php
                  echo '<font style="font-size: 13px;">' . $_SESSION['departaentoClienteCotizacion'] . '</font>';
                ?>
                <br/>
                <br/>
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                Tipología
              </td>
              <td>
                <?php
                  echo $_SESSION['tipologiaClienteCotizacion'];
                ?>
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                Modelo
              </td>
              <td>
                <?php
                  echo $_SESSION['modeloClienteCotizacion'];
                ?>
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                Orientación
                <br/>
                <br/>
              </td>
              <td>
                <?php
                  echo $_SESSION['orientacionClienteCotizacion'];
                ?>
                <br/>
                <br/>
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                <font style="font-weight: bold;">Superficie</font>
              </td>
              <td>

              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                Utiles
              </td>
              <td>
                <?php
                  echo number_format($_SESSION['m2UtilesClienteCotizacion'],2,',','.')  . " Mt2";
                ?>
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                Terraza
              </td>
              <td>
                <?php
                  echo number_format($_SESSION['m2TerrazaClienteCotizacion'],2,',','.')  . " Mt2";
                ?>
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                Totales
                <br/>
                <br/>
              </td>
              <td>
                <?php
                  echo number_format($_SESSION['m2TotalClienteCotizacion'],2,',','.') . " Mt2";
                ?>
                <br/>
                <br/>
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                Unidad de estacionamiento
              </td>
              <td>
                <?php
                  echo $_SESSION['estacionamientosClienteCotizacion'];
                ?>
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                Unidad de bodega
              </td>
              <td>
                <?php
                  echo $_SESSION['bodegasClienteCotizacion'];
                ?>
              </td>
            </tr>
          </table>
          <hr style="color: #c1c1c1; height: 1px;" />
          <table>
            <tr>
              <td style="width: 150px;">

              </td>
              <td style="width: 60px;">
                UF
              </td>
              <td style="width: 80px;">
                &#36;CH
              </td>
              <td style="width: 30px;">

              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                Departamento
              </td>
              <td style="width: 60px;">
                <?php
                  echo number_format($_SESSION['departamentoUfClienteCotizacion'], 2, ',', '.');
                ?>
              </td>
              <td style="width: 80px;">
                <?php
                  echo '$ ' . number_format(($_SESSION['departamentoUfClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.');
                ?>
              </td>
              <td style="width: 30px;">

              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                Bodega
              </td>
              <td style="width: 60px;">
                <?php
                if($_SESSION['accionClienteCotizacion'] != "Leasing"){
                  echo number_format($_SESSION['bodegaUfClienteCotizacion'], 2, ',', '.');
                }
                else{
                  echo number_format(0, 2, ',', '.');
                }
                ?>
              </td>
              <td style="width: 80px;">
                <?php
                if($_SESSION['accionClienteCotizacion'] != "Leasing"){
                  echo '$ ' . number_format(($_SESSION['bodegaUfClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.');
                }
                else{
                  echo '$ ' . number_format(0, 0, '.', '.');
                }
                ?>
              </td>
              <td style="width: 35px;  border-left: 1px solid #c1c1c1;">
                **
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                Estacionamiento
              </td>
              <td style="width: 60px;">
                <?php
                if($_SESSION['accionClienteCotizacion'] != "Leasing"){
                  echo number_format($_SESSION['estacionamientoUfClienteCotizacion'], 2, ',', '.');
                }
                else{
                  echo number_format(0, 2, ',', '.');
                }
                ?>
              </td>
              <td style="width: 80px;">
                <?php
                if($_SESSION['accionClienteCotizacion'] != "Leasing"){
                  echo '$ ' . number_format(($_SESSION['estacionamientoUfClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.');
                }
                else{
                  echo '$ ' . number_format(0, 0, '.', '.');
                }
                ?>
              </td>
              <td style="width: 35px;  border-left: 1px solid #c1c1c1;">
                **
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                <font style="font-weight: bold">Descuento sala</font>
              </td>
              <td style="width: 60px;">
                <?php
                  if($_SESSION['accionClienteCotizacion'] != "Leasing"){
                    echo number_format(($_SESSION['descuento1ClienteCotizacion']*$_SESSION['departamentoUfClienteCotizacion']/100), 2, ',', '.');
                  }
                  else{
                    echo number_format(0, 2, ',', '.');
                  }
                ?>
              </td>
              <td style="width: 80px;">
                <?php
                  if($_SESSION['accionClienteCotizacion'] != "Leasing"){
                    echo '$ ' . number_format(number_format(($_SESSION['descuento1ClienteCotizacion']*$_SESSION['departamentoUfClienteCotizacion']/100), 2, ',', '')*$_SESSION['ufClienteCotizacion'], 0, '.', '.');
                  }
                  else{
                    echo '$ ' . number_format(0, 0, ',', '.');
                  }
                ?>
              </td>
              <td style="width: 35px;  border-left: 1px solid #c1c1c1;">
                <?php
                  if($_SESSION['accionClienteCotizacion'] != "Leasing"){
                    echo number_format($_SESSION['descuento1ClienteCotizacion'], 2, '.', '') . ' %';
                  }
                  else{
                    echo number_format(0, 1, ',', '.') . ' %';
                  }
                ?>
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                <font style="font-weight: bold">Descuento especial</font>
              </td>
              <td style="width: 60px;">
                <?php
                  if($_SESSION['accionClienteCotizacion'] != "Leasing"){
                    echo number_format(($_SESSION['descuento2ClienteCotizacion']*$_SESSION['total2UFClienteCotizacion']/100), 2, ',', '.');
                  }
                  else{
                    echo number_format(0, 2, ',', '.');
                  }
                ?>
              </td>
              <td style="width: 80px;">
                <?php
                  if($_SESSION['accionClienteCotizacion'] != "Leasing"){
                    echo '$ ' . number_format(number_format(($_SESSION['descuento2ClienteCotizacion']*$_SESSION['total2UFClienteCotizacion']/100), 2, ',', '')*$_SESSION['ufClienteCotizacion'], 0, '.', '.');
                  }
                  else{
                    echo '$ ' . number_format(0, 0, ',', '.');
                  }
                ?>
              </td>
              <td style="width: 35px;  border-left: 1px solid #c1c1c1;">
                <?php
                  if($_SESSION['accionClienteCotizacion'] != "Leasing"){
                      echo number_format($_SESSION['descuento2ClienteCotizacion'], 2, '.', '') . ' %';
                  }
                  else{
                    echo number_format(0, 1, ',', '.') . ' %';
                  }
                ?>
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                <font style="font-weight: bold">Total</font>
              </td>
              <td style="width: 60px; border-top: 1px solid #c1c1c1;">
                <?php
                  if($_SESSION['accionClienteCotizacion'] != "Venta"){
                    echo '<font style="font-weight: bold">' . number_format($_SESSION['departamentoUfClienteCotizacion'], 2, ',', '.') . '</font>';
                  }
                  else{
                    echo '<font style="font-weight: bold">' . number_format($_SESSION['totalFUFClienteCotizacion'], 2, ',', '.') . '</font>';
                  }
                ?>
              </td>
              <td style="width: 80px; border-top: 1px solid #c1c1c1;">
                <?php
                  if($_SESSION['accionClienteCotizacion'] != "Venta"){
                    echo '<font style="font-weight: bold">$ ' . number_format(($_SESSION['departamentoUfClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.') . '</font>';
                  }
                  else{
                    echo '<font style="font-weight: bold">$ ' . number_format(($_SESSION['totalFUFClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.') . '</font>';
                  }
                ?>
              </td>
              <td style="width: 30px;">

              </td>
            </tr>
          </table>
          <br/>
          <table>
            <tr>
              <td style="width: 150px;">
                <font style="font-weight: bold">Formas de pago</font>
              </td>
              <td style="width: 60px;">

              </td>
              <td style="width: 80px;">

              </td>
              <td style="width: 30px;">

              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                Reserva
              </td>
              <td style="width: 60px;">
                <?php
                  echo number_format($_SESSION['reservaClienteCotizacion'], 2, ',', '.');
                ?>
              </td>
              <td style="width: 80px;">
                <?php
                  echo '$ ' . number_format(($_SESSION['reservaClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.');
                ?>
              </td>
              <td style="width: 35px;  border-left: 1px solid #c1c1c1;">
                <?php
                  echo number_format((($_SESSION['reservaClienteCotizacion']/$_SESSION['totalFUFClienteCotizacion'])*100), 2, '.', '') . ' %';
                ?>
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                Pie
              </td>
              <td style="width: 60px;">

              </td>
              <td style="width: 80px;">

              </td>
              <td style="width: 30px;">

              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                &nbsp;&nbsp;Pie promesa
              </td>
              <td style="width: 60px;">
                <?php
                  echo number_format($_SESSION['piePromesaClienteCotizacion'], 2, ',', '.');
                ?>
              </td>
              <td style="width: 80px;">
                <?php
                  echo '$ ' . number_format(($_SESSION['piePromesaClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.');
                ?>
              </td>
              <td style="width: 35px;  border-left: 1px solid #c1c1c1;">
                <?php
                  echo number_format((($_SESSION['piePromesaClienteCotizacion']/$_SESSION['totalFUFClienteCotizacion'])*100), 2, '.', '') . ' %';
                ?>
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                &nbsp;&nbsp;Pie cuotas
              </td>
              <td style="width: 60px;">
                <?php
                  echo number_format($_SESSION['pieCuotasClienteCotizacion'], 2, ',', '.');
                ?>
              </td>
              <td style="width: 80px;">
                <?php
                  echo '$ ' . number_format(($_SESSION['pieCuotasClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.');
                ?>
              </td>
              <td style="width: 35px;  border-left: 1px solid #c1c1c1;">
                <?php
                  echo number_format((($_SESSION['pieCuotasClienteCotizacion']/$_SESSION['totalFUFClienteCotizacion'])*100), 2, '.', '') . ' %';
                ?>
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                <?php
                  if($_SESSION['cuotasClienteCotizacion'] == '0'){
                    echo '&nbsp;&nbsp;(' . $_SESSION['cuotasClienteCotizacion'] . ' cuotas) ' . number_format(0, 2, ',', '.');
                  }
                  else{
                    echo '&nbsp;&nbsp;(' . $_SESSION['cuotasClienteCotizacion'] . ' cuotas) ' . number_format($_SESSION['pieCuotasClienteCotizacion']/$_SESSION['cuotasClienteCotizacion'], 2, ',', '.');
                  }
                ?>
              </td>
              <td style="width: 60px;">

              </td>
              <td style="width: 80px;">

              </td>
              <td style="width: 35px;  border-left: 1px solid #c1c1c1;">

              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                Saldo
              </td>
              <td style="width: 60px;">
                <?php
                  echo number_format($_SESSION['saldoClienteCotizacion'], 2, ',', '.');
                ?>
              </td>
              <td style="width: 80px;">
                <?php
                  echo '$ ' . number_format(($_SESSION['saldoClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.');
                ?>
              </td>
              <td style="width: 35px;  border-left: 1px solid #c1c1c1;">
                <?php
                  echo number_format((($_SESSION['saldoClienteCotizacion']/$_SESSION['totalFUFClienteCotizacion'])*100), 2, '.', '') . ' %';
                ?>
              </td>
            </tr>
            <tr>
              <td style="width: 150px;">
                <font style="font-weight: bold">Total</font>
              </td>
              <td style="width: 60px; border-top: 1px solid #c1c1c1;">
                <?php
                  echo '<font style="font-weight: bold">' . number_format($_SESSION['totalFUFClienteCotizacion'], 2, ',', '.') . '</font>';
                ?>
              </td>
              <td style="width: 80px; border-top: 1px solid #c1c1c1;">
                <?php
                  echo '<font style="font-weight: bold">$ ' . number_format(($_SESSION['totalFUFClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.') . '</font>';
                ?>
              </td>
              <td style="width: 30px;">

              </td>
            </tr>
          </table>
        </td>
        <td id="cabecera3Derecha" style="text-align: center; vertical-align: middle; position: fixed">
          <?php
            echo "<img  style='max-width: 300px; max-height: 400px;' src='" . $_SESSION['imgPlanoClienteCotizacion'] . "'>";
          ?>
        </td>
      </tr>
    </table>
    <br/>
    <table style="margin-left: 40px;">
      <tr>
        <td style="width: 700px;">
          <?php
           echo 'Precios Montos en pesos $CH a UF ' . number_format($_SESSION['ufClienteCotizacion'],2,',','.') . ' corresponden como referencia al valor UF al ' . $_SESSION['fechaCotizacion'];

          ?>
        </td>
      </tr>
      <tr style="width: 700px;">
        <td>
          <hr style="color: #c1c1c1; height: 1px;"/>
        </td>
      </tr>
      <tr>
        <td style="width: 700px;">
          <?php
            echo '<font style="text-align: justify; font-size: 8px;">' . $_SESSION['cotiza1ClienteCotizacion'] . '</font>';
          ?>
        </td>
      </tr>
      <tr>
        <td style="width: 700px;">
          <?php
            echo '<font style="text-align: justify; font-size: 8px;">' . $_SESSION['cotiza2ClienteCotizacion'] . '</font>';
          ?>
        </td>
      </tr>
      <tr>
        <td style="width: 700px;">
          <?php
            echo '<font style="text-align: justify; font-size: 8px;">' . $_SESSION['cotiza3ClienteCotizacion'] . '</font>';
          ?>
        </td>
      </tr>
    </table>
    <br/>
    <br/>
    <br/>
    <br/>
    <table  style="margin-left: 40px; font-size: 7px;">
      <tr style=" text-align: center;">
        <td style="width: 210px;">

        </td>
        <td style="width: 300;">
          Simulación Dividendo Crédito Hipotecario
        </td>
        <td style="width: 190px;">

        </td>
      </tr>
    </table>
    <table style="margin-left: 40px; font-size: 7px; border-collapse: collapse;">
      <tr>
        <th style="width: 220px; border-right: 1px solid black;">

        </th>
        <th class="headTabla">
          N° Años
        </th>
        <th class="headTabla">
          Tasa (%)
        </th>
        <th class="headTabla">
          Monto total
        </th>
        <th class="headTabla">
          Dividendo UF
        </th>
        <th class="headTabla">
          Dividendo $CH
        </th>
        <th class="headTabla">
          Renta necesaria
        </th>
        <th style="width: 190px;">

        </th>
      </tr>
      <tr style="text-align: center;">
        <td style="width: 220px; border-right: 1px solid black;">

        </td>
        <td class="bodyTabla">
          15
        </td>
        <td class="bodyTabla">
          <?php
            echo $_SESSION['tasaClienteCotizacion'] . ' %';
          ?>
        </td>
        <td class="bodyTabla">
          <?php
            echo number_format($_SESSION['saldoClienteCotizacion'], 2, ',', '.');
          ?>
        </td>
        <td class="bodyTabla">
          <?php
            if($_SESSION['accionClienteCotizacion'] != "Arriendo"){
              echo number_format($_SESSION['pago15ClienteCotizacion'], 2, ',', '.');
            }
            else{
              echo number_format(0, 2, ',', '.');
            }
          ?>
        </td>
        <td class="bodyTabla">
          <?php
            if($_SESSION['accionClienteCotizacion'] != "Arriendo"){
              echo '$ ' . number_format(($_SESSION['pago15ClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.');
            }
            else{
              echo number_format(0, 2, ',', '.');
            }
          ?>
        </td>
        <td class="bodyTabla">
          <?php
            if($_SESSION['accionClienteCotizacion'] != "Arriendo"){
              echo '$ ' . number_format(($_SESSION['pago15ClienteCotizacion']*$_SESSION['ufClienteCotizacion']*4), 0, '.', '.');
            }
            else{
              echo number_format(0, 2, ',', '.');
            }
          ?>
        </td>
        <td style="width: 190px;">

        </td>
      </tr>
      <tr style="text-align: center;">
        <td style="width: 220px; border-right: 1px solid black;">

        </td>
        <td class="bodyTabla">
          20
        </td>
        <td class="bodyTabla">
          <?php
            echo $_SESSION['tasaClienteCotizacion'] . ' %';
          ?>
        </td>
        <td class="bodyTabla">
          <?php
            echo number_format($_SESSION['saldoClienteCotizacion'], 2, ',', '.');
          ?>
        </td>
        <td class="bodyTabla">
          <?php
            if($_SESSION['accionClienteCotizacion'] != "Arriendo"){
              echo number_format($_SESSION['pago20ClienteCotizacion'], 2, ',', '.');
            }
            else{
              echo number_format(0, 2, ',', '.');
            }
          ?>
        </td>
        <td class="bodyTabla">
          <?php
            if($_SESSION['accionClienteCotizacion'] != "Arriendo"){
              echo '$ ' . number_format(($_SESSION['pago20ClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.');
            }
            else{
              echo number_format(0, 2, ',', '.');
            }
          ?>
        </td>
        <td class="bodyTabla">
          <?php
            if($_SESSION['accionClienteCotizacion'] != "Arriendo"){
              echo '$ ' . number_format(($_SESSION['pago20ClienteCotizacion']*$_SESSION['ufClienteCotizacion']*4), 0, '.', '.');
            }
            else{
              echo number_format(0, 2, ',', '.');
            }
          ?>
        </td>
        <td style="width: 190px;">

        </td>
      </tr>
      <tr style="text-align: center;">
        <td style="width: 220px; border-right: 1px solid black;">

        </td>
        <td class="bodyTabla">
          25
        </td>
        <td class="bodyTabla">
          <?php
            echo $_SESSION['tasaClienteCotizacion'] . ' %';
          ?>
        </td>
        <td class="bodyTabla">
          <?php
            echo number_format($_SESSION['saldoClienteCotizacion'], 2, ',', '.');
          ?>
        </td>
        <td class="bodyTabla">
          <?php
            if($_SESSION['accionClienteCotizacion'] != "Arriendo"){
              echo number_format($_SESSION['pago25ClienteCotizacion'], 2, ',', '.');
            }
            else{
              echo number_format(0, 2, ',', '.');
            }
          ?>
        </td>
        <td class="bodyTabla">
          <?php
            if($_SESSION['accionClienteCotizacion'] != "Arriendo"){
              echo '$ ' . number_format(($_SESSION['pago25ClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.');
            }
            else{
              echo number_format(0, 2, ',', '.');
            }
          ?>
        </td>
        <td class="bodyTabla">
          <?php
            if($_SESSION['accionClienteCotizacion'] != "Arriendo"){
              echo '$ ' . number_format(($_SESSION['pago25ClienteCotizacion']*$_SESSION['ufClienteCotizacion']*4), 0, '.', '.');
            }
            else{
              echo number_format(0, 2, ',', '.');
            }
          ?>
        </td>
        <td style="width: 190px;">

        </td>
      </tr>
      <tr style="text-align: center;">
        <td style="width: 220px; border-right: 1px solid black;">

        </td>
        <td class="bodyTabla">
          30
        </td>
        <td class="bodyTabla">
          <?php
            echo $_SESSION['tasaClienteCotizacion'] . ' %';
          ?>
        </td>
        <td class="bodyTabla">
          <?php
            echo number_format($_SESSION['saldoClienteCotizacion'], 2, ',', '.');
          ?>
        </td>
        <td class="bodyTabla">
          <?php
            if($_SESSION['accionClienteCotizacion'] != "Arriendo"){
              echo number_format($_SESSION['pago30ClienteCotizacion'], 2, ',', '.');
            }
            else{
              echo number_format(0, 2, ',', '.');
            }
          ?>
        </td>
        <td class="bodyTabla">
          <?php
            if($_SESSION['accionClienteCotizacion'] != "Arriendo"){
              echo '$ ' . number_format(($_SESSION['pago30ClienteCotizacion']*$_SESSION['ufClienteCotizacion']), 0, '.', '.');
            }
            else{
              echo number_format(0, 2, ',', '.');
            }
          ?>
        </td>
        <td class="bodyTabla">
          <?php
            if($_SESSION['accionClienteCotizacion'] != "Arriendo"){
              echo '$ ' . number_format(($_SESSION['pago30ClienteCotizacion']*$_SESSION['ufClienteCotizacion']*4), 0, '.', '.');
            }
            else{
              echo number_format(0, 2, ',', '.');
            }
          ?>
        </td>
        <td style="width: 190px;">

        </td>
      </tr>
    </table>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <table style="margin-left: 40px;">
      <tr style="text-align: center;">
        <td style="width: 80px;">
        </td>
        <td style="width: 150px;">
          <hr style="height: 1  px;"/>
          Firma comprador
          <br/>
          <?php
            echo '<b>' . $_SESSION['nombreClienteCotizacion'] . ' ' . $_SESSION['apellidoClienteCotizacion']  . '</b>';
          ?>
        </td>
        <td style="width: 190px;">

        </td>
        <td style="width: 150px;">
          <hr style="height: 1px;"/>
          Firma vendedor
          <br/>
          <?php
            echo '<b>' . $_SESSION['vendedorClienteCotizacion'] . '</b>';
          ?>
        </td>
        <td style="width: 125px; text-align: right;">
          <?php
            echo "<br/>" . $_SESSION['visitaClienteCotizacion'] . "<br/>" .  $_SESSION['publicidadClienteCotizacion'];
          ?>
        </td>
      </tr>
    </table>
  </body>
</html>

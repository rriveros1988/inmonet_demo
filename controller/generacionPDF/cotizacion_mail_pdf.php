<?php
// ini_set('display_errors', 'Off');
// require("../phpmailer/PHPMailerAutoload.php");
date_default_timezone_set('America/Santiago');

require '../phpmailer/src/Exception.php';
require '../phpmailer/src/PHPMailer.php';
require '../phpmailer/src/SMTP.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

session_start();

if(file_exists('../../repositorio/' . $_SESSION['codProyectoClienteCotizacion'] . '/cotizacion/' . $_SESSION['numeroCotizacion'] . '_' . $_SESSION['codProyectoClienteCotizacion'] . '_' . $_SESSION['departamentoClienteCotizacion'] . '_' . str_replace(' ', '_',$_SESSION['nombreClienteCotizacion']) . '_' . str_replace(' ', '_',$_SESSION['apellidoClienteCotizacion']) . '.pdf')){
  //instancio un objeto de la clase PHPMailer
  $mail = new PHPMailer(true); // defaults to using php "mail()"

  //Codificacion
  $mail->CharSet = 'UTF-8';

  //indico a la clase que use SMTP
  $mail->CharSet = 'UTF-8';
  $mail->SMTPSecure = 'tls';
  $mail->Host = "smtp.gmail.com"; // GMail
  $mail->Port = 587;
  $mail->IsSMTP(); // use SMTP
  $mail->SMTPAuth = true;
  //indico un usuario / clave de un usuario
  $mail->Username = "contacto@e-gestiontech.cl";
  $mail->Password = "karen1234_";

  $firma = "--
            <br />
            <img src='cid:firmaPng' alt='Ezentis' style='width: 180px;'>
            <br />
            Gestión inmobiliaria
            <br />
            ..........................................................................................................................................................................
            <br>
            <br>
            AVISO LEGAL.
            <br>
            <font style='margin-top: 0; line-height: 15px;font-family: Arial;font-size:7.5pt; text-align: justify; width: 100%'>
            Este mensaje y sus documentos anexos pueden contener información confidencial o legalmente protegida. Está dirigido única y exclusivamente a la persona o entidad reseñada como destinatarios del mensaje. Si este mensaje le hubiera llegado por error, por favor elimínelo sin revisarlo ni reenviarlo y notifíquelo lo antes posible al remitente. Cualquier divulgación, copia o utilización de dicha información es contraria a la ley. Le agradecemos su colaboración.
            </font>
            <br>";

    $mail->AddEmbeddedImage('../view/img/logos/living_logo_mail.png', 'firmaPng', 'firmaPng.png');

    $body = "<div style='width: 100%; text-align: justify; margin: 0 auto;'>
    <font style='font-size: 14px;'>
    Estimado/a " . $_SESSION['nombreClienteCotizacion'] . ' ' . $_SESSION['apellidoClienteCotizacion'] . ",
    <br />
    <br />
    Junto con saludar, de acuerdo a lo solicitado, adjuntamos cotización por el departamento " .
    $_SESSION['departaentoClienteCotizacion'] . ", cualquier duda al respecto quedamos atentos,
    y esperamos coordinación y/o confirmación para una pronta visita a nuestro proyecto.
    <br />
    <br />
    </font>
    <div'>
        <font style='font-size: 14px;'>
            Saludos cordiales.
        </font>
        <br />
        <br />
        " . $firma . "
    </div>
    ";

    $mail->SetFrom('contacto@e-gestiontech.cl', "Alertas");

    //defino la dirección de email de "reply", a la que responder los mensajes
    //Obs: es bueno dejar la misma dirección que el From, para no caer en spam
    $mail->AddReplyTo($_SESSION['mailUsuario'], $_SESSION['vendedorClienteCotizacion']);
    //Defino la dirección de correo a la que se envía el mensaje

    $listaMails = array($_SESSION['mailClienteCotizacion']);

    //Agregamos destinatarios
    for($i = 0; $i < count($listaMails); $i++){
        $mail->AddAddress($listaMails[$i], $listaMails[$i]);
    }

    $listaMailsCC = array($_SESSION['mailUsuario'],$_SESSION['emailInmoClienteCotizacion']);

    //Agregamos destinatarios
    for($i = 0; $i < count($listaMailsCC); $i++){
        $mail->AddCC($listaMailsCC[$i], $listaMailsCC[$i]);
    }

    $mail->addAttachment('../../repositorio/' . $_SESSION['codProyectoClienteCotizacion'] . '/cotizacion/' . $_SESSION['numeroCotizacion'] . '_' . $_SESSION['codProyectoClienteCotizacion'] . '_' . $_SESSION['departamentoClienteCotizacion'] . '_' . str_replace(' ', '_',$_SESSION['nombreClienteCotizacion']) . '_' . str_replace(' ', '_',$_SESSION['apellidoClienteCotizacion']) . '.pdf');

    $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

    $fecha = strtotime('+0 day');
    $fecha = $dias[date('w', $fecha)]." ".date('d', $fecha)." de ".$meses[date('n', $fecha)-1]. " ".date('Y', $fecha) . " a las " . date('h:i:s A', $fecha);

    $mail->Subject = $_SESSION['nombreProyectoClienteCotizacion'] . " - Cotización Dto. " . $_SESSION['departaentoClienteCotizacion'] .  " - " . $fecha . "";

    //Puedo definir un cuerpo alternativo del mensaje, que contenga solo texto
    $mail->AltBody = $_SESSION['nombreProyectoClienteCotizacion'] . " - Cotización Dto. " . $_SESSION['departaentoClienteCotizacion'] .  " - " . $fecha . "";

    //inserto el texto del mensaje en formato HTML
    $mail->MsgHTML($body);

    //envío el mensaje, comprobando si se envió correctamente
    if($mail->Send()) {
        echo "Ok";
    }
    else{
      //echo $mail->ErrorInfo;
      echo "Sin datos";
    }
}
else{
  echo "Sin datos";
}

//$html2pdf->output();

/*
$desc = fopen("../../tmp/cotiza.pdf", "w+");
fwrite($desc, $documento);
fclose($desc);
*/
?>

<?php
  // ini_set('display_errors', 'On');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
  <head>
    <style type="text/css">
    #tablaOC{
      width: 100%;
      margin-left: 35px;
      margin-right:35px;
      margin-top: 40px;
    }
    #cabecera1Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height:70px;
    }
    #cabecera1Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height:70px;
    }
    #cabecera2Izquierda{
      width: 10%;
      vertical-align: top;
      text-align: left;
      height: 30px;
    }
    #cabecera2Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height: 30px;
    }
    #cabecera3Izquierda{
      width: 50%;
      vertical-align: top;
      text-align: left;
      height: 80px;
    }
    #cabecera3Derecha{
      width: 50%;
      vertical-align: top;
      text-align: right;
      height: 80px;
    }
    .headTabla{
      background-color: #e6f2ff;
      border: 1px solid black;
      padding: 2px;
    }
    .bodyTabla{
      border: 1px solid black;
      padding: 1px;
    }

    td {
      padding: 0;
      margin: 0;
    }

    tr {
      padding: 0;
      padding: 0;
    }

    </style>
    <title>Orden de Compra</title>
  </head>
  <body style="font-size: 12px; font-family: Arial">
    <?php
      session_start();
    ?>
    <table id="tablaOC">
      <tr>
        <td id="cabecera1Izquierda">
          <img src="../../view/img/logos/living_logo.png" style='height: 60px;'>
        </td>
        <td id="cabecera1Derecha" style="font-size:10px;">
          <?php echo $_SESSION['datosProyecto'][0]['DIRECCION'] .'<br>';
                echo $_SESSION['datosProyecto'][0]['FONO1'] . ' - ' . $_SESSION['datosProyecto'][0]['EMAILINMOBILIARIA'] . '<br>';
                echo $_SESSION['datosProyecto'][0]['WEBPROYECTO'];
          ?>
        </td>
      </tr>
      <tr>
        <td id="cabecera2Izquierda">
        </td>
        <td id="cabecera2Derecha">
            <?php
              echo 'Fecha:&nbsp;' . $_SESSION['fechaCotizacion'] .'<br>';
              echo 'Válida por&nbsp;' . $_SESSION['diasCotProyectoCotizacion'] . '&nbsp;días<br>';
              echo 'Nro. Orden:&nbsp;' . $_SESSION['idCotizacion'];
            ?>

        </td>
      </tr>
    </table>
    <table style="margin-left: 38px;">
      <tr>
        <td style="width: 280px;"></td>
        <td style="width: 200px;">
          <font style="font-size: 16px;">ORDEN DE VISITA</font>
        </td>
        <td></td>
      </tr>
    </table>
    <br/>
    <table style="margin-left: 38px;">
      <tr>
        <td>
          <table style="margin-bottom: 3px;">
            <td>
              Esta Empresa le ofrece y le autoriza ver, la siguiente propiedad cuya autorización de Venta y/o Arriendo tiene encargada a:
            </td>
          </table>
          <br/>
          <table style="margin-bottom: 3px;">
            <td style="padding-right: 5px;">
              Sr/Sra.&nbsp;
              <b>
              <?php
                echo $_SESSION['nombreClienteCotizacion'] . ' ' . $_SESSION['apellidoClienteCotizacion'];
              ?>
              </b>
            </td>
            <td style="padding-right: 5px;">
              Rut&nbsp;
              <b>
              <?php
                echo $_SESSION['rutClienteCotizacion'];
              ?>
              </b>
            </td>
            <td style="padding-right: 5px;">
              Celular&nbsp;
              <b>
              <?php
                echo $_SESSION['celularClienteCotizacion'];
              ?>
              </b>
            </td>
            <td>
              Email&nbsp;
              <b>
              <?php
                echo $_SESSION['mailClienteCotizacion'];
              ?>
              </b>
            </td>
          </table>
          <br/>
          <table style="margin-bottom: 3px;">
            <td>
              La siguiente Propiedad ubicada en:&nbsp;<b><?php echo $_SESSION['direccionCorClienteCotizacion'];?></b>
            </td>
          </table>
        </td>
      </tr>
    </table>
    <br/>
    <table style="margin-left: 38px; width: 700px;">
      <tr>
        <td id="cabecera3Izquierda">
          <table>
            <tr>
              <td>
                <?php
                  echo $_SESSION['tipoUnidad'] . "&nbsp;" . $_SESSION['departamentoClienteCotizacion'];
                ?>
              </td>
            </tr>
            <tr>
              <td>
                Tipología&nbsp;
                <?php
                  echo $_SESSION['datosUnidad'][0]['TIPOLOGIADET'];
                ?>
              </td>
            </tr>
            <tr>
              <td>
                Modelo&nbsp;
                <?php
                 echo $_SESSION['datosUnidad'][0]['CODIGOTIPO'];
                ?>
              </td>
            </tr>
            <tr>
              <td>
                Orientación&nbsp;
                <?php
                  echo $_SESSION['datosUnidad'][0]['ORIENTACION'];
                ?>
              </td>
            </tr>
            <tr>
              <td>
                <b>Superficie</b>
              </td>
            </tr>
            <tr>
              <td>
                Utiles&nbsp;
                <?php
                  echo $_SESSION['datosUnidad'][0]['M2UTIL'] . " Mt2";
                ?>
              </td>
            </tr>
            <tr>
              <td>
                Terraza&nbsp;
                <?php
                  echo $_SESSION['datosUnidad'][0]['M2TERRAZA'] . " Mt2";
                ?>
              </td>
            </tr>
            <tr>
              <td>
                Otro&nbsp;
                <?php
                  echo $_SESSION['datosUnidad'][0]['M2OTRO'] . " Mt2";
                ?>
              </td>
            </tr>
            <tr>
              <td>
                Totales&nbsp;
                <?php
                  echo $_SESSION['datosUnidad'][0]['M2TOTAL'] . " Mt2";
                ?>
              </td>
            </tr>
            <tr>
              <td>
                Unidad de estacionamiento&nbsp;
                <?php
                  echo $_SESSION['datosUnidad'][0]['CANTESTACIONAMIENTOS'];
                ?>
              </td>
            </tr>
            <tr>
              <td>
                Unidad de bodega&nbsp;
                <?php
                  echo $_SESSION['datosUnidad'][0]['CANTBODEGAS'];
                ?>
              </td>
            </tr>
            <tr>
              <td>
                Valor &nbsp;
                <?php
                  echo 'UF <b>' . number_format ( $_SESSION['datosUnidad'][0]['VALORUF'], 2 , ',', '.') . '&nbsp;&nbsp;' . $_SESSION['datosUnidad'][0]['ACCIONDET'] .'</b>';
                ?>
              </td>
            </tr>
            <tr>
              <td>
                Valor $ch&nbsp;
                <?php
                  echo '<b>' . number_format(($_SESSION['datosUnidad'][0]['VALORUF']*$_SESSION['valorUFJqueryHoy']), 0, '.', '.')  . '</b>';
                ?>
              </td>
            </tr>
          </table>
        </td>
        <td id="cabecera3Derecha">
          <table>
            <tr>
              <td>
                <img style="height: 300px; object-fit: cover;height: 195px; float:left;" src=<?php echo '"' . $_SESSION['imgPlanoClienteCotizacion'] . '"'; ?>>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <br/>
    <table style="margin-left: 38px;">
      <tr>
        <td style="font-size: 8pt;">
          Precios Montos en pesos $CH a UF <?php echo number_format($_SESSION['valorUFJqueryHoy'], 2 , ',', '.'); ?> corresponden como referencia al valor UF al <?php echo $_SESSION['fechaCotizacion']; ?>
        </td>
      </tr>
    </table>
    <br>
    <table style="margin-left: 38px;">
      <tr>
          <?php

          $text = $_SESSION['descripcion1CorClienteCotizacion'];
          $maxPos = 350;
          if (strlen($text) > $maxPos)
          {
              $lastPos = ($maxPos - 3) - strlen($text);
              $text = substr($text, 0, strrpos($text, ' ', $lastPos)) . '...';
          }
          if (strlen($_SESSION['descripcion1CorClienteCotizacion']) > 350) {
            echo '<td style="width: 700px; text-align: justify;">' . $text;
          }else{
            echo '<td style="width: 700px; text-align: justify;">' . $_SESSION['descripcion1CorClienteCotizacion'];
          }
          echo "</td>";
          ?>
      </tr>
    </table>
    <br/>
    <table style="margin-left: 38px;">
    	<tr>
      		<td style="width:700px; text-align:justify;">
        	 El Cliente ya individualizado declara que a solicitado la presente orden, para visitar la propiedad detallada anteriormente y se compromete a cancelar a <b><?php echo $_SESSION['datosProyecto'][0]['INMOBILIARIA'] ?></b> el <b><?php echo str_replace('.', ',', $_SESSION['datosUnidad'][0]['COMISIONLVCLIENTE']); ?> %</b> de Comisión calculada sobre el monto de la operación de <b>VENTA</b> o la suma equivalente al <b><?php echo str_replace('.', ',', $_SESSION['datosUnidad'][0]['COMISIONVENCLIENTE']); ?> %</b> de la renta pactada si se tratase de <b>ARRIENDO</b> de la propiedad, más los impuestos correspondientes.
    	    </td>
	    </tr>
	    <tr>
	    	<td>
	    		&nbsp;
	    	</td>
	    </tr>
	    <tr>
	    	<td style="width:700px; text-align:justify;">
	    		El Cliente interesado declara que ésta ha sido el primer Corredor Inmobiliario que le ha ofrecido este negocio. Por lo tanto, se compromete a efectuar todas las negociaciones, acuerdos y suscripción de los contratos pertinentes de la propiedad ofrecida por su intermedio.
	    	</td>
	    </tr>
   		<tr>
   			<td style="width:700px; text-align:justify;">
  				Atendido el carácter personal e intransferible de la presente orden, el cliente que suscribe, acepta y consiente en pagar a esta oficina la comisión antes señalada aumentada en un 100% más el impuesto que corresponda, a título de pena, en los siguientes casos.
   			</td>
   		</tr>
  		<tr>
  			<td style="width:700px; text-align:justify;">
  				a) Si el Cliente se entendiera y llegara a acuerdo directamente con el propietario, omitiendo a la presente oficina. b) Si el Cliente se entendiera y llegara a acuerdo con un tercero que represente al propietario ajeno a la presente operación.<br/>
  				c) Si proporcionara la información contenida en la presente orden a terceros para que estos efectúen la operación de que se trata, por su cuenta.
  			</td>
  		</tr>
  		<tr>
  			<td style="width:700px; text-align:justify;">
  				En el caso de dificultad en el cobro de las comisiones ya indicada, estas serán resueltas por un designado por <b><?php echo $_SESSION['datosProyecto'][0]['INMOBILIARIA'] ?></b>, quién actuará en calidad de árbitro arbitrador, en única instancia, esto es sin ulterior recurso.
  			</td>
  		</tr>
  		<tr>
  			<td>
  				&nbsp;
  			</td>
  		</tr>
    </table>
    <table style="margin-left: 40px;">
      <tr>
      <td style="width:700px; text-align:justify;">
        Declaro bajo juramento que solicito y recibo esta orden para mí o para Don ___________________________________________&nbsp;
        y que actúo bajo su autorización y encargo.
          </td>
      </tr>
    </table>
    <br/>
    <br/>
    <br/>
    <br/>
    <table style="margin-left: 40px;">
      <tr style="text-align: center;">
        <td style="width: 200px; border-top: 1px solid black;">
          FIRMA SOLICITANTE
        </td>
        <td style="width: 300px; border-top: 1px solid black;">
          NOMBRE SOLICITANTE
        </td>
        <td style="width: 200px; border-top: 1px solid black;">
          RUT SOLICITANTE
        </td>
      </tr>
    </table>
    <br/>
    <br/>
    <br/>
    <br/>
    <table style="margin-left: 40px;">
      <tr style="text-align: center;">
        <td style="width: 200px; border-top: 1px solid black;">
            <font style="font-size: 10px;"><?php echo $_SESSION['nombreUser']; ?><br>
            Tel: <?php echo $_SESSION['fonoUsuario'];?><br>
       <?php echo $_SESSION['mailUsuario']?></font>
        </td>
        <td style="width: 300px;">

        </td>
        <td style="width: 200px;">

        </td>
      </tr>
    </table>
    <table style="margin-left: 40px;">
      <tr style="text-align: center; font-size: 8px;">
        <td style="width: 190px;">

        </td>
        <td style="width: 320px;">
                    <?php echo $_SESSION['datosProyecto'][0]['DIRECCION'] .' | ' . $_SESSION['datosProyecto'][0]['FONO1'] . ' | ' . $_SESSION['datosProyecto'][0]['EMAILINMOBILIARIA'] . ' | ' . $_SESSION['datosProyecto'][0]['WEBPROYECTO']; ?>
        </td>
        <td style="width: 190px;">

        </td>
      </tr>
    </table>
  </body>
</html>

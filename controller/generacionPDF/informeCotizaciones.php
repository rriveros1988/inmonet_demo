<?php
//ini_set('display_errors', 'On');
require '../html2pdf/vendor/autoload.php';
require('../../model/consultas.php');
date_default_timezone_set("America/Santiago");
session_start();

if(count($_POST) > 0){
  $_SESSION['proyectoInforme'] = $_POST['codigoProyecto'];
}
$datosProyecto = consultaDatosProyecto($_SESSION['proyectoInforme']);
$_SESSION['nombreProyectoInforme'] = $datosProyecto[0]['NOMBRE'];
$_SESSION['anoProyectoInforme'] = 2018;
$_SESSION['fechaProyectoInforme'] = date("d-m-Y");

use Spipu\Html2Pdf\Html2Pdf;

ob_start();

require_once 'informe_cotizacion_plantilla_pdf.php';

$html = ob_get_clean();

//$document = '/var/www/html/Git/inmonet';
//$document = '/home/livingne/inmonet.cl';

$html2pdf = new Html2Pdf('P','LETTER','es','true','UTF-8');
$html2pdf->writeHTML($html);
//$html2pdf->output($document . '/tmp/cotiza.pdf', 'F');
$html2pdf->output();

/*
$desc = fopen("../../tmp/cotiza.pdf", "w+");
fwrite($desc, $documento);
fclose($desc);
*/
?>

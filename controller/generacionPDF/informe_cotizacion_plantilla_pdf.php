<?php
  //ini_set('display_errors', 'On');
  //require('../../model/consultas.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
  <head>
    <style type="text/css">
    .tablaOC{
      width: 100%;
      margin-top: 20px;
    }
    #cabeceraCentral{
      width: 100%;
      vertical-align: top;
      text-align: center;
      height: 40px;
    }
    #cabecera1Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height:70px;
    }
    #cabecera1Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height:70px;
    }
    #cabecera2Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height: 45px;
    }
    #cabecera2Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height: 45px;
    }
    #cabecera3Izquierda{
      width: 45%;
      vertical-align: top;
      text-align: left;
      height: 400px;
    }
    #cabecera3Derecha{
      width: 45%;
      vertical-align: top;
      text-align: right;
      height: 400px;
    }
    .headTabla{
      background-color: #e6f2ff;
      border: 1px solid black;
      padding: 2px;
    }
    .bodyTabla{
      border: 1px solid black;
      padding: 1px;
    }

    td {
      padding: 2;
      margin: 2;
      padding-left: 4;
      padding-right: 4;
      border: 1;
    }

    th {
      padding: 2;
      margin: 2;
      border: 1;
    }

    tr {
      padding: 0;
      padding: 0;
    }

    </style>
    <title>Orden de Compra</title>
  </head>
  <body style="font-size: 11px; font-family: Arial">
    <?php
      session_start();
    ?>
    <table class="tablaOC">
      <tr style="font-size: 18px;">
        <td id="cabeceraCentral" style="border: 0;">
          LIVING NET - Informe de Gestión - Proyecto
          <?php
            echo $_SESSION['nombreProyectoInforme'];
          ?>
          <br/>
          <font style="font-size: 11px;">Al
          <?php
            echo $_SESSION['fechaProyectoInforme'];
          ?>
          </font>
        </td>
      </tr>
    </table>
    <div style="text-align:center;">
      <table style="margin: 0 auto; border: 1; border-collapse: collapse;">
        <tr style="text-align: center; background-color: #d9d9d9;">
          <th COLSPAN="13">COTIZACION POR MES</th>
        </tr>
        <tr style="background-color: #d9d9d9; font-weight: bold;">
          <td>Item</td>
          <td>Ene-<?php echo $_SESSION['anoProyectoInforme'] ?></td>
          <td>Feb-<?php echo $_SESSION['anoProyectoInforme'] ?></td>
          <td>Mar-<?php echo $_SESSION['anoProyectoInforme'] ?></td>
          <td>Abr-<?php echo $_SESSION['anoProyectoInforme'] ?></td>
          <td>May-<?php echo $_SESSION['anoProyectoInforme'] ?></td>
          <td>Jun-<?php echo $_SESSION['anoProyectoInforme'] ?></td>
          <td>Jul-<?php echo $_SESSION['anoProyectoInforme'] ?></td>
          <td>Ago-<?php echo $_SESSION['anoProyectoInforme'] ?></td>
          <td>Sep-<?php echo $_SESSION['anoProyectoInforme'] ?></td>
          <td>Oct-<?php echo $_SESSION['anoProyectoInforme'] ?></td>
          <td>Nov-<?php echo $_SESSION['anoProyectoInforme'] ?></td>
          <td>Dic-<?php echo $_SESSION['anoProyectoInforme'] ?></td>
        </tr>
        <?php
          $datosCotizaEn = '';
        ?>
      </table>
    </div>
  </body>
</html>

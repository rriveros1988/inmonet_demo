<?php
	header('Access-Control-Allow-Origin: *');
	//ini_set('display_errors', 'On');
	require('../model/consultas.php');
	session_start();

	if(count($_POST) >= 0){
    	$row = '';
			if(count($_POST) > 0){
				if($_POST['todas'] == "Todas" && $_POST['codigoProyecto'] == "todos"){
					if($_SESSION['idperfil'] == 1 || $_SESSION['idperfil'] == 2){
						$row = consultaCotizaciones();
					}
					else{
						$row = consultaCotizacionesUsu($_SESSION['rutUser']);
					}
				}
				else if($_POST['todas'] == "Todas" && $_POST['codigoProyecto'] <> "todos"){
					if($_SESSION['idperfil'] == 1 || $_SESSION['idperfil'] == 2){
						$row = consultaCotizacionesTodasProyecto($_POST['codigoProyecto']);
					}
					else{
						$row = consultaCotizacionesUsuTodasProyecto($_SESSION['rutUser'],$_POST['codigoProyecto']);
					}
				}
				else if($_POST['todas'] == "Vigentes" && $_POST['codigoProyecto'] == "todos"){
					if($_SESSION['idperfil'] == 1 || $_SESSION['idperfil'] == 2){
						$row = consultaCotizacionesVigentes();
					}
					else{
						$row = consultaCotizacionesUsuVigentes($_SESSION['rutUser']);
					}
				}
				else if($_POST['todas'] == "Vigentes" && $_POST['codigoProyecto'] <> "todos"){
					if($_SESSION['idperfil'] == 1 || $_SESSION['idperfil'] == 2){
						$row = consultaCotizacionesVigentesProyecto($_POST['codigoProyecto']);
					}
					else{
						$row = consultaCotizacionesUsuVigentesProyecto($_SESSION['rutUser'],$_POST['codigoProyecto']);
					}
				}
			}
			else{
				if($_SESSION['idperfil'] == 1 || $_SESSION['idperfil'] == 2){
	    		$row = consultaCotizacionesVigentes();
				}
				else{
					$row = consultaCotizacionesUsuVigentes($_SESSION['rutUser']);
				}
			}

        if(is_array($row))
        {
					$results = array(
							"sEcho" => 1,
							"iTotalRecords" => count($row),
							"iTotalDisplayRecords" => count($row),
							"aaData"=>$row
					);

					echo json_encode($results);
        }
        else{
					$results = array(
              "sEcho" => 1,
              "iTotalRecords" => 0,
              "iTotalDisplayRecords" => 0,
              "aaData"=>[]
          );
          echo json_encode($results);
        }
	}
	else{
		echo "Sin datos";
	}
?>

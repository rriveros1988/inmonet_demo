<?php
    header('Access-Control-Allow-Origin: *');
    require('../model/consultas.php');
    session_start();
    if(count($_POST) >= 0){
        $anoComision = $_POST['anoComision'];
        $mesComision = $_POST['mesComision'];
        $codigoProyecto = $_POST['codigoProyecto'];
        if ($_SESSION['idperfil'] == 1 || $_SESSION['idperfil'] == 2) {
            if ($codigoProyecto == "Todos" || $codigoProyecto == "todos") {
                $row = consultaProyectosPorVendedor($mesComision, $anoComision);
            }
            else{
                $row = datosFiltroContableMesAnoProyecto($anoComision, $mesComision, $codigoProyecto);
            }
        }

        if(is_array($row))
        {

          $fecha = new DateTime();
          $fecha->setDate($anoComision, $mesComision, 1);
          $ano = $fecha->format('Y');
          $mes = $fecha->format('m');
          $dia = $fecha->format('d');
          $fecha = $fecha->format('t-m-Y');

          $valor = $_POST['valorUFJqueryHoy'];

          for ($i=0; $i < count($row) ; $i++) {
            $row[$i]['TOTALPROMESAPESOS'] = $row[$i]['TOTALPROMESA']*$valor;
            $row[$i]['TOTALESCRITURAPESOS'] = $row[$i]['TOTALESCRITURA']*$valor;
            // if ($row[$i]['CODIGOPROYECTO'] == "COR") {
            //   unset($row[$i]);
            // }
          }
                    $results = array(
                            "sEcho" => 1,
                            "iTotalRecords" => count($row),
                            "iTotalDisplayRecords" => count($row),
                            "aaData"=>$row
                    );

                    echo json_encode($results);
        }
        else{
            $results = array(
              "sEcho" => 1,
              "iTotalRecords" => 0,
              "iTotalDisplayRecords" => 0,
              "aaData"=>[]
          );
          echo json_encode($results);
        }
    }
    else{
        echo "Sin datos";
    }
?>

<?php
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');

	if(count($_POST) == 0){
    	$row = consultaEstadosProyecto();

        if(is_array($row))
        {
            $return = "";

            for($i = 0; $i < count($row); $i++){
                if($row[$i] != null){
                    if($i == 0){
                      $return = $return . utf8_encode($row[$i]['IDESTADOPROYECTO']) . ',' . utf8_encode($row[$i]['NOMBRE']) . ',' . utf8_encode($row[$i]['COLOR']);
                    }
                    else{
                      $return = $return . ',' . utf8_encode($row[$i]['IDESTADOPROYECTO']) . ',' . utf8_encode($row[$i]['NOMBRE']) . ',' . utf8_encode($row[$i]['COLOR']);
                    }
                }
            }

            echo $return;
        }
        else{
            echo "Sin datos";
        }
	}
	else{
		echo "Sin datos";
	}
?>

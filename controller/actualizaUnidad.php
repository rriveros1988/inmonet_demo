<?php
  //ini_set('display_errors', 'On');
  header('Access-Control-Allow-Origin: *');
  require('../model/consultas.php');
  session_start();

	if(count($_POST) > 0){
    //Código proyecto guardado en sesión
    $codigoProyecto = $_SESSION['codigoProyectoBack'];
    //Datos de post
    $tipoDetalleUnidad = $_POST['tipoDetalleUnidad'];
    $accionDetalleUnidad = $_POST['accionDetalleUnidad'];
    $numeroDetalleUnidad = $_POST['numeroDetalleUnidad'];
    $estadoDetalleUnidad = $_POST['estadoDetalleUnidad'];
    $pisoDetalleUnidad = $_POST['pisoDetalleUnidad'];
    $orientacionDetalleUnidad = $_POST['orientacionDetalleUnidad'];
    $cantDormitoriosDetalleUnidad = $_POST['cantDormitoriosDetalleUnidad'];
    $cantBanosDetalleUnidad = $_POST['cantBanosDetalleUnidad'];
    $logiaDetalleUnidad = $_POST['logiaDetalleUnidad'];
    $m2UtilDetalleUnidad = $_POST['m2UtilDetalleUnidad'];
    $m2TerrazaDetalleUnidad = $_POST['m2TerrazaDetalleUnidad'];
    $m2OtroDetalleUnidad = $_POST['m2OtroDetalleUnidad'];
    $valorDetalleUnidad = $_POST['valorDetalleUnidad'];
    $comisionLVDuenoDetalleUnidad = $_POST['comisionLVDuenoDetalleUnidad'];
    $comisionLVClienteDetalleUnidad = $_POST['comisionLVClienteDetalleUnidad'];
    $comisionVenDuenoDetalleUnidad = $_POST['comisionVenDuenoDetalleUnidad'];
    $comisionVenClienteDetalleUnidad = $_POST['comisionVenClienteDetalleUnidad'];
    $direccionDetalleUnidad = $_POST['direccionDetalleUnidad'];
    $comunaDetalleUnidad = $_POST['comunaDetalleUnidad'];
    $codigoTipoDetalleUnidad = $_POST['codigoTipoDetalleUnidad'];
    $cantidadBodegasDetalleUnidad = $_POST['cantidadBodegasDetalleUnidad'];
    $cantidadEstacionamientosDetalleUnidad = $_POST['cantidadEstacionamientosDetalleUnidad'];
    $descripcion1DetalleUnidad = $_POST['descripcion1DetalleUnidad'];
    $descripcion2DetalleUnidad = $_POST['descripcion2DetalleUnidad'];
    $tipologiaDetalleUnidad = $_POST['tipologiaDetalleUnidad'];
    $m2TotalDetalleUnidad = $_POST['m2TotalDetalleUnidad'];

    $rutVendedorDetalleUnidad = $_POST['rutVendedorDetalleUnidad'];
    $rutVendedorDetalleUnidad = str_replace(".","",$rutVendedorDetalleUnidad);
    $nombresVendedorDetalleUnidad = $_POST['nombresVendedorDetalleUnidad'];
    $apellidosVendedorDetalleUnidad = $_POST['apellidosVendedorDetalleUnidad'];
    $celularVendedorDetalleUnidad = $_POST['celularVendedorDetalleUnidad'];
    $emailVendedorDetalleUnidad = $_POST['emailVendedorDetalleUnidad'];

    if($pisoDetalleUnidad == ''){
      $pisoDetalleUnidad = 0;
    };
    if($cantDormitoriosDetalleUnidad == ''){
      $cantDormitoriosDetalleUnidad = 0;
    };
    if($cantBanosDetalleUnidad == ''){
      $cantBanosDetalleUnidad = 0;
    };
    if($logiaDetalleUnidad == ''){
      $logiaDetalleUnidad = 0;
    }
    if($m2UtilDetalleUnidad == ''){
      $m2UtilDetalleUnidad = 0;
    }
    if($m2TerrazaDetalleUnidad == ''){
      $m2TerrazaDetalleUnidad = 0;
    }
    if($m2OtroDetalleUnidad == ''){
      $m2OtroDetalleUnidad = 0;
    }
    if($m2TotalDetalleUnidad == ''){
      $m2TotalDetalleUnidad = 0;
    }
    if($valorDetalleUnidad == ''){
      $valorDetalleUnidad = 0;
    }
    if($comisionLVDuenoDetalleUnidad == ''){
      $comisionLVDuenoDetalleUnidad = 0;
    }
    if($comisionLVClienteDetalleUnidad == ''){
      $comisionLVClienteDetalleUnidad = 0;
    }
    if($comisionVenDuenoDetalleUnidad == ''){
      $comisionVenDuenoDetalleUnidad = 0;
    }
    if($comisionVenClienteDetalleUnidad == ''){
      $comisionVenClienteDetalleUnidad = 0;
    }
    if($cantidadBodegasDetalleUnidad == ''){
      $cantidadBodegasDetalleUnidad = 0;
    }
    if($cantidadEstacionamientosDetalleUnidad == ''){
      $cantidadEstacionamientosDetalleUnidad = 0;
    }

    //Imágenes
    $imgPlano = '';
    $imgOtra = '';

    if($codigoProyecto != 'COR'){
      $pathPlano = $_FILES['imgPlanoDetalleUnidad']['name'];
      $extPlano = pathinfo($pathPlano, PATHINFO_EXTENSION);
      if($extPlano != ''){
        $imgPlano =  "../view/img/imagenes_cliente/proyectos/" . $codigoProyecto . "/unidad/plano/" . $codigoProyecto  . "_" . $codigoTipoDetalleUnidad . "_plano." . $extPlano;
      }
      else{
        $imgPlano =  "../view/img/imagenes_cliente/proyectos/" . $codigoProyecto . "/unidad/plano/" . $_POST['infoImgPlanoDetalleUnidad'];
      }

      $pathOtra = $_FILES['imgOtraDetalleUnidad']['name'];
      $extOtra = pathinfo($pathOtra, PATHINFO_EXTENSION);
      if($extOtra != ''){
        $imgOtra =  "../view/img/imagenes_cliente/proyectos/" . $codigoProyecto . "/unidad/otra/" . $codigoProyecto . "_" . $codigoTipoDetalleUnidad . "_otra." . $extOtra;
      }
      else{
        $imgOtra =  "../view/img/imagenes_cliente/proyectos/" . $codigoProyecto . "/unidad/otra/" . $_POST['infoImgOtraDetalleUnidad'];
      }
    }
    else{
      $pathPlano = $_FILES['imgPlanoDetalleUnidad']['name'];
      $extPlano = pathinfo($pathPlano, PATHINFO_EXTENSION);
      if($extPlano != ''){
        $imgPlano =  "../view/img/imagenes_cliente/proyectos/" . $codigoProyecto . "/unidad/plano/" . $codigoProyecto  . "_" . $numeroDetalleUnidad . "_plano." . $extPlano;
      }
      else{
        $imgPlano =  "../view/img/imagenes_cliente/proyectos/" . $codigoProyecto . "/unidad/plano/" . $_POST['infoImgPlanoDetalleUnidad'];
      }

      $pathOtra = $_FILES['imgOtraDetalleUnidad']['name'];
      $extOtra = pathinfo($pathOtra, PATHINFO_EXTENSION);
      if($extOtra != ''){
        $imgOtra =  "../view/img/imagenes_cliente/proyectos/" . $codigoProyecto . "/unidad/otra/" . $codigoProyecto . "_" . $numeroDetalleUnidad . "_otra." . $extOtra;
      }
      else{
        $imgOtra =  "../view/img/imagenes_cliente/proyectos/" . $codigoProyecto . "/unidad/otra/" . $_POST['infoImgOtraDetalleUnidad'];
      }
    }

    $row = actualizaUnidad($codigoProyecto,	$tipoDetalleUnidad,	$accionDetalleUnidad,	$numeroDetalleUnidad,	$estadoDetalleUnidad,	$pisoDetalleUnidad,	$orientacionDetalleUnidad,	$cantDormitoriosDetalleUnidad,	$cantBanosDetalleUnidad,	$logiaDetalleUnidad,	$m2UtilDetalleUnidad,	$m2TerrazaDetalleUnidad,	$m2OtroDetalleUnidad,	$valorDetalleUnidad,	$comisionLVDuenoDetalleUnidad,	$comisionLVClienteDetalleUnidad,	$comisionVenDuenoDetalleUnidad,	$comisionVenClienteDetalleUnidad,	$direccionDetalleUnidad,	$comunaDetalleUnidad,	$codigoTipoDetalleUnidad,	$cantidadBodegasDetalleUnidad,	$cantidadEstacionamientosDetalleUnidad,	$descripcion1DetalleUnidad,	$descripcion2DetalleUnidad,
  $imgPlano,$imgOtra,$m2TotalDetalleUnidad,$tipologiaDetalleUnidad,$rutVendedorDetalleUnidad, $nombresVendedorDetalleUnidad,$apellidosVendedorDetalleUnidad,$celularVendedorDetalleUnidad,$emailVendedorDetalleUnidad);

    if($row == "Ok")
    {
      move_uploaded_file($_FILES['imgPlanoDetalleUnidad']['tmp_name'],$imgPlano);
      move_uploaded_file($_FILES['imgOtraDetalleUnidad']['tmp_name'],$imgOtra);

      if($codigoProyecto != 'COR'){
        $act = actualizaIMG($codigoProyecto, $codigoTipoDetalleUnidad, $imgPlano, $imgOtra);
      }

      echo "Ok";
    }
    else{
      //echo $row;
      echo "Sin datos";
    }
	}
	else{
		echo "Sin datos";
	}
?>

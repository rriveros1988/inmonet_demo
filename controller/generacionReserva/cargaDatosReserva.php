<?php
date_default_timezone_set("America/Santiago");
require('../../model/consultas.php');
session_start();

$ch = curl_init();

$fecha = new Datetime();
$dia = $fecha->format('d');
$mes = $fecha->format('m');
$ano = $fecha->format('Y');

$fecha = $fecha->format('d-m-Y');

$valor = $_POST['valorUFJqueryHoy'];

$_SESSION['reservaUFActual'] = $valor;

$cotizacion = consultaCotizacionEspecifica($_POST['codigoProyecto'], $_POST['numeroCotizacion']);
$proyecto = consultaProyectoEspecifico($cotizacion[0]['IDPROYECTO']);
$cliente = consultaClienteEspecifico($cotizacion[0]['IDCLIENTE']);
$unidad = consultaUnidadEspecifica($cotizacion[0]['IDUNIDAD']);
$cotizacionBodega = consultaCotizacionBodega($cotizacion[0]['IDCOTIZACION']);
$cotizacionEstacionamiento = consultaCotizacionEstacionamiento($cotizacion[0]['IDCOTIZACION']);
$usuario = chequeaUsuario($_SESSION['rutUser']);

$_SESSION['reservaCodigoProyecto'] = $_POST['codigoProyecto'];

//datos extra
$_SESSION['idCotizacion'] = $cotizacion[0]['IDCOTIZACION'];
$_SESSION['idCliente1'] = $cliente[0]['IDCLIENTE'];
$_SESSION['idUnidad'] = $unidad[0]['IDUNIDAD'];
$_SESSION['idProyecto'] = $proyecto[0]['IDPROYECTO'];
$_SESSION['tipoUnidad'] = $unidad[0]['TIPOUNIDAD'];

$datetime1=new DateTime();
if($proyecto[0]['FECHAENTREGA'] == ''){
  $datetime2=new DateTime();
}
else{
  $datetime2=new DateTime($proyecto[0]['FECHAENTREGA']);
}

$interval=$datetime2->diff($datetime1);

$intervalMeses=$interval->format("%m");

$intervalAnos = $interval->format("%y")*12;

$_SESSION['mesesCuotasProyecto'] = ($intervalMeses+$intervalAnos);

$_SESSION['reservaNombreProyecto'] = $proyecto[0]['NOMBRE'];
$_SESSION['reservaLogoProyecto'] = '../' . $proyecto[0]['LOGO'];
if($_SESSION['reservaLogoProyecto'] == '../'){
  $_SESSION['reservaLogoProyecto'] = '../../view/img/imagenes_cliente/proyectos/NO_LOGO/no_logo.png';
}

$_SESSION['idUsuario'] = $usuario['IDUSUARIO'];

//Generamos datos en session para la IMG reserva
if($unidad[0]['IMGPLANO'] == NULL){
  $_SESSION['reservaImgPlano'] = "../../view/img/imagenes_cliente/proyectos/NO_LOGO/no_logo.png";
}
else{
  $_SESSION['reservaImgPlano'] = $unidad[0]['IMGPLANO'];
}

//Campos de cliente deben ser chequeados para ingreso de reserva
$_SESSION['reservaNombreCliente'] = $cliente[0]['NOMBRES'];
$_SESSION['reservaApellidoCliente'] = $cliente[0]['APELLIDOS'];
$_SESSION['reservaRutCliente'] = $cliente[0]['RUT'];
$_SESSION['reservaCelularCliente'] = $cliente[0]['CELULAR'];
$_SESSION['reservaEmailCliente'] = $cliente[0]['EMAIL'];
$_SESSION['reservaDomicilioCliente'] = $cliente[0]['DOMICILIO'];
$_SESSION['reservaNumeroDomicilioCliente'] = $cliente[0]['NUMERODOMICILIO'];
$_SESSION['reservaComunaCliente'] = $cliente[0]['COMUNA'];
$_SESSION['reservaCiudadCliente'] = $cliente[0]['CIUDAD'];
$_SESSION['reservaRegionCliente'] = $cliente[0]['REGION'];
$_SESSION['reservaPaisCliente'] = $cliente[0]['PAIS'];
$_SESSION['reservaProfesionCliente'] = $cliente[0]['PROFESION'];
$_SESSION['reservaInstitucionCliente'] = $cliente[0]['INSTITUCION'];
$_SESSION['reservaNacionalidadCliente'] = $cliente[0]['NACIONALIDAD'];
$_SESSION['reservaSexoCliente'] = $cliente[0]['SEXO'];

//Generamos datos en session para la reserva
$fechaCotiza = new Datetime($cotizacion[0]['FECHA']);
$_SESSION['reservaFechaCotiza'] = $fechaCotiza->format('d-m-Y');
$_SESSION['reservaNumeroCotiza'] = $cotizacion[0]['CODIGO'];
$_SESSION['reservaValidesReserva'] = $proyecto[0]['DIASRES'];


//Demas datos
$_SESSION['reservaNumeroDepto'] = $unidad[0]['CODIGO'];
$_SESSION['reservaTipologiaDepto'] = $unidad[0]['TIPOLOGIADET'];
$_SESSION['reservaModeloDepto'] = $unidad[0]['CODIGOTIPO'];
$_SESSION['reservaOrientacionDepto'] = $unidad[0]['ORIENTACION'];
$_SESSION['reservaMT2UtilesDepto'] = $unidad[0]['M2UTIL'];
$_SESSION['reservaMT2TerrazaDepto'] = $unidad[0]['M2TERRAZA'];
$_SESSION['reservaMT2TotalDepto'] = $unidad[0]['M2TOTAL'];
$_SESSION['reservaEst'] = $cotizacion[0]['CANTEST'];
$_SESSION['reservaBod'] = $cotizacion[0]['CANTBOD'];
$_SESSION['reservaValorDepto'] = $cotizacion[0]['VALORUNIDADUF'];
$_SESSION['reservaAccion'] = $unidad[0]['ACCIONDET'];

if($_SESSION["reservaAccion"] != "Leasing"){
  //UF de bodega y estacionamiento
  $_SESSION['reservaValorEst'] = 0;
  $_SESSION['reservaValorBod'] = 0;

  for($i = 0; $i < count($cotizacionEstacionamiento); $i++){
    $_SESSION['reservaValorEst'] = $_SESSION['reservaValorEst'] + $cotizacionEstacionamiento[0]['VALORUF'];
  }

  for($i = 0; $i < count($cotizacionBodega); $i++){
    $_SESSION['reservaValorBod'] = $_SESSION['reservaValorBod'] + $cotizacionBodega[0]['VALORUF'];
  }


  $_SESSION['reservaValorBrutoUF'] = round(($_SESSION['reservaValorDepto']+$_SESSION['reservaValorEst']+$_SESSION['reservaValorBod']),2);

  //Seguimos con datos necesarios
  $_SESSION['reservaBono'] = $cotizacion[0]['BONOVENTA'];

  $_SESSION['reservaDescuentoSala'] = number_format(($cotizacion[0]['DESCUENTO1UF']),2, '.', '');
  $_SESSION['reservaDescuentoEspecial'] = number_format(($cotizacion[0]['DESCUENTO2UF']), 2, '.', '');

  $_SESSION['reservaTotal2'] = number_format(($_SESSION['reservaValorDepto']-$_SESSION['reservaValorDepto']*$_SESSION['reservaDescuentoSala']/100),2,'.','');

  $_SESSION['reservaTotal3'] = number_format(
  ($_SESSION['reservaValorDepto'] -
  ($_SESSION['reservaValorDepto']*$_SESSION['reservaDescuentoSala']/100)-
  ($_SESSION['reservaTotal2']*$_SESSION['reservaDescuentoEspecial']/100)
  ),2,'.','');

  if($_SESSION['reservaValorDepto'] != 0){
    $_SESSION['reservaTotalF'] = number_format(
    (
    ($_SESSION['reservaValorDepto']+$_SESSION['reservaValorEst']+$_SESSION['reservaValorBod'])-
    ($_SESSION['reservaValorDepto']*$_SESSION['reservaDescuentoSala']/100)-
    ($_SESSION['reservaTotal2']*$_SESSION['reservaDescuentoEspecial']/100)-
    ($_SESSION['reservaBono'])
    ),2,'.','');
  }
  else{
    $_SESSION['reservaBono'] = 0;
    $_SESSION['reservaTotalF'] = number_format(
    (
    ($_SESSION['reservaValorDepto']+$_SESSION['reservaValorEst']+$_SESSION['reservaValorBod'])
    ),2,'.','');
  }

  $_SESSION['reservaFecha'] = date('d-m-Y');
  $_SESSION['reservaReserva'] = $cotizacion[0]['VALORRESERVAUF'];
  $_SESSION['reservaPiePromesa'] = $cotizacion[0]['VALORPIEPROMESAUF'];
  $_SESSION['reservaPieCuotas'] = $cotizacion[0]['VALORPIESALDOUF'];

  if($_SESSION['reservaReserva']+$_SESSION['reservaPiePromesa']+$_SESSION['reservaPieCuotas'] > $_SESSION['reservaTotalF']){
    $_SESSION['reservaPieCuotas'] = $_SESSION['reservaTotalF'] - ($_SESSION['reservaReserva']+$_SESSION['reservaPiePromesa']);
    $_SESSION['reservaPieSaldo'] = 0;
  }
  else{
    $_SESSION['reservaPieSaldo'] = $_SESSION['reservaTotalF'] - ($_SESSION['reservaReserva']+$_SESSION['reservaPiePromesa']+$_SESSION['reservaPieCuotas']);
  }

  $_SESSION['reservaPieCantCuotas'] = $cotizacion[0]['CANTIDADCUOTASPIE'];
  if($_SESSION['mesesCuotasProyecto'] < $_SESSION['reservaPieCantCuotas']){
    $_SESSION['reservaPieCantCuotas'] = $_SESSION['mesesCuotasProyecto'];
  }
}
else if($_SESSION["reservaAccion"] == "Leasing"){
  //UF de bodega y estacionamiento
  $_SESSION['reservaValorEst'] = 0;
  $_SESSION['reservaValorBod'] = 0;

  for($i = 0; $i < count($cotizacionEstacionamiento); $i++){
    $_SESSION['reservaValorEst'] = $_SESSION['reservaValorEst'] + $cotizacionEstacionamiento[0]['VALORUF'];
  }

  for($i = 0; $i < count($cotizacionBodega); $i++){
    $_SESSION['reservaValorBod'] = $_SESSION['reservaValorBod'] + $cotizacionBodega[0]['VALORUF'];
  }


  $_SESSION['reservaValorBrutoUF'] = round(($_SESSION['reservaValorDepto']+$_SESSION['reservaValorEst']+$_SESSION['reservaValorBod']),2);

  //Seguimos con datos necesarios
  $_SESSION['reservaBono'] = $cotizacion[0]['BONOVENTA'];

  $_SESSION['reservaDescuentoSala'] = number_format(0,2, '.', '');
  $_SESSION['reservaDescuentoEspecial'] = number_format(0, 2, '.', '');

  $_SESSION['reservaTotal2'] = number_format(($_SESSION['reservaValorDepto']-$_SESSION['reservaValorDepto']*$_SESSION['reservaDescuentoSala']/100),2,'.','');

  $_SESSION['reservaTotal3'] = number_format(
  ($_SESSION['reservaValorDepto'] -
  ($_SESSION['reservaValorDepto']*$_SESSION['reservaDescuentoSala']/100)-
  ($_SESSION['reservaTotal2']*$_SESSION['reservaDescuentoEspecial']/100)
  ),2,'.','');

  $_SESSION['reservaFecha'] = date('d-m-Y');
  $_SESSION['reservaReserva'] = $cotizacion[0]['VALORRESERVAUF'];
  $_SESSION['reservaPiePromesa'] = $cotizacion[0]['VALORPIEPROMESAUF'];
  $_SESSION['reservaPieCuotas'] = $cotizacion[0]['VALORPIESALDOUF'];
  $_SESSION['reservaSaldoTotal'] = $cotizacion[0]['VALORSALDOTOTALUF'];

  $_SESSION['reservaTotalF'] = $_SESSION['reservaReserva']  + $_SESSION['reservaPiePromesa'] +  $_SESSION['reservaPieCuotas'] + $_SESSION['reservaSaldoTotal'];

  if($_SESSION['reservaReserva']+$_SESSION['reservaPiePromesa']+$_SESSION['reservaPieCuotas'] > $_SESSION['reservaTotalF']){
    $_SESSION['reservaPieCuotas'] = $_SESSION['reservaTotalF'] - ($_SESSION['reservaReserva']+$_SESSION['reservaPiePromesa']);
    $_SESSION['reservaPieSaldo'] = 0;
  }
  else{
    $_SESSION['reservaPieSaldo'] = $_SESSION['reservaTotalF'] - ($_SESSION['reservaReserva']+$_SESSION['reservaPiePromesa']+$_SESSION['reservaPieCuotas']);
  }

  $_SESSION['reservaPieCantCuotas'] = $cotizacion[0]['CANTIDADCUOTASPIE'];
  if($_SESSION['mesesCuotasProyecto'] < $_SESSION['reservaPieCantCuotas']){
    $_SESSION['reservaPieCantCuotas'] = $_SESSION['mesesCuotasProyecto'];
  }
}


echo $proyecto[0]['NOMBRE'];
?>

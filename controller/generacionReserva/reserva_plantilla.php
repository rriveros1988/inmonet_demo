<script>
	function randomTextoLoadJs(){
	  var str = '';
	  var ref = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRST0123456789';
	  for (var i = 0; i < 8 ; i++)
	  {
	    str += ref.charAt(Math.floor(Math.random()*ref.length));
	  }
	  return str;
	}
  var js = document.createElement('script');
  js.src = 'view/js/funciones.js?idLoad=' + randomTextoLoadJs();
  document.getElementsByTagName('head')[0].appendChild(js);
</script>
<style type="text/css">
.headTabla{
  background-color: #e6f2ff;
  border: 1px solid black;
  padding: 2px;
}
.bodyTabla{
  border: 1px solid black;
  padding: 1px;
}
label{
  font-size: 12px;
}
input{
  font-size: 12px;
}
</style>
<?php
  session_start();
?>
<div id="divDerechaCrearReserva" style="padding-left: 0; padding-right: 0;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Fecha cotización:</label>
    <input disabled value=<?php echo '"' . $_SESSION['reservaFechaCotiza'] . '"' ?> class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Numero cotización:</label>
    <input disabled  value=<?php echo '"' . $_SESSION['reservaNumeroCotiza']  . '"'; ?> class="form-control"></input><!-- Pendiente -->
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Nombre cliente:</label>
    <input id="reservaNombreCliente" disabled value=<?php echo '"' . $_SESSION['reservaNombreCliente'] . '"'; ?> class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Apellido cliente:</label>
    <input id="reservaApellidoCliente" disabled value=<?php echo '"' . $_SESSION['reservaApellidoCliente'] . '"'; ?> class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Rut:</label>
    <input id="reservaRutCliente" disabled value=<?php echo '"' . $_SESSION['reservaRutCliente'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Celular:</label>
    <input id="reservaCelularCliente" disabled value=<?php echo '"' . $_SESSION['reservaCelularCliente'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
    <label>Email:</label>
    <input id="reservaEmailCliente" disabled value=<?php echo '"' . $_SESSION['reservaEmailCliente'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="margin-top: 20pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
    <button style="width: 100%; margin-bottom: 5pt;" class="btn btn-primary" id="anexoDatosCliente"><i clasS="fa fa-plus-square"></i>&nbsp;&nbsp;Agregar datos de cliente 1</button>
  </div>
  <div style="margin-top: 20pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
    <button disabled style="width: 100%; margin-bottom: 5pt;" class="btn btn-primary" id="anexoDatosCliente2"><i clasS="fa fa-plus-square"></i>&nbsp;&nbsp;Agregar datos de cliente 2</button>
  </div>
</div>
<div id="divIzquierdaCrearReserva" style="padding-left: 0; padding-right: 0;   border-left-style: solid; border-left-width: 1px; border-left-color: #c1c1c1;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Fecha:</label>
    <input disabled value=<?php echo '"' . $_SESSION['reservaFecha'] . '"' ?> class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Reserva válida por:</label>
    <input disabled  value=<?php echo '"' . $_SESSION['reservaValidesReserva']  . ' días"'; ?> class="form-control"></input><!-- Pendiente -->
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Vendedor:</label>
    <select id="reservaVendedor" class="form-control custom">
      <?php
        // ini_set('display_errors', 'On');
        require('../../model/consultas.php');
        $ven = consultaUsuariosVendedores();
        $us = consultaCotizacionEspecifica($_SESSION['reservaCodigoProyecto'], $_SESSION['reservaNumeroCotiza']);
        for($i = 0; $i < count($ven); $i++){
          if($ven[$i]['IDUSUARIO'] === $us[0]['IDUSUARIO']){
              echo '<option selected value="' . $ven[$i]['IDUSUARIO'] . '">' . $ven[$i]['NOMBRE'] . '</option>';
          }
          else{
              echo '<option value="' . $ven[$i]['IDUSUARIO'] . '">' . $ven[$i]['NOMBRE'] . '</option>';
          }
        }
      ?>
    </select>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Departamento:</label>
    <input disabled value=<?php echo '"' . $_SESSION['reservaNumeroDepto'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Tipología:</label>
    <input disabled value=<?php echo '"' . $_SESSION['reservaTipologiaDepto'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Modelo:</label>
    <input disabled value=<?php echo '"' . $_SESSION['reservaModeloDepto'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Orientación:</label>
    <input disabled value=<?php echo '"' . $_SESSION['reservaOrientacionDepto'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Bodegas:</label>
    <input disabled value=<?php echo '"' . $_SESSION['reservaBod'] . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Estacionamientos:</label>
    <input disabled value=<?php echo '"' . $_SESSION['reservaEst']  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Utiles:</label>
    <input disabled value=<?php echo '"' . number_format($_SESSION['reservaMT2UtilesDepto'],2,',','.')  . ' Mt2"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Terraza:</label>
    <input disabled value=<?php echo '"' . number_format($_SESSION['reservaMT2TerrazaDepto'],2,',','.')  . ' Mt2"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Total:</label>
    <input disabled value=<?php echo '"' . number_format($_SESSION['reservaMT2TotalDepto'],2,',','.')  . ' Mt2"'; ?>  class="form-control"></input>
  </div>
</div>

<div style="text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <hr style="background-color: #c1c1c1; height: 1px;" />
</div>

<div id="divIzquierdaCrearReserva" style="text-align: left; padding-left: 0; padding-right: 0;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Departamento:</label>
    <input id="departamentoUFEditable" disabled value=<?php echo '"UF ' . number_format($_SESSION['reservaValorDepto'], 2, ',', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input disabled value=<?php echo '"$ ' . number_format(($_SESSION['reservaValorDepto']*$_SESSION['reservaUFActual']), 0, '.', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Bodega:</label>
    <input id="bodegaUFEditable" disabled value=<?php
      if($_SESSION['reservaAccion'] != "Leasing"){
        echo '"UF ' . number_format($_SESSION['reservaValorBod'], 2, ',', '.')  . '"';
      }
      else{
        echo '"UF ' . number_format(0, 2, ',', '.')  . '"';
      }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="bodegaMontoEditable" disabled value=

    <?php
    if($_SESSION['reservaAccion'] != "Leasing"){
      echo '"$ ' . number_format(($_SESSION['reservaValorBod']*$_SESSION['reservaUFActual']), 0, '.', '.')  . '"';
    }
    else{
      echo '"$ ' . number_format(0, 0, '.', '.')  . '"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Estacionamiento:</label>
    <input id="estacionamientoUFEditable" disabled value=
    <?php
    if($_SESSION['reservaAccion'] != "Leasing"){
      echo '"UF ' . number_format($_SESSION['reservaValorEst'], 2, ',', '.')  . '"';
    }
    else{
      echo '"UF ' . number_format(0, 2, ',', '.')  . '"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="estacionamientoMontoEditable" disabled value=
    <?php
    if($_SESSION['reservaAccion'] != "Leasing"){
      echo '"$ ' . number_format(($_SESSION['reservaValorEst']*$_SESSION['reservaUFActual']), 0, '.', '.')  . '"';
    }
    else{
      echo '"$ ' . number_format(0, 0, '.', '.')  . '"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Bono:</label>
    <input id="bonoUFEditable" disabled value=
    <?php
    if($_SESSION['reservaAccion'] != "Leasing"){
      echo '"UF ' . number_format($_SESSION['reservaBono'], 2, ',', '.')  . '"';
    }
    else{
      echo '"UF ' . number_format(0, 2, ',', '.')  . '"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="bonoMontoEditable" disabled value=
    <?php
    if($_SESSION['reservaAccion'] != "Leasing"){
      echo '"$ ' . number_format(($_SESSION['reservaBono']*$_SESSION['reservaUFActual']), 0, '.', '.')  . '"';
    }
    else{
      echo '"$ ' . number_format(0, 0, '.', '.')  . '"';
    }
    ?>  class="form-control"></input>
  </div>

  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Desc. sala:</label>
    <input  id="desc1UFEditable" disabled value=<?php
    if($_SESSION['reservaAccion'] != "Leasing"){
      echo '"UF ' . number_format(($_SESSION['reservaDescuentoSala']*$_SESSION['reservaValorDepto']/100), 2, ',', '.')  . '"';
    }
    else{
      echo '"UF ' . number_format(0, 2, ',', '.')  . '"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input disabled value=<?php
    if($_SESSION['reservaAccion'] != "Leasing"){
      echo '"$ ' . number_format(number_format(($_SESSION['reservaDescuentoSala']*$_SESSION['reservaValorDepto']/100), 2, '.', '.')*$_SESSION['reservaUFActual'], 0, '.', '.')  . '"';
    }
    else{
      echo '"$ ' . number_format(0, 0, ',', '.')  . '"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input disabled value=<?php
    if($_SESSION['reservaAccion'] != "Leasing"){
      echo '"' . number_format($_SESSION['reservaDescuentoSala'], 2, '.', '')  . ' %"';
    }
    else{
      echo '"' . number_format(0, 2, '.', '')  . ' %"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Desc. especial:</label>
    <input id="desc2UFEditable" disabled value=<?php
     if($_SESSION['reservaAccion'] != "Leasing"){
       echo '"UF ' .  number_format(($_SESSION['reservaDescuentoEspecial']*$_SESSION['reservaTotal2']/100), 2, ',', '.') . '"';
     }
     else{
       echo '"UF ' .  number_format(0, 2, ',', '.') . '"';
     }
     ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input disabled value=<?php
     if($_SESSION['reservaAccion'] != "Leasing"){
       echo '"$ ' . number_format(number_format(($_SESSION['reservaDescuentoEspecial']*$_SESSION['reservaTotal2']/100), 2, '.', '.')*$_SESSION['reservaUFActual'], 0, '.', '.')  . '"';
     }
     else{
       echo '"$ ' . number_format(0, 0, '.', '.')  . '"';
     }
     ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input disabled value=<?php
    if($_SESSION['reservaAccion'] != "Leasing"){
      echo '"' . number_format($_SESSION['reservaDescuentoEspecial'], 2, '.', '')  . ' %"';
    }
    else{
      echo '"' . number_format(0, 2, '.', '')  . ' %"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label>Total:</label>
    <input id="totalUnoUFEditable" disabled value=<?php
        if($_SESSION['reservaAccion'] != "Venta"){
          echo '"UF ' . number_format($_SESSION['reservaValorDepto'], 2, ',', '.')  . '"';
        }
        else{
          echo '"UF ' . number_format($_SESSION['reservaTotalF'], 2, ',', '.')  . '"';
        }
     ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="totalUnoMontoEditable" disabled value=<?php
    if($_SESSION['reservaAccion'] != "Venta"){
      echo '"$ ' . number_format(($_SESSION['reservaValorDepto']*$_SESSION['reservaUFActual']), 0, '.', '.')  . '"';
    }
    else{
      echo '"$ ' . number_format(($_SESSION['reservaTotalF']*$_SESSION['reservaUFActual']), 0, '.', '.')  . '"';
    }
    ?>  class="form-control"></input>
  </div>
</div>
<div id="divDerechaCrearReserva" style="text-align: left; margin-top: 5pt;" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <div id="divBodegasCrearReserva" class="col-lg-12 col-md-12	col-sm-12 col-xs-12" style="margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; margin-bottom: 10pt;">
    <label style="font-weight: bold;">Bodegas:</label>
    <select multiple="multiple" id="bodegasCrearReserva" name="bodegasCrearReserva[]">

    </select>
  </div>
  <div id="divEstacionamientosCrearReserva" class="col-lg-12 col-md-12	col-sm-12 col-xs-12" style="margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; margin-bottom: 10pt;">
    <label style="font-weight: bold;">Estacionamientos:</label>
    <select multiple="multiple" id="estacionamientosCrearReserva" name="estacionamientosCrearReserva[]">

    </select>
  </div>
  <div id="divObservacionCrearReserva" class="col-lg-12 col-md-12	col-sm-12 col-xs-12" style="margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; margin-bottom: 10pt;">
    <label style="font-weight: bold;">Observación:</label>
    <textarea rows="3" class="col-lg-12 col-md-12	col-sm-12 col-xs-12" style="padding: 5pt 5pt 5pt 5pt; resize: none; margin-bottom: 5pt; max-height: 80px; height: 80px;" id="observacionCrearReserva" maxlength="2000"></textarea>
  </div>
</div>

<div style="text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <hr style="background-color: #c1c1c1; height: 1px;" />
</div>

<div style="text-align: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <font style="font-weight: bold;">Formas de pago</font>
</div>
<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0;  padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Reserva:</label>
    <input id="reservaUFEditable" disabled value=<?php echo '"UF ' . number_format($_SESSION['reservaReserva'], 2, ',', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0;  padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Monto $:</label>
    <input id="reservaMontoEditable" value=<?php echo '"$ ' . number_format(($_SESSION['reservaReserva']*$_SESSION['reservaUFActual']), 0, '.', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0;  padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Porcentaje:</label>
    <input id="reservaPorcentajeEditable" disabled value=<?php echo '"' . number_format((($_SESSION['reservaReserva']/$_SESSION['reservaTotalF'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
  </div>
</div>

<!--Campos nuevos -->
<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Forma pago reserva:</label>
    <select class="form-control custom" type="text" id="reservaFormaPago">

    </select>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Banco:</label>
    <input id="reservaBanco" type="text" class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Serie/Nro:</label>
    <input id="reservaSerie" type="text" class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <label>Nro Trans/Cheque:</label>
    <input id="reservaNro" type="text" class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <label>Fecha cheque:</label>
    <input id="reservaFechaCheque" type="text" class="form-control"></input>
  </div>
</div>
<!-- Fin campos nuevos -->

<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Pie promesa contado:</label>
    <input id="piePromesaUFEditable" disabled value=<?php echo '"UF ' .  number_format($_SESSION['reservaPiePromesa'], 2, ',', '.') . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="piePromesaMontoEditable" value=<?php echo '"$ ' . number_format(($_SESSION['reservaPiePromesa']*$_SESSION['reservaUFActual']), 0, '.', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="piePromesaPorEditable" disabled value=<?php echo '"' . number_format((($_SESSION['reservaPiePromesa']/$_SESSION['reservaTotalF'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
  </div>
</div>
<div class="col-lg-6 col-md-6 hidden-sm hidden-xs col-lg-offset-6 col-md-offset-6">
</div>

<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Pie promesa cuotas:</label>
    <input id="pieCuotasUFEditable" disabled value=<?php echo '"UF ' . number_format($_SESSION['reservaPieCuotas'], 2, ',', '.') . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="pieCuotasMontoEditable" value=<?php echo '"$ ' . number_format(($_SESSION['reservaPieCuotas']*$_SESSION['reservaUFActual']), 0, '.', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="pieCuotasPorEditable" disabled value=<?php echo '"' . number_format((($_SESSION['reservaPieCuotas']/$_SESSION['reservaTotalF'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
  </div>
</div>
<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Cuotas:</label>
    <input id="cuotasEditable" type="number" step="1" min="1" max=<?php echo '"' . $_SESSION['mesesCuotasProyecto']  . '"'; ?> value=<?php echo '"' . $_SESSION['reservaPieCantCuotas']  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="cuotasUFEditable" disabled value=<?php
    if($_SESSION['reservaPieCantCuotas'] == '0'){
      echo '"UF ' . number_format(0, 2, ',', '.')   . '"';
    }
    else{
      echo '"UF ' . number_format($_SESSION['reservaPieCuotas']/$_SESSION['reservaPieCantCuotas'], 2, ',', '.')   . '"';
    }
    ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="cuotasMontoEditable" disabled value=<?php
    if($_SESSION['reservaPieCantCuotas'] == '0'){
      echo '"$ ' . number_format(0, 0, ',', '.')   . '"';
    }
    else{
      echo '"$ ' . number_format($_SESSION['reservaUFActual']*number_format($_SESSION['reservaPieCuotas']/$_SESSION['reservaPieCantCuotas'],2,'.',''), 0, ',', '.')   . '"';
    }
    ?>  class="form-control"></input>
  </div>
</div>

<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Saldo:</label>
    <input id="saldoUFEditable" disabled value=<?php echo '"UF ' . number_format($_SESSION['reservaPieSaldo'], 2, ',', '.') . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="saldoMontoEditable" disabled value=<?php echo '"$ ' . number_format(($_SESSION['reservaPieSaldo']*$_SESSION['reservaUFActual']), 0, '.', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="saldoPorEditable" disabled value=<?php echo '"' . number_format((($_SESSION['reservaPieSaldo']/$_SESSION['reservaTotalF'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
  </div>
</div>
<div class="col-lg-6 col-md-6 hidden-sm hidden-xs col-lg-offset-6 col-md-offset-6">
</div>

<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" >
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label>Total:</label>
    <input id="totalUFEditable" disabled value=<?php echo '"UF ' . number_format($_SESSION['reservaTotalF'], 2, ',', '.') . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="totalMontoEditable" disabled value=<?php echo '"$ ' . number_format(($_SESSION['reservaTotalF']*$_SESSION['reservaUFActual']), 0, '.', '.')  . '"'; ?>  class="form-control"></input>
  </div>
  <div style="text-align: left; margin-top: 5pt; padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <label class="hidden-sm hidden-xs">&nbsp;</label>
    <input id="totalPorEditable" disabled value=<?php echo '"' . number_format((($_SESSION['reservaTotalF']/$_SESSION['reservaTotalF'])*100), 2, '.', '') . ' %"'; ?>  class="form-control"></input>
  </div>
</div>
<div style="text-align: left; margin-top: 20pt;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <?php
   echo 'Precios Montos en pesos $CH a UF <font id="valorUFDia">' . number_format($_SESSION['reservaUFActual'], 2, ',', '.') . '</font> corresponden como referencia al valor UF al ' . $_SESSION['reservaFecha'];

  ?>
</div>

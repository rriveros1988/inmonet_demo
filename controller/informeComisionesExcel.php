<?php
	ini_set('display_errors', 'On');
	header('Access-Control-Allow-Origin: *');
	require('../model/consultas.php');
	require("PHPExcel.php");

	if(count($_POST) >= 0){
			$fechaUF = $_POST['datosUF'];
			$codigoProyecto = $_POST['codProyecto'];
			$fechaIni = $_POST['fechaIni'];
			$fechaFin = $_POST['fechaFin'];
			$tipo = $_POST['tipo'];

			$datosUF = array();
			for($i = 0; $i < count($fechaUF); $i++){
				$ch = curl_init();

				$fecha = new DateTime();
				$fecha->setDate($fechaUF[$i]['ano'], $fechaUF[$i]['mes'], 1);
				$ano = $fecha->format('Y');
				$mes = $fecha->format('m');
				$dia = $fecha->format('d');
				$fecha = $fecha->format('t-m-Y');

				// set url
				curl_setopt($ch, CURLOPT_URL, "https://sistemasetl.com/uf/valorUFDia.php?fecha=" . $fecha);

		 	 	//return the transfer as a string
		 	 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		 	 	// $output contains the output string
		 	 	$output = curl_exec($ch);

				array_push($datosUF,array($fechaUF[$i]['ano'],$fechaUF[$i]['mes'],$output));
			}

			$row = null;
			$row2 = null;

			if($tipo == 'vendedor'){
				if($codigoProyecto != "todos"){
					$row = consultaComisionesVendedorMesAnoInforme($codigoProyecto, $fechaIni, $fechaFin);
				}
				else{
					$row = consultaComisionesVendedorMesAnoInformeTodos($fechaIni, $fechaFin);
				}
			}
			else if($tipo == 'empresa'){
				if($codigoProyecto != 'todos'){
					$row2 = consultaComisionesEmpresaMesAnoInforme($codigoProyecto, $fechaIni, $fechaFin);
				}
				else{
					$row2 = consultaComisionesEmpresaMesAnoInformeTodos($fechaIni, $fechaFin);
				}
			}
			else{
				if($codigoProyecto != 'todos'){
					$row = consultaComisionesVendedorMesAnoInforme($codigoProyecto, $fechaIni, $fechaFin);
					$row2 = consultaComisionesEmpresaMesAnoInforme($codigoProyecto, $fechaIni, $fechaFin);
				}
				else{
					$row = consultaComisionesVendedorMesAnoInformeTodos($fechaIni, $fechaFin);
					$row2 = consultaComisionesEmpresaMesAnoInformeTodos($fechaIni, $fechaFin);
				}
			}

			$datosExcel = array();
	    $datosExcel[] = array('RUT','NOMBRE', 'CODIGOPROYECTO','OPERACION','DEPTO','EST','BOD','VALORNETOUF','FECHACOMISIONPROMESA','COMISIONPROMESA','UFCOMISIONPROMESA','MONTOPESOSCOMISIONPROMESA','ESTADOPAGOCOMISIONPROMESA','FECHACOMISIONESCRITURA','COMISIONESCRITURA','UFCOMISIONESCRITURA','MONTOPESOSCOMISIONESCRITURA','ESTADOPAGOCOMISIONESCRITURA');

			if(is_array($row)){
		    for($i = 0; $i < count($row); $i++){
						$fecha = new DateTime($row[$i]['FECHACOMISIONPROMESA']);
						$ano = $fecha->format('Y');
						$mes = $fecha->format('m');
						$ufEscritura = 0;
						for($z = 0; $z < count($datosUF); $z++){
							if($datosUF[$z][0] == $ano && $datosUF[$z][1] == $mes){
								$ufPromesa = str_replace(',','.',str_replace('.','',$datosUF[$z][2]));
								break;
							}
						}
						$fecha = new DateTime($row[$i]['FECHACOMISIONESCRITURA']);
						$ano = $fecha->format('Y');
						$mes = $fecha->format('m');
						$ufEscritura = 0;
						for($z = 0; $z < count($datosUF); $z++){
							if($datosUF[$z][0] == $ano && $datosUF[$z][1] == $mes){
								$ufEscritura = str_replace(',','.',str_replace('.','',$datosUF[$z][2]));
								break;
							}
						}
		        $datosExcel[] = array($row[$i]['RUT'],$row[$i]['NOMBRE'],$row[$i]['CODIGOPROYECTO'],$row[$i]['NUMERO'],$row[$i]['DEPTO'],$row[$i]['EST'],$row[$i]['BOD'],number_format((real)$row[$i]['VALORNETOUF'],2,',',''),$row[$i]['FECHACOMISIONPROMESA'],str_replace('.',',',$row[$i]['COMISIONPROMESA']),number_format((real)$row[$i]['UFPROMESA'],2,',',''),number_format((real)($row[$i]['UFPROMESA']*$ufPromesa),0,',',''),$row[$i]['ESTADOPROMESA'],$row[$i]['FECHACOMISIONESCRITURA'],str_replace('.',',',$row[$i]['COMISIONESCRITURA']),number_format((real)$row[$i]['UFESCRITURA'],2,',',''),number_format((real)($row[$i]['UFESCRITURA']*$ufEscritura),0,',',''),$row[$i]['ESTADOESCRITURA']);
		    }
			}
			if(is_array($row2)){
				for($i = 0; $i < count($row2); $i++){
						$fecha = new DateTime($row2[$i]['FECHACOMISIONPROMESA']);
						$ano = $fecha->format('Y');
						$mes = $fecha->format('m');
						$ufEscritura = 0;
						for($z = 0; $z < count($datosUF); $z++){
							if($datosUF[$z][0] == $ano && $datosUF[$z][1] == $mes){
								$ufPromesa = str_replace(',','.',str_replace('.','',$datosUF[$z][2]));
								break;
							}
						}
						$fecha = new DateTime($row2[$i]['FECHACOMISIONESCRITURA']);
						$ano = $fecha->format('Y');
						$mes = $fecha->format('m');
						$ufEscritura = 0;
						for($z = 0; $z < count($datosUF); $z++){
							if($datosUF[$z][0] == $ano && $datosUF[$z][1] == $mes){
								$ufEscritura = str_replace(',','.',str_replace('.','',$datosUF[$z][2]));
								break;
							}
						}
		       	$datosExcel[] = array('','Empresa',$row2[$i]['CODIGOPROYECTO'],$row2[$i]['NUMERO'],$row2[$i]['DEPTO'],$row2[$i]['EST'],$row2[$i]['BOD'],number_format((real)$row2[$i]['VALORNETOUF'],2,',',''),$row2[$i]['FECHACOMISIONPROMESA'],str_replace('.',',',$row2[$i]['COMISIONPROMESA']),number_format((real)$row2[$i]['UFPROMESA'],2,',',''),number_format((real)($row2[$i]['UFPROMESA']*$ufPromesa),0,',',''),$row2[$i]['ESTADOPROMESA'],$row2[$i]['FECHACOMISIONESCRITURA'],str_replace('.',',',$row2[$i]['COMISIONESCRITURA']),number_format((real)$row2[$i]['UFESCRITURA'],2,',',''),number_format((real)($row2[$i]['UFESCRITURA']*$ufEscritura),0,',',''),$row2[$i]['ESTADOESCRITURA']);
		    }
			}

			$excel = new PHPExcel();

	    // Fill worksheet from values in array
	    $excel->getActiveSheet()->fromArray($datosExcel, null, 'A1');

	    // Rename worksheet
	    $excel->getActiveSheet()->setTitle("Pendientes_Instala");

	    // Set AutoSize for name and email fields
	    $excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	    $excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	    $excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	    $excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	    $excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	    $excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	    $excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	    $excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	    $excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	    $excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	    $excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	    $excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	    $excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	    $excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
	    $excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
	    $excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
	    $excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
	    $excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);

			$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    	$writer->save("informe_comisiones.xlsx");

			echo "ok";
	}
	else{
		echo "Sin datos";
	}
?>

<?php
    header('Access-Control-Allow-Origin: *');
    require('../model/consultas.php');

    if(count($_POST) >= 0){
        $filtromesano = consultaFiltroContableMesAno();
        $filtromesano = $filtromesano[0][0];
        $filtromesano = explode(" - ", $filtromesano);
        $ano = $filtromesano[0];
        $mes = $filtromesano[1];

        $row = consultaProyectosPorEmpresa($mes, $ano);
        $rowCor = consultaComisionCorTotal($mes, $ano);

        if(is_array($row))
        {

          $fecha = new DateTime();
          $fecha->setDate($ano, $mes, 1);
          $ano = $fecha->format('Y');
          $mes = $fecha->format('m');
          $dia = $fecha->format('d');
          $fecha = $fecha->format('t-m-Y');

          $valor = $_POST['valorUFJqueryHoy'];

          for ($i=0; $i < count($row) ; $i++) {
            if($row[$i]['CODIGOPROYECTO'] == 'COR'){
              $row[$i]['TOTALPROMESAPESOS'] = $rowCor[0]['VALOR'];
              $row[$i]['TOTALESCRITURAPESOS'] = 0;
            }
            else{
              $row[$i]['TOTALPROMESAPESOS'] = $row[$i]['TOTALPROMESA']*$valor;
              $row[$i]['TOTALESCRITURAPESOS'] = $row[$i]['TOTALESCRITURA']*$valor;
            }
          }
          $results = array(
                  "sEcho" => 1,
                  "iTotalRecords" => count($row),
                  "iTotalDisplayRecords" => count($row),
                  "aaData"=>$row
          );

          echo json_encode($results);
        }
        else{
            $results = array(
              "sEcho" => 1,
              "iTotalRecords" => 0,
              "iTotalDisplayRecords" => 0,
              "aaData"=>[]
          );
          echo json_encode($results);
        }
    }
    else{
        echo "Sin datos";
    }
?>

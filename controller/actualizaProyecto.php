<?php
  header('Access-Control-Allow-Origin: *');
  require('../model/consultas.php');
  session_start();

  if(count($_POST) > 0){
    $codigoDetalleProyecto = $_POST['codigoDetalleProyecto'];
    $nombreDetalleProyecto = $_POST['nombreDetalleProyecto'];
    $inmobiliariaDetalleProyecto = $_POST['inmobiliariaDetalleProyecto'];
    $emailInmobiliariaDetalleProyecto = $_POST['emailInmobiliariaDetalleProyecto'];
    $direccionDetalleProyecto = $_POST['direccionDetalleProyecto'];
    $fono1InmobiliariaDetalleProyecto = $_POST['fono1InmobiliariaDetalleProyecto'];
    $fono2InmobiliariaDetalleProyecto = $_POST['fono2InmobiliariaDetalleProyecto'];
    $fInicioVentaDetalleProyecto = $_POST['fInicioVentaDetalleProyecto'];
    $fFinObraDetalleProyecto = $_POST['fFinObraDetalleProyecto'];
    $fEntregaDetalleProyecto = $_POST['fEntregaDetalleProyecto'];
    $emailDetalleProyecto = $_POST['emailDetalleProyecto'];
    $webDetalleProyecto = $_POST['webDetalleProyecto'];
    $comisionLVPromesaDetalleProyecto = $_POST['comisionLVPromesaDetalleProyecto'];
    $comisionLVEscrituraDetalleProyecto = $_POST['comisionLVEscrituraDetalleProyecto'];
    $comisionVenPromesaDetalleProyecto = $_POST['comisionVenPromesaDetalleProyecto'];
    $comisionVenEscrituraDetalleProyecto = $_POST['comisionVenEscrituraDetalleProyecto'];
    $descuentoSala1DetalleProyecto = $_POST['descuentoSala1DetalleProyecto'];
    $infoDescuento1DetalleProyecto = $_POST['infoDescuento1DetalleProyecto'];
    $descuentoSala2DetalleProyecto = $_POST['descuentoSala2DetalleProyecto'];
    $infoDescuento2DetalleProyecto = $_POST['infoDescuento2DetalleProyecto'];
    $descuentoSala3DetalleProyecto = $_POST['descuentoSala3DetalleProyecto'];
    $bonoVentaDetalleProyecto = $_POST['bonoVentaDetalleProyecto'];
    $cotiza1DetalleProyecto = $_POST['cotiza1DetalleProyecto'];
    $cotiza2DetalleProyecto = $_POST['cotiza2DetalleProyecto'];
    $cotiza3DetalleProyecto = $_POST['cotiza3DetalleProyecto'];
    $estadoDetalleProyecto = $_POST['estadoDetalleProyecto'];
    $infoBonoVentaDetalleProyecto = $_POST['infoBonoVentaDetalleProyecto'];
    $reserva1DetalleProyecto = $_POST['reserva1DetalleProyecto'];
    $reserva2DetalleProyecto = $_POST['reserva2DetalleProyecto'];
    $reserva3DetalleProyecto = $_POST['reserva3DetalleProyecto'];
    $diasCotProyecto = $_POST['diasCotDetalleProyecto'];
    $diasResProyecto = $_POST['diasResDetalleProyecto'];
    $defaultPiePromesa = $_POST['defaultPiePromemsaDetalleProyecto'];
    $defaultPieCuotas = $_POST['defaultPieCuotasDetalleProyecto'];
    $defaultTasa = $_POST['defaultTasaDetalleProyecto'];
    $comisionIVADetalleProyecto = $_POST['comisionIVADetalleProyecto'];
    $unidadesSaltoDetalleProyecto = $_POST['unidadesSaltoDetalleProyecto'];
    $comisionLVPromesaDetalleProyecto2 = $_POST['comisionLVPromesaDetalleProyecto2'];
    $comisionLVEscrituraDetalleProyecto2 = $_POST['comisionLVEscrituraDetalleProyecto2'];
    $comisionVenPromesaDetalleProyecto2 = $_POST['comisionVenPromesaDetalleProyecto2'];
    $comisionVenEscrituraDetalleProyecto2 = $_POST['comisionVenEscrituraDetalleProyecto2'];

    if($diasResProyecto == ''){
      $diasResProyecto = 0;
    }
    if($diasCotProyecto == ''){
      $diasCotProyecto = 0;
    }
    if($defaultPiePromesa == ''){
      $defaultPiePromesa = 0;
    }
    if($defaultPieCuotas == ''){
      $defaultPieCuotas= 0;
    }
    if($defaultTasa == ''){
      $defaultTasa = 0;
    }
    if($fono1InmobiliariaDetalleProyecto == ''){
      $fono1InmobiliariaDetalleProyecto = 0;
    };
    if($fono2InmobiliariaDetalleProyecto == ''){
      $fono2InmobiliariaDetalleProyecto = 0;
    };
    if($comisionLVPromesaDetalleProyecto == ''){
      $comisionLVPromesaDetalleProyecto = 0;
    };
    if($comisionLVEscrituraDetalleProyecto == ''){
      $comisionLVEscrituraDetalleProyecto = 0;
    }
    if($comisionVenPromesaDetalleProyecto == ''){
      $comisionVenPromesaDetalleProyecto = 0;
    }
    if($comisionVenEscrituraDetalleProyecto == ''){
      $comisionVenEscrituraDetalleProyecto = 0;
    }
    if($descuentoSala1DetalleProyecto == ''){
      $descuentoSala1DetalleProyecto = 0;
    }
    if($descuentoSala2DetalleProyecto == ''){
      $descuentoSala2DetalleProyecto = 0;
    }
    if($descuentoSala3DetalleProyecto == ''){
      $descuentoSala3DetalleProyecto = 0;
    }
    if($bonoVentaDetalleProyecto == ''){
      $bonoVentaDetalleProyecto = 0;
    }
    if($fInicioVentaDetalleProyecto == ''){
      $fInicioVentaDetalleProyecto = '1900-01-01';
    }
    else{
      $date = new DateTime($fInicioVentaDetalleProyecto);
      $fInicioVentaDetalleProyecto = $date->format('Y-m-d');
    }
    if($fFinObraDetalleProyecto == ''){
      $fFinObraDetalleProyecto = '1900-01-01';
    }
    else{
      $date = new DateTime($fFinObraDetalleProyecto);
      $fFinObraDetalleProyecto = $date->format('Y-m-d');
    }
    if($fEntregaDetalleProyecto == ''){
      $fEntregaDetalleProyecto = '1900-01-01';
    }
    else{
      $date = new DateTime($fEntregaDetalleProyecto);
      $fEntregaDetalleProyecto = $date->format('Y-m-d');
    }
    if ($comisionIVADetalleProyecto == '') {
      $comisionIVADetalleProyecto = 0;
    }
    if($unidadesSaltoDetalleProyecto == ''){
      $unidadesSaltoDetalleProyecto = 0;
    }
    if($comisionLVPromesaDetalleProyecto2 == ''){
      $comisionLVPromesaDetalleProyecto2 = 0;
    }
    if($comisionLVEscrituraDetalleProyecto2 == ''){
      $comisionLVEscrituraDetalleProyecto2 = 0;
    }
    if($comisionVenPromesaDetalleProyecto2 == ''){
      $comisionVenPromesaDetalleProyecto2 = 0;
    }
    if($comisionVenEscrituraDetalleProyecto2 == ''){
      $comisionVenEscrituraDetalleProyecto2 = 0;
    }

    if(!is_dir("../view/img/imagenes_cliente/proyectos/" . $codigoDetalleProyecto)){
      mkdir("../view/img/imagenes_cliente/proyectos/" . $codigoDetalleProyecto, 0777);
      mkdir("../view/img/imagenes_cliente/proyectos/" . $codigoDetalleProyecto . "/logo",0777);
      mkdir("../view/img/imagenes_cliente/proyectos/" . $codigoDetalleProyecto . "/unidad",0777);
      mkdir("../view/img/imagenes_cliente/proyectos/" . $codigoDetalleProyecto . "/unidad/plano",0777);
      mkdir("../view/img/imagenes_cliente/proyectos/" . $codigoDetalleProyecto . "/unidad/otra",0777);
    }
    if(!is_dir("../repositorio/" . $codigoDetalleProyecto)){
      mkdir("../repositorio/" . $codigoDetalleProyecto, 0777);
      mkdir("../repositorio/" . $codigoDetalleProyecto . "/cotizacion", 0777);
    }

    $path = $_FILES['logoDetalleProyecto']['name'];
    $ext = pathinfo($path, PATHINFO_EXTENSION);
    if($ext != ''){
      $logo =  "../view/img/imagenes_cliente/proyectos/" . $codigoDetalleProyecto . "/logo" . "/" . $codigoDetalleProyecto  . "." . $ext;
    }
    else{
      $logo = '';
    }

    $row = actualizaProyecto($codigoDetalleProyecto,	$nombreDetalleProyecto, $inmobiliariaDetalleProyecto,	$emailInmobiliariaDetalleProyecto,	$direccionDetalleProyecto,	$fono1InmobiliariaDetalleProyecto,	$fono2InmobiliariaDetalleProyecto,	$fInicioVentaDetalleProyecto,	$fFinObraDetalleProyecto,	$fEntregaDetalleProyecto,	$emailDetalleProyecto,	$webDetalleProyecto,	$comisionLVPromesaDetalleProyecto,	$comisionLVEscrituraDetalleProyecto,	$comisionVenPromesaDetalleProyecto,	$comisionVenEscrituraDetalleProyecto,	$descuentoSala1DetalleProyecto,	$infoDescuento1DetalleProyecto,	$descuentoSala2DetalleProyecto,	$infoDescuento2DetalleProyecto,	$descuentoSala3DetalleProyecto,	$bonoVentaDetalleProyecto,	$cotiza1DetalleProyecto, $cotiza2DetalleProyecto, $cotiza3DetalleProyecto, $estadoDetalleProyecto, $logo, $infoBonoVentaDetalleProyecto, $reserva1DetalleProyecto, $reserva2DetalleProyecto, $reserva3DetalleProyecto, $diasCotProyecto, $diasResProyecto, $defaultPiePromesa, $defaultPieCuotas, $defaultTasa, $comisionIVADetalleProyecto, $unidadesSaltoDetalleProyecto, $comisionLVPromesaDetalleProyecto2, $comisionLVEscrituraDetalleProyecto2, $comisionVenPromesaDetalleProyecto2, $comisionVenEscrituraDetalleProyecto2);

    if($row == "Ok")
    {
      move_uploaded_file($_FILES['logoDetalleProyecto']['tmp_name'],$logo);
      echo "Ok¬" . $_POST['codigoDetalleProyecto'];
    }
    else{
      echo "Sin datos";
    }
	}
	else{
		echo "Sin datos";
	}
?>

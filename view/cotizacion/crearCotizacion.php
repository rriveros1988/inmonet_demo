<script>
	function randomTextoLoadJs(){
	  var str = '';
	  var ref = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRST0123456789';
	  for (var i = 0; i < 8 ; i++)
	  {
	    str += ref.charAt(Math.floor(Math.random()*ref.length));
	  }
	  return str;
	}
  var js = document.createElement('script');
  js.src = 'view/js/funciones.js?idLoad=' + randomTextoLoadJs();
  document.getElementsByTagName('head')[0].appendChild(js);
</script>
<div id="idMainCrearCotizacion" style="margin-top: 50pt;">
	<div id="mainCrearCotizacionVistaPrevia" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10pt; margin-bottom: 30pt; display: none;">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
			<div id="divVpTituloCrearCotizacion" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 10pt;">
				<font style="font-size: 16pt; font-weight: bold;">Detalle cotización:&nbsp;Proyecto&nbsp;<font id="tituloProyectoACotizar"></font></font>
			</div>
		</div>
		<div id="divVpCrearCotizacion" style="margin-bottom: 20pt;" class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
			<div style="display: none;" id="contenedorVPCotizacion">
			</div>
			<div style="display: none;" id="contenedorVPCotizacionCorretaje">
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
			<div id="divVpRegresarCrearCotizacion" style="margin-top: 20pt;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
				<button style="width: 100%; margin-bottom: 5pt;" class="btn btn-primary" name="vpRegresarCrearCotizacion" id="vpRegresarCrearCotizacion"><i clasS="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;Regresar</button>
			</div>
			<div id="divVpGuardarCotizacion" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
					<button style="width: 100%; margin-bottom: 5pt;" class="btn btn-primary" name="vpGuardarCotizacion" id="vpGuardarCotizacion"><i clasS="fa fa-save"></i>&nbsp;&nbsp;Guardar</button>
			</div>
			<div id="divVpPDFCotizacion" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
					<button disabled style="width: 100%; margin-bottom: 5pt;" class="btn btn-primary" name="vpPDFCotizacion" id="vpPDFCotizacion"><i clasS="fa fa-file-pdf-o"></i>&nbsp;&nbsp;PDF</button>
			</div>
			<div id="divVpImprimirCotizacion" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
					<button disabled style="width: 100%; margin-bottom: 5pt;" class="btn btn-primary" name="vpImprimirCotizacion" id="vpImprimirCotizacion"><i clasS="fa fa-print"></i>&nbsp;&nbsp;Imprimir</button>
			</div>
			<div id="divVpMailCotizacion" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
					<button disabled style="width: 100%; margin-bottom: 5pt;" class="btn btn-primary" name="vpMailCotizacion" id="vpMailCotizacion"><i clasS="fa fa-envelope-o"></i>&nbsp;&nbsp;Mail</button>
			</div>
			<div id="contenedorIMGVPCotizacion">
			</div>
		</div>
	</div>
		<div id="mainCrearCotizacion" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10pt; margin-bottom: 30pt;">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div id="divTituloCrearCotizacion" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 10pt;">
					<font style="font-size: 16pt; font-weight: bold;">Creación de cotización</font>
				</div>
			</div>
      <div id="datosDeCrearCotizacion" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<form enctype="multipart/form-data" id="formDatosCrearCotizacion" style="text-align: left;">
        <!-- Datos Básicos -->
				<div id="divProyectoCrearCotizacion" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 5pt;">
					<label style="font-weight: normal;">Proyecto</label>
					<select class="form-control custom" type="text" name="proyectoCrearCotizacion" id="proyectoCrearCotizacion"  style="margin-bottom: 5pt; width: 100%;">

					</select>
				</div>
				<div id="divUnidadCrearCotizacion" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 5pt;">
					<label style="font-weight: normal;">Unidad</label>
					<select class="form-control custom" type="text" name="unidadCrearCotizacion" id="unidadCrearCotizacion"  style="margin-bottom: 5pt; width: 100%;">

					</select>
				</div>
				<div id="divAccionCrearCotizacion" class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 5pt;">
					<label style="font-weight: normal;">Acción</label>
					<select class="form-control custom" type="text" name="accionCrearCotizacion" id="accionCrearCotizacion"  style="margin-bottom: 5pt; width: 100%;">

					</select>
				</div>
				<div id="divMedioContactoCrearCotizacion" class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 5pt;">
					<label style="font-weight: normal;">Medio contacto</label>
					<select class="form-control custom" type="text" name="medioContactoCrearCotizacion" id="medioContactoCrearCotizacion"  style="margin-bottom: 5pt; width: 100%;">

					</select>
				</div>
				<div id="divCotizaEnCrearCotizacion" class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 5pt;">
					<label style="font-weight: normal;">Cotiza en</label>
					<select class="form-control custom" type="text" name="cotizaEnContactoCrearCotizacion" id="cotizaEnContactoCrearCotizacion"  style="margin-bottom: 5pt; width: 100%;">

					</select>
				</div>
				<div id="divIzquierdaCrearCotizacion" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div id="divRutClienteCrearCotizacion" class="col-lg-12 col-md-12	col-sm-12 col-xs-12" style="margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0;">
						<label style="font-weight: normal;">RUT Cliente</label>
						<input class="form-control" type="text" name="rutClienteCrearCotizacion" id="rutClienteCrearCotizacion"  style="margin-bottom: 5pt;  width: 100%;" onblur="rellenaDatosCliente();">
	  			</div>
					<div id="divNombresClienteCrearCotizacion" class="col-lg-12 col-md-12	col-sm-12 col-xs-12" style="margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0;">
						<label style="font-weight: normal;">Nombre cliente</label>
						<input class="form-control" type="text" name="nombresClienteCrearCotizacion" id="nombresClienteCrearCotizacion"  style="margin-bottom: 5pt;">
	  			</div>
					<div id="divApellidosClienteCrearCotizacion" class="col-lg-12 col-md-12	col-sm-12 col-xs-12" style="margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0;">
						<label style="font-weight: normal;">Apellidos Cliente</label>
						<input class="form-control" type="text" name="apellidosClienteCrearCotizacion" id="apellidosClienteCrearCotizacion"  style="margin-bottom: 5pt;">
	  			</div>
					<div id="divCelularClienteCrearCotizacion" class="col-lg-12 col-md-12	col-sm-12 col-xs-12" style="margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0;">
						<label style="font-weight: normal;">Celular cliente</label>
						<input class="form-control" type="text" name="celularClienteCrearCotizacion" id="celularClienteCrearCotizacion"  style="margin-bottom: 5pt;">
	  			</div>
					<div id="divEmailClienteCrearCotizacion" class="col-lg-12 col-md-12	col-sm-12 col-xs-12" style="margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0;">
						<label style="font-weight: normal;">E-Mail cliente</label>
						<input class="form-control" type="text" name="emailClienteCrearCotizacion" id="emailClienteCrearCotizacion"  style="margin-bottom: 5pt;" onblur="validacionEmail4();">
	  			</div>
				</div>
				<div id="divDerechaCrearCotizacion" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div id="divBodegasCrearCotizacion" class="col-lg-12 col-md-12	col-sm-12 col-xs-12" style="margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; margin-bottom: 10pt;">
						<label style="font-weight: normal;">Bodegas</label>
						<select multiple="multiple" id="bodegasCrearCotizacion" name="bodegasCrearCotizacion[]">

						</select>
					</div>
					<div id="divEstacionamientosCrearCotizacion" class="col-lg-12 col-md-12	col-sm-12 col-xs-12" style="margin-left: 0; margin-right: 0; padding-left: 0; padding-right: 0; margin-bottom: 10pt;">
						<label style="font-weight: normal;">Estacionamientos</label>
						<select multiple="multiple" id="estacionamientosCrearCotizacion" name="estacionamientosCrearCotizacion[]">

						</select>
					</div>
				</div>
				<div id="divTituloDatosBasicosProyectoCrear" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 5pt;">
					<hr class="lineaSeparadora">
					<font style="font-weight: bold; font-size: 14pt;">Formas de pago</font>
        </div>
				<div id="divReservaClienteCrearCotizacion" class="col-lg-2 col-md-2	col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 5pt;">
					<label style="font-weight: normal;">Valor reserva (UF)</label>
					<input disabled class="form-control" type="text" name="reservaClienteCrearCotizacion" id="reservaClienteCrearCotizacion"  style="margin-bottom: 5pt;">
				</div>
				<div id="divPiePromesaClienteCrearCotizacion" class="col-lg-2 col-md-2	col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 5pt;">
					<label style="font-weight: normal;">Pie promesa (%)</label>
					<input class="form-control" type="number" step="1"  value="5" min="0" max="100" name="piePromesaClienteCrearCotizacion" id="piePromesaClienteCrearCotizacion"  style="margin-bottom: 5pt;" />
				</div>
				<div id="divdPieCuotasClienteCrearCotizacion" class="col-lg-2 col-md-2	col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 5pt;">
					<label style="font-weight: normal;">Pie cuotas (%)</label>
					<input class="form-control" type="number" step="1"  value="15" min="1" max="100" name="pieCuotasClienteCrearCotizacion" id="pieCuotasClienteCrearCotizacion"  style="margin-bottom: 5pt;">
				</div>
				<div id="divCuotasCrearCotizacion" class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 5pt;">
					<label style="font-weight: normal;">Cantidad cuotas (N°)</label>
					<input class="form-control" type="number" step="1" value="1" min="1" name="cuotasCrearCotizacion" id="cuotasCrearCotizacion"  style="margin-bottom: 5pt;">
				</div>
				<div id="divSaldoCrearCotizacion" class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 5pt;">
					<label style="font-weight: normal;">Saldo (%)</label>
					<input disabled class="form-control" type="number" step="1"  value="80" min="0" max="100" name="saldoCrearCotizacion" id="saldoCrearCotizacion"  style="margin-bottom: 5pt;">
				</div>
				<div id="divTotalCrearCotizacion" class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 5pt;">
					<label style="font-weight: normal;">Total (%)</label>
					<input disabled class="form-control" type="number" step="1"  value="100" min="0" max="100" name="totalCrearCotizacion" id="totalCrearCotizacion"  style="margin-bottom: 5pt;">
				</div>
				<div id="divDescuento1ClienteCrearCotizacion" class="col-lg-2 col-md-2	col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 5pt;">
					<label style="font-weight: normal;">Descuento sala (%)</label>
					<select class="form-control custom" type="text" name="descuento1ClienteCrearCotizacion" id="descuento1ClienteCrearCotizacion"  style="margin-bottom: 5pt;">

					</select>
				</div>
				<div id="divDescuento2ClienteCrearCotizacion" class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 5pt;">
					<label style="font-weight: normal;">Descuento especial (%)</label>
					<select class="form-control custom" type="text" name="descuento2ClienteCrearCotizacion" id="descuento2ClienteCrearCotizacion"  style="margin-bottom: 5pt;">

					</select>
				</div>
				<div id="divTazaReferenciaCrearCotizacion" class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 5pt;">
					<label style="font-weight: normal;">Tasa hipotecaria (%)</label>
					<select class="form-control custom" type="number" step="0.5" min="2" max="5.5" name="tazaReferenciaCrearCotizacion" id="tazaReferenciaCrearCotizacion"  style="margin-bottom: 5pt;">

					</select>
				</div>
				<div id="divBonoVetaCrearCotizacion" class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: left; margin-bottom: 5pt;">
					<label style="font-weight: normal;">Bono (%)</label>
					<input disabled class="form-control" type="text" name="bonoVentaCrearCotizacion" id="bonoVentaCrearCotizacion"  style="margin-bottom: 5pt;">
				</div>
			</form>
      </div>
      <!-- Botones -->
      <div id="botonesCrearCotizacion" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 10pt;">
        <div id="divGenerarCrearCotizacion" class="col-lg-2 col-md-2 col-sm-12 col-xs-12" >
          <button style="width: 100%; margin-bottom: 5pt;" class="btn btn-primary" name="generarCrearCotizacion" id="generarCrearCotizacion"><i clasS="fa fa-file-o"></i>&nbsp;&nbsp;Generar</button>
        </div>
        <div id="divLimpiarCrearCotizacion" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <button style="width: 100%; margin-bottom: 5pt;" class="btn btn-primary" name="limpiarCrearCotizacion" id="limpiarCrearCotizacion"><i class="glyphicon glyphicon-erase"></i>&nbsp;&nbsp;Limpiar datos cliente</button>
        </div>
      </div>
		</div>
</div>

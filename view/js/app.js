var app = angular.module("DCMAPP", ["ngRoute"]);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when("/home", {
        controller: "homeController",
        controllerAs: "vm",
        templateUrl : "view/home/home.html"
    })
    .when("/logged", {
        controller: "loggedController",
        controllerAs: "vm",
        templateUrl : "view/home/logged.html"
    })
    .when("/crearCliente", {
        controller: "crearClienteController",
        controllerAs: "vm",
        templateUrl : "view/cliente/crearCliente.html"
    })
    .when("/crearProyecto", {
        controller: "crearProyectoController",
        controllerAs: "vm",
        templateUrl : "view/proyecto/crearProyecto.html"
    })
    .when("/listadoProyectos", {
        controller: "listadoProyectosController",
        controllerAs: "vm",
        templateUrl : "view/proyecto/listadoProyectos.html"
    })
    .when("/listadoCotizaciones", {
        controller: "listadoCotizacionesController",
        controllerAs: "vm",
        templateUrl : "view/cotizacion/listadoCotizaciones.html"
    })
    .when("/detalleProyecto", {
        controller: "detalleProyectoController",
        controllerAs: "vm",
        templateUrl : "view/proyecto/detalleProyecto.html"
    })
    .when("/unidadesProyecto", {
        controller: "unidadesProyectoController",
        controllerAs: "vm",
        templateUrl : "view/proyecto/unidadesProyecto.html"
    })
    .when("/crearUnidadProyecto", {
        controller: "crearUnidadProyectoController",
        controllerAs: "vm",
        templateUrl : "view/proyecto/crearUnidadProyecto.html"
    })
    .when("/detalleUnidadProyecto", {
        controller: "detalleUnidadProyectoController",
        controllerAs: "vm",
        templateUrl : "view/proyecto/detalleUnidadProyecto.html"
    })
    .when("/listadoClientes", {
        controller: "listadoClientesController",
        controllerAs: "vm",
        templateUrl : "view/cliente/listadoClientes.html"
    })
    .when("/detalleCliente", {
        controller: "detalleClienteController",
        controllerAs: "vm",
        templateUrl : "view/cliente/detalleCliente.html"
    })
    .when("/listadoUsuarios", {
        controller: "listadoUsuariosController",
        controllerAs: "vm",
        templateUrl : "view/usuario/listadoUsuarios.html"
    })
    .when("/crearUsuario", {
        controller: "crearUsuarioController",
        controllerAs: "vm",
        templateUrl : "view/usuario/crearUsuario.html"
    })
    .when("/detalleUsuario", {
        controller: "detalleUsuarioController",
        controllerAs: "vm",
        templateUrl : "view/usuario/detalleUsuario.html"
    })
    .when("/asignacionProyecto", {
        controller: "asignacionProyectoController",
        controllerAs: "vm",
        templateUrl : "view/usuario/asignacionProyecto.html"
    })
    .when("/crearCotizacion", {
        controller: "crearCotizacionController",
        controllerAs: "vm",
        templateUrl : "view/cotizacion/crearCotizacion.html"
    })
    .when("/listadoCotizacionesReserva", {
        controller: "listadoCotizacionesReservaController",
        controllerAs: "vm",
        templateUrl : "view/reserva/listadoCotizacionesReserva.html"
    })
    .when("/listadoReservas", {
        controller: "listadoReservasController",
        controllerAs: "vm",
        templateUrl : "view/reserva/listadoReservas.html"
    })
    .when("/detalleReserva", {
        controller: "detalleReservaController",
        controllerAs: "vm",
        templateUrl : "view/reserva/detalleReserva.html"
    })
    .when("/listadoReservasPromesa",{
        controller: "listadoReservasPromesaController",
        controllerAs: "vm",
        templateUrl : "view/promesa/listadoReservasPromesa.html"
    })
    .when("/listadoPromesas",{
        controller: "listadoPromesasController",
        controllerAs: "vm",
        templateUrl : "view/promesa/listadoPromesas.html"
    })
    .when("/detallePromesa", {
        controller: "detallePromesaController",
        controllerAs: "vm",
        templateUrl : "view/promesa/detallePromesa.html"
    })
    .when("/listadoPromesasEscritura",{
        controller: "listadoPromesasEscrituraController",
        controllerAs: "vm",
        templateUrl : "view/escritura/listadoPromesasEscritura.html"
    })
    .when("/listadoEscrituras",{
        controller: "listadoEscriturasController",
        controllerAs: "vm",
        templateUrl : "view/escritura/listadoEscrituras.html"
    })
    .when("/detalleEscritura", {
        controller: "detalleEscrituraController",
        controllerAs: "vm",
        templateUrl : "view/escritura/detalleEscritura.html"
    })
    .when("/listadoContable", {
        controller: "listadoContableController",
        controllerAs: "vm",
        templateUrl : "view/contabilidad/listadoContable.html"
    })
    .when("/verUnidadContable",{
        controller: "verUnidadContableController",
        controllerAs: "vm",
        templateUrl : "view/contabilidad/verUnidadContable.html"
    })
    .when("/listadoComisiones", {
        controller: "listadoComisionesController",
        controllerAs: "vm",
        templateUrl : "view/comisiones/listadoComisiones.html"
    })
    .when("/listadoComisionesVendedor",{
        controller: "listadoComisionesVendedorController",
        controllerAs: "vm",
        templateUrl : "view/comisiones/listadoComisionesVendedor.html"
    })
    .when("/listadoComisionesEmpresa",{
      controller : "listadoComisionesEmpresaController",
      controllerAs: "vm",
      templateUrl : "view/comisiones/listadoComisionesEmpresa.html"
    })
    .when("/comisionesEmpresa",{
      controller : "comisionesEmpresaController",
      controllerAs: "vm",
      templateUrl : "view/comisiones/comisionesEmpresa.html"
    })
    .otherwise({redirectTo: '/logged'});

    $locationProvider.hashPrefix('');
});

app.controller("loggedController", function(){
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
  $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
  $('#modalAlertas').modal('show');
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != "" && p[4] != "Bloqueado"){
          setTimeout(function(){
              if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                  if(($(window).height() - 200) > 330){
                      $("#mainLogged").height($(window).height() - 200);
                  }
                  else{
                      $("#mainLogged").height(330);
                  }
              }
              else{
                $("#mainLogged").height($(window).height() - 150);
              }

              $('#header').fadeIn();
              $('#footer').fadeIn();
              $('#contenido').fadeIn();

              setTimeout(function(){
                $('#modalAlertas').modal('hide');
              },1000);
          },1600);
        }
        else{
          window.location.href = "#/home";
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("homeController", function(){
    $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
    $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
    $('#modalAlertas').modal('show');
    $.ajax({
      url:   'controller/limpia_session.php',
      type:  'post',
      success:  function (response) {

      }
    });
    $.ajax({
      url:   'controller/datosRefresh.php',
      type:  'post',
      success:  function (response) {
        var p = response.split(",");
        if(response.localeCompare("Sin datos")!= 0 && response != ""){
          var dataset = [];
          if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
            var URLactual = window.location;
            url = URLactual.toString().split("#");
            url2 = url[0].split('?');
            console.log(url2);
            urlSave = p[3].toString().split("#");
            urlSave2 = urlSave[0].split("?");
            console.log(urlSave2);
            if(url2[0] !== urlSave2[0]){
              window.location.href = "#/home";
              $('#contenido').fadeIn();
              $('#header').fadeIn();
              $('#footer').fadeIn();
              setTimeout(function(){
                $('#modalAlertas').modal('hide');
                $("#loginSystem").show("slide", {direction: "up"}, 800);
              },1000);
            }
            else{
              window.location.href = "#/logged";
              setTimeout(function(){
                  $('#contenido').fadeIn();
                  $('#header').fadeIn();
                  $('#footer').fadeIn();
                  setTimeout(function(){
                    $('#modalAlertas').modal('hide');
                  },1000);
              },1500);
            }
          }
        }
        else{
          setTimeout(function(){
              $('#contenido').fadeIn();
              $('#header').fadeIn();
              $('#footer').fadeIn();
              setTimeout(function(){
                $('#modalAlertas').modal('hide');
                $("#loginSystem").show("slide", {direction: "up"}, 800);
              },1000);
          },1500);
          window.location.href = "#/home";
        }
      }
    });
});

app.controller("construccionController", function(){
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
    $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
    $('#modalAlertas').modal('show');
    $.ajax({
      url:   'controller/limpia_session.php',
      type:  'post',
      success:  function (response) {

      }
    });
    setTimeout(function(){
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            if(($(window).height() - 200) > 330){
                $("#mainConstruccion").height($(window).height() - 200);
            }
            else{
                $("#mainConstruccion").height(330);
            }
        }
        else{
          $("#mainConstruccion").height($(window).height() - 150);
        }
        $('#modalAlertas').modal('hide');
        $('#contenido').fadeIn();
        $('#header').fadeIn();
        $('#footer').fadeIn();
    },1500);
});

app.controller("crearUsuarioController", function(){
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
    $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
    $('#modalAlertas').modal('show');
    $.ajax({
      url:   'controller/limpia_session.php',
      type:  'post',
      success:  function (response) {

      }
    });
    $.ajax({
      url:   'controller/datosRefresh.php',
      type:  'post',
      success:  function (response) {
        var p = response.split(",");
        if(response.localeCompare("Sin datos")!= 0 && response != ""){
          var dataset = [];
          if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
            setTimeout(function(){
                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                  $("#idMainCrearUsuario").css("margin-top","50pt");
                }
                else{
                  $.ajax({
                    url:   'controller/datosPerfil.php',
                    type:  'post',
                    success:  function (response) {
                      var p = response.split(",");
                      var cuerpo = '';
                      if(response.localeCompare("Sin datos")!= 0 && response != ""){
                        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                          for(var i = 0; i < p.length; i=i+2){
                            cuerpo = cuerpo + '<option value="' + p[i] + '">' + p[i+1] + '</option>';
                          }
                          setTimeout(function(){
                            $("#perfilCrearUsuario").html(cuerpo);
                          },500);
                        }

                        $("#passCrearUsuario").val(randomTexto());

                        $('#contenido').fadeIn();
                        $('#header').fadeIn();
                        $('#footer').fadeIn();

                        setTimeout(function(){
                          $('#modalAlertas').modal('hide');
                        },1000);
                      }
                    }
                  });
                }
            },1500);
          }
        }
        else{
          window.location.href = "#/home";
        }
      }
    });
});

app.controller("crearClienteController", function(){
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
    $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
    $('#modalAlertas').modal('show');
    $.ajax({
      url:   'controller/limpia_session.php',
      type:  'post',
      success:  function (response) {

      }
    });
    $.ajax({
      url:   'controller/datosRefresh.php',
      type:  'post',
      success:  function (response) {
        var p = response.split(",");
        if(response.localeCompare("Sin datos")!= 0 && response != ""){
          var dataset = [];
          if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
            setTimeout(function(){
                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                  $("#idMainCrearCliente").css("margin-top","50pt");
                }
                else{

                }

                $.ajax({
                  url:   'controller/datosEstadosCliente.php',
                  type:  'post',
                  success:  function (response) {
                    var p = response.split(",");
                    var cuerpo = '';
                    if(response.localeCompare("Sin datos")!= 0 && response != ""){
                      if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                        for(var i = 0; i < p.length; i=i+2){
                          cuerpo = cuerpo + '<option value="' + p[i] + '">' + p[i+1] + '</option>';
                        }
                        setTimeout(function(){
                          $("#estadoCrearCliente").html(cuerpo);
                        },500);
                      }

                      $.ajax({
                        url:   'controller/datosEstadoCivil.php',
                        type:  'post',
                        success:  function (response) {
                          var p = response.split(",");
                          var cuerpo = '';
                          if(response.localeCompare("Sin datos")!= 0 && response != ""){
                            if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                              for(var i = 0; i < p.length; i=i+2){
                                cuerpo = cuerpo + '<option value="' + p[i] + '">' + p[i+1] + '</option>';
                              }
                              setTimeout(function(){
                                $("#estadoCivilCrearCliente").html(cuerpo);
                              },500);
                            }

                            $.ajax({
                              url:   'controller/datosNivelEducacional.php',
                              type:  'post',
                              success:  function (response) {
                                var p = response.split(",");
                                var cuerpo = '';
                                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                    for(var i = 0; i < p.length; i=i+2){
                                      cuerpo = cuerpo + '<option value="' + p[i] + '">' + p[i+1] + '</option>';
                                    }
                                    setTimeout(function(){
                                      $("#nivelEducacionalCrearCliente").html(cuerpo);
                                    },500);
                                  }

                                  $.ajax({
                                    url:   'controller/datosCasaHabita.php',
                                    type:  'post',
                                    success:  function (response) {
                                      var p = response.split(",");
                                      var cuerpo = '';
                                      if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                          for(var i = 0; i < p.length; i=i+2){
                                            cuerpo = cuerpo + '<option value="' + p[i] + '">' + p[i+1] + '</option>';
                                          }
                                          setTimeout(function(){
                                            $("#casaHabitacionalCrearCliente").html(cuerpo);
                                          },500);
                                        }

                                        $.ajax({
                                          url:   'controller/datosMotivoCompra.php',
                                          type:  'post',
                                          success:  function (response) {
                                            var p = response.split(",");
                                            var cuerpo = '';
                                            if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                              if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                                for(var i = 0; i < p.length; i=i+2){
                                                  cuerpo = cuerpo + '<option value="' + p[i] + '">' + p[i+1] + '</option>';
                                                }
                                                setTimeout(function(){
                                                  $("#motivoCrearCliente").html(cuerpo);
                                                },500);
                                              }

                                              //Agregado de funciones a inputs
                                              $("#fNacCrearCliente").datepicker({
                                                dateFormat: "dd-mm-yy",
                                                changeMonth: true,
                                                changeYear: true,
                                                minDate: new Date(1920, 0, 1),
                                                yearRange: '1920:2018'
                                              });

                                              $("#fAltaCrearCliente").datepicker({
                                                dateFormat: "dd-mm-yy",
                                                changeMonth: true,
                                                changeYear: true
                                              });

                                              $("#EMFIngresoCrearCliente").datepicker({
                                                dateFormat: "dd-mm-yy",
                                                changeMonth: true,
                                                changeYear: true,
                                                minDate: new Date(1920, 0, 1),
                                                yearRange: '1920:2018'
                                              });

                                              var date = new Date();

                                              var day = date.getDate();
                                              if(day < 10){
                                                day = "0" + day;
                                              }
                                              var month = date.getMonth() + 1;
                                              if(month < 10){
                                                month = "0" + month;
                                              }
                                              var year = date.getFullYear();

                                              $("#fAltaCrearCliente").val(day + "-" + month + "-" + year);

                                              var datosDetalle = $("#datosDeCrearCliente");
                                              $(datosDetalle).find('label').each(function() {
                                                $(this).css("font-weight","normal");
                                              });

                                              $('#contenido').fadeIn();
                                              $('#header').fadeIn();
                                              $('#footer').fadeIn();

                                              setTimeout(function(){
                                                $('#modalAlertas').modal('hide');
                                              },1000);
                                            }
                                          }
                                        });
                                      }
                                    }
                                  });
                                }
                              }
                            });
                          }
                        }
                      });
                    }
                  }
                });
            },1500);
          }
        }
        else{
          window.location.href = "#/home";
        }
      }
    });
});

app.controller("crearCotizacionController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
  $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
  $('#modalAlertas').modal('show');
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
              if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                $("#idMainCrearCotizacion").css("margin-top","50pt");
                $("#mainCrearCotizacionVistaPrevia").css("margin-top","50pt");
                $("#divBodegasCrearCotizacion").css("margin-left","0pt");
                $("#divEstacionamientosCrearCotizacion").css("margin-left","0pt");
                $("#divRutClienteCrearCotizacion").css("margin-left","0pt");
                $("#divNombresClienteCrearCotizacion").css("margin-left","0pt");
                $("#divApellidosClienteCrearCotizacion").css("margin-left","0pt");
                $("#divCelularClienteCrearCotizacion").css("margin-left","0pt");
                $("#divEmailClienteCrearCotizacion").css("margin-left","0pt");
                $("#divVpTituloCrearCotizacion").css("text-align","center");
              }
              else{
              }
              var parametros3;
              $.ajax({
                url:   'controller/datosProyectosMiUsuario.php',
                type:  'post',
                success:  function (response) {
                  var p = response.split(",");
                  var cuerpo = "";
                  if(response.localeCompare("Sin datos")!= 0 && response != ""){
                    if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                      for(var i = 0; i < p.length; i=i+4){
                        cuerpo += "<option value=" + p[i] + ">" + p[i+1] + "</option>";
                      }
                    }
                    $("#proyectoCrearCotizacion").html(cuerpo);

                    var parametros = {
                      "codigoProyecto": p[0]
                    }

                    var defaultPiePromesa = 0;
                    var defaultPieCuotas = 0;
                    var defaultTasa = 0;

                    $.ajax({
                      url:   'controller/datosProyectoEspecifico.php',
                      type:  'post',
                      data:  parametros,
                      success:  function (response) {
                        var p = response.split("¬");
                        if(response.localeCompare("Sin datos")!= 0 && response != ""){
                          if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                             if(p[33] !== ''){
                               defaultPiePromesa = p[33];
                             }
                             if(p[34] !== ''){
                               defaultPieCuotas = p[34];
                             }
                             if(p[35] !== ''){
                               defaultTasa = p[35];
                             }
                             if(p[9] !== ''){
                               var past_date =  new Date();
                               var current_date = new Date(p[9].replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                               var difference = (current_date.getFullYear()*12 + current_date.getMonth()) - (past_date.getFullYear()*12 + past_date.getMonth());
                               if(difference < 0){
                                 difference = 0;
                                 $("#cuotasCrearCotizacion").attr("max",difference);
                                 $("#cuotasCrearCotizacion").val(difference);
                               }
                               else{
                                 $("#cuotasCrearCotizacion").attr("max",difference);
                                 $("#cuotasCrearCotizacion").val(difference);
                               }
                             }
                             else{
                               $("#cuotasCrearCotizacion").attr("max",1);
                               $("#cuotasCrearCotizacion").val(1);
                             }
                          }
                        }
                      }
                    });

                    $.ajax({
                      url:   'controller/datosClientes.php',
                      type:  'post',
                      success:  function (response) {
                        var p = response.split("¬");
                        var listaClientes = [];
                        if(response.localeCompare("Sin datos")!= 0 && response != ""){
                          if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                            for(var i = 0; i < p.length; i=i+7){
                              listaClientes.push(p[i]);
                            }
                          }
                          $('#rutClienteCrearCotizacion').autocomplete({
                            // req will contain an object with a "term" property that contains the value
                            // currently in the text input.  responseFn should be invoked with the options
                            // to display to the user.
                            source: function (req, responseFn) {
                              // Escape any RegExp meaningful characters such as ".", or "^" from the
                              // keyed term.
                              var term = $.ui.autocomplete.escapeRegex(req.term),
                                // '^' is the RegExp character to match at the beginning of the string.
                                // 'i' tells the RegExp to do a case insensitive match.
                                matcher = new RegExp('^' + term, 'i'),
                                // Loop over the options and selects only the ones that match the RegExp.
                                matches = $.grep(listaClientes, function (item) {
                                  return matcher.test(item);
                                });
                              // Return the matched options.
                              responseFn(matches);
                            }
                          });
                        }
                      }
                    });

                    var parametros2 = {
                      "codigoProyecto": parametros['codigoProyecto']
                    }

                    $.ajax({
                      url:   'controller/datosAccionCotiza.php',
                      data:  parametros2,
                      type:  'post',
                      success:  function (response) {
                        var p = response.split(",");
                        var valorAccion = "";
                        console.log(p);
                        if(response.localeCompare("Sin datos")!= 0 && response != ""){
                          if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                            for(var i = 0; i < p.length; i=i+1){
                              valorAccion += "<option value=" + p[i] + ">" + p[i] + "</option>";
                            }
                            $("#accionCrearCotizacion").html(valorAccion);

                            if($("#accionCrearCotizacion").val() === "Arriendo"){
                              $("#descuento1ClienteCrearCotizacion").html("<option value=0>0</option>");
                              $("#descuento2ClienteCrearCotizacion").html("<option value=0>0</option>");
                              $("#bonoVentaCrearCotizacion").val(0);
                              $("#tazaReferenciaCrearCotizacion").html("<option value=0>0</option>");
                              $("#descuento1ClienteCrearCotizacion").attr("disabled","disabled");
                              $("#descuento2ClienteCrearCotizacion").attr("disabled","disabled");
                              $("#bonoVentaCrearCotizacion").attr("disabled","disabled");
                              $("#tazaReferenciaCrearCotizacion").attr("disabled","disabled");
                            }
                            else{
                              $.ajax({
                                url:   'controller/datosDesBono.php',
                                data:  parametros,
                                type:  'post',
                                success:  function (response) {
                                  var p = response.split(",");
                                  var cuerpoDes1 = "";
                                  var cuerpoDes2 = "";
                                  if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                    if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                      for(var i = 0; i <= parseFloat(p[0]); i=i+0.5){
                                        if(i === 3){
                                          cuerpoDes1 += "<option value=" + i + " selected>" + i + "</option>";
                                        }
                                        else{
                                          cuerpoDes1 += "<option value=" + i + ">" + i + "</option>";
                                        }
                                      }
                                      for(var j = 0; j <= parseFloat(p[1]); j=j+0.5){
                                        if(j === 0){
                                          cuerpoDes2 += "<option value=" + j + " selected>" + j + "</option>";
                                        }
                                        else{
                                          cuerpoDes2 += "<option value=" + j + ">" + j + "</option>";
                                        }
                                      }
                                    }
                                    $("#descuento1ClienteCrearCotizacion").html(cuerpoDes1);
                                    $("#descuento2ClienteCrearCotizacion").html(cuerpoDes2);
                                    $("#bonoVentaCrearCotizacion").val(parseFloat(p[2]));
                                  }
                                }
                              });
                              var taza = "";
                              for(var i = 2; i <= parseFloat(5.5); i=i+0.5){
                                if(i === 4){
                                  taza += "<option value=" + i + " selected>" + i + "</option>";
                                }
                                else{
                                  taza += "<option value=" + i + ">" + i + "</option>";
                                }
                              }
                              $("#tazaReferenciaCrearCotizacion").html(taza);
                              $("#tazaReferenciaCrearCotizacion").val(defaultTasa);
                            }

                            parametros.accionUnidad = $("#accionCrearCotizacion").val();

                            $.ajax({
                              url:   'controller/datosUnidadesProyectoCotizacion.php',
                              data:  parametros,
                              type:  'post',
                              success:  function (response) {
                                var p = response.split("¬");
                                var cuerpo2 = "";
                                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                    for(var i = 0; i < p.length; i=i+5){
                                      if(parametros['codigoProyecto'] === "COR"){
                                        cuerpo2 += "<option value=" + p[i] + ">" + (p[i] + ' - ' + p[i+4] + ' - ' + p[i+2] + ' - ' + p[i+3]) + "</option>";
                                      }
                                      else{
                                        cuerpo2 += "<option value=" + p[i] + ">" + (p[i] + ' - ' + p[i+4] + ' - ' + p[i+2]) + "</option>";
                                      }
                                    }
                                  }
                                  $("#unidadCrearCotizacion").html(cuerpo2);

                                  var parametros3 = {
                                    "codigoProyecto": parametros2['codigoProyecto'],
                                    "codigoUnidad": p[0],
                                    "accionUnidad": $("#accionCrearCotizacion").val()
                                  }

                                  $.ajax({
                                    url:   'controller/datosMedioContacto.php',
                                    type:  'post',
                                    success:  function (response) {
                                      var p = response.split(",");
                                      var cuerpo3 = "";
                                      if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                          for(var i = 0; i < p.length; i=i+2){
                                            cuerpo3 += "<option value=" + p[i] + ">" + p[i+1] + "</option>";
                                          }
                                        }
                                        $("#medioContactoCrearCotizacion").html(cuerpo3);

                                              $.ajax({
                                                url:   'controller/datosBodegasDisponiblesProyecto.php',
                                                data:  parametros,
                                                type:  'post',
                                                success:  function (response) {
                                                  var p = response.split(",");
                                                  var cuerpo6 = "";
                                                  if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                                    if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                                      for(var i = 0; i < p.length; i=i+3){
                                                        cuerpo6 += "<option value=" + p[i] + ">" + (p[i+1] + ' - ' + p[i+2] + 'UF') + "</option>";
                                                      }
                                                    }
                                                    $("#bodegasCrearCotizacion").html(cuerpo6);

                                                    $.ajax({
                                                      url:   'controller/datosEstacionamientosDisponiblesProyecto.php',
                                                      data:  parametros,
                                                      type:  'post',
                                                      success:  function (response) {
                                                        var p = response.split(",");
                                                        var cuerpo6 = "";
                                                        if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                                          if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                                            for(var i = 0; i < p.length; i=i+3){
                                                              cuerpo6 += "<option value=" + p[i] + ">" + (p[i+1] + ' - ' + p[i+2] + 'UF') + "</option>";
                                                            }
                                                          }
                                                          $("#estacionamientosCrearCotizacion").html(cuerpo6);

                                                          $.ajax({
                                                            url:   'controller/datosCotizaEn.php',
                                                            type:  'post',
                                                            success:  function (response) {
                                                              var p = response.split(",");
                                                              var cuerpo7 = "";
                                                              if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                                                if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                                                  for(var i = 0; i < p.length; i=i+2){
                                                                    cuerpo7 += "<option value=" + p[i] + ">" + p[i+1] + "</option>";
                                                                  }
                                                                }

                                                                $("#cotizaEnContactoCrearCotizacion").html(cuerpo7);

                                                                if($("#accionCrearCotizacion").val() === "Arriendo"){
                                                                  $("#piePromesaClienteCrearCotizacion").val(0);
                                                                  $("#pieCuotasClienteCrearCotizacion").val(0);
                                                                  $("#piePromesaClienteCrearCotizacion").attr("disabled","disabled");
                                                                  $("#pieCuotasClienteCrearCotizacion").attr("disabled","disabled");
                                                                  $("#saldoCrearCotizacion").val(100);
                                                                  $("#reservaClienteCrearCotizacion").val(0);
                                                                  $("#cuotasCrearCotizacion").val(1);
                                                                  setTimeout(function(){
                                                                      $('#contenido').fadeIn();
                                                                      $('#header').fadeIn();
                                                                      $('#footer').fadeIn();
                                                                      $("#bodegasCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });
                                                                      $("#estacionamientosCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });

                                                                      $('#modalAlertas').modal('hide');
                                                                    },1000);
                                                                }
                                                                else{
                                                                  if($("#accionCrearCotizacion").val() === "Leasing"){
                                                                    $("#pieCuotasClienteCrearCotizacion").val(0);
                                                                    $("#pieCuotasClienteCrearCotizacion").attr("disabled","disabled");
                                                                    $("#piePromesaClienteCrearCotizacion").val(15);
                                                                    $("#piePromesaClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#saldoCrearCotizacion").val(85);
                                                                  }
                                                                  else{
                                                                    $("#pieCuotasClienteCrearCotizacion").val(defaultPieCuotas);
                                                                    $("#pieCuotasClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#piePromesaClienteCrearCotizacion").val(defaultPiePromesa);
                                                                    $("#piePromesaClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#saldoCrearCotizacion").val(100 - $("#pieCuotasClienteCrearCotizacion").val() - $("#piePromesaClienteCrearCotizacion").val());
                                                                  }
                                                                  $.ajax({
                                                                    url:   'controller/datosValorReserva.php',
                                                                    //data: parametros3,
                                                                    data:  parametros,
                                                                    type:  'post',
                                                                    success:  function (response) {
                                                                      var p = response.split(",");
                                                                      var cuerpo7 = "";
                                                                      if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                                                        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                                                            $("#reservaClienteCrearCotizacion").val(p[0]);
                                                                          }
                                                                          setTimeout(function(){
                                                                            $('#contenido').fadeIn();
                                                                            $('#header').fadeIn();
                                                                            $('#footer').fadeIn();
                                                                            $("#bodegasCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $("#estacionamientosCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });

                                                                            $('#modalAlertas').modal('hide');
                                                                          },1000);
                                                                        }
                                                                        else{
                                                                          $("#reservaClienteCrearCotizacion").val(0);
                                                                          setTimeout(function(){
                                                                            $('#contenido').fadeIn();
                                                                            $('#header').fadeIn();
                                                                            $('#footer').fadeIn();
                                                                            $("#bodegasCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $("#estacionamientosCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $('#modalAlertas').modal('hide');
                                                                          },1000);
                                                                        }
                                                                      }
                                                                    });
                                                                }
                                                              }
                                                              else{
                                                                if($("#accionCrearCotizacion").val() === "Arriendo"){
                                                                  $("#piePromesaClienteCrearCotizacion").val(0);
                                                                  $("#pieCuotasClienteCrearCotizacion").val(0);
                                                                  $("#piePromesaClienteCrearCotizacion").attr("disabled","disabled");
                                                                  $("#pieCuotasClienteCrearCotizacion").attr("disabled","disabled");
                                                                  $("#saldoCrearCotizacion").val(100);
                                                                  $("#reservaClienteCrearCotizacion").val(0);
                                                                  $("#cuotasCrearCotizacion").val(1);
                                                                  setTimeout(function(){
                                                                      $('#contenido').fadeIn();
                                                                      $('#header').fadeIn();
                                                                      $('#footer').fadeIn();
                                                                      $("#bodegasCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });
                                                                      $("#estacionamientosCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });

                                                                      $('#modalAlertas').modal('hide');
                                                                    },1000);
                                                                }
                                                                else{
                                                                  if($("#accionCrearCotizacion").val() === "Leasing"){
                                                                    $("#pieCuotasClienteCrearCotizacion").val(0);
                                                                    $("#pieCuotasClienteCrearCotizacion").attr("disabled","disabled");
                                                                    $("#piePromesaClienteCrearCotizacion").val(15);
                                                                    $("#piePromesaClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#saldoCrearCotizacion").val(85);
                                                                  }
                                                                  else{
                                                                    $("#pieCuotasClienteCrearCotizacion").val(defaultPieCuotas);
                                                                    $("#pieCuotasClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#piePromesaClienteCrearCotizacion").val(defaultPiePromesa);
                                                                    $("#piePromesaClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#saldoCrearCotizacion").val(100 - $("#pieCuotasClienteCrearCotizacion").val() - $("#piePromesaClienteCrearCotizacion").val());
                                                                  }
                                                                  $.ajax({
                                                                    url:   'controller/datosValorReserva.php',
                                                                    //data: parametros3,
                                                                    data:  parametros,
                                                                    type:  'post',
                                                                    success:  function (response) {
                                                                      var p = response.split(",");
                                                                      var cuerpo7 = "";
                                                                      if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                                                        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                                                            $("#reservaClienteCrearCotizacion").val(p[0]);
                                                                          }
                                                                          setTimeout(function(){
                                                                            $('#contenido').fadeIn();
                                                                            $('#header').fadeIn();
                                                                            $('#footer').fadeIn();
                                                                            $("#bodegasCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $("#estacionamientosCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });

                                                                            $('#modalAlertas').modal('hide');
                                                                          },1000);
                                                                        }
                                                                        else{
                                                                          $("#reservaClienteCrearCotizacion").val(0);
                                                                          setTimeout(function(){
                                                                            $('#contenido').fadeIn();
                                                                            $('#header').fadeIn();
                                                                            $('#footer').fadeIn();
                                                                            $("#bodegasCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $("#estacionamientosCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $('#modalAlertas').modal('hide');
                                                                          },1000);
                                                                        }
                                                                      }
                                                                    });
                                                                }
                                                              }
                                                            }
                                                          });
                                                        }
                                                        else{
                                                          $.ajax({
                                                            url:   'controller/datosCotizaEn.php',
                                                            type:  'post',
                                                            success:  function (response) {
                                                              var p = response.split(",");
                                                              var cuerpo7 = "";
                                                              if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                                                if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                                                  for(var i = 0; i < p.length; i=i+2){
                                                                    cuerpo7 += "<option value=" + p[i] + ">" + p[i+1] + "</option>";
                                                                  }
                                                                }
                                                                $("#cotizaEnContactoCrearCotizacion").html(cuerpo7);

                                                                if($("#accionCrearCotizacion").val() === "Arriendo"){
                                                                  $("#piePromesaClienteCrearCotizacion").val(0);
                                                                  $("#pieCuotasClienteCrearCotizacion").val(0);
                                                                  $("#piePromesaClienteCrearCotizacion").attr("disabled","disabled");
                                                                  $("#pieCuotasClienteCrearCotizacion").attr("disabled","disabled");
                                                                  $("#saldoCrearCotizacion").val(100);
                                                                  $("#reservaClienteCrearCotizacion").val(0);
                                                                  $("#cuotasCrearCotizacion").val(1);
                                                                  setTimeout(function(){
                                                                      $('#contenido').fadeIn();
                                                                      $('#header').fadeIn();
                                                                      $('#footer').fadeIn();
                                                                      $("#bodegasCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });
                                                                      $("#estacionamientosCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });

                                                                      $('#modalAlertas').modal('hide');
                                                                    },1000);
                                                                }
                                                                else{
                                                                  if($("#accionCrearCotizacion").val() === "Leasing"){
                                                                    $("#pieCuotasClienteCrearCotizacion").val(0);
                                                                    $("#pieCuotasClienteCrearCotizacion").attr("disabled","disabled");
                                                                    $("#piePromesaClienteCrearCotizacion").val(15);
                                                                    $("#piePromesaClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#saldoCrearCotizacion").val(85);
                                                                  }
                                                                  else{
                                                                    $("#pieCuotasClienteCrearCotizacion").val(defaultPieCuotas);
                                                                    $("#pieCuotasClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#piePromesaClienteCrearCotizacion").val(defaultPiePromesa);
                                                                    $("#piePromesaClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#saldoCrearCotizacion").val(100 - $("#pieCuotasClienteCrearCotizacion").val() - $("#piePromesaClienteCrearCotizacion").val());
                                                                  }
                                                                  $.ajax({
                                                                    url:   'controller/datosValorReserva.php',
                                                                    //data: parametros3,
                                                                    data:  parametros,
                                                                    type:  'post',
                                                                    success:  function (response) {
                                                                      var p = response.split(",");
                                                                      var cuerpo7 = "";
                                                                      if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                                                        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                                                            $("#reservaClienteCrearCotizacion").val(p[0]);
                                                                          }
                                                                          setTimeout(function(){
                                                                            $('#contenido').fadeIn();
                                                                            $('#header').fadeIn();
                                                                            $('#footer').fadeIn();
                                                                            $("#bodegasCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $("#estacionamientosCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });

                                                                            $('#modalAlertas').modal('hide');
                                                                          },1000);
                                                                        }
                                                                        else{
                                                                          $("#reservaClienteCrearCotizacion").val(0);
                                                                          setTimeout(function(){
                                                                            $('#contenido').fadeIn();
                                                                            $('#header').fadeIn();
                                                                            $('#footer').fadeIn();
                                                                            $("#bodegasCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $("#estacionamientosCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $('#modalAlertas').modal('hide');
                                                                          },1000);
                                                                        }
                                                                      }
                                                                    });
                                                                }
                                                              }
                                                              else{
                                                                if($("#accionCrearCotizacion").val() === "Arriendo"){
                                                                  $("#piePromesaClienteCrearCotizacion").val(0);
                                                                  $("#pieCuotasClienteCrearCotizacion").val(0);
                                                                  $("#piePromesaClienteCrearCotizacion").attr("disabled","disabled");
                                                                  $("#pieCuotasClienteCrearCotizacion").attr("disabled","disabled");
                                                                  $("#saldoCrearCotizacion").val(100);
                                                                  $("#reservaClienteCrearCotizacion").val(0);
                                                                  $("#cuotasCrearCotizacion").val(1);
                                                                  setTimeout(function(){
                                                                      $('#contenido').fadeIn();
                                                                      $('#header').fadeIn();
                                                                      $('#footer').fadeIn();
                                                                      $("#bodegasCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });
                                                                      $("#estacionamientosCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });

                                                                      $('#modalAlertas').modal('hide');
                                                                    },1000);
                                                                }
                                                                else{
                                                                  if($("#accionCrearCotizacion").val() === "Leasing"){
                                                                    $("#pieCuotasClienteCrearCotizacion").val(0);
                                                                    $("#pieCuotasClienteCrearCotizacion").attr("disabled","disabled");
                                                                    $("#piePromesaClienteCrearCotizacion").val(15);
                                                                    $("#piePromesaClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#saldoCrearCotizacion").val(85);
                                                                  }
                                                                  else{
                                                                    $("#pieCuotasClienteCrearCotizacion").val(defaultPieCuotas);
                                                                    $("#pieCuotasClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#piePromesaClienteCrearCotizacion").val(defaultPiePromesa);
                                                                    $("#piePromesaClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#saldoCrearCotizacion").val(100 - $("#pieCuotasClienteCrearCotizacion").val() - $("#piePromesaClienteCrearCotizacion").val());
                                                                  }
                                                                  $.ajax({
                                                                    url:   'controller/datosValorReserva.php',
                                                                    //data: parametros3,
                                                                    data:  parametros,
                                                                    type:  'post',
                                                                    success:  function (response) {
                                                                      var p = response.split(",");
                                                                      var cuerpo7 = "";
                                                                      if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                                                        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                                                            $("#reservaClienteCrearCotizacion").val(p[0]);
                                                                          }
                                                                          setTimeout(function(){
                                                                            $('#contenido').fadeIn();
                                                                            $('#header').fadeIn();
                                                                            $('#footer').fadeIn();
                                                                            $("#bodegasCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $("#estacionamientosCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });

                                                                            $('#modalAlertas').modal('hide');
                                                                          },1000);
                                                                        }
                                                                        else{
                                                                          $("#reservaClienteCrearCotizacion").val(0);
                                                                          setTimeout(function(){
                                                                            $('#contenido').fadeIn();
                                                                            $('#header').fadeIn();
                                                                            $('#footer').fadeIn();
                                                                            $("#bodegasCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $("#estacionamientosCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $('#modalAlertas').modal('hide');
                                                                          },1000);
                                                                        }
                                                                      }
                                                                    });
                                                                }
                                                              }
                                                            }
                                                          });
                                                        }
                                                      }
                                                    });
                                                  }
                                                  else{
                                                    $.ajax({
                                                      url:   'controller/datosCotizaEn.php',
                                                      type:  'post',
                                                      success:  function (response) {
                                                        var p = response.split(",");
                                                        var cuerpo7 = "";
                                                        if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                                          if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                                            for(var i = 0; i < p.length; i=i+2){
                                                              cuerpo7 += "<option value=" + p[i] + ">" + p[i+1] + "</option>";
                                                            }
                                                          }
                                                          $("#cotizaEnContactoCrearCotizacion").html(cuerpo7);

                                                          if($("#accionCrearCotizacion").val() === "Arriendo"){
                                                            $("#piePromesaClienteCrearCotizacion").val(0);
                                                            $("#pieCuotasClienteCrearCotizacion").val(0);
                                                            $("#piePromesaClienteCrearCotizacion").attr("disabled","disabled");
                                                            $("#pieCuotasClienteCrearCotizacion").attr("disabled","disabled");
                                                            $("#saldoCrearCotizacion").val(100);
                                                            $("#reservaClienteCrearCotizacion").val(0);
                                                            $("#cuotasCrearCotizacion").val(1);
                                                            setTimeout(function(){
                                                                $('#contenido').fadeIn();
                                                                $('#header').fadeIn();
                                                                $('#footer').fadeIn();
                                                                $("#bodegasCrearCotizacion").multiSelect({
                                                                  selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                  selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                });
                                                                $("#estacionamientosCrearCotizacion").multiSelect({
                                                                  selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                  selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                });

                                                                $('#modalAlertas').modal('hide');
                                                              },1000);
                                                          }
                                                          else{
                                                            if($("#accionCrearCotizacion").val() === "Leasing"){
                                                              $("#pieCuotasClienteCrearCotizacion").val(0);
                                                              $("#pieCuotasClienteCrearCotizacion").attr("disabled","disabled");
                                                              $("#piePromesaClienteCrearCotizacion").val(15);
                                                              $("#piePromesaClienteCrearCotizacion").removeAttr("disabled");
                                                              $("#saldoCrearCotizacion").val(85);
                                                            }
                                                            else{
                                                              $("#pieCuotasClienteCrearCotizacion").val(defaultPieCuotas);
                                                              $("#pieCuotasClienteCrearCotizacion").removeAttr("disabled");
                                                              $("#piePromesaClienteCrearCotizacion").val(defaultPiePromesa);
                                                              $("#piePromesaClienteCrearCotizacion").removeAttr("disabled");
                                                              $("#saldoCrearCotizacion").val(100 - $("#pieCuotasClienteCrearCotizacion").val() - $("#piePromesaClienteCrearCotizacion").val());
                                                            }
                                                            $.ajax({
                                                              url:   'controller/datosValorReserva.php',
                                                              //data: parametros3,
                                                              data:  parametros,
                                                              type:  'post',
                                                              success:  function (response) {
                                                                var p = response.split(",");
                                                                var cuerpo7 = "";
                                                                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                                                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                                                      $("#reservaClienteCrearCotizacion").val(p[0]);
                                                                    }
                                                                    setTimeout(function(){
                                                                      $('#contenido').fadeIn();
                                                                      $('#header').fadeIn();
                                                                      $('#footer').fadeIn();
                                                                      $("#bodegasCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });
                                                                      $("#estacionamientosCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });

                                                                      $('#modalAlertas').modal('hide');
                                                                    },1000);
                                                                  }
                                                                  else{
                                                                    $("#reservaClienteCrearCotizacion").val(0);
                                                                    setTimeout(function(){
                                                                      $('#contenido').fadeIn();
                                                                      $('#header').fadeIn();
                                                                      $('#footer').fadeIn();
                                                                      $("#bodegasCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });
                                                                      $("#estacionamientosCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });
                                                                      $('#modalAlertas').modal('hide');
                                                                    },1000);
                                                                  }
                                                                }
                                                              });
                                                          }
                                                        }
                                                        else{
                                                          if($("#accionCrearCotizacion").val() === "Arriendo"){
                                                            $("#piePromesaClienteCrearCotizacion").val(0);
                                                            $("#pieCuotasClienteCrearCotizacion").val(0);
                                                            $("#piePromesaClienteCrearCotizacion").attr("disabled","disabled");
                                                            $("#pieCuotasClienteCrearCotizacion").attr("disabled","disabled");
                                                            $("#saldoCrearCotizacion").val(100);
                                                            $("#reservaClienteCrearCotizacion").val(0);
                                                            $("#cuotasCrearCotizacion").val(1);
                                                            setTimeout(function(){
                                                                $('#contenido').fadeIn();
                                                                $('#header').fadeIn();
                                                                $('#footer').fadeIn();
                                                                $("#bodegasCrearCotizacion").multiSelect({
                                                                  selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                  selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                });
                                                                $("#estacionamientosCrearCotizacion").multiSelect({
                                                                  selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                  selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                });

                                                                $('#modalAlertas').modal('hide');
                                                              },1000);
                                                          }
                                                          else{
                                                            if($("#accionCrearCotizacion").val() === "Leasing"){
                                                              $("#pieCuotasClienteCrearCotizacion").val(0);
                                                              $("#pieCuotasClienteCrearCotizacion").attr("disabled","disabled");
                                                              $("#piePromesaClienteCrearCotizacion").val(15);
                                                              $("#piePromesaClienteCrearCotizacion").removeAttr("disabled");
                                                              $("#saldoCrearCotizacion").val(85);
                                                            }
                                                            else{
                                                              $("#pieCuotasClienteCrearCotizacion").val(defaultPieCuotas);
                                                              $("#pieCuotasClienteCrearCotizacion").removeAttr("disabled");
                                                              $("#piePromesaClienteCrearCotizacion").val(defaultPiePromesa);
                                                              $("#piePromesaClienteCrearCotizacion").removeAttr("disabled");
                                                              $("#saldoCrearCotizacion").val(100 - $("#pieCuotasClienteCrearCotizacion").val() - $("#piePromesaClienteCrearCotizacion").val());
                                                            }
                                                            $.ajax({
                                                              url:   'controller/datosValorReserva.php',
                                                              //data: parametros3,
                                                              data:  parametros,
                                                              type:  'post',
                                                              success:  function (response) {
                                                                var p = response.split(",");
                                                                var cuerpo7 = "";
                                                                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                                                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                                                      $("#reservaClienteCrearCotizacion").val(p[0]);
                                                                    }
                                                                    setTimeout(function(){
                                                                      $('#contenido').fadeIn();
                                                                      $('#header').fadeIn();
                                                                      $('#footer').fadeIn();
                                                                      $("#bodegasCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });
                                                                      $("#estacionamientosCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });

                                                                      $('#modalAlertas').modal('hide');
                                                                    },1000);
                                                                  }
                                                                  else{
                                                                    $("#reservaClienteCrearCotizacion").val(0);
                                                                    setTimeout(function(){
                                                                      $('#contenido').fadeIn();
                                                                      $('#header').fadeIn();
                                                                      $('#footer').fadeIn();
                                                                      $("#bodegasCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });
                                                                      $("#estacionamientosCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });
                                                                      $('#modalAlertas').modal('hide');
                                                                    },1000);
                                                                  }
                                                                }
                                                              });
                                                          }
                                                        }
                                                      }
                                                    });
                                                  }
                                                }
                                              });
                                            }
                                            else{
                                              $.ajax({
                                                url:   'controller/datosBodegasDisponiblesProyecto.php',
                                                data:  parametros,
                                                type:  'post',
                                                success:  function (response) {
                                                  var p = response.split(",");
                                                  var cuerpo6 = "";
                                                  if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                                    if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                                      for(var i = 0; i < p.length; i=i+3){
                                                        cuerpo6 += "<option value=" + p[i] + ">" + (p[i+1] + ' - ' + p[i+2] + 'UF') + "</option>";
                                                      }
                                                    }
                                                    $("#bodegasCrearCotizacion").html(cuerpo6);

                                                    $.ajax({
                                                      url:   'controller/datosEstacionamientosDisponiblesProyecto.php',
                                                      data:  parametros,
                                                      type:  'post',
                                                      success:  function (response) {
                                                        var p = response.split(",");
                                                        var cuerpo6 = "";
                                                        if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                                          if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                                            for(var i = 0; i < p.length; i=i+3){
                                                              cuerpo6 += "<option value=" + p[i] + ">" + (p[i+1] + ' - ' + p[i+2] + 'UF') + "</option>";
                                                            }
                                                          }
                                                          $("#estacionamientosCrearCotizacion").html(cuerpo6);

                                                          $.ajax({
                                                            url:   'controller/datosCotizaEn.php',
                                                            type:  'post',
                                                            success:  function (response) {
                                                              var p = response.split(",");
                                                              var cuerpo7 = "";
                                                              if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                                                if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                                                  for(var i = 0; i < p.length; i=i+2){
                                                                    cuerpo7 += "<option value=" + p[i] + ">" + p[i+1] + "</option>";
                                                                  }
                                                                }
                                                                $("#cotizaEnContactoCrearCotizacion").html(cuerpo7);

                                                                if($("#accionCrearCotizacion").val() === "Arriendo"){
                                                                  $("#piePromesaClienteCrearCotizacion").val(0);
                                                                  $("#pieCuotasClienteCrearCotizacion").val(0);
                                                                  $("#piePromesaClienteCrearCotizacion").attr("disabled","disabled");
                                                                  $("#pieCuotasClienteCrearCotizacion").attr("disabled","disabled");
                                                                  $("#saldoCrearCotizacion").val(100);
                                                                  $("#reservaClienteCrearCotizacion").val(0);
                                                                  $("#cuotasCrearCotizacion").val(1);
                                                                  setTimeout(function(){
                                                                      $('#contenido').fadeIn();
                                                                      $('#header').fadeIn();
                                                                      $('#footer').fadeIn();
                                                                      $("#bodegasCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });
                                                                      $("#estacionamientosCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });

                                                                      $('#modalAlertas').modal('hide');
                                                                    },1000);
                                                                }
                                                                else{
                                                                  if($("#accionCrearCotizacion").val() === "Leasing"){
                                                                    $("#pieCuotasClienteCrearCotizacion").val(0);
                                                                    $("#pieCuotasClienteCrearCotizacion").attr("disabled","disabled");
                                                                    $("#piePromesaClienteCrearCotizacion").val(15);
                                                                    $("#piePromesaClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#saldoCrearCotizacion").val(85);
                                                                  }
                                                                  else{
                                                                    $("#pieCuotasClienteCrearCotizacion").val(defaultPieCuotas);
                                                                    $("#pieCuotasClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#piePromesaClienteCrearCotizacion").val(defaultPiePromesa);
                                                                    $("#piePromesaClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#saldoCrearCotizacion").val(100 - $("#pieCuotasClienteCrearCotizacion").val() - $("#piePromesaClienteCrearCotizacion").val());
                                                                  }
                                                                  $.ajax({
                                                                    url:   'controller/datosValorReserva.php',
                                                                    //data: parametros3,
                                                                    data:  parametros,
                                                                    type:  'post',
                                                                    success:  function (response) {
                                                                      var p = response.split(",");
                                                                      var cuerpo7 = "";
                                                                      if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                                                        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                                                            $("#reservaClienteCrearCotizacion").val(p[0]);
                                                                          }
                                                                          setTimeout(function(){
                                                                            $('#contenido').fadeIn();
                                                                            $('#header').fadeIn();
                                                                            $('#footer').fadeIn();
                                                                            $("#bodegasCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $("#estacionamientosCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });

                                                                            $('#modalAlertas').modal('hide');
                                                                          },1000);
                                                                        }
                                                                        else{
                                                                          $("#reservaClienteCrearCotizacion").val(0);
                                                                          setTimeout(function(){
                                                                            $('#contenido').fadeIn();
                                                                            $('#header').fadeIn();
                                                                            $('#footer').fadeIn();
                                                                            $("#bodegasCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $("#estacionamientosCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $('#modalAlertas').modal('hide');
                                                                          },1000);
                                                                        }
                                                                      }
                                                                    });
                                                                }
                                                              }
                                                              else{
                                                                if($("#accionCrearCotizacion").val() === "Arriendo"){
                                                                  $("#piePromesaClienteCrearCotizacion").val(0);
                                                                  $("#pieCuotasClienteCrearCotizacion").val(0);
                                                                  $("#piePromesaClienteCrearCotizacion").attr("disabled","disabled");
                                                                  $("#pieCuotasClienteCrearCotizacion").attr("disabled","disabled");
                                                                  $("#saldoCrearCotizacion").val(100);
                                                                  $("#reservaClienteCrearCotizacion").val(0);
                                                                  $("#cuotasCrearCotizacion").val(1);
                                                                  setTimeout(function(){
                                                                      $('#contenido').fadeIn();
                                                                      $('#header').fadeIn();
                                                                      $('#footer').fadeIn();
                                                                      $("#bodegasCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });
                                                                      $("#estacionamientosCrearCotizacion").multiSelect({
                                                                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                      });

                                                                      $('#modalAlertas').modal('hide');
                                                                    },1000);
                                                                }
                                                                else{
                                                                  if($("#accionCrearCotizacion").val() === "Leasing"){
                                                                    $("#pieCuotasClienteCrearCotizacion").val(0);
                                                                    $("#pieCuotasClienteCrearCotizacion").attr("disabled","disabled");
                                                                    $("#piePromesaClienteCrearCotizacion").val(15);
                                                                    $("#piePromesaClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#saldoCrearCotizacion").val(85);
                                                                  }
                                                                  else{
                                                                    $("#pieCuotasClienteCrearCotizacion").val(defaultPieCuotas);
                                                                    $("#pieCuotasClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#piePromesaClienteCrearCotizacion").val(defaultPiePromesa);
                                                                    $("#piePromesaClienteCrearCotizacion").removeAttr("disabled");
                                                                    $("#saldoCrearCotizacion").val(100 - $("#pieCuotasClienteCrearCotizacion").val() - $("#piePromesaClienteCrearCotizacion").val());
                                                                  }
                                                                  $.ajax({
                                                                    url:   'controller/datosValorReserva.php',
                                                                    //data: parametros3,
                                                                    data:  parametros,
                                                                    type:  'post',
                                                                    success:  function (response) {
                                                                      var p = response.split(",");
                                                                      var cuerpo7 = "";
                                                                      if(response.localeCompare("Sin datos")!= 0 && response != ""){
                                                                        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                                                                            $("#reservaClienteCrearCotizacion").val(p[0]);
                                                                          }
                                                                          setTimeout(function(){
                                                                            $('#contenido').fadeIn();
                                                                            $('#header').fadeIn();
                                                                            $('#footer').fadeIn();
                                                                            $("#bodegasCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $("#estacionamientosCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });

                                                                            $('#modalAlertas').modal('hide');
                                                                          },1000);
                                                                        }
                                                                        else{
                                                                          $("#reservaClienteCrearCotizacion").val(0);
                                                                          setTimeout(function(){
                                                                            $('#contenido').fadeIn();
                                                                            $('#header').fadeIn();
                                                                            $('#footer').fadeIn();
                                                                            $("#bodegasCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $("#estacionamientosCrearCotizacion").multiSelect({
                                                                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                                                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                                                            });
                                                                            $('#modalAlertas').modal('hide');
                                                                          },1000);
                                                                        }
                                                                      }
                                                                    });
                                                                }
                                                              }
                                                            }
                                                          });
                                                        }
                                                      }
                                                    });
                                                  }
                                                }
                                              });
                                      }
                                    }
                                  });
                                }
                                else{
                                  cuerpo2 += "<option value='Sin unidades disponibles'>Sin unidades disponibles</option>";
                                  $("#unidadCrearCotizacion").html(cuerpo2);

                                  $("#limpiarCrearCotizacion").attr("disabled","disabled");
                                  $("#generarCrearCotizacion").attr("disabled","disabled");

                                  setTimeout(function(){
                                    $('#contenido').fadeIn();
                                    $('#header').fadeIn();
                                    $('#footer').fadeIn();
                                    $("#bodegasCrearCotizacion").multiSelect({
                                      selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                      selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                    });
                                    $("#estacionamientosCrearCotizacion").multiSelect({
                                      selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                      selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                                    });

                                    var datosBloq = $("#formDatosCrearCotizacion");
                                    $(datosBloq).find('input').each(function() {
                                      $(this).attr("disabled","disabled");
                                    });
                                    $(datosBloq).find('select').each(function() {
                                      $(this).attr("disabled","disabled");
                                    });

                                    $("#bodegasCrearCotizacion").removeAttr("disabled");
                                    $("#estacionamientosCrearCotizacion").removeAttr("disabled");
                                    $("#proyectoCrearCotizacion").removeAttr("disabled");

                                    $('#modalAlertas').modal('hide');
                                  },1000);
                                }
                              }
                            });
                          }
                          else{
                            cuerpo2 += "<option value='Sin unidades disponibles'>Sin unidades disponibles</option>";
                            $("#unidadCrearCotizacion").html(cuerpo2);

                            $("#limpiarCrearCotizacion").attr("disabled","disabled");
                            $("#generarCrearCotizacion").attr("disabled","disabled");

                            setTimeout(function(){
                              $('#contenido').fadeIn();
                              $('#header').fadeIn();
                              $('#footer').fadeIn();
                              $("#bodegasCrearCotizacion").multiSelect({
                                selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                              });
                              $("#estacionamientosCrearCotizacion").multiSelect({
                                selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                                selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                              });

                              var datosBloq = $("#formDatosCrearCotizacion");
                              $(datosBloq).find('input').each(function() {
                                $(this).attr("disabled","disabled");
                              });
                              $(datosBloq).find('select').each(function() {
                                $(this).attr("disabled","disabled");
                              });

                              $("#bodegasCrearCotizacion").removeAttr("disabled");
                              $("#estacionamientosCrearCotizacion").removeAttr("disabled");
                              $("#proyectoCrearCotizacion").removeAttr("disabled");

                              $('#modalAlertas').modal('hide');
                            },1000);
                          }
                        }
                        else{
                          cuerpo2 += "<option value='Sin unidades disponibles'>Sin unidades disponibles</option>";
                          $("#unidadCrearCotizacion").html(cuerpo2);

                          $("#limpiarCrearCotizacion").attr("disabled","disabled");
                          $("#generarCrearCotizacion").attr("disabled","disabled");

                          setTimeout(function(){
                            $('#contenido').fadeIn();
                            $('#header').fadeIn();
                            $('#footer').fadeIn();
                            $("#bodegasCrearCotizacion").multiSelect({
                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                            });
                            $("#estacionamientosCrearCotizacion").multiSelect({
                              selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                              selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                            });

                            var datosBloq = $("#formDatosCrearCotizacion");
                            $(datosBloq).find('input').each(function() {
                              $(this).attr("disabled","disabled");
                            });
                            $(datosBloq).find('select').each(function() {
                              $(this).attr("disabled","disabled");
                            });

                            $("#bodegasCrearCotizacion").removeAttr("disabled");
                            $("#estacionamientosCrearCotizacion").removeAttr("disabled");
                            $("#proyectoCrearCotizacion").removeAttr("disabled");

                            $('#modalAlertas').modal('hide');
                          },1000);
                        }
                      }
                    });
                  }
                  else{
                    var cuerpo = "<option value='Sin proyectos asignados'>Sin proyectos asignados</option>";
                    $("#proyectoCrearCotizacion").html(cuerpo);
                    var cuerpo2 = "<option value='Sin unidades disponibles'>Sin unidades disponibles</option>";
                    $("#unidadCrearCotizacion").html(cuerpo2);

                    $("#limpiarCrearCotizacion").attr("disabled","disabled");
                    $("#generarCrearCotizacion").attr("disabled","disabled");

                    setTimeout(function(){
                      $('#contenido').fadeIn();
                      $('#header').fadeIn();
                      $('#footer').fadeIn();
                      $("#bodegasCrearCotizacion").multiSelect({
                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                      });
                      $("#estacionamientosCrearCotizacion").multiSelect({
                        selectableHeader: "<div class='custom-header'>&nbsp;Disponibles</div>",
                        selectionHeader: "<div class='custom-header'>&nbsp;Seleccionadas</div>"
                      });

                      var datosBloq = $("#formDatosCrearCotizacion");
                      $(datosBloq).find('input').each(function() {
                        $(this).attr("disabled","disabled");
                      });
                      $(datosBloq).find('select').each(function() {
                        $(this).attr("disabled","disabled");
                      });

                      $('#modalAlertas').modal('hide');
                    },1000);
                  }
                }
              });
            },1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("crearProyectoController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
  $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
  $('#modalAlertas').modal('show');
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
              if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                $("#idMainCrearProyecto").css("margin-top","50pt");
              }
              else{

              }
              //Agregado de funciones a inputs
              $("#fInicioVentaProyectoCrear").datepicker({
                dateFormat: "dd-mm-yy",
                changeMonth: true,
                changeYear: true
              });
              $("#fFinObraProyectoCrear").datepicker({
                dateFormat: "dd-mm-yy",
                changeMonth: true,
                changeYear: true
              });
              $("#fEntregaProyectoCrear").datepicker({
                dateFormat: "dd-mm-yy",
                changeMonth: true,
                changeYear: true
              });

              $.ajax({
                url:   'controller/datosEstadosProyecto.php',
                type:  'post',
                success:  function (response) {
                  var p = response.split(",");
                  var cuerpo = '';
                  if(response.localeCompare("Sin datos")!= 0 && response != ""){
                    if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                      for(var i = 0; i < p.length; i=i+3){
                        cuerpo = cuerpo + '<option value="' + p[i] + '">' + p[i+1] + '</option>';
                      }
                      setTimeout(function(){
                        $("#estadoProyectoCrear").html(cuerpo);
                      },500);
                    }
                    $('#contenido').fadeIn();
                    $('#header').fadeIn();
                    $('#footer').fadeIn();

                    var datosDetalle = $("#datosDeProyectoCrear");
                    $(datosDetalle).find('label').each(function() {
                      $(this).css("font-weight","normal");
                    });

                    setTimeout(function(){
                      $('#modalAlertas').modal('hide');
                    },1000);
                  }
                }
              });
          },1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("listadoCotizacionesController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
  $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
  $('#modalAlertas').modal('show');
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#idMainListadoCotizaciones").css("margin-top","50pt");
              $("#divTituloListadoCotizaciones").css("text-align","center");
            }
            else{
              $("#divTituloListadoCotizaciones").css("text-align","left");
              $("#divTituloListadoCotizaciones").css("margin-bottom","-10pt");
            }
            var largo = ($(window).height() - 370);
            var f = new Date();
            $('#tablaListadoCotizaciones').DataTable( {
              buttons: [
                {
                    extend: 'excel',
                    exportOptions: {
                      columns: [ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,23 ],
                      format: {
                          body: function(data, row, column, node) {
                             return column == 12 ? data.replace( '.', '' ).replace( ',', '.' ) : data.replace('.','').replace(',','.');

                          }
                      }
                    },
                    title: null,
                    text: 'Excel'
                },
                {
                    extend: 'pdfHtml5',
                    title: null,
                    text: 'PDF',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: {
                      columns: [ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,23 ]
                    },
                    customize: function (doc) {
                    //Remove the title created by datatTables
                    doc.content.splice(1,0);

                    doc['footer']=(function(page, pages)  {
                      return {
                        columns: [
                          {
                            alignment: 'left',
                            text: [
                              { text: page.toString(), italics: true },
                              ' de ',
                              { text: pages.toString(), italics: true }
                            ]
                          }
                        ],
                        margin: [40,10,0,0]
                      }
                    });

                    doc['header']=(function() {
                      return {
                        columns: [
                          {
                            image: logo,
                            width: 80,
                            alignment: 'left'
                          },
                          {
                            alignment: 'center',
                            fontSize: 11,
                            text: 'Listado de cotizaciones',
                            margin: [0,60,0,0],

                          },
                          {
                            text: f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear(),
                            fontSize: 11,
                            alignment: 'right',
                            width: 80
                          }
                        ],
                        margin: [40,40,60,0]
                      }
                    });

                    doc.pageMargins = [10,140,10,60];
                      doc.content[0].table.widths = [20,20,50,30,100,100,45,45,100,20,20,30,35,'*','*',30,30,30];
                    doc.defaultStyle.fontSize = 8;
                    doc.styles.tableHeader.fontSize = 8;
                    doc.styles.tableHeader.alignment = 'left';
                  }
                }
              ],
              ajax: {
                    url: "controller/datosCotizaciones.php",
                    type: 'POST'
                },
                "aoColumns": [
                    { mData: 'CODIGOPROYECTO' } ,
                    { mData: 'CODIGO' },
                    { mData: 'FECHA', render: $.fn.dataTable.render.moment( 'DD-MM-YYYY', 'DD-MM-YYYY' )},
                    { mData: 'DEPTO' },
                    { mData: 'NOMBRES' },
                    { mData: 'APELLIDOS' },
                    { mData: 'RUT' },
                    { mData: 'CELULAR' },
                    { mData: 'EMAIL' },
                    { mData: 'EST' },
                    { mData: 'BOD' },
                    { mData: 'FORMA' },
                    { mData: 'VALORTOTALUF', render: $.fn.dataTable.render.number( '.', ',', 2, '' ) },
                    { mData: 'VENDEDOR' },
                    { mData: 'MEDIO' },
                    { mData: 'COTIZAEN' },
                    { mData: 'ESTADONIVELINTERES' },
                    { mData: 'ESTADO' },
                    { mData: 'ESTADOSEGUIMIENTO1' },
                    { mData: 'ESTADOSEGUIMIENTO2' },
                    { mData: 'ESTADOSEGUIMIENTO3' },
                    { mData: 'ESTADOSEGUIMIENTO4' },
                    { mData: 'RUTA_PDF' },
                    { mData: 'ESTADOTEXTO' }
                ],
                columns: [
                    { title: "Cod." },
                    { title: "Nro." },
                    { title: "Fecha" },
                    { title: "Unidad" },
                    { title: "Nombres" },
                    { title: "Apellidos" },
                    { title: "Rut" },
                    { title: "Celular" },
                    { title: "E-Mail" },
                    { title: "Est" },
                    { title: "Bod" },
                    { title: "Forma" },
                    { title: "Valor UF" },
                    { title: "Vendedor" },
                    { title: "Medio" },
                    { title: "Visita" },
                    { title: "Interés" },
                    { title: "Estado" },
                    { title: "Sem 1" },
                    { title: "Sem 2" },
                    { title: "Sem 3" },
                    { title: "Sem 4" },
                    { title: "Ruta PDF" },
                    { title: "Estado" }
                ],
                "columnDefs": [
                  {
                      "targets": [ 22 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 23 ],
                      "visible": false,
                      "searchable": false
                  },
                ],
                "deferRender": true,
                "scrollY": largo,
                "scrollCollapse": true,
                "scrollX": true,
                "paging":         false,
                "ordering": true,
                "order": [[ 1, "desc" ]],
                "info":     true,
                "dom": 'frBtip',
                "language": {
                    "zeroRecords": "No hay datos disponibles",
                    "info": "Registro _START_ de _END_ de _TOTAL_",
                    "infoEmpty": "No hay datos disponibles",
                    "paginate": {
                        "previous": "Anterior",
                        "next": "Siguiente"
                    },
                    "search": "Buscar: "
                },
                "destroy": true
            });

            $.ajax({
              url:   'controller/datosProyectosMiUsuarioListadoCotiza.php',
              type:  'post',
              success:  function (response) {
                var p = response.split(",");
                var cuerpo = "";
                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                    for(var i = 0; i < p.length; i=i+4){
                      cuerpo += "<option value=" + p[i] + ">" + p[i+1] + "</option>";
                    }
                  }
                  cuerpo += "<option value=todos selected>Todos</option>";
                  $("#mostrarProyectoCotizacion").html(cuerpo);
                }
              }
            });

            $.ajax({
              url:   'controller/datosRefresh.php',
              type:  'post',
              success:  function (response) {
                var p = response.split(",");
                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                  var dataset = [];
                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                    if(p[2] !== '1' && p[2] !== '2'){
                      $("#buttonEliminarCotizaciones").remove();
                      if(p[2] === '5' || p[2] === '4'){
                        $("#divListadoCotizacionesSeguimiento").remove();
                      }
                      else{
                        $("#divListadoCotizacionesSeguimiento").show();
                      }
                    }
                    else{
                      $("#buttonEliminarCotizaciones").show();
                      $("#divListadoCotizacionesSeguimiento").show();
                    }
                    setTimeout(function(){
                      $('#contenido').fadeIn();
                      $('#header').fadeIn();
                      $('#footer').fadeIn();
                      $('#tablaListadoCotizaciones').DataTable().columns.adjust();
                      $('#modalAlertas').modal('hide');
                    },1200);
                  }
                }
              }
            });
          },1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("listadoProyectosController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
  $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
  $('#modalAlertas').modal('show');
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#idMainListadoProyectos").css("margin-top","50pt");
              $("#divTituloListadoProyectos").css("text-align","center");
            }
            else{
              $("#divTituloListadoProyectos").css("text-align","left");
              $("#divTituloListadoProyectos").css("margin-bottom","-10pt");
            }

            $.ajax({
              url:   'controller/datosProyectos.php',
              type:  'post',
              success:  function (response) {
                var p = response.split(",");
                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                  var dataset = [];
                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                    for(var i = 0; i < p.length; i=i+7){
                      var element = [p[i],p[i+1],p[i+2],p[i+3],p[i+4],p[i+5],p[i+6]];
                      dataset.push(element);
                    }
                    var largo = ($(window).height() - 370);
                    var f = new Date();
                    $('#tablaListadoProyectos').DataTable( {
                        data: dataset,
                        buttons: [
                          {
                              extend: 'excel',
                              title: null,
                              text: 'Excel'
                          },
                          {
                              extend: 'pdfHtml5',
                              title: null,
                              text: 'PDF',
                              customize: function (doc) {
                  						//Remove the title created by datatTables
                  						doc.content.splice(1,0);

                              doc['footer']=(function(page, pages)  {
                                return {
                                  columns: [
                                    {
                                      alignment: 'left',
                                      text: [
                                        { text: page.toString(), italics: true },
                                        ' de ',
                                        { text: pages.toString(), italics: true }
                                      ]
                                    }
                                  ],
                                  margin: [40,10,0,0]
                                }
                              });

                              doc['header']=(function() {
                                return {
                                  columns: [
                                    {
                                      image: logo,
                                      width: 80,
                                      alignment: 'left'
                                    },
                                    {
                                      alignment: 'center',
                                      fontSize: 11,
                                      text: 'Listado de proyectos',
                                      margin: [0,60,0,0],

                                    },
                                    {
                                      text: f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear(),
                                      fontSize: 11,
                                      alignment: 'right',
                                      width: 80
                                    }
                                  ],
                                  margin: [40,40,60,0]
                                }
                              });

                              doc.pageMargins = [10,140,10,60];
                              doc.content[0].table.widths = ['*','*','*','*','*','*','*'];
                              doc.defaultStyle.fontSize = 8;
                              doc.styles.tableHeader.fontSize = 8;
                              doc.styles.tableHeader.alignment = 'left';
                            }
                          }
                        ],
                        columns: [
                            { title: "Código" },
                            { title: "Nombre" },
                            { title: "Inmobiliaria" },
                            { title: "Fecha inicio venta", render: $.fn.dataTable.render.moment( 'DD-MM-YYYY', 'DD-MM-YYYY' )},
                            { title: "Fecha fin obra", render: $.fn.dataTable.render.moment( 'DD-MM-YYYY', 'DD-MM-YYYY' )},
                            { title: "Fecha entrega", render: $.fn.dataTable.render.moment( 'DD-MM-YYYY', 'DD-MM-YYYY' )},
                            { title: "Estado" },
                        ],
                        "scrollY": largo,
                        "scrollCollapse": true,
                        "scrollX": true,
                        "paging":  false,
                        "ordering": true,
                        "order": [[ 0, "asc" ]],
                        "info":     true,
                        "dom": 'frBtip',
                        "language": {
                            "zeroRecords": "No hay datos disponibles",
                            "info": "Registro _START_ de _END_ de _TOTAL_",
                            "infoEmpty": "No hay datos disponibles",
                            "paginate": {
                                "previous": "Anterior",
                                "next": "Siguiente"
                            },
                            "search": "Buscar: "
                        }
                    });
                  }
                  else{
                    var f = new Date();
                    $('#tablaListadoProyectos').DataTable( {
                        buttons: [
                          {
                              extend: 'excel',
                              title: null,
                              text: 'Excel'
                          },
                          {
                              extend: 'pdfHtml5',
                              title: null,
                              text: 'PDF',
                              customize: function (doc) {
                  						//Remove the title created by datatTables
                  						doc.content.splice(1,0);

                              doc['footer']=(function(page, pages)  {
                                return {
                                  columns: [
                                    {
                                      alignment: 'left',
                                      text: [
                                        { text: page.toString(), italics: true },
                                        ' de ',
                                        { text: pages.toString(), italics: true }
                                      ]
                                    }
                                  ],
                                  margin: [40,10,0,0]
                                }
                              });

                              doc['header']=(function() {
                                return {
                                  columns: [
                                    {
                                      image: logo,
                                      width: 80,
                                      alignment: 'left'
                                    },
                                    {
                                      alignment: 'center',
                                      fontSize: 11,
                                      text: 'Listado de proyectos',
                                      margin: [0,60,0,0],

                                    },
                                    {
                                      text: f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear(),
                                      fontSize: 11,
                                      alignment: 'right',
                                      width: 80
                                    }
                                  ],
                                  margin: [40,40,60,0]
                                }
                              });

                              doc.pageMargins = [10,140,60,30];
                              doc.content[0].table.widths = ['*','*','*','*','*','*','*'];
                              doc.defaultStyle.fontSize = 8;
                              doc.styles.tableHeader.fontSize = 8;
                              doc.styles.tableHeader.alignment = 'left';
                            }
                          }
                        ],
                        columns: [
                            { title: "Código" },
                            { title: "Nombre" },
                            { title: "Inmobiliaria" },
                            { title: "Fecha inicio venta", render: $.fn.dataTable.render.moment( 'DD-MM-YYYY', 'DD-MM-YYYY' )},
                            { title: "Fecha fin obra", render: $.fn.dataTable.render.moment( 'DD-MM-YYYY', 'DD-MM-YYYY' )},
                            { title: "Fecha entrega", render: $.fn.dataTable.render.moment( 'DD-MM-YYYY', 'DD-MM-YYYY' )},
                            { title: "Estado" },
                        ],
                        "scrollY": largo,
                        "scrollCollapse": true,
                        "scrollX": true,
                        "paging":  false,
                        "ordering": true,
                        "order": [[ 0, "asc" ]],
                        "info":     true,
                        "dom": 'frBtip',
                        "language": {
                            "zeroRecords": "No hay datos disponibles",
                            "info": "Registro _START_ de _END_ de _TOTAL_",
                            "infoEmpty": "No hay datos disponibles",
                            "paginate": {
                                "previous": "Anterior",
                                "next": "Siguiente"
                            },
                            "search": "Buscar: "
                        }
                    });
                  }
                }
                else{
                  var f = new Date();
                  $('#tablaListadoProyectos').DataTable( {
                      data: dataset,
                      buttons: [
                        {
                            extend: 'excel',
                            title: null,
                            text: 'Excel'
                        },
                        {
                            extend: 'pdfHtml5',
                            title: null,
                            text: 'PDF',
                            customize: function (doc) {
                            //Remove the title created by datatTables
                            doc.content.splice(1,0);

                            doc['footer']=(function(page, pages)  {
                              return {
                                columns: [
                                  {
                                    alignment: 'left',
                                    text: [
                                      { text: page.toString(), italics: true },
                                      ' de ',
                                      { text: pages.toString(), italics: true }
                                    ]
                                  }
                                ],
                                margin: [40,10,0,0]
                              }
                            });

                            doc['header']=(function() {
                              return {
                                columns: [
                                  {
                                    image: logo,
                                    width: 80,
                                    alignment: 'left'
                                  },
                                  {
                                    alignment: 'center',
                                    fontSize: 11,
                                    text: 'Listado de proyectos',
                                    margin: [0,60,0,0],

                                  },
                                  {
                                    text: f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear(),
                                    fontSize: 11,
                                    alignment: 'right',
                                    width: 80
                                  }
                                ],
                                margin: [40,40,60,0]
                              }
                            });

                            doc.pageMargins = [10,140,60,30];
                            doc.content[0].table.widths = ['*','*','*','*','*','*','*'];
                            doc.defaultStyle.fontSize = 8;
                            doc.styles.tableHeader.fontSize = 8;
                            doc.styles.tableHeader.alignment = 'left';
                          }
                        }
                      ],
                      columns: [
                          { title: "Código" },
                          { title: "Nombre" },
                          { title: "Inmobiliaria" },
                          { title: "Fecha inicio venta", render: $.fn.dataTable.render.moment( 'DD-MM-YYYY', 'DD-MM-YYYY' )},
                          { title: "Fecha fin obra", render: $.fn.dataTable.render.moment( 'DD-MM-YYYY', 'DD-MM-YYYY' )},
                          { title: "Fecha entrega", render: $.fn.dataTable.render.moment( 'DD-MM-YYYY', 'DD-MM-YYYY' )},
                          { title: "Estado" },
                      ],
                      "scrollY": largo,
                      "scrollCollapse": true,
                      "scrollX": true,
                      "paging":  false,
                      "ordering": true,
                      "order": [[ 0, "asc" ]],
                      "info":     true,
                      "dom": 'frBtip',
                      "language": {
                          "zeroRecords": "No hay datos disponibles",
                          "info": "Registro _START_ de _END_ de _TOTAL_",
                          "infoEmpty": "No hay datos disponibles",
                          "paginate": {
                              "previous": "Anterior",
                              "next": "Siguiente"
                          },
                          "search": "Buscar: "
                      }
                  });
                }

                $.ajax({
                  url:   'controller/datosRefresh.php',
                  type:  'post',
                  success:  function (response) {
                    var p = response.split(",");
                    if(response.localeCompare("Sin datos")!= 0 && response != ""){
                      var dataset = [];
                      if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                        if(p[2] !== '1' && p[2] !== '2'){
                          $("#divListadoProyectosDetalle").remove();
                          $("#divListadoProyectosEliminar").remove();
                        }
                        else{
                          $("#divListadoProyectosDetalle").show();
                          $("#divListadoProyectosEliminar").show();
                          if(p[2] === '2'){
                            $("#divListadoProyectosEliminar").remove();
                          }
                        }
                      }
                    }
                  }
                });

                setTimeout(function(){
                  $('#contenido').fadeIn();
                  $('#header').fadeIn();
                  $('#footer').fadeIn();
                  $('#tablaListadoProyectos').DataTable().columns.adjust();
                  $('#modalAlertas').modal('hide');
                },1000);
              }
            });
          }, 1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("listadoClientesController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
          $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
          $('#modalAlertas').modal('show');
          setTimeout(function(){
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#idMainListadoClientes").css("margin-top","50pt");
              $("#divTituloListadoClientes").css("text-align","center");
            }
            else{
              $("#divTituloListadoClientes").css("text-align","left");
              $("#divTituloListadoClientes").css("margin-bottom","-15pt");
            }

            $.ajax({
              url:   'controller/datosClientes.php',
              type:  'post',
              success:  function (response) {
                var p = response.split("¬");
                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                  var dataset = [];
                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                    for(var i = 0; i < p.length; i=i+7){
                      var element = [p[i],p[i+2],p[i+1],p[i+3],p[i+4],p[i+5],p[i+6]];
                      dataset.push(element);
                    }
                    var largo = ($(window).height() - 370);
                    var f = new Date();
                    $('#tablaListadoClientes').DataTable( {
                        data: dataset,
                        buttons: [
                          {
                              extend: 'excel',
                              title: null,
                              text: 'Excel'
                          },
                          {
                              extend: 'pdfHtml5',
                              title: null,
                              text: 'PDF',
                              customize: function (doc) {
                  						//Remove the title created by datatTables
                  						doc.content.splice(1,0);

                              doc['footer']=(function(page, pages)  {
                                return {
                                  columns: [
                                    {
                                      alignment: 'left',
                                      text: [
                                        { text: page.toString(), italics: true },
                                        ' de ',
                                        { text: pages.toString(), italics: true }
                                      ]
                                    }
                                  ],
                                  margin: [40,10,0,0]
                                }
                              });

                              doc['header']=(function() {
                                return {
                                  columns: [
                                    {
                                      image: logo,
                                      width: 80,
                                      alignment: 'left'
                                    },
                                    {
                                      alignment: 'center',
                                      fontSize: 11,
                                      text: 'Listado de clientes',
                                      margin: [0,60,0,0],

                                    },
                                    {
                                      text: f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear(),
                                      fontSize: 11,
                                      alignment: 'right',
                                      width: 80
                                    }
                                  ],
                                  margin: [40,40,60,0]
                                }
                              });

                              doc.pageMargins = [10,140,10,60];
                              doc.content[0].table.widths = [45,95,95,150,50,50,40];
                              doc.defaultStyle.fontSize = 8;
                              doc.styles.tableHeader.fontSize = 8;
                              doc.styles.tableHeader.alignment = 'left';
                            }
                          }
                        ],
                        columns: [
                            { title: "Rut", render: $.fn.dataTable.render.text()},
                            { title: "Nombres" },
                            { title: "Apellidos" },
                            { title: "E-Mail" },
                            { title: "Celular" },
                            { title: "Fecha alta", render: $.fn.dataTable.render.moment( 'DD-MM-YYYY', 'DD-MM-YYYY' )},
                            { title: "Estado" },
                        ],
                        "scrollY": largo,
                        "scrollCollapse": true,
                        "scrollX": true,
                        "paging":  false,
                        "ordering": true,
                        "order": [[ 0, "asc" ]],
                        "info":     true,
                        "dom": 'frBtip',
                        "language": {
                            "zeroRecords": "No hay datos disponibles",
                            "info": "Registro _START_ de _END_ de _TOTAL_",
                            "infoEmpty": "No hay datos disponibles",
                            "paginate": {
                                "previous": "Anterior",
                                "next": "Siguiente"
                            },
                            "search": "Buscar: "
                        }
                    });
                  }
                }

                $.ajax({
                  url:   'controller/datosRefresh.php',
                  type:  'post',
                  success:  function (response) {
                    var p = response.split(",");
                    if(response.localeCompare("Sin datos")!= 0 && response != ""){
                      var dataset = [];
                      if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                        if(p[2] !== '1' && p[2] !== '2'){
                          $("#divListadoClientesEliminar").remove();
                        }
                        else{
                          $("#divListadoClientesEliminar").show();
                        }
                      }
                    }
                  }
                });

                setTimeout(function(){
                  $('#contenido').fadeIn();
                  $('#header').fadeIn();
                  $('#footer').fadeIn();
                  $('#tablaListadoClientes').DataTable().columns.adjust();
                  $('#modalAlertas').modal('hide');
                },1000);
              }
            });
          }, 1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("listadoUsuariosController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
  $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
  $('#modalAlertas').modal('show');
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#idMainListadoUsuarios").css("margin-top","50pt");
              $("#divTituloListadoUsuarios").css("text-align","center");
            }
            else{
              $("#divTituloListadoUsuarios").css("text-align","left");
              $("#divTituloListadoUsuarios").css("margin-bottom","-15pt");
            }

            $.ajax({
              url:   'controller/datosRefresh.php',
              type:  'post',
              success:  function (response) {
                var p = response.split(",");
                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                  var dataset = [];
                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                    if(p[2] === '2'){
                      $("#divListadoUsuariosEliminar").remove();
                    }
                    else{
                      $("#divListadoUsuariosEliminar").show();
                    }
                  }
                }
              }
            });

            $.ajax({
              url:   'controller/datosUsuarios.php',
              type:  'post',
              success:  function (response) {
                var p = response.split(",");
                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                  var dataset = [];
                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                    for(var i = 0; i < p.length; i=i+6){
                      var element = [p[i],p[i+1],p[i+2],p[i+3],p[i+4],p[i+5]];
                      dataset.push(element);
                    }
                    var largo = ($(window).height() - 370);
                    var f = new Date();
                    $('#tablaListadoUsuarios').DataTable( {
                        data: dataset,
                        buttons: [
                          {
                              extend: 'excel',
                              title: null,
                              text: 'Excel'
                          },
                          {
                              extend: 'pdfHtml5',
                              title: null,
                              text: 'PDF',
                              customize: function (doc) {
                  						//Remove the title created by datatTables
                  						doc.content.splice(1,0);

                              doc['footer']=(function(page, pages)  {
                                return {
                                  columns: [
                                    {
                                      alignment: 'left',
                                      text: [
                                        { text: page.toString(), italics: true },
                                        ' de ',
                                        { text: pages.toString(), italics: true }
                                      ]
                                    }
                                  ],
                                  margin: [40,10,0,0]
                                }
                              });

                              doc['header']=(function() {
                                return {
                                  columns: [
                                    {
                                      image: logo,
                                      width: 80,
                                      alignment: 'left'
                                    },
                                    {
                                      alignment: 'center',
                                      fontSize: 11,
                                      text: 'Listado de usuarios',
                                      margin: [0,60,0,0],

                                    },
                                    {
                                      text: f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear(),
                                      fontSize: 11,
                                      alignment: 'right',
                                      width: 80
                                    }
                                  ],
                                  margin: [40,40,60,0]
                                }
                              });

                              doc.pageMargins = [10,140,10,60];
                              doc.content[0].table.widths = [50,95,95,155,50,90];
                              doc.defaultStyle.fontSize = 8;
                              doc.styles.tableHeader.fontSize = 8;
                              doc.styles.tableHeader.alignment = 'left';
                            }
                          }
                        ],
                        columns: [
                            { title: "Rut" },
                            { title: "Apellidos" },
                            { title: "Nombres" },
                            { title: "E-Mail" },
                            { title: "Fono" },
                            { title: "Perfil" },
                        ],
                        "destroy": true,
                        "scrollY": largo,
                        "scrollX": true,
                        "scrollCollapse": true,
                        "paging":  false,
                        "ordering": true,
                        "order": [[ 0, "asc" ]],
                        "info":     true,
                        "dom": 'frBtip',
                        "language": {
                            "zeroRecords": "No hay datos disponibles",
                            "info": "Registro _START_ de _END_ de _TOTAL_",
                            "infoEmpty": "No hay datos disponibles",
                            "paginate": {
                                "previous": "Anterior",
                                "next": "Siguiente"
                            },
                            "search": "Buscar: "
                        }
                    });
                  }
                }
                else {
                  var dataset = [];
                  $('#tablaListadoUsuarios').DataTable( {
                      data: dataset,
                      buttons: [
                        {
                            extend: 'excel',
                            title: null,
                            text: 'Excel'
                        },
                        {
                            extend: 'pdfHtml5',
                            title: null,
                            text: 'PDF',
                            customize: function (doc) {
                            //Remove the title created by datatTables
                            doc.content.splice(1,0);

                            doc['footer']=(function(page, pages)  {
                              return {
                                columns: [
                                  {
                                    alignment: 'left',
                                    text: [
                                      { text: page.toString(), italics: true },
                                      ' de ',
                                      { text: pages.toString(), italics: true }
                                    ]
                                  }
                                ],
                                margin: [40,10,0,0]
                              }
                            });

                            doc['header']=(function() {
                              return {
                                columns: [
                                  {
                                    image: logo,
                                    width: 80,
                                    alignment: 'left'
                                  },
                                  {
                                    alignment: 'center',
                                    fontSize: 11,
                                    text: 'Listado de usuarios',
                                    margin: [0,60,0,0],

                                  },
                                  {
                                    text: f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear(),
                                    fontSize: 11,
                                    alignment: 'right',
                                    width: 80
                                  }
                                ],
                                margin: [40,40,60,0]
                              }
                            });

                            doc.pageMargins = [10,140,10,60];
                            doc.content[0].table.widths = [50,95,95,155,50,90];
                            doc.defaultStyle.fontSize = 8;
                            doc.styles.tableHeader.fontSize = 8;
                            doc.styles.tableHeader.alignment = 'left';
                          }
                        }
                      ],
                      columns: [
                          { title: "Rut" },
                          { title: "Apellidos" },
                          { title: "Nombres" },
                          { title: "E-Mail" },
                          { title: "Fono" },
                          { title: "Perfil" },
                      ],
                      "destroy": true,
                      "scrollY": largo,
                      "scrollX": true,
                      "scrollCollapse": true,
                      "paging":  false,
                      "ordering": true,
                      "order": [[ 0, "asc" ]],
                      "info":     true,
                      "dom": 'frBtip',
                      "language": {
                          "zeroRecords": "No hay datos disponibles",
                          "info": "Registro _START_ de _END_ de _TOTAL_",
                          "infoEmpty": "No hay datos disponibles",
                          "paginate": {
                              "previous": "Anterior",
                              "next": "Siguiente"
                          },
                          "search": "Buscar: "
                      }
                  });
                }
                setTimeout(function(){
                  $('#contenido').fadeIn();
                  $('#header').fadeIn();
                  $('#footer').fadeIn();
                  $('#tablaListadoUsuarios').DataTable().columns.adjust();
                  $('#modalAlertas').modal('hide');
                },1000);
              }
            });
          }, 1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("detalleProyectoController", function(){
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          $.ajax({
            url:   'controller/datosRefresh.php',
            type:  'post',
            success:  function (response) {
              var p = response.split(",");
              if(response.localeCompare("Sin datos")!= 0 && response != ""){
                var dataset = [];
                if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                  if(p[2] === '2'){
                    $("#divEditarDetalleProyecto").remove();
                    $("#divGuardarDetalleProyecto").remove();
                  }
                  else{
                    $("#divEditarDetalleProyecto").show();
                    $("#divGuardarDetalleProyecto").show();
                  }
                }
              }
            }
          });
          setTimeout(function(){
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#idMainDetalleProyecto").css("margin-top","50pt");
            }
            else{

            }


          }, 1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("detalleUsuarioController", function(){
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          $.ajax({
            url:   'controller/datosRefresh.php',
            type:  'post',
            success:  function (response) {
              var p = response.split(",");
              if(response.localeCompare("Sin datos")!= 0 && response != ""){
                var dataset = [];
                if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                  if(p[2] === '2'){
                    $("#divEditarDetalleUsuario").remove();
                    $("#divGuardarDetalleUsuario").remove();
                  }
                  else{
                    $("#divEditarDetalleUsuario").show();
                    $("#divGuardarDetalleUsuario").show();
                  }
                }
              }
            }
          });
          setTimeout(function(){
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#idMainDetalleUsuario").css("margin-top","50pt");
            }
            else{

            }
          }, 1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("detalleUnidadProyectoController", function(){
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          $.ajax({
            url:   'controller/datosRefresh.php',
            type:  'post',
            success:  function (response) {
              var p = response.split(",");
              if(response.localeCompare("Sin datos")!= 0 && response != ""){
                var dataset = [];
                if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                  if(p[2] !== '1' && p[2] !== '2'){
                    $("#divEditarDetalleUnidad").remove();
                    $("#divGuardarDetalleUnidad").remove();
                    if(p[2] === '6'){
                      $("#divSoloCorretaje").show();
                      $("#divEditarDetalleUnidad").show();
                      $("#divGuardarDetalleUnidad").show();
                      $("#divImgUnidad").show();
                    }
                    else{
                      $("#divSoloCorretaje").remove();
                      $("#divEditarDetalleUnidad").remove();
                      $("#divGuardarDetalleUnidad").remove();
                      $("#divImgUnidad").remove();
                    }
                  }
                  else{
                    $("#divEditarDetalleUnidad").show();
                    $("#divGuardarDetalleUnidad").show();
                    $("#divSoloCorretaje").show();
                    $("#divImgUnidad").show();
                  }
                }
              }
            }
          });
          setTimeout(function(){
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#idMainDetalleUnidad").css("margin-top","50pt");
            }
            else{

            }
          }, 1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("asignacionProyectoController", function(){
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#idMainListadoProyectosAsignar").css("margin-top","50pt");
              $("#divTituloListadoProyectosAsignarDisponibles").css("text-align","center");
              $("#divTituloListadoProyectosAsignarAsignados").css("text-align","center");
              $("#divTituloListadoProyectosAsignarPrincipal").css("text-align","center");
            }
            else{
              $("#divTituloListadoProyectosAsignarDisponibles").css("text-align","left");
              $("#divTituloListadoProyectosAsignarDisponibles").css("margin-bottom","-10pt");
              $("#divTituloListadoProyectosAsignarAsignados").css("text-align","left");
              $("#divTituloListadoProyectosAsignarAsignados").css("margin-bottom","-10pt");
              $("#divTituloListadoProyectosAsignarPrincipal").css("text-align","left");
              $("#divTituloListadoProyectosAsignarPrincipal").css("margin-bottom","-10pt");
            }
          }, 1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("detalleClienteController", function(){
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#idMainDetalleCliente").css("margin-top","50pt");
            }
            else{

            }
          }, 1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("unidadesProyectoController", function(){
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $("#idMainUnidadesProyecto").css("margin-top","50pt");
    $("#divTituloUnidadesProyecto").css("text-align","center");
  }
  else{
    $("#divTituloUnidadesProyecto").css("text-align","left");
    $("#divTituloUnidadesProyecto").css("margin-bottom","-15pt");
  }
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          $.ajax({
            url:   'controller/datosRefresh.php',
            type:  'post',
            success:  function (response) {
              var p = response.split(",");
              if(response.localeCompare("Sin datos")!= 0 && response != ""){
                var dataset = [];
                if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                  if(p[2] !== '1' && p[2] !== '2'){
                    $("#divIngresarUnidadesProyecto").remove();
                    $("#divEliminarUnidadesProyecto").remove();
                    if(p[2] === '6'){
                      $("#divIngresarUnidadesProyecto").show();
                    }
                    else {
                      $("#divIngresarUnidadesProyecto").remove();
                    }
                  }
                  else{
                    $("#divIngresarUnidadesProyecto").show();
                    $("#divEliminarUnidadesProyecto").show();
                  }
                }
              }
            }
          });
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("crearUnidadProyectoController", function(){
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
              if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                $("#idMainCrearUnidad").css("margin-top","50pt");
              }
              else{

              }
          },1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("detalleReservaController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
            $('#contenido').fadeIn();
            $('#header').fadeIn();
            $('#footer').fadeIn();
          },1000);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("listadoCotizacionesReservaController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
  $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
  $('#modalAlertas').modal('show');
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#idMainListadoCotizacionesReserva").css("margin-top","50pt");
              $("#divTituloListadoCotizacionesReserva").css("text-align","center");
            }
            else{
              $("#divTituloListadoCotizacionesReserva").css("text-align","left");
              $("#divTituloListadoCotizacionesReserva").css("margin-bottom","-10pt");
            }

            var largo = ($(window).height() - 330);
            var f = new Date();
            $('#tablaListadoCotizacionesReserva').DataTable( {
              ajax: {
                    url: "controller/datosCotizacionesReserva.php",
                    type: 'POST'
                },
                "aoColumns": [
                    { mData: 'CODIGOPROYECTO' } ,
                    { mData: 'CODIGO' },
                    { mData: 'FECHA', render: $.fn.dataTable.render.moment( 'DD-MM-YYYY', 'DD-MM-YYYY' )},
                    { mData: 'DEPTO' },
                    { mData: 'NOMBRES' },
                    { mData: 'APELLIDOS' },
                    { mData: 'RUT' },
                    { mData: 'CELULAR' },
                    { mData: 'EMAIL' },
                    { mData: 'EST' },
                    { mData: 'BOD' },
                    { mData: 'FORMA' },
                    { mData: 'VALORTOTALUF', render: $.fn.dataTable.render.number( '.', ',', 2, '' ) },
                    { mData: 'VENDEDOR' },
                    { mData: 'MEDIO' },
                    { mData: 'COTIZAEN' },
                    { mData: 'ESTADONIVELINTERES' },
                    { mData: 'ESTADO' },
                ],
                columns: [
                    { title: "Cod." },
                    { title: "Nro." },
                    { title: "Fecha" },
                    { title: "Unidad" },
                    { title: "Nombres" },
                    { title: "Apellidos" },
                    { title: "Rut" },
                    { title: "Celular" },
                    { title: "E-Mail" },
                    { title: "Est" },
                    { title: "Bod" },
                    { title: "Forma" },
                    { title: "Valor UF" },
                    { title: "Vendedor" },
                    { title: "Medio" },
                    { title: "Visita" },
                    { title: "Interés" },
                    { title: "Estado" },
                ],
                "deferRender": true,
                "scrollY": largo,
                "scrollCollapse": true,
                "scrollX": true,
                "paging":         false,
                "ordering": true,
                "order": [[ 1, "desc" ]],
                "info":     true,
                "dom": 'frtip',
                "language": {
                    "zeroRecords": "No hay datos disponibles",
                    "info": "Registro _START_ de _END_ de _TOTAL_",
                    "infoEmpty": "No hay datos disponibles",
                    "paginate": {
                        "previous": "Anterior",
                        "next": "Siguiente"
                    },
                    "search": "Buscar: "
                },
                "destroy": true
            });

            setTimeout(function(){
              $('#contenido').fadeIn();
              $('#header').fadeIn();
              $('#footer').fadeIn();
              $('#tablaListadoCotizacionesReserva').DataTable().columns.adjust();
              $('#modalAlertas').modal('hide');
            },1000);
          },1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("listadoReservasController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
  $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
  $('#modalAlertas').modal('show');
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#idMainListadoReservas").css("margin-top","50pt");
              $("#divTituloListadoReservas").css("text-align","center");
            }
            else{
              $("#divTituloListadoReservas").css("text-align","left");
              $("#divTituloListadoReservas").css("margin-bottom","-10pt");
            }
            var largo = ($(window).height() - 370);
            var f = new Date();
            $('#tablaListadoReservas').DataTable( {
              buttons: [
                {
                    extend: 'excel',
                    title: null,
                    text: 'Excel',
                    exportOptions: {
                      columns: [ 0,1,2,3,4,5,6,7,8,9,28,26,27 ],
                      format: {
                          body: function(data, row, column, node) {
                             return column == 9 ? data.replace( '.', '' ).replace( ',', '.' ) : data.replace('.','').replace(',','.');

                          }
                      }
                    },
                },
                {
                    extend: 'pdfHtml5',
                    title: null,
                    text: 'PDF',
                    //orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: {
                      columns: [ 0,1,2,3,4,5,6,7,8,9,10 ]
                    },
                    customize: function (doc) {
                    //Remove the title created by datatTables
                    doc.content.splice(1,0);

                    doc['footer']=(function(page, pages)  {
                      return {
                        columns: [
                          {
                            alignment: 'left',
                            text: [
                              { text: page.toString(), italics: true },
                              ' de ',
                              { text: pages.toString(), italics: true }
                            ]
                          }
                        ],
                        margin: [40,10,0,0]
                      }
                    });

                    doc['header']=(function() {
                      return {
                        columns: [
                          {
                            image: logo,
                            width: 80,
                            alignment: 'left'
                          },
                          {
                            alignment: 'center',
                            fontSize: 11,
                            text: 'Listado de reservas',
                            margin: [0,60,0,0],

                          },
                          {
                            text: f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear(),
                            fontSize: 11,
                            alignment: 'right',
                            width: 80
                          }
                        ],
                        margin: [40,40,60,0]
                      }
                    });

                    doc.pageMargins = [10,140,10,60];
                    doc.content[0].table.widths = [20,20,45,28,110,110,45,25,25,38,50];
                    doc.defaultStyle.fontSize = 8;
                    doc.styles.tableHeader.fontSize = 8;
                    doc.styles.tableHeader.alignment = 'left';
                  }
                }
              ],
              ajax: {
                    url: "controller/datosReservas.php",
                    type: 'POST'
                },
                "aoColumns": [
                    { mData: 'CODIGOPROYECTO' } ,
                    { mData: 'NUMERO' },
                    { mData: 'FECHA', render: $.fn.dataTable.render.moment( 'YYYY-MM-DD', 'DD-MM-YYYY' )},
                    { mData: 'UNIDAD' },
                    { mData: 'NOMBRES' },
                    { mData: 'APELLIDOS' },
                    { mData: 'RUT' },
                    { mData: 'EST' },
                    { mData: 'BOD' },
                    { mData: 'VALORTOTALUF' , render: $.fn.dataTable.render.number( '.', ',', 2, '' ) },
                    { mData: 'ESTADO' },
                    { mData: 'FICHA', className: "centerDataTable"},
                    { mData: 'FICHA_PDF' },
                    { mData: 'COTIZA', className: "centerDataTable"},
                    { mData: 'COTIZA_PDF' },
                    { mData: 'RCOMPRA', className: "centerDataTable"},
                    { mData: 'RCOMPRA_PDF' },
                    { mData: 'CI', className: "centerDataTable"},
                    { mData: 'CI_PDF' },
                    { mData: 'PB', className: "centerDataTable"},
                    { mData: 'PB_PDF' },
                    { mData: 'CC', className: "centerDataTable"},
                    { mData: 'CC_PDF' },
                    { mData: 'CR', className: "centerDataTable"},
                    { mData: 'CR_PDF' },
                    { mData: 'IDUSUARIO' },
                    { mData: 'CELULAR' },
                    { mData: 'EMAIL' },
                    { mData: 'ESTADO_TEXTO' }
                ],
                columns: [
                    { title: "Cod." },
                    { title: "Nro." },
                    { title: "Fecha" },
                    { title: "Unidad" },
                    { title: "Nombres" },
                    { title: "Apellidos" },
                    { title: "Rut" },
                    { title: "Est" },
                    { title: "Bod" },
                    { title: "Total UF" },
                    { title: "Estado" },
                    { title: "F" },
                    { title: "F_RUTA" },
                    { title: "CO" },
                    { title: "CO_RUTA" },
                    { title: "RC" },
                    { title: "RC_RUTA" },
                    { title: "CI" },
                    { title: "CI_RUTA" },
                    { title: "PB" },
                    { title: "PB_RUTA" },
                    { title: "CC" },
                    { title: "CC_RUTA" },
                    { title: "CR" },
                    { title: "CR_RUTA" },
                    { title: "IDUSUARIO" },
                    { title: "Celular" },
                    { title: "Email" },
                    { title: "Estado Texto" }
                ],
                "columnDefs": [
                  {
                      "targets": [ 12 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 14 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 16 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 18 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 20 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 22 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 24 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 25 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 26 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 27 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 28 ],
                      "visible": false,
                      "searchable": false
                  }
                ],
                "deferRender": true,
                "scrollY": largo,
                "scrollCollapse": true,
                "scrollX": true,
                "paging":         false,
                "ordering": true,
                "order": [[ 2, "desc" ]],
                "info":     true,
                "dom": 'frBtip',
                "language": {
                    "zeroRecords": "No hay datos disponibles",
                    "info": "Registro _START_ de _END_ de _TOTAL_",
                    "infoEmpty": "No hay datos disponibles",
                    "paginate": {
                        "previous": "Anterior",
                        "next": "Siguiente"
                    },
                    "search": "Buscar: "
                },
                "destroy": true
            });

            $.ajax({
              url:   'controller/datosProyectosMiUsuarioListadoCotiza.php',
              type:  'post',
              success:  function (response) {
                var p = response.split(",");
                var cuerpo = "";
                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                    for(var i = 0; i < p.length; i=i+4){
                      cuerpo += "<option value=" + p[i] + ">" + p[i+1] + "</option>";
                    }
                  }
                  cuerpo += "<option value=todos selected>Todos</option>";
                  $("#mostrarProyectoReserva").html(cuerpo);
                }
              }
            });

            $.ajax({
              url:   'controller/datosRefresh.php',
              type:  'post',
              success:  function (response) {
                var p = response.split(",");
                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                  var dataset = [];
                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                    if(p[2] !== '1' && p[2] !== '2'){
                      if(p[2] === '5' || p[2] === '4'){
                        $("#divListadoAutorizarReserva").remove();
                        $("#divListadoEliminarReserva").remove();
                        $("#divListadoSubirDocumentosReserva").remove();
                      }
                      else{
                        $("#divListadoAutorizarReserva").remove();
                      }
                    }
                    else{
                      $("#divListadoAutorizarReserva").show();
                      $("#divListadoEliminarReserva").show();
                      $("#divListadoSubirDocumentosReserva").show();
                    }
                  }
                  setTimeout(function(){
                    $('#contenido').fadeIn();
                    $('#header').fadeIn();
                    $('#footer').fadeIn();
                    $('#tablaListadoReservas').DataTable().columns.adjust();
                    $('#modalAlertas').modal('hide');
                  },1200);
                }
              }
            });
          },1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

// ================== Nico ======================= //

app.controller("listadoReservasPromesaController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
  $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
  $('#modalAlertas').modal('show');
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#idMainListadoReservasPromesa").css("margin-top","50pt");
              $("#divTituloListadoReservasPromesa").css("text-align","center");
            }
            else{
              $("#divTituloListadoReservasPromesa").css("text-align","left");
              $("#divTituloListadoReservasPromesa").css("margin-bottom","-10pt");
            }

            var largo = ($(window).height() - 330);
            var f = new Date();
            $('#tablaListadoReservasPromesa').DataTable( {
              ajax: {
                    url: "controller/datosReservasPromesa.php",
                    type: 'POST'
                },
                "aoColumns": [
                    { mData: 'CODIGOPROYECTO' } ,
                    { mData: 'NUMERO' },
                    { mData: 'FECHA', render: $.fn.dataTable.render.moment( 'DD-MM-YYYY', 'DD-MM-YYYY' )},
                    { mData: 'DEPTO' },
                    { mData: 'NOMBRES' },
                    { mData: 'APELLIDOS' },
                    { mData: 'RUT' },
                    { mData: 'EST' },
                    { mData: 'BOD' },
                    { mData: 'VALORTOTALUF', render: $.fn.dataTable.render.number( '.', ',', 2, '' ) },
                    { mData: 'VENDEDOR' },
                    { mData: 'ESTADO' },
                ],
                columns: [
                    { title: "Cod." },
                    { title: "Nro." },
                    { title: "Fecha" },
                    { title: "Unidad" },
                    { title: "Nombres" },
                    { title: "Apellidos" },
                    { title: "Rut" },
                    { title: "Est" },
                    { title: "Bod" },
                    { title: "Total UF" },
                    { title: "Vendedor" },
                    { title: "Estado" },
                ],
                "deferRender": true,
                "scrollY": largo,
                "scrollCollapse": true,
                "scrollX": true,
                "paging":         false,
                "ordering": true,
                "order": [[ 1, "desc" ]],
                "info":     true,
                "dom": 'frtip',
                "language": {
                    "zeroRecords": "No hay datos disponibles",
                    "info": "Registro _START_ de _END_ de _TOTAL_",
                    "infoEmpty": "No hay datos disponibles",
                    "paginate": {
                        "previous": "Anterior",
                        "next": "Siguiente"
                    },
                    "search": "Buscar: "
                },
                "destroy": true
            });

            setTimeout(function(){
              $('#contenido').fadeIn();
              $('#header').fadeIn();
              $('#footer').fadeIn();
              $('#tablaListadoReservasPromesa').DataTable().columns.adjust();
              $('#modalAlertas').modal('hide');
            },1000);
          },1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("listadoPromesasController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
  $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
  $('#modalAlertas').modal('show');
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#idMainListadoPromesas").css("margin-top","50pt");
              $("#divTituloListadoPromesas").css("text-align","center");
            }
            else{
              $("#divTituloListadoPromesas").css("text-align","left");
              $("#divTituloListadoPromesas").css("margin-bottom","-10pt");
            }
            var largo = ($(window).height() - 370);
            var f = new Date();
            $('#tablaListadoPromesas').DataTable( {
              buttons: [
                {
                    extend: 'excel',
                    title: null,
                    text: 'Excel',
                    exportOptions: {
                      columns: [ 0,1,2,3,4,5,6,7,8,9,36,34,35 ]
                    },
                },
                {
                    extend: 'pdfHtml5',
                    title: null,
                    text: 'PDF',
                    //orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: {
                      columns: [ 0,1,2,3,4,5,6,7,8,9,10 ]
                    },
                    customize: function (doc) {
                    //Remove the title created by datatTables
                    doc.content.splice(1,0);

                    doc['footer']=(function(page, pages)  {
                      return {
                        columns: [
                          {
                            alignment: 'left',
                            text: [
                              { text: page.toString(), italics: true },
                              ' de ',
                              { text: pages.toString(), italics: true }
                            ]
                          }
                        ],
                        margin: [40,10,0,0]
                      }
                    });

                    doc['header']=(function() {
                      return {
                        columns: [
                          {
                            image: logo,
                            width: 80,
                            alignment: 'left'
                          },
                          {
                            alignment: 'center',
                            fontSize: 11,
                            text: 'Listado de promesas',
                            margin: [0,60,0,0],

                          },
                          {
                            text: f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear(),
                            fontSize: 11,
                            alignment: 'right',
                            width: 80
                          }
                        ],
                        margin: [40,40,60,0]
                      }
                    });

                    doc.pageMargins = [10,140,10,60];
                    doc.content[0].table.widths = [20,20,45,28,110,110,45,25,25,38,50];
                    doc.defaultStyle.fontSize = 8;
                    doc.styles.tableHeader.fontSize = 8;
                    doc.styles.tableHeader.alignment = 'left';
                  }
                }
              ],
              ajax: {
                    url: "controller/datosPromesas.php",
                    type: 'POST'
                },
                "aoColumns": [
                    { mData: 'CODIGOPROYECTO' } ,
                    { mData: 'NUMERO' },
                    { mData: 'FECHA', render: $.fn.dataTable.render.moment( 'YYYY-MM-DD', 'DD-MM-YYYY' )},
                    { mData: 'UNIDAD' },
                    { mData: 'NOMBRES' },
                    { mData: 'APELLIDOS' },
                    { mData: 'RUT' },
                    { mData: 'EST' },
                    { mData: 'BOD' },
                    { mData: 'VALORTOTALUF' },
                    { mData: 'ESTADO' },
                    { mData: 'FICHA', className: "centerDataTable"},
                    { mData: 'FICHA_PDF' },
                    { mData: 'COTIZA', className: "centerDataTable"},
                    { mData: 'COTIZA_PDF' },
                    { mData: 'RCOMPRA', className: "centerDataTable"},
                    { mData: 'RCOMPRA_PDF' },
                    { mData: 'CI', className: "centerDataTable"},
                    { mData: 'CI_PDF' },
                    { mData: 'PB', className: "centerDataTable"},
                    { mData: 'PB_PDF' },
                    { mData: 'CC', className: "centerDataTable"},
                    { mData: 'CC_PDF' },
                    { mData: 'CR', className: "centerDataTable"},
                    { mData: 'CR_PDF' },
                    { mData: 'PR', className: "centerDataTable"},
                    { mData: 'PR_PDF' },
                    { mData: 'CP', className: "centerDataTable"},
                    { mData: 'CP_PDF' },
                    { mData: 'PF', className: "centerDataTable"},
                    { mData: 'PF_PDF' },
                    { mData: 'PS', className: "centerDataTable"},
                    { mData: 'PS_PDF' },
                    { mData: 'IDUSUARIO' },
                    { mData: 'CELULAR' },
                    { mData: 'EMAIL' },
                    { mData: 'ESTADO_TEXTO' },
                    { mData: 'ACCION' },
                ],
                columns: [
                    { title: "Cod." },
                    { title: "Nro." },
                    { title: "Fecha" },
                    { title: "Unidad" },
                    { title: "Nombres" },
                    { title: "Apellidos" },
                    { title: "Rut" },
                    { title: "Est" },
                    { title: "Bod" },
                    { title: "Total UF" },
                    { title: "Estado" },
                    { title: "F" },
                    { title: "F_RUTA" },
                    { title: "CO" },
                    { title: "CO_RUTA" },
                    { title: "RC" },
                    { title: "RC_RUTA" },
                    { title: "CI" },
                    { title: "CI_RUTA" },
                    { title: "PB" },
                    { title: "PB_RUTA" },
                    { title: "CC" },
                    { title: "CC_RUTA" },
                    { title: "CR" },
                    { title: "CR_RUTA" },
                    { title: "PR" },
                    { title: "PR_RUTA" },
                    { title: "CP" },
                    { title: "CP_RUTA" },
                    { title: "PF" },
                    { title: "PF_RUTA" },
                    { title: "PS" },
                    { title: "PS_RUTA" },
                    { title: "IDUSUARIO" },
                    { title: "Celular" },
                    { title: "Email" },
                    { title: "Estado Texto" },
                    { title: "Accion"},
                ],
                "columnDefs": [
                  {
                      "targets": [ 12 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 14 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 16 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 18 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 20 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 22 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 24 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 26 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 28 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 30 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 32 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 33 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 34 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 35 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 36 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 37 ],
                      "visible": false,
                      "searchable": false
                  }
                ],
                "deferRender": true,
                "scrollY": largo,
                "scrollCollapse": true,
                "scrollX": true,
                "paging":         false,
                "ordering": true,
                "order": [[ 2, "desc" ]],
                "info":     true,
                "dom": 'frBtip',
                "language": {
                    "zeroRecords": "No hay datos disponibles",
                    "info": "Registro _START_ de _END_ de _TOTAL_",
                    "infoEmpty": "No hay datos disponibles",
                    "paginate": {
                        "previous": "Anterior",
                        "next": "Siguiente"
                    },
                    "search": "Buscar: "
                },
                "destroy": true
            });

            $.ajax({
              url:   'controller/datosProyectosMiUsuarioListadoCotiza.php',
              type:  'post',
              success:  function (response) {
                var p = response.split(",");
                var cuerpo = "";
                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                    for(var i = 0; i < p.length; i=i+4){
                      cuerpo += "<option value=" + p[i] + ">" + p[i+1] + "</option>";
                    }
                  }
                  cuerpo += "<option value=todos selected>Todos</option>";
                  $("#mostrarProyectoPromesa").html(cuerpo);
                }
              }
            });

            $.ajax({
              url:   'controller/datosRefresh.php',
              type:  'post',
              success:  function (response) {
                var p = response.split(",");
                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                  var dataset = [];
                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                    if(p[2] !== '1' && p[2] !== '2'){
                      if(p[2] === '5' || p[2] === '4'){
                        $("#divListadoAutorizarPromesa").remove();
                        $("#divListadoSubirDocumentosPromesa").remove();
                        $("#divListadoEliminarPromesa").remove();
                      }
                      else{
                        $("#divListadoAutorizarPromesa").remove();
                      }
                    }
                    else{
                      $("#divListadoAutorizarPromesa").show();
                    }
                  }
                  setTimeout(function(){
                    $('#contenido').fadeIn();
                    $('#header').fadeIn();
                    $('#footer').fadeIn();
                    $('#tablaListadoPromesas').DataTable().columns.adjust();
                    $('#modalAlertas').modal('hide');
                  },1200);
                }
              }
            });
          },1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("detallePromesaController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
            $('#contenido').fadeIn();
            $('#header').fadeIn();
            $('#footer').fadeIn();
          },1000);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("listadoPromesasEscrituraController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
  $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
  $('#modalAlertas').modal('show');
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#idMainListadoPromesasEscritura").css("margin-top","50pt");
              $("#divTituloListadoPromesasEscritura").css("text-align","center");
            }
            else{
              $("#divTituloListadoPromesasEscritura").css("text-align","left");
              $("#divTituloListadoPromesasEscritura").css("margin-bottom","-10pt");
            }

            var largo = ($(window).height() - 330);
            var f = new Date();
            $('#tablaListadoPromesasEscritura').DataTable( {
              ajax: {
                    url: "controller/datosPromesasEscritura.php",
                    type: 'POST'
                },
                "aoColumns": [
                    { mData: 'CODIGOPROYECTO' } ,
                    { mData: 'NUMERO' },
                    { mData: 'FECHA', render: $.fn.dataTable.render.moment( 'DD-MM-YYYY', 'DD-MM-YYYY' )},
                    { mData: 'DEPTO' },
                    { mData: 'NOMBRES' },
                    { mData: 'APELLIDOS' },
                    { mData: 'RUT' },
                    { mData: 'EST' },
                    { mData: 'BOD' },
                    { mData: 'VALORTOTALUF', render: $.fn.dataTable.render.number( '.', ',', 2, '' ) },
                    { mData: 'ESTADO' },
                ],
                columns: [
                    { title: "Cod." },
                    { title: "Nro." },
                    { title: "Fecha" },
                    { title: "Unidad" },
                    { title: "Nombres" },
                    { title: "Apellidos" },
                    { title: "Rut" },
                    { title: "Est" },
                    { title: "Bod" },
                    { title: "Total UF" },
                    { title: "Estado" },
                ],
                "deferRender": true,
                "scrollY": largo,
                "scrollCollapse": true,
                "scrollX": true,
                "paging":         false,
                "ordering": true,
                "order": [[ 2, "desc" ]],
                "info":     true,
                "dom": 'frtip',
                "language": {
                    "zeroRecords": "No hay datos disponibles",
                    "info": "Registro _START_ de _END_ de _TOTAL_",
                    "infoEmpty": "No hay datos disponibles",
                    "paginate": {
                        "previous": "Anterior",
                        "next": "Siguiente"
                    },
                    "search": "Buscar: "
                },
                "destroy": true
            });

            setTimeout(function(){
              $('#contenido').fadeIn();
              $('#header').fadeIn();
              $('#footer').fadeIn();
              $('#tablaListadoPromesasEscritura').DataTable().columns.adjust();
              $('#modalAlertas').modal('hide');
            },1000);
          },1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("listadoEscriturasController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
  $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
  $('#modalAlertas').modal('show');
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#idMainListadoEscrituras").css("margin-top","50pt");
              $("#divTituloListadoEscrituras").css("text-align","center");
            }
            else{
              $("#divTituloListadoEscrituras").css("text-align","left");
              $("#divTituloListadoEscrituras").css("margin-bottom","-10pt");
            }
            var largo = ($(window).height() - 370);
            var f = new Date();
            $('#tablaListadoEscrituras').DataTable( {
              buttons: [
                {
                    extend: 'excel',
                    title: null,
                    text: 'Excel',
                    exportOptions: {
                      columns: [ 0,1,2,3,4,5,6,7,8,9,42,40,41 ]
                    },
                },
                {
                    extend: 'pdfHtml5',
                    title: null,
                    text: 'PDF',
                    //orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: {
                      columns: [ 0,1,2,3,4,5,6,7,8,9,10 ]
                    },
                    customize: function (doc) {
                    //Remove the title created by datatTables
                    doc.content.splice(1,0);

                    doc['footer']=(function(page, pages)  {
                      return {
                        columns: [
                          {
                            alignment: 'left',
                            text: [
                              { text: page.toString(), italics: true },
                              ' de ',
                              { text: pages.toString(), italics: true }
                            ]
                          }
                        ],
                        margin: [40,10,0,0]
                      }
                    });

                    doc['header']=(function() {
                      return {
                        columns: [
                          {
                            image: logo,
                            width: 80,
                            alignment: 'left'
                          },
                          {
                            alignment: 'center',
                            fontSize: 11,
                            text: 'Listado de escrituras',
                            margin: [0,60,0,0],

                          },
                          {
                            text: f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear(),
                            fontSize: 11,
                            alignment: 'right',
                            width: 80
                          }
                        ],
                        margin: [40,40,60,0]
                      }
                    });

                    doc.pageMargins = [10,140,10,60];
                    doc.content[0].table.widths = [20,20,45,28,110,110,45,25,25,38,50];
                    doc.defaultStyle.fontSize = 8;
                    doc.styles.tableHeader.fontSize = 8;
                    doc.styles.tableHeader.alignment = 'left';
                  }
                }
              ],
              ajax: {
                    url: "controller/datosEscrituras.php",
                    type: 'POST'
                },
                "aoColumns": [
                    { mData: 'CODIGOPROYECTO' } ,
                    { mData: 'NUMERO' },
                    { mData: 'FECHA', render: $.fn.dataTable.render.moment( 'YYYY-MM-DD', 'DD-MM-YYYY' )},
                    { mData: 'UNIDAD' },
                    { mData: 'NOMBRES' },
                    { mData: 'APELLIDOS' },
                    { mData: 'RUT' },
                    { mData: 'EST' },
                    { mData: 'BOD' },
                    { mData: 'VALORTOTALUF' },
                    { mData: 'ESTADO' },
                    { mData: 'FICHA', className: "centerDataTable"},
                    { mData: 'FICHA_PDF' },
                    { mData: 'COTIZA', className: "centerDataTable"},
                    { mData: 'COTIZA_PDF' },
                    { mData: 'RCOMPRA', className: "centerDataTable"},
                    { mData: 'RCOMPRA_PDF' },
                    { mData: 'CI', className: "centerDataTable"},
                    { mData: 'CI_PDF' },
                    { mData: 'PB', className: "centerDataTable"},
                    { mData: 'PB_PDF' },
                    { mData: 'CC', className: "centerDataTable"},
                    { mData: 'CC_PDF' },
                    { mData: 'CR', className: "centerDataTable"},
                    { mData: 'CR_PDF' },
                    { mData: 'PR', className: "centerDataTable"},
                    { mData: 'PR_PDF' },
                    { mData: 'CP', className: "centerDataTable"},
                    { mData: 'CP_PDF' },
                    { mData: 'PF', className: "centerDataTable"},
                    { mData: 'PF_PDF' },
                    { mData: 'PS', className: "centerDataTable"},
                    { mData: 'PS_PDF' },
                    { mData: 'ES', className: "centerDataTable"},
                    { mData: 'ES_PDF' },
                    { mData: 'EN', className: "centerDataTable"},
                    { mData: 'EN_PDF' },
                    { mData: 'DE', className: "centerDataTable"},
                    { mData: 'DE_PDF' },
                    { mData: 'IDUSUARIO' },
                    { mData: 'CELULAR' },
                    { mData: 'EMAIL' },
                    { mData: 'ESTADO_TEXTO' }
                ],
                columns: [
                    { title: "Cod." },
                    { title: "Nro." },
                    { title: "Fecha" },
                    { title: "Unidad" },
                    { title: "Nombres" },
                    { title: "Apellidos" },
                    { title: "Rut" },
                    { title: "Est" },
                    { title: "Bod" },
                    { title: "Total UF" },
                    { title: "Estado" },
                    { title: "F" },
                    { title: "F_RUTA" },
                    { title: "CO" },
                    { title: "CO_RUTA" },
                    { title: "RC" },
                    { title: "RC_RUTA" },
                    { title: "CI" },
                    { title: "CI_RUTA" },
                    { title: "PB" },
                    { title: "PB_RUTA" },
                    { title: "CC" },
                    { title: "CC_RUTA" },
                    { title: "CR" },
                    { title: "CR_RUTA" },
                    { title: "PR" },
                    { title: "PR_RUTA" },
                    { title: "CP" },
                    { title: "CP_RUTA" },
                    { title: "PF" },
                    { title: "PF_RUTA" },
                    { title: "PS" },
                    { title: "PS_RUTA" },
                    { title: "ES" },
                    { title: "ES_RUTA" },
                    { title: "EN" },
                    { title: "EN_RUTA" },
                    { title: "DE" },
                    { title: "DE_RUTA" },
                    { title: "IDUSUARIO" },
                    { title: "Celular" },
                    { title: "Email" },
                    { title: "Estado Texto" }
                ],
                "columnDefs": [
                  {
                      "targets": [ 12 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 14 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 16 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 18 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 20 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 22 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 24 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 26 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 28 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 30 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 32 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 34 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 36 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 38 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 39 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 40 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 41 ],
                      "visible": false,
                      "searchable": false
                  },
                  {
                      "targets": [ 42 ],
                      "visible": false,
                      "searchable": false
                  }
                ],
                "deferRender": true,
                "scrollY": largo,
                "scrollCollapse": true,
                "scrollX": true,
                "paging":         false,
                "ordering": true,
                "order": [[ 2, "desc" ]],
                "info":     true,
                "dom": 'frBtip',
                "language": {
                    "zeroRecords": "No hay datos disponibles",
                    "info": "Registro _START_ de _END_ de _TOTAL_",
                    "infoEmpty": "No hay datos disponibles",
                    "paginate": {
                        "previous": "Anterior",
                        "next": "Siguiente"
                    },
                    "search": "Buscar: "
                },
                "destroy": true
            });

            $.ajax({
              url:   'controller/datosProyectosMiUsuarioListadoCotiza.php',
              type:  'post',
              success:  function (response) {
                var p = response.split(",");
                var cuerpo = "";
                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                    for(var i = 0; i < p.length; i=i+4){
                      cuerpo += "<option value=" + p[i] + ">" + p[i+1] + "</option>";
                    }
                  }
                  cuerpo += "<option value=todos selected>Todos</option>";
                  $("#mostrarProyectoEscritura").html(cuerpo);
                }
              }
            });

            $.ajax({
              url:   'controller/datosRefresh.php',
              type:  'post',
              success:  function (response) {
                var p = response.split(",");
                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                  var dataset = [];
                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                    if(p[2] !== '1' && p[2] !== '2'){
                      if(p[2] === '5' || p[2] === '4'){
                          $("#divListadoAutorizarEscritura").remove();
                          $("#divListadoSubirDocumentosEscritura").remove();
                          $("#divListadoEliminarEscritura").remove();
                      }
                      else{
                          $("#divListadoAutorizarEscritura").remove();
                      }
                    }
                    else{
                      $("#divListadoAutorizarEscritura").show();
                    }
                  }
                  setTimeout(function(){
                    $('#contenido').fadeIn();
                    $('#header').fadeIn();
                    $('#footer').fadeIn();
                    $('#tablaListadoEscrituras').DataTable().columns.adjust();
                    $('#modalAlertas').modal('hide');
                  },1200);
                }
              }
            });
          },1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});
app.controller("detalleEscrituraController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  setTimeout(function(){
    $('#contenido').fadeIn();
    $('#header').fadeIn();
    $('#footer').fadeIn();
  },1000);
});

app.controller("listadoContableController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
  $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
  $('#modalAlertas').modal('show');
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
              $("#idMainListadoContable").css("margin-top","50pt");
              $("#divTituloListadoContable").css("text-align","center");
            }
            else{
              $("#divTituloListadoContable").css("text-align","left");
              $("#divTituloListadoContable").css("margin-bottom","-10pt");
            }

            var largo = ($(window).height() - 330);
            var f = new Date();
            $('#tablaListadoContable').DataTable( {
              buttons: [
                {
                    extend: 'excel',
                    title: null,
                    text: 'Excel',
                    exportOptions: {
                      columns: [ 0,1,2,3,4,5,6,7,16,9,10,11,12,13,14,15 ],
                      format: {
                          body: function(data, row, column, node) {
                             return column >= 5 && column <= 7 ? data.replace( '.', '' ).replace( ',', '.' ) : data.replace('.','').replace(',','.');
                          }
                      }
                    },
                },
                {
                    extend: 'pdfHtml5',
                    title: null,
                    text: 'PDF',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: {
                      columns: [ 0,1,2,3,4,5,6,7,16,9,10,11,12,13,14,15 ]
                    },
                    customize: function (doc) {
                    //Remove the title created by datatTables
                    doc.content.splice(1,0);

                    doc['footer']=(function(page, pages)  {
                      return {
                        columns: [
                          {
                            alignment: 'left',
                            text: [
                              { text: page.toString(), italics: true },
                              ' de ',
                              { text: pages.toString(), italics: true }
                            ]
                          }
                        ],
                        margin: [40,10,0,0]
                      }
                    });

                    doc['header']=(function() {
                      return {
                        columns: [
                          {
                            image: logo,
                            width: 80,
                            alignment: 'left'
                          },
                          {
                            alignment: 'center',
                            fontSize: 11,
                            text: 'Listado Contable',
                            margin: [0,60,0,0],

                          },
                          {
                            text: f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear(),
                            fontSize: 11,
                            alignment: 'right',
                            width: 80
                          }
                        ],
                        margin: [40,40,60,0]
                      }
                    });

                    doc.pageMargins = [10,140,10,60];
                    doc.content[0].table.widths = [40,40,35,25,25,45,45,45,60,70,40,60,80,80,60,120];
                    doc.defaultStyle.fontSize = 8;
                    doc.styles.tableHeader.fontSize = 8;
                    doc.styles.tableHeader.alignment = 'left';
                  }
                }
              ],
              ajax: {
                    url: "controller/datosOperacionesContables.php",
                    type: 'POST'
                },
                "aoColumns": [
                    { mData: 'CODIGOPROYECTO' } ,
                    { mData: 'NUMERO' },
                    { mData: 'DEPTO'},
                    { mData: 'EST' },
                    { mData: 'BOD' },
                    { mData: 'VALORTOTALUF', render: $.fn.dataTable.render.number( '.', ',', 2, '' ) },
                    { mData: 'VALORPIE', render: $.fn.dataTable.render.number( '.', ',', 2, '' ) },
                    { mData: 'VALORSALDOUF', render: $.fn.dataTable.render.number( '.', ',', 2, '' ) },
                    { mData: 'ESTADO' },
                    { mData: 'BANCO' },
                    { mData: 'INVERSION' },
                    { mData: 'RUT' },
                    { mData: 'NOMBRES' },
                    { mData: 'APELLIDOS' },
                    { mData: 'CELULAR' },
                    { mData: 'EMAIL' },
                    { mData: 'ESTADO_TEXTO' },
                ],
                columns: [
                    { title: "Proyecto" },
                    { title: "Operación" },
                    { title: "Depto" },
                    { title: "EST" },
                    { title: "BOD" },
                    { title: "Total UF" },
                    { title: "Pie UF" },
                    { title: "Sado UF" },
                    { title: "Estado" },
                    { title: "Banco" },
                    { title: "Inversión" },
                    { title: "Rut Cliente" },
                    { title: "Nombres" },
                    { title: "Apellidos" },
                    { title: "Celular" },
                    { title: "Email" },
                    { title: "Estado Texto" },
                ],
                "columnDefs": [
                  {
                      "targets": [ 16 ],
                      "visible": false,
                      "searchable": false
                  }
                ],
                "deferRender": true,
                "scrollY": largo,
                "scrollCollapse": true,
                "scrollX": true,
                "paging":         false,
                "ordering": true,
                "order": [[ 2, "asc" ]],
                "info":     true,
                "dom": 'frBtip',
                "language": {
                    "zeroRecords": "No hay datos disponibles",
                    "info": "Registro _START_ de _END_ de _TOTAL_",
                    "infoEmpty": "No hay datos disponibles",
                    "paginate": {
                        "previous": "Anterior",
                        "next": "Siguiente"
                    },
                    "search": "Buscar: "
                },
                "destroy": true
            });
            $.ajax({
              url:   'controller/datosProyectosMiUsuarioListadoCotiza.php',
              type:  'post',
              success:  function (response) {
                var p = response.split(",");
                var cuerpo = "";
                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                    for(var i = 0; i < p.length; i=i+4){
                      cuerpo += "<option value=" + p[i] + ">" + p[i+1] + "</option>";
                    }
                  }
                  cuerpo += "<option value='todos' selected>Todos</option>";
                  $("#mostrarProyectosContable").html(cuerpo);
                }
              }
            });
            $.ajax({
              url:   'controller/datosEstadosUnidad.php',
              type:  'post',
              success:  function (response) {
                var p = response.split(",");
                var cuerpo = "";
                if(response.localeCompare("Sin datos")!= 0 && response != ""){
                  if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
                    for(var i = 6; i < p.length; i=i+3){
                      cuerpo += "<option value=" + p[i] + ">" + p[i+1] + "</option>";
                    }
                  }
                  cuerpo += "<option value='todos' selected>Todos</option>";
                  $("#mostrarEstadosContable").html(cuerpo);
                }
              }
            });

            setTimeout(function(){
              $('#contenido').fadeIn();
              $('#header').fadeIn();
              $('#footer').fadeIn();
              $('#tablaListadoContable').DataTable().columns.adjust();
              $('#modalAlertas').modal('hide');
            },1000);
          },1500);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});

app.controller("verUnidadContableController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $.ajax({
    url:   'controller/datosRefresh.php',
    type:  'post',
    success:  function (response) {
      var p = response.split(",");
      if(response.localeCompare("Sin datos")!= 0 && response != ""){
        var dataset = [];
        if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
          setTimeout(function(){
            $('#contenido').fadeIn();
            $('#header').fadeIn();
            $('#footer').fadeIn();
          },1000);
        }
      }
      else{
        window.location.href = "#/home";
      }
    }
  });
});
app.controller("listadoComisionesController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
  $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
  $('#modalAlertas').modal('show');
  setTimeout(function(){
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      $("#idMainListadoComisiones").css("margin-top","50pt");
      $("#divTituloListadoComisiones").css("text-align","center");
    }
    else{
      $("#divTituloListadoComisiones").css("text-align","left");
      $("#divTituloListadoComisiones").css("margin-bottom","-10pt");
    }

    var largo = ($(window).height() - 330);
    var f = new Date();
    $.ajax({
      url:   'controller/datosMesAnoComision.php',
      type:  'post',
      success:  function (response) {
        var p = response.split(",");
        var cuerpo = "";
        if(response.localeCompare("Sin datos")!= 0 && response != ""){
          if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
            for(var i = 0; i < p.length; i++){
              cuerpo += "<option value='"+p[i]+"'>" + p[i] + "</option>";
            }
          }
          $("#mostrarMesAnoComisiones").html(cuerpo);
          setTimeout(function(){
            var separa = $("#mostrarMesAnoComisiones :selected").text().split(' - ');
            var ano = separa[0];
            var mes = separa[1];
            var ultimoDia = new Date(ano, mes, 0);
            var fechaJqueryUF = moment(ultimoDia).format('DD-MM-YYYY');

            var dia = fechaJqueryUF.split('-')[0];
            var mes = fechaJqueryUF.split('-')[1];
            var ano = fechaJqueryUF.split('-')[2];

            $.ajax({
              url:   'controller/cargaUF.php?fecha=' + fechaJqueryUF,
              success:  function (response) {
                var valorUFJqueryHoy = 0;
                if(response !== '0' && response !== 'Sin datos'){
                  var valorUFJqueryHoy = response.replace('.','').replace(',','.');
                }
              var parametrosJquery = {
                "valorUFJqueryHoy": valorUFJqueryHoy
              }
              $('#tablaListadoComisiones').DataTable( {
                ajax: {
                      url: "controller/datosComisionesProyectosPorVendedor.php",
                      data: parametrosJquery,
                      type: 'POST'
                  },
                  "aoColumns": [
                      { mData: 'VENDEDOR' } ,
                      { mData: 'RUT' },
                      { mData: 'CODIGOPROYECTO'},
                      { mData: 'TOTALPROMESA', render: $.fn.dataTable.render.number( '.', ',', 2, '' ), "defaultContent": '0,00' },
                      { mData: 'TOTALPROMESAPESOS', render: $.fn.dataTable.render.number( '.', ',', 0, '$ ' ), "defaultContent": '0' },
                      { mData: 'TOTALESCRITURA', render: $.fn.dataTable.render.number( '.', ',', 2, '' ), "defaultContent": '0,00' },
                      { mData: 'TOTALESCRITURAPESOS', render: $.fn.dataTable.render.number( '.', ',', 0, '$ ' ), "defaultContent": '0' },
                  ],
                  columns: [
                      { title: "Nombre Vendedor" },
                      { title: "Rut" },
                      { title: "Proyecto" },
                      { title: "Total Promesa UF"},
                      { title: "Total Promesa $"},
                      { title: "Total Escritura UF"},
                      { title: "Total Escritura $"},
                  ],
                  buttons: [
                          {
                              extend: 'excel',
                              exportOptions: {
                                columns: ':visible',
                                format: {
                                    body: function(data, row, column, node) {
                                       return column >= 4 && column <= 6? data.replace( '.', '' ).replace( ',', '.' ).replace( '$ ', '' ) : data.replace('.','').replace(',','.').replace( '$ ', '' );

                                    }
                                }
                              },
                              title: null,
                              text: 'Excel'
                          },
                          {
                              extend: 'pdfHtml5',
                              title: null,
                              text: 'PDF',
                              pageSize: 'LEGAL',
                              exportOptions: {
                                columns: [ 0,1,2,3,4,5,6 ]
                              },
                              customize: function (doc) {
                              //Remove the title created by datatTables
                              doc.content.splice(1,0);

                              doc['footer']=(function(page, pages)  {
                                return {
                                  columns: [
                                    {
                                      alignment: 'left',
                                      text: [
                                        { text: page.toString(), italics: true },
                                        ' de ',
                                        { text: pages.toString(), italics: true }
                                      ]
                                    }
                                  ],
                                  margin: [40,10,0,0]
                                }
                              });

                              doc['header']=(function() {
                                return {
                                  columns: [
                                    {
                                      image: logo,
                                      width: 80,
                                      alignment: 'left'
                                    },
                                    {
                                      alignment: 'center',
                                      fontSize: 11,
                                      text: 'Listado de comisiones',
                                      margin: [0,60,0,0],

                                    },
                                    {
                                      text: f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear(),
                                      fontSize: 11,
                                      alignment: 'right',
                                      width: 80
                                    }
                                  ],
                                  margin: [40,40,60,0]
                                }
                              });

                              doc.pageMargins = [10,140,10,60];
                                doc.content[0].table.widths = ['*','*','*','*','*','*','*'];
                              doc.defaultStyle.fontSize = 8;
                              doc.styles.tableHeader.fontSize = 8;
                              doc.styles.tableHeader.alignment = 'left';
                            }
                          }
                        ],
                  "deferRender": true,
                  "scrollY": largo,
                  "scrollCollapse": true,
                  "scrollX": true,
                  "paging":         false,
                  "ordering": true,
                  "order": [[ 1, "asc" ]],
                  "info":     true,
                  "dom": 'frBtip',
                  "language": {
                      "zeroRecords": "No hay datos disponibles",
                      "info": "Registro _START_ de _END_ de _TOTAL_",
                      "infoEmpty": "No hay datos disponibles",
                      "paginate": {
                          "previous": "Anterior",
                          "next": "Siguiente"
                      },
                      "search": "Buscar: "
                  },
                  "destroy": true,
                  "footerCallback": function ( row, data, start, end, display ) {
                    var datosPromesaUF = this.api().column(3).data();
                    var totalPromesaUF = 0;
                    for (var i = 0; i < datosPromesaUF.length; i++) {
                      if (datosPromesaUF[i] !== '' && datosPromesaUF[i] !== null && datosPromesaUF[i] !== undefined) {
                        totalPromesaUF += parseFloat(datosPromesaUF[i]);
                      }
                    }
                    totalPromesaUF = roundNumber(totalPromesaUF,2);

                    var datosPromesaPesos = this.api().column(4).data();
                    var totalPromesaPesos = 0;
                    for (var i = 0; i < datosPromesaPesos.length; i++) {
                      if (datosPromesaPesos[i] !== '' && datosPromesaPesos[i] !== null && datosPromesaPesos[i] !== undefined) {
                        totalPromesaPesos += parseInt(datosPromesaPesos[i]);
                      }
                    }

                    var datosEscrituraUF = this.api().column(5).data();
                    var totalEscrituraUF = 0;
                    for (var i = 0; i < datosEscrituraUF.length; i++) {
                      if (datosEscrituraUF[i] !== '' && datosEscrituraUF[i] !== null && datosEscrituraUF[i] !== undefined) {
                        totalEscrituraUF += parseFloat(datosEscrituraUF[i]);
                      }
                    }
                    totalEscrituraUF = roundNumber(totalEscrituraUF,2);

                    var datosEscrituraPesos = this.api().column(6).data();
                    var totalEscrituraPesos = 0;
                    for (var i = 0; i < datosEscrituraPesos.length; i++) {
                      if (datosEscrituraPesos[i] !== '' && datosEscrituraPesos[i] !== null && datosEscrituraPesos[i] !== undefined) {
                        totalEscrituraPesos += parseInt(datosEscrituraPesos[i]);
                      }
                    }

                    $(this.api().column(3).footer()).html(totalPromesaUF.formatMoney(2,'.',',').toString());
                    $(this.api().column(4).footer()).html("$ "+totalPromesaPesos.formatMoney(0,'.',',').toString());
                    $(this.api().column(5).footer()).html(totalEscrituraUF.formatMoney(2,'.',',').toString());
                    $(this.api().column(6).footer()).html("$ "+totalEscrituraPesos.formatMoney(0,'.',',').toString());
                    if (valorUFJqueryHoy == 0) {
                      $("#textoUF").html("&nbsp;&nbsp;(valor UF no publicado para el mes actual)");
                    }else{
                      $("#textoUF").html('');
                    }
                  },
                  'initComplete': function(){
                    setTimeout(function() {
                      $('#tablaListadoComisiones').DataTable().columns.adjust();
                      $('#modalAlertas').modal('hide');
                    }, 1000);
                  },
              });
            }
          });
          },500);
        }else{
          $('#tablaListadoComisiones').DataTable( {
            ajax: {
                  url: "controller/datosComisionesProyectosPorVendedor.php",
                  type: 'POST'
              },
              "aoColumns": [
                  { mData: 'VENDEDOR' } ,
                  { mData: 'RUT' },
                  { mData: 'CODIGOPROYECTO'},
                  { mData: 'TOTALPROMESA', render: $.fn.dataTable.render.number( '.', ',', 2, '' ), "defaultContent": '0,00' },
                  { mData: 'TOTALPROMESAPESOS', render: $.fn.dataTable.render.number( '.', ',', 0, '$ ' ), "defaultContent": '0' },
                  { mData: 'TOTALESCRITURA', render: $.fn.dataTable.render.number( '.', ',', 2, '' ), "defaultContent": '0,00' },
                  { mData: 'TOTALESCRITURAPESOS', render: $.fn.dataTable.render.number( '.', ',', 0, '$ ' ), "defaultContent": '0' },
              ],
              columns: [
                  { title: "Nombre Vendedor" },
                  { title: "Rut" },
                  { title: "Proyecto" },
                  { title: "Total Promesa UF"},
                  { title: "Total Promesa $"},
                  { title: "Total Escritura UF"},
                  { title: "Total Escritura $"},
              ],
              buttons: [
                      {
                          extend: 'excel',
                          exportOptions: {
                            columns: ':visible',
                            format: {
                                body: function(data, row, column, node) {
                                   return column >= 4 && column <= 6? data.replace( '.', '' ).replace( ',', '.' ).replace( '$ ', '' ) : data.replace('.','').replace(',','.').replace( '$ ', '' );

                                }
                            }
                          },
                          title: null,
                          text: 'Excel'
                      },
                      {
                          extend: 'pdfHtml5',
                          title: null,
                          text: 'PDF',
                          pageSize: 'LEGAL',
                          exportOptions: {
                            columns: [ 0,1,2,3,4,5,6 ]
                          },
                          customize: function (doc) {
                          //Remove the title created by datatTables
                          doc.content.splice(1,0);

                          doc['footer']=(function(page, pages)  {
                            return {
                              columns: [
                                {
                                  alignment: 'left',
                                  text: [
                                    { text: page.toString(), italics: true },
                                    ' de ',
                                    { text: pages.toString(), italics: true }
                                  ]
                                }
                              ],
                              margin: [40,10,0,0]
                            }
                          });

                          doc['header']=(function() {
                            return {
                              columns: [
                                {
                                  image: logo,
                                  width: 80,
                                  alignment: 'left'
                                },
                                {
                                  alignment: 'center',
                                  fontSize: 11,
                                  text: 'Listado de comisiones',
                                  margin: [0,60,0,0],

                                },
                                {
                                  text: f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear(),
                                  fontSize: 11,
                                  alignment: 'right',
                                  width: 80
                                }
                              ],
                              margin: [40,40,60,0]
                            }
                          });

                          doc.pageMargins = [10,140,10,60];
                            doc.content[0].table.widths = ['*','*','*','*','*','*','*'];
                          doc.defaultStyle.fontSize = 8;
                          doc.styles.tableHeader.fontSize = 8;
                          doc.styles.tableHeader.alignment = 'left';
                        }
                      }
                    ],
              "deferRender": true,
              "scrollY": largo,
              "scrollCollapse": true,
              "scrollX": true,
              "paging":         false,
              "ordering": true,
              "order": [[ 1, "asc" ]],
              "info":     true,
              "dom": 'frBtip',
              "language": {
                  "zeroRecords": "No hay datos disponibles",
                  "info": "Registro _START_ de _END_ de _TOTAL_",
                  "infoEmpty": "No hay datos disponibles",
                  "paginate": {
                      "previous": "Anterior",
                      "next": "Siguiente"
                  },
                  "search": "Buscar: "
              },
              "destroy": true,
              'initComplete': function(){
                setTimeout(function() {
                  $('#tablaListadoComisiones').DataTable().columns.adjust();
                  $('#modalAlertas').modal('hide');
                }, 1000);
              },
          });
        }
      }
    });
    $.ajax({
      url:   'controller/datosProyectosMiUsuarioListadoCotiza.php',
      type:  'post',
      success:  function (response) {
        var p = response.split(",");
        var cuerpo = "";
        if(response.localeCompare("Sin datos")!= 0 && response != ""){
          if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
            for(var i = 0; i < p.length; i=i+4){
              cuerpo += "<option value=" + p[i] + ">" + p[i+1] + "</option>";
            }
          }
          if (cuerpo.indexOf("<option value=COR>CORRETAJE</option>")>-1) {
            var separa = cuerpo.split("<option value=COR>CORRETAJE</option>");
            cuerpo = separa[0]+separa[1];
          }
          cuerpo += "<option value='todos' selected>Todos</option>";
          $("#mostrarProyectosComisiones").html(cuerpo);
        }
      }
    });

    setTimeout(function(){
      $('#contenido').fadeIn();
      $('#header').fadeIn();
      $('#footer').fadeIn();
    },1000);
  },1500);
});
app.controller("listadoComisionesVendedorController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  setTimeout(function(){
    $('#contenido').fadeIn();
    $('#header').fadeIn();
    $('#footer').fadeIn();
  },1000);
});

app.controller("listadoComisionesEmpresaController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {

    }
  });
  $("#modalAlertas").modal({backdrop: 'static', keyboard: false});
  $("#textoModal").html("<img src='view/img/logos/dc_logo_transparente.png' class='splash_charge_logo'><img src='view/img/load_verde.gif' class='splash_charge'>");
  $('#modalAlertas').modal('show');
  setTimeout(function(){
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      $("#idMainListadoComisionesEmpresa").css("margin-top","50pt");
      $("#divTituloListadoComisionesEmpresa").css("text-align","center");
    }
    else{
      $("#divTituloListadoComisionesEmpresa").css("text-align","left");
      $("#divTituloListadoComisionesEmpresa").css("margin-bottom","-10pt");
    }

    var largo = ($(window).height() - 330);
    var f = new Date();
    $.ajax({
      url:   'controller/datosMesAnoComision.php',
      type:  'post',
      success:  function (response) {
        var p = response.split(",");
        var cuerpo = "";
        if(response.localeCompare("Sin datos")!= 0 && response != ""){
          if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
            for(var i = 0; i < p.length; i++){
              cuerpo += "<option value='"+p[i]+"'>" + p[i] + "</option>";
            }
          }
          $("#mostrarMesAnoComisionesEmpresa").html(cuerpo);
          setTimeout(function(){
            var separa = $("#mostrarMesAnoComisionesEmpresa :selected").text().split(' - ');
            var ano = separa[0];
            var mes = separa[1];
            var ultimoDia = new Date(ano, mes, 0);
            var fechaJqueryUF = moment(ultimoDia).format('DD-MM-YYYY');

            var dia = fechaJqueryUF.split('-')[0];
            var mes = fechaJqueryUF.split('-')[1];
            var ano = fechaJqueryUF.split('-')[2];

            $.ajax({
              url:   'controller/cargaUF.php?fecha=' + fechaJqueryUF,
              success:  function (response) {
                var valorUFJqueryHoy = 0;
                if(response !== '0' && response !== 'Sin datos'){
                  var valorUFJqueryHoy = response.replace('.','').replace(',','.');
                }
              var parametrosJquery = {
                "valorUFJqueryHoy": valorUFJqueryHoy
              }
              $('#tablaListadoComisionesEmpresa').DataTable( {
                ajax: {
                      url: "controller/datosComisionesProyectosPorEmpresa.php",
                      data: parametrosJquery,
                      type: 'POST'
                  },
                  "aoColumns": [
                      { mData: 'CODIGOPROYECTO'},
                      { mData: 'TOTALPROMESA', render: $.fn.dataTable.render.number( '.', ',', 2, '' ), "defaultContent": '0,00' },
                      { mData: 'TOTALPROMESAPESOS', render: $.fn.dataTable.render.number( '.', ',', 0, '$ ' ), "defaultContent": '0' },
                      { mData: 'TOTALESCRITURA', render: $.fn.dataTable.render.number( '.', ',', 2, '' ), "defaultContent": '0,00' },
                      { mData: 'TOTALESCRITURAPESOS', render: $.fn.dataTable.render.number( '.', ',', 0, '$ ' ), "defaultContent": '0' },
                  ],
                  columns: [
                      { title: "Proyecto" },
                      { title: "Total Promesa UF"},
                      { title: "Total Promesa $"},
                      { title: "Total Escritura UF"},
                      { title: "Total Escritura $"},
                  ],
                  buttons: [
                          {
                              extend: 'excel',
                              exportOptions: {
                                columns: ':visible',
                                format: {
                                    body: function(data, row, column, node) {
                                       return column >= 2 && column <= 4? data.replace( '.', '' ).replace( ',', '.' ).replace( '$ ', '' ) : data.replace('.','').replace(',','.').replace( '$ ', '' );

                                    }
                                }
                              },
                              title: null,
                              text: 'Excel'
                          },
                          {
                              extend: 'pdfHtml5',
                              title: null,
                              text: 'PDF',
                              pageSize: 'LEGAL',
                              exportOptions: {
                                columns: [ 0,1,2,3,4 ]
                              },
                              customize: function (doc) {
                              //Remove the title created by datatTables
                              doc.content.splice(1,0);

                              doc['footer']=(function(page, pages)  {
                                return {
                                  columns: [
                                    {
                                      alignment: 'left',
                                      text: [
                                        { text: page.toString(), italics: true },
                                        ' de ',
                                        { text: pages.toString(), italics: true }
                                      ]
                                    }
                                  ],
                                  margin: [40,10,0,0]
                                }
                              });

                              doc['header']=(function() {
                                return {
                                  columns: [
                                    {
                                      image: logo,
                                      width: 80,
                                      alignment: 'left'
                                    },
                                    {
                                      alignment: 'center',
                                      fontSize: 11,
                                      text: 'Listado de comisiones empresa',
                                      margin: [0,60,0,0],

                                    },
                                    {
                                      text: f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear(),
                                      fontSize: 11,
                                      alignment: 'right',
                                      width: 80
                                    }
                                  ],
                                  margin: [40,40,60,0]
                                }
                              });

                              doc.pageMargins = [10,140,10,60];
                                doc.content[0].table.widths = ['*','*','*','*','*'];
                              doc.defaultStyle.fontSize = 8;
                              doc.styles.tableHeader.fontSize = 8;
                              doc.styles.tableHeader.alignment = 'left';
                            }
                          }
                        ],
                  "deferRender": true,
                  "scrollY": largo,
                  "scrollCollapse": true,
                  "scrollX": true,
                  "paging":         false,
                  "ordering": true,
                  "order": [[ 1, "asc" ]],
                  "info":     true,
                  "dom": 'frBtip',
                  "language": {
                      "zeroRecords": "No hay datos disponibles",
                      "info": "Registro _START_ de _END_ de _TOTAL_",
                      "infoEmpty": "No hay datos disponibles",
                      "paginate": {
                          "previous": "Anterior",
                          "next": "Siguiente"
                      },
                      "search": "Buscar: "
                  },
                  "destroy": true,
                  "footerCallback": function ( row, data, start, end, display ) {
                    var datosPromesaUF = this.api().column(1).data();
                    var totalPromesaUF = 0;
                    for (var i = 0; i < datosPromesaUF.length; i++) {
                      if (datosPromesaUF[i] !== '' && datosPromesaUF[i] !== null && datosPromesaUF[i] !== undefined) {
                        totalPromesaUF += parseFloat(datosPromesaUF[i]);
                      }
                    }
                    totalPromesaUF = roundNumber(totalPromesaUF,2);

                    var datosPromesaPesos = this.api().column(2).data();
                    var totalPromesaPesos = 0;
                    for (var i = 0; i < datosPromesaPesos.length; i++) {
                      if (datosPromesaPesos[i] !== '' && datosPromesaPesos[i] !== null && datosPromesaPesos[i] !== undefined) {
                        totalPromesaPesos += parseInt(datosPromesaPesos[i]);
                      }
                    }

                    var datosEscrituraUF = this.api().column(3).data();
                    var totalEscrituraUF = 0;
                    for (var i = 0; i < datosEscrituraUF.length; i++) {
                      if (datosEscrituraUF[i] !== '' && datosEscrituraUF[i] !== null && datosEscrituraUF[i] !== undefined) {
                        totalEscrituraUF += parseFloat(datosEscrituraUF[i]);
                      }
                    }
                    totalEscrituraUF = roundNumber(totalEscrituraUF,2);

                    var datosEscrituraPesos = this.api().column(4).data();
                    var totalEscrituraPesos = 0;
                    for (var i = 0; i < datosEscrituraPesos.length; i++) {
                      if (datosEscrituraPesos[i] !== '' && datosEscrituraPesos[i] !== null && datosEscrituraPesos[i] !== undefined) {
                        totalEscrituraPesos += parseInt(datosEscrituraPesos[i]);
                      }
                    }

                    $(this.api().column(1).footer()).html(totalPromesaUF.formatMoney(2,'.',',').toString());
                    $(this.api().column(2).footer()).html("$ "+totalPromesaPesos.formatMoney(0,'.',',').toString());
                    $(this.api().column(3).footer()).html(totalEscrituraUF.formatMoney(2,'.',',').toString());
                    $(this.api().column(4).footer()).html("$ "+totalEscrituraPesos.formatMoney(0,'.',',').toString());
                    if (valorUFJqueryHoy == 0) {
                      $("#textoUF").html("&nbsp;&nbsp;(valor UF no publicado para el mes actual, si el proyecto es COR debe ingresar el valor manualmente)");
                    }else{
                      $("#textoUF").html('');
                    }
                  },
                  'initComplete': function(){
                    setTimeout(function() {
                      $('#contenido').fadeIn();
                      $('#header').fadeIn();
                      $('#footer').fadeIn();
                      $('#tablaListadoComisionesEmpresa').DataTable().columns.adjust();
                      setTimeout(function() {
                        $('#modalAlertas').modal('hide');
                      }, 500);
                    }, 1000);
                  },
              });
            }
          });
          },500);
        }else{
          $('#tablaListadoComisionesEmpresa').DataTable( {
                ajax: {
                      url: "controller/datosComisionesProyectosPorEmpresa.php",
                      type: 'POST'
                  },
                  "aoColumns": [
                      { mData: 'CODIGOPROYECTO'},
                      { mData: 'TOTALPROMESA', render: $.fn.dataTable.render.number( '.', ',', 2, '' ), "defaultContent": '0,00' },
                      { mData: 'TOTALPROMESAPESOS', render: $.fn.dataTable.render.number( '.', ',', 0, '$ ' ), "defaultContent": '0' },
                      { mData: 'TOTALESCRITURA', render: $.fn.dataTable.render.number( '.', ',', 2, '' ), "defaultContent": '0,00' },
                      { mData: 'TOTALESCRITURAPESOS', render: $.fn.dataTable.render.number( '.', ',', 0, '$ ' ), "defaultContent": '0' },
                  ],
                  columns: [
                      { title: "Proyecto" },
                      { title: "Total Promesa UF"},
                      { title: "Total Promesa $"},
                      { title: "Total Escritura UF"},
                      { title: "Total Escritura $"},
                  ],
                  buttons: [
                          {
                              extend: 'excel',
                              exportOptions: {
                                columns: ':visible',
                                format: {
                                    body: function(data, row, column, node) {
                                       return column >= 2 && column <= 4? data.replace( '.', '' ).replace( ',', '.' ).replace( '$ ', '' ) : data.replace('.','').replace(',','.').replace( '$ ', '' );

                                    }
                                }
                              },
                              title: null,
                              text: 'Excel'
                          },
                          {
                              extend: 'pdfHtml5',
                              title: null,
                              text: 'PDF',
                              pageSize: 'LEGAL',
                              exportOptions: {
                                columns: [ 0,1,2,3,4 ]
                              },
                              customize: function (doc) {
                              //Remove the title created by datatTables
                              doc.content.splice(1,0);

                              doc['footer']=(function(page, pages)  {
                                return {
                                  columns: [
                                    {
                                      alignment: 'left',
                                      text: [
                                        { text: page.toString(), italics: true },
                                        ' de ',
                                        { text: pages.toString(), italics: true }
                                      ]
                                    }
                                  ],
                                  margin: [40,10,0,0]
                                }
                              });

                              doc['header']=(function() {
                                return {
                                  columns: [
                                    {
                                      image: logo,
                                      width: 80,
                                      alignment: 'left'
                                    },
                                    {
                                      alignment: 'center',
                                      fontSize: 11,
                                      text: 'Listado de comisiones empresa',
                                      margin: [0,60,0,0],

                                    },
                                    {
                                      text: f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear(),
                                      fontSize: 11,
                                      alignment: 'right',
                                      width: 80
                                    }
                                  ],
                                  margin: [40,40,60,0]
                                }
                              });

                              doc.pageMargins = [10,140,10,60];
                                doc.content[0].table.widths = ['*','*','*','*','*'];
                              doc.defaultStyle.fontSize = 8;
                              doc.styles.tableHeader.fontSize = 8;
                              doc.styles.tableHeader.alignment = 'left';
                            }
                          }
                        ],
                  "deferRender": true,
                  "scrollY": largo,
                  "scrollCollapse": true,
                  "scrollX": true,
                  "paging":         false,
                  "ordering": true,
                  "order": [[ 1, "asc" ]],
                  "info":     true,
                  "dom": 'frBtip',
                  "language": {
                      "zeroRecords": "No hay datos disponibles",
                      "info": "Registro _START_ de _END_ de _TOTAL_",
                      "infoEmpty": "No hay datos disponibles",
                      "paginate": {
                          "previous": "Anterior",
                          "next": "Siguiente"
                      },
                      "search": "Buscar: "
                  },
                  "destroy": true,
                  'initComplete': function(){
                    setTimeout(function() {
                      $('#contenido').fadeIn();
                      $('#header').fadeIn();
                      $('#footer').fadeIn();
                      $('#tablaListadoComisionesEmpresa').DataTable().columns.adjust();
                      setTimeout(function() {
                        $('#modalAlertas').modal('hide');
                      }, 500);
                    }, 1000);
                  },
              });
        }
      }
    });
    $.ajax({
      url:   'controller/datosProyectosMiUsuarioListadoCotiza.php',
      type:  'post',
      success:  function (response) {
        var p = response.split(",");
        var cuerpo = "";
        if(response.localeCompare("Sin datos")!= 0 && response != ""){
          if(p[0].localeCompare("Sin datos") != 0 && p[0] != ""){
            for(var i = 0; i < p.length; i=i+4){
              cuerpo += "<option value=" + p[i] + ">" + p[i+1] + "</option>";
            }
          }
          cuerpo += "<option value='todos' selected>Todos</option>";
          $("#mostrarProyectosComisionesEmpresa").html(cuerpo);
        }
      }
    });
  },1500);
});
app.controller("comisionesEmpresaController", function(){
  $.ajax({
    url:   'controller/limpia_session.php',
    type:  'post',
    success:  function (response) {
    }
  });
});

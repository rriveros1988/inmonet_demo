google.charts.load('current', {packages: ['corechart']});
google.charts.setOnLoadCallback(drawChart);
var h = $(window).height() - 270;
var w = $(window).width()/2;

function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['TRAMO', 'Cantidad', { 'role': 'style' }, { 'role': 'annotation' },],
      ['Manana', 0, '#11ca3c', '0'],
      ['Tarde', 0, '#11ca3c', '0'],
      ['Noche', 0, '#11ca3c', '0']     
    ]);

    var options = {
        title: 'CONSULTA GARANTIAS POR TRAMO',
        legend: {position: 'none'},
        isStacked: false,
        titleTextStyle: {
    		color: '#FFF'
		},
        'chartArea': {'left': 35},
        annotations: {
            textStyle: {
                fontSize: 11,
                bold: true,
                color: '#FFF'
            }
        },
        vAxis: {
            textStyle: {
                fontSize: 12,
                bold: true,
                color: '#FFF'
            },
            viewWindow: {
                min: 0
            }
        },
        hAxis: {
            textStyle: {
                fontSize: 12,
                color: '#FFF'
            },
            slantedText: true, 
            slantedTextAngle: 50
        },
        'height':   h,
        'width':   w,
        animation:{
            duration: 1000,
            easing: 'out',
        },
        backgroundColor: { fill:'transparent' }
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('cantidadPorTramo'));
    chart.draw(data, options);

    setTimeout(function(){ 
    	$.ajax({
		    url:   'controller/datosCantidadTramo.php',
		    type:  'post',
		    success:  function (response) {
		        var p = response.split(",");
	            if(p != null){
	            	if(p[0] != "Sin datos" && p[0] != "Error"){
	            		for(var i = 0; i < p.length; i=i+2){
		                    var a = i + 1;
		                    switch (p[i]) {
		                        case 'Manana':
		                        	data.setValue(0,1, p[a]);
	                                data.setValue(0,3, '' + p[a] + '');
		                        break;
		                        case 'Tarde':
		                        	data.setValue(1,1, p[a]);
	                                data.setValue(1,3, '' + p[a] + '');
		                        break;
		                        case 'Noche':
		                        	data.setValue(2,1, p[a]);
	                                data.setValue(2,3, '' + p[a] + '');
		                        break;
		                   	}
		              	}
		              	
		              	//Re-Gráficar
	   		 			chart.draw(data, options);
	            	}
	            }
		  	}
		});
    },2500);                      
}
<script>
	function randomTextoLoadJs(){
	  var str = '';
	  var ref = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRST0123456789';
	  for (var i = 0; i < 8 ; i++)
	  {
	    str += ref.charAt(Math.floor(Math.random()*ref.length));
	  }
	  return str;
	}
  var js = document.createElement('script');
  js.src = 'view/js/funciones.js?idLoad=' + randomTextoLoadJs();
  document.getElementsByTagName('head')[0].appendChild(js);
</script>

<!-- Modal de alertas -->
<div id="modalAlertas" class="modal fade" role="dialog" style="z-index: 1800;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body alerta-modal-body">
        <h4 id="textoModal"></h4>
        <button id="buttonAceptarAlerta" style="margin-top: 10px; display: none;" type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
        <button id="botonAceptarCambioPass" style="margin-top: 10px; display: none;" type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal Cerrar sesion -->
<div id="modalCerrarSesion" class="modal fade" role="dialog" style="z-index: 1800;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body alerta-modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="textoModal">¿Desea cerrar sesión?</h4>
        <button id="botonConfirmaCierreSesion" style="margin-top: 20px; margin-bottom: 10px;" type="button" class="btn btn-primary">Cerrar</button>
        <button style="margin-top: 20px; margin-bottom: 10px; margin-left: 15px;" type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal cambia pass -->
<div id="modalCambiaPass" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cambio de contraseña</h4>
      </div>
      <div class="modal-body">
        <div style="margin-top: 10px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <input id="passNuevo" type="password" class="form-control" title="Contraseña nueva" placeholder="Contraseña nueva">
        </div>
        <div style="margin-top: 10px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <input id="passNuevoConfirmar" type="password" class="form-control" title="Repita su nueva contraseña" placeholder="Repita su nueva contraseña">
        </div>
        <button id="botonCambiarPass" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Cambiar</button>
        <button id="botonCerrarCambioPass" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Cerrar</button>
        <label id="mensajeCambioPass" style="color: red; margin-left: 190px; display: none; font-size: 12px;"></label>
    </div>
  </div>
  </div>
</div>

<!-- Modal agrega unidad -->
<div id="modalAgregaUnidad" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar unidad</h4>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>

<div id="modalSeguimiento" class="modal fade" role="dialog">
  <div class="modal-dialog modal-xs">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><font id="tituloCotizacion">Seguimiento</font></h4>
      </div>
      <div class="modal-body">
        <div id="cuerpoSeguimiento" class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; width: 100%;">
          <label>Sem 1</label>
          <input disabled id="seguimientoSem1" class="form-control" type="number" min="1" max="4">
          <br/>
          <label>Sem 2</label>
          <input disabled id="seguimientoSem2" class="form-control" type="number" min="1" max="4">
          <br/>
          <label>Sem 3</label>
          <input disabled id="seguimientoSem3" class="form-control" type="number" min="1" max="4">
          <br/>
          <label>Sem 4</label>
          <input disabled id="seguimientoSem4" class="form-control" type="number" min="1" max="4">
        </div>
      </div>
      <button disabled id="actualizarSeguimiento" style="margin-left: 15px; margin-top: 20px; margin-bottom: 20px;" type="button" class="btn btn-primary">Actualizar</button>
      <button id="cerrarModalSeguimiento" style="margin-left: 15px; margin-top: 20px; margin-bottom: 20px;" type="button" class="btn btn-primary">Cerrar</button>
      <label id="mensajeSeg" style="margin-left: 100px; color: red;">No esta en semana de seguimiento</label>
    </div>
  </div>
</div>

<!-- Modal informe cotizaciones -->
<div id="modalInformeCotiza" class="modal fade" role="dialog">
  <div class="modal-dialog modal-xs">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><font id="tituloInformeCotiza">Informe de gestión</font></h4>
      </div>
      <div class="modal-body">
        <div id="cuerpoInformeCotiza" class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; width: 100%; padding-left: 0; padding-right: 0;">
          <label>Proyecto:</label>
          <select id="proyectosDisponiblesInf" type="select" class="form-control custom">
          </select>
        </div>
      </div>
      <button disabled id="generarInformeCotiza" style="margin-left: 15px; margin-top: 20px; margin-bottom: 20px;" type="button" class="btn btn-primary">Generar</button>
      <button id="cerrarModalInformeCotiza" style="margin-left: 15px; margin-top: 20px; margin-bottom: 20px;" type="button" class="btn btn-primary">Cerrar</button>
      <label id="mensajeInformeCotiza" style="margin-left: 100px; color: red; display: none;">Informe será enviado a su e-mail</label>
    </div>
  </div>
</div>

<!-- Modal edita valor unidad -->
<div id="modalEditaValorUnidad" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Editar valor de unidades</h4>
      </div>
      <div class="modal-body">
        <div style="margin-top: 10px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <input id="valorNuevo" type="text" class="form-control" title="Valor (UF)" placeholder="Valor (UF)">
        </div>
        <button id="botonEditarValoresUnidad" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Cambiar</button>
        <button id="botonCerrarValoresUnidad" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Cerrar</button>
    </div>
  </div>
  </div>
</div>

<!-- Modal actualizar Cliente 1 Reserva-->
<div id="modalActualizarCliente1Reserva" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Datos primer cliente</h4>
      </div>
      <div class="modal-body">
        <div style="margin-top: 10px;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <label>RUT *</label>
          <input disabled id="cliente1RUT" type="text" class="form-control" title="RUT" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
          <label>Nombre *</label>
          <input id="cliente1Nombre" type="text" class="form-control" title="Nombe" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
          <label>Apellido *</label>
          <input id="cliente1Apellido" type="text" class="form-control" title="Nombe" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Domicilio *</label>
          <input id="cliente1Domicilio" type="text" class="form-control" title="Domicilio" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Número *</label>
          <input id="cliente1Numero" type="text" class="form-control" title="Número" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Nro. Depto/Casa</label>
          <input id="cliente1DeptoCasa" type="text" class="form-control" title="Número" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Comuna *</label>
          <input id="cliente1Comuna" type="text" class="form-control" title="Comuna" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Ciudad *</label>
          <input id="cliente1Ciudad" type="text" class="form-control" title="Ciudad" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Región *</label>
          <input id="cliente1Region" type="text" class="form-control" title="Región" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>País *</label>
          <input id="cliente1Pais" type="text" class="form-control" title="País" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Fono</label>
          <input id="cliente1Fono" type="text" class="form-control" title="Fono" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>E-Mail *</label>
          <input id="cliente1Mail" type="text" class="form-control" title="E-Mail" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Fecha nacimiento *</label>
          <input id="cliente1FNac" type="text" class="form-control" title="Fecha Nacimiento" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Profesión *</label>
          <input id="cliente1Profesion" type="text" class="form-control" title="Profesión" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Institución *</label>
          <input id="cliente1Institucion" type="text" class="form-control" title="Institución" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Actividad *</label>
          <input id="cliente1Actividad" type="text" class="form-control" title="Nacionalidad" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Residencia *</label>
          <input id="cliente1Residencia" type="text" class="form-control" title="Nacionalidad" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          <label>Nacionalidad *</label>
          <input id="cliente1Nacionalidad" type="text" class="form-control" title="Nacionalidad" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <label>Sexo *</label>
          <select class="form-control custom" type="text" id="cliente1Sexo"  style="margin-bottom: 5pt; width: 100%;">
            <option value="M">M</option>
            <option value="F">F</option>
          </select>
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Estado civil *</label>
          <select class="form-control custom" type="text" id="cliente1EstCivil"  style="margin-bottom: 5pt; width: 100%;">

          </select>
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Nivel educacional *</label>
          <select class="form-control custom" type="text" id="cliente1NivelEdu"  style="margin-bottom: 5pt; width: 100%;">

          </select>
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Casa habitaconal *</label>
          <select class="form-control custom" type="text" id="cliente1CasaHabita"  style="margin-bottom: 5pt; width: 100%;">

          </select>
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Motivo compra *</label>
          <select class="form-control custom" type="text" id="cliente1MotivoCompra"  style="margin-bottom: 5pt; width: 100%;">

          </select>
        </div>
        <button id="botonGuardarCliente1" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Guardar</button>
        <button id="botonCerrarCliente1" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Cerrar</button>
    </div>
  </div>
  </div>
</div>

<!-- Modal actualizar Cliente 2 Reserva-->
<div id="modalActualizarCliente2Reserva" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Datos segundo cliente</h4>
      </div>
      <div class="modal-body">
        <div style="margin-top: 10px;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <label>RUT</label>
          <input id="cliente2RUT" type="text" class="form-control" title="RUT" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
          <label>Nombre</label>
          <input id="cliente2Nombre" type="text" class="form-control" title="Nombe" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
          <label>Apellido</label>
          <input id="cliente2Apellido" type="text" class="form-control" title="Nombe" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Domicilio</label>
          <input id="cliente2Domicilio" type="text" class="form-control" title="Domicilio" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Número</label>
          <input id="cliente2Numero" type="text" class="form-control" title="Número" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Comuna</label>
          <input id="cliente2Comuna" type="text" class="form-control" title="Comuna" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Ciudad</label>
          <input id="cliente2Ciudad" type="text" class="form-control" title="Ciudad" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Región</label>
          <input id="cliente2Region" type="text" class="form-control" title="Región" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>País</label>
          <input id="cliente2Pais" type="text" class="form-control" title="País" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Fono</label>
          <input id="cliente2Fono" type="text" class="form-control" title="Fono" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>E-Mail</label>
          <input id="cliente2Mail" type="text" class="form-control" title="E-Mail" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <label>Fecha nacimiento</label>
          <input id="cliente2FNac" type="text" class="form-control" title="Fecha Nacimiento" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Profesión</label>
          <input id="cliente2Profesion" type="text" class="form-control" title="Profesión" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Institución</label>
          <input id="cliente2Institucion" type="text" class="form-control" title="Institución" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <label>Nacionalidad</label>
          <input id="cliente2Nacionalidad" type="text" class="form-control" title="Nacionalidad" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <label>Sexo</label>
          <select class="form-control custom" type="text" id="cliente2Sexo"  style="margin-bottom: 5pt; width: 100%;">
            <option value="M">M</option>
            <option value="F">F</option>
          </select>
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Estado civil</label>
          <input id="cliente2EstCivil" type="text" class="form-control" title="Estado civil" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Nivel educacional</label>
          <input id="cliente2NivelEdu" type="text" class="form-control" title="Nivel educacional" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Casa habitaconal</label>
          <input id="cliente2CasaHabita" type="text" class="form-control" title="Casa habitacional" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Motivo compra</label>
          <input id="cliente2MotivoCompra" type="text" class="form-control" title="Motivo compra" >
        </div>
        <button id="botonGuardarCliente2" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Guardar</button>
        <button id="botonCerrarCliente2" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Cerrar</button>
    </div>
  </div>
  </div>
</div>

<!-- Modal Subir documentos -->
<div id="modalSubirDocumentosReserva" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Subir documentos a Reserva: </h4><font id="numeroOperacionReserva"></font>
      </div>
      <div class="modal-body">
        <div style="margin-top: 10px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <form enctype="multipart/form-data" id="formDatosSubirDocumentosReserva" style="text-align: left;">
            <div id="divFichaReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Ficha</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="fichaReservaDoc" name="fichaReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="infoFichaReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divCotizacionReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Cotización</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="cotizacionDoc" name="cotizacionDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="infoCotizacionDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divRCompraReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Reserva de compra</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="rcompraReservaDoc" name="rcompraReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="infoRCompraReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divCIReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Cédula de identidad</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="ciReservaDoc" name="ciReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="infoCIReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divPbReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Preaprobación bancaria</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="pbReservaDoc" name="pbReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="infoPbReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divCcReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Carpeta cliente</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="ccReservaDoc" name="ccReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="infoCcReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divCrReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Comprobante pago reserva</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="crReservaDoc" name="crReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="infoCrReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
          </form>
        </div>

        <button id="botonGuardarDocumentosReserva" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Guardar</button>
        <button id="botonCerrarDocumentosReserva" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Cerrar</button>
      </div>
  </div>
  </div>
</div>

<!-- Modal autorizar reserva -->
<div id="modalCambiarEstadoReserva" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cambiar estado de reserva: </h4><font id="ProyectoReservaCambioEstado"></font>
      </div>
      <div class="modal-body" style="text-align: left;">
        <div class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; width: 100%; padding-left: 0; padding-right: 0;">
          <label>Estado:</label>
          <select id="estadoReservas" type="select" class="form-control custom">
          </select>
        </div>
        <button id="botonCambiarEstadoReserva" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Guardar</button>
        <button id="botonCerrarCambiarEstadoReserva" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
    </div>
  </div>
  </div>
</div>

<!-- Modal autorizar reserva -->
<div id="modalCambiarEstadoPromesa" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cambiar estado de promesa: </h4><font id="ProyectoPromesaCambioEstado"></font>
      </div>
      <div class="modal-body" style="text-align: left;">
        <div class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; width: 100%; padding-left: 0; padding-right: 0;">
          <label>Estado:</label>
          <select id="estadoPromesas" type="select" class="form-control custom">
          </select>
        </div>
        <button id="botonCambiarEstadoPromesa" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Guardar</button>
        <button id="botonCerrarCambiarEstadoPromesa" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
    </div>
  </div>
  </div>
</div>

<!-- Modal PDF -->
<div id="modalPDFAyuda" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 90%;">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span class="fa fa-file-pdf-o"></span>&nbsp;&nbsp;Ayuda del sitio </h4><font id="ProyectoPromesaCambioEstado"></font>
      </div>
      <div class="modal-body" style="text-align: left;">
        <div>
          <object id="pdfDeAyuda" width="100%">
            Error: Embedded data could not be displayed.
          </object>
        </div>
      </div>
  </div>
  </div>
</div>

<!-- Modal informe comisiones -->
<div id="modalInformeComisiones" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      	<h4 class="modal-title"><span class="fa fa-file-excel-o"></span>&nbsp;&nbsp;Informe de comisiones </h4>
      </div>
      <div class="modal-body">
				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: left; margin-top: 5pt;">
					<label>Rango:</label>
				</div>
				<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
					<input id="rangoInformes" type="text" class="form-control">
				</div>
				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: left; margin-top: 15px;">
					<label>Proyecto:</label>
				</div>
				<div style="margin-top: 10px;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<select id="proyectoInformes" type="select" class="form-control custom">
					</select>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: left; margin-top: 17px;">
					<label>Tipo:</label>
				</div>
				<div style="margin-top: 10px;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<select id="tipoInformes" type="select" class="form-control custom">
						<option value="todos">Todos</option>
						<option value="vendedor">Vendedor</option>
						<option value="empresa">Empresa</option>
					</select>
				</div>
        <button id="botonGenerarInformeComisiones" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Generar</button>
        <button id="botonCerrarInformeComisiones" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Cerrar</button>
    </div>
  </div>
  </div>
</div>

<!-- Modal informe gestion -->
<div id="modalInformeGestion" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      	<h4 class="modal-title"><span class="fa fa-file-pdf-o"></span>&nbsp;&nbsp;Informe gestión </h4>
      </div>
      <div class="modal-body">
				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: left; margin-top: 10pt;">
					<label>Mes-Año:</label>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="margin-top: 5pt;">
					<input id="mesAnoGestion" type="text" class="form-control">
				</div>
				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: left; margin-top: 10pt;">
					<label>Tipo:</label>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="margin-top: 5pt;">
					<select id="tipoUnidadGestión" type="select" class="form-control custom">
					</select>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: left; margin-top: 10pt;">
					<label>Proyecto:</label>
				</div>
				<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" style="margin-top: 5pt;">
					<select id="proyectoGestión" type="select" class="form-control custom">
					</select>
				</div>
        <button id="botonGenerarInformeGestion" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Generar</button>
        <button id="botonCerrarInformeGestion" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Cerrar</button>
    </div>
  </div>
  </div>
</div>

<!-- NICO -->
<!-- Modal Subir documentos a Promesa-->
<!-- Modal Subir documentos a Promesa-->
<div id="modalSubirDocumentosPromesa" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Subir documentos a Promesa: </h4><font id="numeroOperacionPromesa"></font>
      </div>
      <div class="modal-body">
        <div style="margin-top: 10px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="formDatosSubirDocumentosPromesa" style="text-align: left;">
            <div id="divFichaReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Ficha</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="promesa_fichaReservaDoc" name="fichaReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="promesa_infoFichaReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divCotizacionReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Cotización</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="promesa_cotizacionDoc" name="cotizacionDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="promesa_infoCotizacionDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divRCompraReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Reserva de compra</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="promesa_rcompraReservaDoc" name="rcompraReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="promesa_infoRCompraReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divCIReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Cédula de identidad</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="promesa_ciReservaDoc" name="ciReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="promesa_infoCIReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divPbReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Preaprobación bancaria</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="promesa_pbReservaDoc" name="pbReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="promesa_infoPbReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divCcReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Carpeta cliente</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="promesa_ccReservaDoc" name="ccReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="promesa_infoCcReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divCrReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Comprobante pago reserva</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="promesa_crReservaDoc" name="crReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="promesa_infoCrReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divPrPromesa" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Ficha promesa</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="prPromesaDoc" name="prPromesaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="infoPRPromesaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divCpPromesa" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Comprobantes pago promesa</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="cpPromesaDoc" name="cpPromesaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="infoCPPromesaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divPfPromesa" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Promesa firmada</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="pfPromesaDoc" name="pfPromesaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="infoPFPromesaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divPsPromesa" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Poliza seguros</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="psPromesaDoc" name="psPromesaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="infoPSPromesaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
          </form>
        </div>

        <button id="botonGuardarDocumentosPromesa" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Guardar</button>
        <button id="botonCerrarDocumentosPromesa" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Cerrar</button>
      </div>
  </div>
  </div>
</div>
<!-- Modal Subir documentos a Escritura-->
<div id="modalSubirDocumentosEscritura" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Subir documentos a Escritura: </h4><font id="numeroOperacionEscritura"></font>
      </div>
      <div class="modal-body">
        <div style="margin-top: 10px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form enctype="multipart/form-data" id="formDatosSubirDocumentosEscritura" style="text-align: left;">
            <div id="divFichaReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Ficha</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="escritura_fichaReservaDoc" name="fichaReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="escritura_infoFichaReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divCotizacionReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Cotización</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="escritura_cotizacionDoc" name="cotizacionDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="escritura_infoCotizacionDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divRCompraReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Reserva de compra</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="escritura_rcompraReservaDoc" name="rcompraReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="escritura_infoRCompraReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divCIReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Cédula de identidad</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="escritura_ciReservaDoc" name="ciReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="escritura_infoCIReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divPbReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Preaprobación bancaria</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="escritura_pbReservaDoc" name="pbReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="escritura_infoPbReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divCcReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Carpeta cliente</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="escritura_ccReservaDoc" name="ccReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="escritura_infoCcReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divCrReserva" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Comprobante pago reserva</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="escritura_crReservaDoc" name="crReservaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="escritura_infoCrReservaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divPrPromesa" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Ficha promesa</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="escritura_prPromesaDoc" name="prPromesaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="escritura_infoPRPromesaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divCpPromesa" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Comprobantes pago promesa</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="escritura_cpPromesaDoc" name="cpPromesaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="escritura_infoCPPromesaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divPfPromesa" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Promesa firmada</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="escritura_pfPromesaDoc" name="pfPromesaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="escritura_infoPFPromesaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divPsPromesa" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Poliza seguros</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="escritura_psPromesaDoc" name="psPromesaDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="escritura_infoPSPromesaDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divEsEscritura" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Ficha escritura</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="esEscrituraDoc" name="esEscrituraDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="infoESEscrituraDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divEnEscritura" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Escritura notarial</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="enEscrituraDoc" name="enEscrituraDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="infoENEscrituraDoc" type="text" class="form-control" readonly>
              </div>
            </div>
            <div id="divDeEscritura" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <label>Documento de entrega unidad</label>
              <div class="input-group">
                  <label class="input-group-btn">
                      <span class="btn btn-default btn-file" style="margin-bottom: 5pt; text-align: left; color: grey;">
                          Examinar... <input type="file" id="deEscrituraDoc" name="deEscrituraDoc" accept="application/pdf" style="display: none;" multiple>
                      </span>
                  </label>
                  <input disabled id="infoDEEscrituraDoc" type="text" class="form-control" readonly>
              </div>
            </div>
          </form>
        </div>

        <button id="botonGuardarDocumentosEscritura" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Guardar</button>
        <button id="botonCerrarDocumentosEscritura" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Cerrar</button>
      </div>
  </div>
  </div>
</div>

<!-- Modal autorizar escritura -->
<div id="modalCambiarEstadoEscritura" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cambiar estado de escritura: </h4><font id="ProyectoEscrituraCambioEstado"></font>
      </div>
      <div class="modal-body" style="text-align: left;">
        <div class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; width: 100%; padding-left: 0; padding-right: 0;">
          <label>Estado:</label>
          <select id="estadoEscrituras" type="select" class="form-control custom">
          </select>
        </div>
        <button id="botonCambiarEstadoEscritura" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Guardar</button>
        <button id="botonCerrarCambiarEstadoEscritura" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
    </div>
  </div>
  </div>
</div>

<!-- Modal comentarios contable -->
<div id="modalComentariosContable" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Comentarios</h4>
        <h5 class="modal-title">Proyecto: <font id="nombreProyectoComentario"></font> - Nro. Ope: <font id="numeroOperacionComentario"></font> - Unidad: <font id="unidadComentario"></font></h5>
      </div>
      <div class="modal-body" style="text-align: left;">
        <div id="comentariosIzquierda" class="col-lg-6 col-md-6 col-sm-12">
          <div id="divComentarioReservaContable" class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; width: 100%; padding-left: 0; padding-right: 0;">
            <label>Comentario Reserva:</label>
            <textarea rows="3" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 5pt 5pt 5pt 5pt; resize: none; margin-bottom: 5pt; max-height: 80px; height: 80px;" id="comentarioReservaContable" maxlength="2000"></textarea>
          </div>
          <div id="divComentarioPromesaContable" class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; width: 100%; padding-left: 0; padding-right: 0;">
            <label>Comentario Promesa:</label>
            <textarea rows="3" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 5pt 5pt 5pt 5pt; resize: none; margin-bottom: 5pt; max-height: 80px; height: 80px;" id="comentarioPromesaContable" maxlength="2000"></textarea>
          </div>
          <div id="divBancoContable" class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; width: 100%; padding-left: 0; padding-right: 0;">
            <label>Banco Operación:</label>
            <select class="form-control custom" style="padding: 5pt 5pt 5pt 5pt; margin-bottom: 5pt;" id="bancoContable">
            </select>
          </div>
        </div>
        <div id="comentariosDerecha" class="col-lg-6 col-md-6 col-sm-12">
          <div id="divComentarioEscrituraContable" class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; width: 100%; padding-left: 0; padding-right: 0;">
            <label>Comentario Escritura:</label>
            <textarea rows="3" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 5pt 5pt 5pt 5pt; resize: none; margin-bottom: 5pt; max-height: 80px; height: 80px;" id="comentarioEscrituraContable" maxlength="2000"></textarea>
          </div>
          <div id="divComentarioGeneralContable" class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; width: 100%; padding-left: 0; padding-right: 0;">
            <label>Comentario General:</label>
            <textarea rows="3" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 5pt 5pt 5pt 5pt; resize: none; margin-bottom: 5pt; max-height: 80px; height: 80px;" id="comentarioGeneralContable" maxlength="2000"></textarea>
          </div>
          <div id="divUsoContable" class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; width: 100%; padding-left: 0; padding-right: 0;">
            <label>Tipo Inversión:</label>
            <select class="form-control custom" style="padding: 5pt 5pt 5pt 5pt; margin-bottom: 5pt;" id="usoContable">
            </select>
          </div>
        </div>
        <button id="botonGuardarComentariosContable" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Guardar</button>
        <button id="botonCerrarComentariosContable" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal cambio de estado Comision -->
<div id="modalCambiarEstadoComision" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cambiar estado de Comision: </h4>
        Proyecto: <font id="proyectoComisionCambioEstado"></font><br>
        Vendedor: <font id="vendedorComisionCambioEstado"></font><br>
        N° operación: <font id="nroOperacionComisionCambioEstado"></font>
        <input type="text" id="rutComisionCambioEstado" disabled hidden>
      </div>
      <div class="modal-body" style="text-align: left;">
        <div id="divCambioEstadoComisionPromesa" class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; width: 100%; padding-left: 0; padding-right: 0;">
          <div class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; padding-left: 0;">
            <label>Estado pago promesa:</label>
            <select id="estadoPagoPromesa" type="select" class="form-control custom">
              <option value="NO PAGADO">No pagado</option>
              <option value="PAGADO">Pagado</option>
            </select>
          </div>
        </div>
        <div id="divCambioEstadoComisionEscritura" class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; width: 100%; padding-left: 0; padding-right: 0;">
          <div class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; padding-left: 0;">
            <label>Estado pago escritura:</label>
            <select id="estadoPagoEscritura" type="select" class="form-control custom">
              <option value="NO PAGADO">No pagado</option>
              <option value="PAGADO">Pagado</option>
            </select>
          </div>
        </div>
        <button id="botonCambiarEstadoComision" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Guardar</button>
        <button id="botonCerrarCambiarEstadoComision" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
    </div>
  </div>
  </div>
</div>
<!-- Modal cambio de estado Comision -->
<div id="modalPagoComisionPromesa" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cambiar estado de Comision: </h4>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-left: 0;">
	        Proyecto: <font id="proyectoPagoComisionPromesa"></font><br>
	        N° operación: <font id="nroOperacionPagoComisionPromesa"></font>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-left: 0;">
					Valor UF: <font id="ufDiaComisionPromesa"></font>
				</div>
        <input type="hidden" id="comision_codigoProyecto"/>
        <input type="hidden" id="comision_numeroOperacion"/>
        <input type="hidden" id="comision_estadoPromesa"/>
        <input type="hidden" id="comision_unidadRevisar"/>
        <input type="hidden" id="textoEstadoPromesa"/>
      </div>
      <div class="modal-body" style="text-align: left;">
        <div id="divComisionDueno" class="row" style="margin-top: -14pt;">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h4>Comisión Dueño (<b>UF <font id="comisionDuenoModal"></font></b>):</h4>
              </div>
            </div>
						<div class="row">
						  <div class="col-lg-6 col-md-6 col-sm-12">
						    <label>Valor UF:</label>
						    <input id="valorUFcomisionDuenoEmpresa2" type="text" class="form-control">
						  </div>
						  <div class="col-lg-6 col-md-6 col-sm-12">
						    <label>Monto ($):</label>
						    <input id="montoComisionDuenoEmpresa2" type="text" class="form-control">
						  </div>
						</div>
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-12">
                <label>Forma pago comisión:</label>
                <select class="form-control custom" type="text" id="comisionDuenoFormaPago">

                </select>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12">
                <label>Banco:</label>
                <input id="comisionDuenoBanco" type="text" class="form-control">
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12">
                <label>Serie/Nro:</label>
                <input id="comisionDuenoSerie" type="text" class="form-control">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <label>Nro Trans/Cheque:</label>
                <input id="comisionDuenoNro" type="text" class="form-control">
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <label>Fecha cheque:</label>
                <input id="comisionDuenoFechaCheque" type="text" class="form-control">
              </div>
            </div>
          </div>
        </div>
        <div id="divComisionCliente" class="row" style="margin-top: 4px;">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h4>Comisión Cliente (<b>UF <font id="comisionClienteModal"></font></b>):</h4>
              </div>
            </div>
						<div class="row">
						  <div class="col-lg-6 col-md-6 col-sm-12">
						    <label>Valor UF:</label>
						    <input id="valorUFcomisionClienteEmpresa2" type="text" class="form-control">
						  </div>
						  <div class="col-lg-6 col-md-6 col-sm-12">
						    <label>Monto ($):</label>
						    <input id="montoComisionClienteEmpresa2" type="text" class="form-control">
						  </div>
						</div>
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-12">
                <label>Forma pago comisión:</label>
                <select class="form-control custom" type="text" id="comisionClienteFormaPago">

                </select>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12">
                <label>Banco:</label>
                <input id="comisionClienteBanco" type="text" class="form-control">
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12">
                <label>Serie/Nro:</label>
                <input id="comisionClienteSerie" type="text" class="form-control">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <label>Nro Trans/Cheque:</label>
                <input id="comisionClienteNro" type="text" class="form-control">
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <label>Fecha cheque:</label>
                <input id="comisionClienteFechaCheque" type="text" class="form-control">
              </div>
            </div>
          </div>
        </div>
        <button id="guardarPagoComision" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Guardar</button>
        <button id="omitirPagoComision" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary" >Omitir</button>
    </div>
  </div>
  </div>
</div>
<!-- Modal cambio de estado Comision -->
<div id="modalCambiarEstadoComisionEmpresa" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cambiar estado de Comision: </h4>
        Proyecto: <font id="proyectoComisionCambioEstadoEmpresa"></font><br>
        Actor: <font id="actorComisionCambioEstadoEmpresa"></font><br>
        N° operación: <font id="nroOperacionComisionCambioEstadoEmpresa"></font>
      </div>
      <div class="modal-body" style="text-align: left;">
        <div id="divCambioEstadoComisionPromesaEmpresa" class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; width: 100%; padding-left: 0; padding-right: 0;">
          <div class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; padding-left: 0;">
            <label>Estado pago promesa:</label>
            <select id="estadoPagoPromesaEmpresa" type="select" class="form-control custom">
              <option value="NO PAGADO">No pagado</option>
              <option value="PAGADO">Pagado</option>
            </select>
          </div>
        </div>
        <!-- <div id="divCambioEstadoComisionEscritura" class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; width: 100%; padding-left: 0; padding-right: 0;">
          <div class="col-lg-12 col-md-12 col-sm-12" style="margin: 0 auto; padding-left: 0;">
            <label>Estado pago escritura:</label>
            <select id="estadoPagoEscritura" type="select" class="form-control custom">
              <option value="NO PAGADO">No pagado</option>
              <option value="PAGADO">Pagado</option>
            </select>
          </div>
        </div> -->
        <button id="botonCambiarEstadoComisionEmpresa" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Guardar</button>
        <button id="botonCerrarCambiarEstadoComisionEmpresa" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
    </div>
  </div>
  </div>
</div>
<div id="modalPagoComisionPromesaEmpresa" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cambiar estado de Comision: </h4>
        Proyecto: <font id="proyectoPagoComisionPromesaEmpresa"></font><br>
        N° operación: <font id="nroOperacionPagoComisionPromesaEmpresa"></font><br>
        Actor: <font id="actorPagoComisionPromesaEmpresa"></font>
        <input type="hidden" id="comision_codigoProyectoEmpresa"/>
        <input type="hidden" id="comision_numeroOperacionEmpresa"/>
        <input type="hidden" id="comision_estadoPromesaEmpresa"/>
        <input type="hidden" id="comision_unidadRevisarEmpresa"/>
      </div>
      <div class="modal-body" style="text-align: left;">
        <div id="divComisionDuenoEmpresa" class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h4>Comisión Dueño:</h4>
              </div>
            </div>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12">
                <label>Valor UF:</label>
                <input id="valorUFcomisionDuenoEmpresa" type="text" class="form-control">
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <label>Monto ($):</label>
                <input id="montoComisionDuenoEmpresa" type="text" class="form-control">
              </div>
						</div>
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-12">
                <label>Forma pago comisión:</label>
                <select class="form-control custom" type="text" id="comisionDuenoFormaPagoEmpresa">

                </select>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12">
                <label>Banco:</label>
                <input id="comisionDuenoBancoEmpresa" type="text" class="form-control">
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12">
                <label>Serie/Nro:</label>
                <input id="comisionDuenoSerieEmpresa" type="text" class="form-control">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <label>Nro Trans/Cheque:</label>
                <input id="comisionDuenoNroEmpresa" type="text" class="form-control">
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <label>Fecha cheque:</label>
                <input id="comisionDuenoFechaChequeEmpresa" type="text" class="form-control">
              </div>
            </div>
          </div>
        </div>
        <div id="divComisionClienteEmpresa" class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <h4>Comisión Cliente:</h4>
              </div>
            </div>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12">
                <label>Valor UF:</label>
                <input id="valorUFcomisionClienteEmpresa" type="text" class="form-control">
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <label>Monto ($):</label>
                <input id="montoComisionClienteEmpresa" type="text" class="form-control">
              </div>
						</div>
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-12">
                <label>Forma pago comisión:</label>
                <select class="form-control custom" type="text" id="comisionClienteFormaPagoEmpresa">

                </select>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12">
                <label>Banco:</label>
                <input id="comisionClienteBancoEmpresa" type="text" class="form-control">
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12">
                <label>Serie/Nro:</label>
                <input id="comisionClienteSerieEmpresa" type="text" class="form-control">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12">
                <label>Nro Trans/Cheque:</label>
                <input id="comisionClienteNroEmpresa" type="text" class="form-control">
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12">
                <label>Fecha cheque:</label>
                <input id="comisionClienteFechaChequeEmpresa" type="text" class="form-control">
              </div>
            </div>
          </div>
        </div>
        <button id="guardarPagoComisionEmpresa" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Guardar</button>
        <button id="cerrarPagoComisionEmpresa" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
    </div>
  </div>
  </div>
</div>

<!-- Modal actualizar Cliente Nuevo Reserva-->
<div id="modalActualizarClienteNuevoReserva" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Datos cliente a quien cede</h4>
      </div>
      <div id="cuerpoModalCLienteNuevo" class="modal-body">
        <div style="margin-top: 10px;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <label>RUT *</label>
          <input id="ClienteNuevoRUT" type="text" class="form-control" title="RUT" onblur="rellenaDatosClienteNuevo();">
        </div>
        <div style="margin-top: 10px;" class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
          <label>Nombre *</label>
          <input id="ClienteNuevoNombre" type="text" class="form-control" title="Nombe" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
          <label>Apellido *</label>
          <input id="ClienteNuevoApellido" type="text" class="form-control" title="Nombe" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Domicilio *</label>
          <input id="ClienteNuevoDomicilio" type="text" class="form-control" title="Domicilio" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Número *</label>
          <input id="ClienteNuevoNumero" type="text" class="form-control" title="Número" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Nro. Depto/Casa</label>
          <input id="ClienteNuevoDeptoCasa" type="text" class="form-control" title="Número" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Comuna *</label>
          <input id="ClienteNuevoComuna" type="text" class="form-control" title="Comuna" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Ciudad *</label>
          <input id="ClienteNuevoCiudad" type="text" class="form-control" title="Ciudad" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Región *</label>
          <input id="ClienteNuevoRegion" type="text" class="form-control" title="Región" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>País *</label>
          <input id="ClienteNuevoPais" type="text" class="form-control" title="País" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Fono</label>
          <input id="ClienteNuevoFono" type="text" class="form-control" title="Fono" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>E-Mail *</label>
          <input id="ClienteNuevoMail" type="text" class="form-control" title="E-Mail" onblur="validacionEmail45();">
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Fecha nacimiento *</label>
          <input id="ClienteNuevoFNac" type="text" class="form-control" title="Fecha Nacimiento" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Profesión *</label>
          <input id="ClienteNuevoProfesion" type="text" class="form-control" title="Profesión" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Institución *</label>
          <input id="ClienteNuevoInstitucion" type="text" class="form-control" title="Institución" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Actividad *</label>
          <input id="ClienteNuevoActividad" type="text" class="form-control" title="Nacionalidad" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Residencia *</label>
          <input id="ClienteNuevoResidencia" type="text" class="form-control" title="Nacionalidad" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          <label>Nacionalidad *</label>
          <input id="ClienteNuevoNacionalidad" type="text" class="form-control" title="Nacionalidad" >
        </div>
        <div style="margin-top: 10px;" class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <label>Sexo *</label>
          <select class="form-control custom" type="text" id="ClienteNuevoSexo"  style="margin-bottom: 5pt; width: 100%;">
            <option value="M">M</option>
            <option value="F">F</option>
          </select>
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Estado civil *</label>
          <select class="form-control custom" type="text" id="ClienteNuevoEstCivil"  style="margin-bottom: 5pt; width: 100%;">

          </select>
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Nivel educacional *</label>
          <select class="form-control custom" type="text" id="ClienteNuevoNivelEdu"  style="margin-bottom: 5pt; width: 100%;">

          </select>
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Casa habitaconal *</label>
          <select class="form-control custom" type="text" id="ClienteNuevoCasaHabita"  style="margin-bottom: 5pt; width: 100%;">

          </select>
        </div>
        <div style="margin-top: 10px;" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <label>Motivo compra *</label>
          <select class="form-control custom" type="text" id="ClienteNuevoMotivoCompra"  style="margin-bottom: 5pt; width: 100%;">

          </select>
        </div>
        <button id="botonGuardarClienteNuevo" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Guardar</button>
        <button id="botonCerrarClienteNuevo" style="margin-top: 20px; margin-left: 15px; margin-bottom: 10px;" type="button" class="btn btn-primary">Cerrar</button>
				<input id="ClienteNuevoConfirmar" style="display: none;">
    </div>
  </div>
  </div>
</div>
